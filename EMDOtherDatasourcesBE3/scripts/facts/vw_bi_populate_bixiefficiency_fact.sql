/******************************************************************************************************************/
/*   Script         : bi_populate_bixiefficiency_fact	                                                          */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 25 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_bixiefficiency                                                    */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   25 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   30 Mar 2017         CristianT       1.1               Adding logic for Count FTE measure                     */
/*   31 Mar 2017         CristianT       1.2               Changed the logic for Total Hours not to be divided by the number of rows
                                                           Added delete/insert statements for emd586 inside the script to prevent delays caused by the harmonization */
/******************************************************************************************************************/

DROP TABLE IF EXISTS bixiefficiencyhistory_delete;
CREATE TABLE bixiefficiencyhistory_delete
AS
SELECT fact_bixiefficiencyid
FROM fact_bixiefficiencyhistory
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_bixiefficiencyhistory
WHERE fact_bixiefficiencyid IN (SELECT fact_bixiefficiencyid FROM bixiefficiencyhistory_delete);

DROP TABLE IF EXISTS bixiefficiencyhistory_delete;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_bixiefficiencyhistory';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_bixiefficiencyhistory', ifnull(max(fact_bixiefficiencyid), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_bixiefficiencyhistory;


DROP TABLE IF EXISTS tmp_fact_bixiefficiency;
CREATE TABLE tmp_fact_bixiefficiency
AS
SELECT *
FROM fact_bixiefficiencyhistory
WHERE 1 = 0;

INSERT INTO tmp_fact_bixiefficiency(
fact_bixiefficiencyid,
snapshotdate,
dim_dateidsnapshot,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_run,
dd_mo,
dd_mostatus,
dd_reviewstatus,
dd_batch,
dd_materialid,
dd_materialdesc,
dd_mbr_var,
dd_mbr_ver,
dd_startdate,
dim_dateidstart,
dd_enddate,
dim_dateidend,
dd_sfo,
dd_sfostatus,
dd_boid,
dd_bodesc,
dd_pu,
dd_pu2,
dd_pu3,
dd_sfoproductcode,
dd_service,
ct_total_hr,
ct_avg_people,
ct_fte
)
SELECT (SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_bixiefficiencyhistory') + ROW_NUMBER() over(order by '') AS fact_bixiefficiencyid,
       t.*
FROM (
SELECT CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END as snapshotdate,
       1 as dim_dateidsnapshot,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(ekemo.RUN, 'Not Set') as dd_run,
       ifnull(ekemo.MO, 'Not Set') as dd_mo,
       ifnull(ekemo.MO_STATUS, 0) as dd_mostatus,
       ifnull(ekemo.REVIEW_STATUS, 0) as dd_reviewstatus,
       ifnull(ekemo.BATCH, 'Not Set') as dd_batch,
       ifnull(ekemo.MATERIAL_ID, 'Not Set') as dd_materialid,
       ifnull(ekemo.MATERIAL_DESC, 'Not Set') as dd_materialdesc,
       ifnull(ekemo.MBR_VAR, 'Not Set') as dd_mbr_var,
       ifnull(ekemo.MBR_VER, 0) as dd_mbr_ver,
       ifnull(ekemo.START_DATE, '0001-01-01') as dd_startdate,
       1 as dim_dateidstart,
       ifnull(ekemo.END_DATE, '0001-01-01') as dd_enddate,
       1 as dim_dateidend,
       ifnull(ekesfo.sfo, 0) as dd_sfo,
       ifnull(ekesfo.SFO_STATUS, 0) as dd_sfostatus,
       ifnull(ekesfo.BO_ID, 'Not Set') as dd_boid,
       ifnull(ekesfo.BO_DESC, 'Not Set') as dd_bodesc,
       ifnull(ekesfo.pu, 'Not Set') as dd_pu,
       ifnull(ekesfo.PU2, 'Not Set') as dd_pu2,
       ifnull(ekesfo.pu3, 'Not Set') as dd_pu3,
       ifnull(ekesfo.product_code, 'Not Set') as dd_sfoproductcode,
       'Not Set' as dd_service,
       0 as ct_total_hr,
       0 as ct_avg_people,
       0 as ct_fte
FROM EKE4_SFO_LIST ekesfo,
     EKE_MO_LIST ekemo
WHERE 1 = 1
      AND ekemo.mo = ekesfo.mo
      ) t;

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.dim_dateidsnapshot = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_bixiefficiency tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.snapshotdate = dt.datevalue
      AND tmp.dim_dateidsnapshot <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.dim_dateidstart = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_bixiefficiency tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.dd_startdate = dt.datevalue
      AND tmp.dim_dateidstart <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.dim_dateidend = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_bixiefficiency tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.dd_enddate = dt.datevalue
      AND tmp.dim_dateidend <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.dd_service = CASE
                       WHEN tmp.dd_pu3 LIKE '%USP%' THEN 'USP'
                       WHEN tmp.dd_pu3 LIKE '%DSP%' THEN 'DSP'
                       ELSE 'Not Set'
                     END
WHERE tmp.dd_service <> CASE
                          WHEN tmp.dd_pu3 LIKE '%USP%' THEN 'USP'
                          WHEN tmp.dd_pu3 LIKE '%DSP%' THEN 'DSP'
                          ELSE 'Not Set'
                        END;

DROP TABLE IF EXISTS tmp_calculatehours;
CREATE TABLE tmp_calculatehours
AS
SELECT dt.calendarweekyr,
       usr.dd_service,
       CASE usr.dd_subservice
         WHEN 'USP 1' THEN 'USP1'
         WHEN 'USP 2' THEN 'USP2'
         WHEN 'USP Suite 2' THEN 'USP2'
         WHEN 'USP Suite 3' THEN 'USP3'
         WHEN 'USP Suite 4' THEN 'USP4'
         WHEN 'Suite 1 DSP' THEN 'DSP1'
         WHEN 'Suite 2 DSP' THEN 'DSP2'
         WHEN 'Suite 3 DSP' THEN 'DSP3'
         WHEN 'Suite 4 DSP' THEN 'DSP4'
         WHEN 'DSP 1' THEN 'DSP1'
         WHEN 'DSP 2' THEN 'DSP2'
         ELSE usr.dd_subservice
       END as dd_subservice,
       SUM(CASE
             WHEN length(BIXI_hours) = 4 THEN substr(BIXI_hours, 1, 1)
             WHEN length(BIXI_hours) = 5 THEN substr(BIXI_hours, 1, 2)
             WHEN length(BIXI_hours) = 6 THEN substr(BIXI_hours, 1, 3)
             WHEN length(BIXI_hours) = 7 THEN substr(BIXI_hours, 1, 4)
             ELSE 0
       END) + (SUM(CASE
                            WHEN length(BIXI_hours) = 4 THEN substr(BIXI_hours, 3, 2)
                            WHEN length(BIXI_hours) = 5 THEN substr(BIXI_hours, 4, 2)
                            WHEN length(BIXI_hours) = 6 THEN substr(BIXI_hours, 5, 2)
                            WHEN length(BIXI_hours) = 7 THEN substr(BIXI_hours, 6, 2)
                            ELSE 0
                          END)/60) as total_hr
FROM MT_BIXI_EMPLOYEE_HOURS bix,
     dim_date dt,
     dim_qualityusers usr
WHERE 1 = 1
      AND bix.BIXI_hours NOT LIKE '%-%'
      AND bix.BIXI_date = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND bix.BIXI_muid = usr.merck_uid
GROUP BY dt.calendarweekyr,
         usr.dd_service,
         CASE usr.dd_subservice
           WHEN 'USP 1' THEN 'USP1'
           WHEN 'USP 2' THEN 'USP2'
           WHEN 'USP Suite 2' THEN 'USP2'
           WHEN 'USP Suite 3' THEN 'USP3'
           WHEN 'USP Suite 4' THEN 'USP4'
           WHEN 'Suite 1 DSP' THEN 'DSP1'
           WHEN 'Suite 2 DSP' THEN 'DSP2'
           WHEN 'Suite 3 DSP' THEN 'DSP3'
           WHEN 'Suite 4 DSP' THEN 'DSP4'
           WHEN 'DSP 1' THEN 'DSP1'
           WHEN 'DSP 2' THEN 'DSP2'
           ELSE usr.dd_subservice
         END;

/* 31 Mar 2017 CristianT Start: Changed the logic for Total Hours not to be divided by the number of rows */
/* Old logic is commented here
DROP TABLE IF EXISTS tmp_countrecords
CREATE TABLE tmp_countrecords
AS
SELECT tmp.dd_pu3 as subservice,
       count(*) as cnt_record
FROM tmp_fact_bixiefficiency tmp,
     dim_date snps,
     dim_date strt,
     dim_date endd
WHERE 1 = 1
      AND tmp.dim_dateidsnapshot = snps.dim_dateid
      AND tmp.dim_dateidstart = strt.dim_dateid
      AND tmp.dim_dateidend = endd.dim_dateid
      AND UPPER(tmp.dd_run) NOT LIKE '%TEST%'
	  AND tmp.dd_run <> 'Not Set'
	  AND tmp.dim_dateidstart <> 1
      AND (strt.calendarweekyr = snps.calendarweekyr OR endd.calendarweekyr >= snps.calendarweekyr)
GROUP BY tmp.dd_pu3

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.ct_total_hr = calc.total_hr / cntrec.cnt_record
FROM tmp_calculatehours calc,
     dim_date snps,
     dim_date strt,
     dim_date endd,
     tmp_countrecords cntrec,
     tmp_fact_bixiefficiency tmp
WHERE tmp.dd_pu3 = calc.dd_subservice
      AND tmp.dim_dateidsnapshot = snps.dim_dateid
      AND tmp.dim_dateidstart = strt.dim_dateid
      AND tmp.dim_dateidend = endd.dim_dateid
      AND snps.calendarweekyr = calc.calendarweekyr
      AND UPPER(tmp.dd_run) NOT LIKE '%TEST%'
	  AND tmp.dd_run <> 'Not Set'
	  AND tmp.dim_dateidstart <> 1
      AND calc.total_hr <> 0
      AND calc.dd_subservice = cntrec.subservice
      AND calc.dd_service = tmp.dd_service
      AND strt.calendarweekyr = snps.calendarweekyr

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.ct_total_hr = calc.total_hr / cntrec.cnt_record
FROM tmp_calculatehours calc,
     dim_date snps,
     dim_date strt,
     dim_date endd,
     tmp_countrecords cntrec,
     tmp_fact_bixiefficiency tmp
WHERE tmp.dd_pu3 = calc.dd_subservice
      AND tmp.dim_dateidsnapshot = snps.dim_dateid
      AND tmp.dim_dateidstart = strt.dim_dateid
      AND tmp.dim_dateidend = endd.dim_dateid
      AND snps.calendarweekyr = calc.calendarweekyr
      AND UPPER(tmp.dd_run) NOT LIKE '%TEST%'
	  AND tmp.dd_run <> 'Not Set'
	  AND tmp.dim_dateidstart <> 1
      AND calc.total_hr <> 0
      AND calc.dd_subservice = cntrec.subservice
      AND calc.dd_service = tmp.dd_service
      AND endd.calendarweekyr >= snps.calendarweekyr

*/

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.ct_total_hr = calc.total_hr
FROM tmp_calculatehours calc,
     dim_date snps,
     tmp_fact_bixiefficiency tmp
WHERE tmp.dd_pu3 = calc.dd_subservice
      AND tmp.dim_dateidsnapshot = snps.dim_dateid
      AND snps.calendarweekyr = calc.calendarweekyr
      AND calc.dd_service = tmp.dd_service;

/* 31 Mar 2017 CristianT End */

DROP TABLE IF EXISTS tmp_countavgpeople;
CREATE TABLE tmp_countavgpeople
AS
SELECT calendarweekyr,
       dd_service,
       dd_subservice,
       round(avg(nr_of_people), 2) as nr_of_people
FROM(
SELECT dt.calendarweekyr,
       dt.datevalue,
       usr.dd_service,
       CASE usr.dd_subservice
         WHEN 'USP 1' THEN 'USP1'
         WHEN 'USP Suite 2' THEN 'USP2'
         WHEN 'USP Suite 3' THEN 'USP3'
         WHEN 'USP Suite 4' THEN 'USP4'
         WHEN 'Suite 1 DSP' THEN 'DSP1'
         WHEN 'Suite 2 DSP' THEN 'DSP2'
         WHEN 'Suite 3 DSP' THEN 'DSP3'
         WHEN 'Suite 4 DSP' THEN 'DSP4'
         ELSE usr.dd_subservice
       END as dd_subservice,
       count(*) as nr_of_people
FROM MT_BIXI_EMPLOYEE_HOURS bix,
     dim_date dt,
     dim_qualityusers usr
WHERE 1 = 1
      AND bix.BIXI_hours NOT LIKE '%-%'
      AND bix.BIXI_date = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND bix.BIXI_muid = usr.merck_uid
GROUP BY dt.calendarweekyr,
         dt.datevalue,
         /* usr.dd_departmenttype, */
         usr.dd_service,
         usr.dd_subservice
) t
GROUP BY calendarweekyr,
         dd_service,
         dd_subservice;

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.ct_avg_people = nr_of_people
FROM tmp_countavgpeople calc,
     dim_date snps,
     dim_date strt,
     dim_date endd,
     tmp_fact_bixiefficiency tmp
WHERE tmp.dd_pu3 = calc.dd_subservice
      AND calc.dd_service = tmp.dd_service
      AND tmp.dim_dateidsnapshot = snps.dim_dateid
      AND tmp.dim_dateidstart = strt.dim_dateid
      AND tmp.dim_dateidend = endd.dim_dateid
      AND snps.calendarweekyr = calc.calendarweekyr
      AND UPPER(tmp.dd_run) NOT LIKE '%TEST%'
	  AND tmp.dd_run <> 'Not Set'
	  AND tmp.dim_dateidstart <> 1
      AND endd.calendarweekyr >= snps.calendarweekyr;

DROP TABLE IF EXISTS tmp_calculatehours;
DROP TABLE IF EXISTS tmp_countrecords;
DROP TABLE IF EXISTS tmp_countavgpeople;

/* 30 Mar 2017 CristianT Start: Adding logic for Count FTE measure */
DROP TABLE IF EXISTS tmp_countfte;
CREATE TABLE tmp_countfte
AS
SELECT usr.dd_service as dd_service,
       CASE usr.dd_subservice
         WHEN 'USP 1' THEN 'USP1'
         WHEN 'USP 2' THEN 'USP2'
         WHEN 'USP Suite 2' THEN 'USP2'
         WHEN 'USP Suite 3' THEN 'USP3'
         WHEN 'USP Suite 4' THEN 'USP4'
         WHEN 'Suite 1 DSP' THEN 'DSP1'
         WHEN 'Suite 2 DSP' THEN 'DSP2'
         WHEN 'Suite 3 DSP' THEN 'DSP3'
         WHEN 'Suite 4 DSP' THEN 'DSP4'
         WHEN 'DSP 1' THEN 'DSP1'
         WHEN 'DSP 2' THEN 'DSP2'
         ELSE usr.dd_subservice
       END as dd_subservice,
       count(*) as ct_fte
FROM dim_qualityusers usr
GROUP BY usr.dd_service,
         CASE usr.dd_subservice
           WHEN 'USP 1' THEN 'USP1'
           WHEN 'USP 2' THEN 'USP2'
           WHEN 'USP Suite 2' THEN 'USP2'
           WHEN 'USP Suite 3' THEN 'USP3'
           WHEN 'USP Suite 4' THEN 'USP4'
           WHEN 'Suite 1 DSP' THEN 'DSP1'
           WHEN 'Suite 2 DSP' THEN 'DSP2'
           WHEN 'Suite 3 DSP' THEN 'DSP3'
           WHEN 'Suite 4 DSP' THEN 'DSP4'
           WHEN 'DSP 1' THEN 'DSP1'
           WHEN 'DSP 2' THEN 'DSP2'
           ELSE usr.dd_subservice
         END;

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.ct_fte = calc.ct_fte
FROM tmp_countfte calc,
     tmp_fact_bixiefficiency tmp
WHERE tmp.dd_pu3 = calc.dd_subservice
      AND tmp.dd_service = calc.dd_service;

DROP TABLE IF EXISTS tmp_countfte;

/* 30 Mar 2017 CristianT End */

UPDATE tmp_fact_bixiefficiency tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_bixiefficiency tmp;

INSERT INTO fact_bixiefficiencyhistory(
fact_bixiefficiencyid,
snapshotdate,
dim_dateidsnapshot,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_run,
dd_mo,
dd_mostatus,
dd_reviewstatus,
dd_batch,
dd_materialid,
dd_materialdesc,
dd_mbr_var,
dd_mbr_ver,
dd_startdate,
dim_dateidstart,
dd_enddate,
dim_dateidend,
dd_sfo,
dd_sfostatus,
dd_boid,
dd_bodesc,
dd_pu,
dd_pu2,
dd_pu3,
dd_sfoproductcode,
dd_service,
ct_total_hr,
ct_avg_people,
ct_fte
)
SELECT fact_bixiefficiencyid,
       snapshotdate,
       dim_dateidsnapshot,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_run,
       dd_mo,
       dd_mostatus,
       dd_reviewstatus,
       dd_batch,
       dd_materialid,
       dd_materialdesc,
       dd_mbr_var,
       dd_mbr_ver,
       dd_startdate,
       dim_dateidstart,
       dd_enddate,
       dim_dateidend,
       dd_sfo,
       dd_sfostatus,
       dd_boid,
       dd_bodesc,
       dd_pu,
       dd_pu2,
       dd_pu3,
       dd_sfoproductcode,
       dd_service,
       ct_total_hr,
       ct_avg_people,
       ct_fte
FROM tmp_fact_bixiefficiency;

DROP TABLE IF EXISTS tmp_fact_bixiefficiency;

DROP TABLE IF EXISTS bixiefficiency_delete;
CREATE TABLE bixiefficiency_delete
AS
SELECT fact_bixiefficiencyid
FROM fact_bixiefficiency
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_bixiefficiency
WHERE fact_bixiefficiencyid IN (SELECT fact_bixiefficiencyid FROM bixiefficiency_delete);

DROP TABLE IF EXISTS bixiefficiency_delete;


DROP TABLE IF EXISTS bixiefficiency_insert;
CREATE TABLE bixiefficiency_insert
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_bixiefficiencyhistory f,
     dim_date dt
WHERE dim_dateidsnapshot = dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

INSERT INTO fact_bixiefficiency
SELECT h.*
FROM fact_bixiefficiencyhistory h,
     bixiefficiency_insert ins
WHERE h.dim_dateidsnapshot = ins.dim_dateidsnapshot
      AND ins.rowno = 1
      AND NOT EXISTS (SELECT 1 FROM fact_bixiefficiency t WHERE t.dim_dateidsnapshot = ins.dim_dateidsnapshot);

DROP TABLE IF EXISTS bixiefficiency_insert;

DROP TABLE IF EXISTS tmp_delete_bixi;
CREATE TABLE tmp_delete_bixi
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_bixiefficiency f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO fact_bixiefficiency f
USING tmp_delete_bixi del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_delete_bixi;

/* 31 Mar 2017 CristianT Start: Added delete/insert statements for emd586 inside the script to prevent delays caused by the harmonization */
DROP TABLE IF EXISTS emd586bixiefficiency_delete;
CREATE TABLE emd586bixiefficiency_delete
AS
SELECT fact_bixiefficiencyid
FROM emd586.fact_bixiefficiency
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM emd586.fact_bixiefficiency
WHERE fact_bixiefficiencyid IN (SELECT fact_bixiefficiencyid FROM emd586bixiefficiency_delete);

DROP TABLE IF EXISTS emd586bixiefficiency_delete;


INSERT INTO emd586.fact_bixiefficiency
SELECT h.*
FROM fact_bixiefficiency h
WHERE h.snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;


DROP TABLE IF EXISTS tmp_emd586deletebixi;
CREATE TABLE tmp_emd586deletebixi
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM emd586.fact_bixiefficiency f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO emd586.fact_bixiefficiency f
USING tmp_emd586deletebixi del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_emd586deletebixi;