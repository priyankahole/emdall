/*****************************************************************************************************************/
/*   Script         : exa_fact_mdaqaalert                                                                        */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 17 Oct 2018                                                                                */
/*   Description    : Populating script of fact_mdaqaalert                                                       */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   06 Nov 2018         CristianT      1.0          Creating the script                                         */
/*   14 May 2019         CristianT      1.1          Changed the source of data to HDFS                          */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_mdaqaalert;
CREATE TABLE tmp_fact_mdaqaalert
AS
SELECT *
FROM fact_mdaqaalert
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_mdaqaalert;
CREATE TABLE number_fountain_fact_mdaqaalert LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_mdaqaalert
SELECT 'fact_mdaqaalert', ifnull(max(fact_mdaqaalertid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_mdaqaalert;


INSERT INTO tmp_fact_mdaqaalert(
fact_mdaqaalertid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pr_id,
dd_title,
dd_project,
dd_record_state,
dd_assignedto,
dd_assignedto_userid,
dim_qualityusersidassignedto,
dd_date_opened,
dim_dateidopened,
dd_date_current_state,
dim_dateidcurrentstate,
dd_date_due,
dim_dateiddue,
dd_lateflag,
dd_hc_originating_site,
dd_hc_investigating_site
)
SELECT (SELECT max_id from number_fountain_fact_mdaqaalert WHERE table_name = 'fact_mdaqaalert') + ROW_NUMBER() over(order by '') AS fact_mdaqaalertid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(tw.pr_id, 0) as dd_pr_id,
       ifnull(tw.title, 'Not Set') as dd_title,
       ifnull(tw.project, 'Not Set') as dd_project,
       ifnull(tw.record_state, 'Not Set') as dd_record_state,
       ifnull(tw.assignedto, 'Not Set') as dd_assignedto,
       ifnull(tw.assignedto_userid, 'Not Set') as dd_assignedto_userid,
       1 as dim_qualityusersidassignedto,
       ifnull(tw.date_opened, '0001-01-01') as dd_date_opened,
       1 as dim_dateidopened,
       ifnull(tw.date_current_state, '0001-01-01') as dd_date_current_state,
       1 as dim_dateidcurrentstate,
       ifnull(tw.date_due, '0001-01-01') as dd_date_due,
       1 as dim_dateiddue,
       'Not Set' as dd_lateflag,
       ifnull(tw.hc_originating_site, 'Not Set') as dd_hc_originating_site,
       ifnull(tw.hc_investigating_site, 'Not Set') as dd_hc_investigating_site
FROM tw_rpt_main tw
     INNER JOIN dim_qualityusers usr ON tw.ASSIGNEDTO_USERID = usr.MERCK_UID or tw.ASSIGNEDTO_USERID = usr.USERID
WHERE tw.hc_site = 'Health Care'
      AND tw.project in ('Deviation','Investigation','Investigation Standalone')
      AND lower(tw.record_state) not like '%closed%'
      AND (lower(tw.hc_originating_site) like '%darmstadt%' OR lower(tw.hc_investigating_site) like '%darmstadt%')
      AND usr.cmgnumber = '1500'
      /* AND usr.department_number in ('00002042','00002040','00002031','00141688','00141683') */
      AND usr.dd_departmenttype in ('MGD','QAO');

UPDATE tmp_fact_mdaqaalert tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_mdaqaalert tmp;

UPDATE tmp_fact_mdaqaalert tmp
SET tmp.dim_dateidopened = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_mdaqaalert tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.dd_date_opened = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidopened <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mdaqaalert tmp
SET tmp.dim_dateidcurrentstate = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_mdaqaalert tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.dd_date_current_state = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidcurrentstate <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mdaqaalert tmp
SET tmp.dim_dateiddue = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_mdaqaalert tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.dd_date_due = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateiddue <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mdaqaalert tmp
SET tmp.dim_qualityusersidassignedto = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_mdaqaalert tmp
WHERE tmp.dd_assignedto_userid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dim_qualityusersidassignedto <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_mdaqaalert tmp
SET tmp.dim_qualityusersidassignedto = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_mdaqaalert tmp
WHERE tmp.dd_assignedto_userid = usr.USERID
      AND usr.rowiscurrent = 1
      AND tmp.dim_qualityusersidassignedto <> ifnull(usr.dim_qualityusersid, 1);

/* First Step of late Deviation: Open more then 30 days */
UPDATE tmp_fact_mdaqaalert tmp
SET dd_lateflag = 'Y'
FROM tmp_fact_mdaqaalert tmp,
     dim_date dt
WHERE tmp.dd_project = 'Deviation'
      AND tmp.dim_dateidopened = dt.dim_dateid
      AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date END - dt.datevalue > 30;

/* First Step of late Investigation: Open more then 28 days */
UPDATE tmp_fact_mdaqaalert tmp
SET dd_lateflag = 'Y'
FROM tmp_fact_mdaqaalert tmp,
     dim_date dt
WHERE tmp.dd_project <> 'Deviation'
      AND tmp.dim_dateidopened = dt.dim_dateid
      AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 28;

/* Second Step of late Deviation */
DROP TABLE IF EXISTS tmp_2ndstep_late_deviation;
CREATE TABLE tmp_2ndstep_late_deviation
AS
SELECT fact_mdaqaalertid,
       tmp.dd_pr_id,
       tmp.dd_record_state,
       current_date as curr_date,
       dt.datevalue as curr_state_date,
       current_date - dt.datevalue as date_dif,
       CASE
         WHEN tmp.dd_record_state = 'Open' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 1 THEN 'Y'
         WHEN tmp.dd_record_state = 'QA Assessment' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 1 THEN 'Y'
         WHEN tmp.dd_record_state = 'Investigation In Progress' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 20 THEN 'Y'
         WHEN tmp.dd_record_state = 'QA Due Date Decision' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 1 THEN 'Y'
         WHEN tmp.dd_record_state = 'Waiting for Investigation(s) and CAPA(s)' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 1 THEN 'Y'
         WHEN tmp.dd_record_state = 'QA Closure Approval Required' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 3 THEN 'Y'
         ELSE 'Not Set'
       END as dd_lateflag
FROM tmp_fact_mdaqaalert tmp,
     dim_date dt
WHERE tmp.dd_project = 'Deviation'
      AND tmp.dd_lateflag <> 'Y'
      AND dt.dim_dateid = tmp.dim_dateidcurrentstate;

UPDATE tmp_fact_mdaqaalert tmp
SET dd_lateflag = late.dd_lateflag
FROM tmp_fact_mdaqaalert tmp,
     tmp_2ndstep_late_deviation late
WHERE tmp.fact_mdaqaalertid = late.fact_mdaqaalertid;

DROP TABLE IF EXISTS tmp_2ndstep_late_deviation;

/* Second Step of late Investigation */
DROP TABLE IF EXISTS tmp_2ndstep_late_investigation;
CREATE TABLE tmp_2ndstep_late_investigation
AS
SELECT fact_mdaqaalertid,
       tmp.dd_pr_id,
       tmp.dd_record_state,
       CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end as curr_date,
       dt.datevalue as curr_state_date,
       CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue as date_dif,
       CASE
         WHEN tmp.dd_record_state = 'Open' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 1 THEN 'Y'
         WHEN tmp.dd_record_state = 'Investigation In Progress' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 21 THEN 'Y'
         WHEN tmp.dd_record_state = 'Waiting for CAPAs' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 1 THEN 'Y'
         WHEN tmp.dd_record_state = 'QA Closure Approval Required' AND CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - dt.datevalue > 3 THEN 'Y'
         ELSE 'Not Set'
       END as dd_lateflag
FROM tmp_fact_mdaqaalert tmp,
     dim_date dt
WHERE tmp.dd_project <> 'Deviation'
      AND tmp.dd_lateflag <> 'Y'
      AND dt.dim_dateid = tmp.dim_dateidcurrentstate;

UPDATE tmp_fact_mdaqaalert tmp
SET dd_lateflag = late.dd_lateflag
FROM tmp_fact_mdaqaalert tmp,
     tmp_2ndstep_late_investigation late
WHERE tmp.fact_mdaqaalertid = late.fact_mdaqaalertid;

DROP TABLE IF EXISTS tmp_2ndstep_late_investigation;


TRUNCATE TABLE fact_mdaqaalert;
INSERT INTO fact_mdaqaalert(
fact_mdaqaalertid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pr_id,
dd_title,
dd_project,
dd_record_state,
dd_assignedto,
dd_assignedto_userid,
dim_qualityusersidassignedto,
dd_date_opened,
dim_dateidopened,
dd_date_current_state,
dim_dateidcurrentstate,
dd_date_due,
dim_dateiddue,
dd_lateflag,
dd_hc_originating_site,
dd_hc_investigating_site
)
SELECT fact_mdaqaalertid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_pr_id,
       dd_title,
       dd_project,
       dd_record_state,
       dd_assignedto,
       dd_assignedto_userid,
       dim_qualityusersidassignedto,
       dd_date_opened,
       dim_dateidopened,
       dd_date_current_state,
       dim_dateidcurrentstate,
       dd_date_due,
       dim_dateiddue,
       dd_lateflag,
       dd_hc_originating_site,
       dd_hc_investigating_site
FROM tmp_fact_mdaqaalert;

DROP TABLE IF EXISTS tmp_fact_mdaqaalert;

DELETE FROM emd586.fact_mdaqaalert;
INSERT INTO emd586.fact_mdaqaalert SELECT * FROM fact_mdaqaalert;