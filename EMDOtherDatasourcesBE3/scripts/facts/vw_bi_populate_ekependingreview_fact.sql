/******************************************************************************************************************/
/*   Script         : bi_populate_ekependingreview_fact	                                                          */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 05 Oct 2016                                                                                 */
/*   Description    : Populating script of fact_ekependingreview                                                  */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   05 Oct 2016         Cristian T      1.0               Creating the script.                                   */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_ekependingreview;
CREATE TABLE tmp_fact_ekependingreview
AS
SELECT *
FROM fact_ekependingreview
WHERE 1 = 0;

ALTER TABLE tmp_fact_ekependingreview ADD COLUMN start_date DATE;
ALTER TABLE tmp_fact_ekependingreview ADD COLUMN end_date DATE;
ALTER TABLE tmp_fact_ekependingreview ADD COLUMN last_sign DATE;

INSERT INTO tmp_fact_ekependingreview(
fact_ekependingreviewid,
dd_run,
dd_mo,
dd_mo_status,
dd_review_status,
dd_batch,
dd_material_id,
dd_material_desc,
dd_mbr_var,
dd_mbr_ver,
dim_dateidstart_date,
dim_dateidend_date,
dd_date_status,
dim_dateidlast_sign,
dd_pu,
dd_product_code,
ct_pu_target,
dd_pu_desc,
dd_pu_groupe,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pulevel_target,
dd_pugroupe_target,
start_date,
end_date,
last_sign
)
SELECT ROW_NUMBER() over(order by '') as fact_ekependingreviewid,
       ifnull(eke.run, 'Not Set') as dd_run,
       ifnull(eke.mo, 'Not Set') as dd_mo,
       ifnull(eke.mo_status, 0) as dd_mo_status,
       ifnull(eke.review_status, 0) as dd_review_status,
       ifnull(eke.batch, 'Not Set') as dd_batch,
       ifnull(eke.material_id, 'Not Set') as dd_material_id,
       ifnull(eke.material_desc, 'Not Set') as dd_material_desc,
       ifnull(eke.mbr_var, 'Not Set') as dd_mbr_var,
       ifnull(eke.mbr_ver, 0) as dd_mbr_ver,
       1 as dim_dateidstart_date,
       1 as dim_dateidend_date,
       ifnull(eke.date_status, 0) as dd_date_status,
       1 as dim_dateidlast_sign,
       ifnull(eke.pu, 'Not Set') as dd_pu,
       ifnull(eke.product_code, 'Not Set') as dd_product_code,
       0 as ct_pu_target,
       'Not Set' as dd_pu_desc,
       'Not Set' as dd_pu_groupe,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Not Set' as dd_pulevel_target,
       'Not Set' as dd_pugroupe_target,
       eke.start_date as start_date,
       eke.end_date as end_date,
       eke.last_sign as last_sign
FROM EKE4_MO_PENDING_REVIEW eke;

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dim_dateidstart_date = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_ekependingreview tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.start_date = dt.datevalue
      AND tmp.dim_dateidstart_date <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dim_dateidend_date = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_ekependingreview tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.end_date = dt.datevalue
      AND tmp.dim_dateidend_date <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dim_dateidlast_sign = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_ekependingreview tmp
WHERE dt.companycode = 'Not Set'
      AND tmp.last_sign = dt.datevalue
      AND tmp.dim_dateidlast_sign <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dd_pu_groupe = grp.name
FROM pu_groupe pu,
     groupe grp,
     tmp_fact_ekependingreview tmp
WHERE tmp.dd_pu = pu.pu_id
      AND grp.id = pu.groupe_id;

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dd_pulevel_target = case
                              when dd_pu = 'BP10' then '70'
                              when dd_pu = 'CELLS1' then '1'
                              when dd_pu = 'CELLS2' then '1'
                              when dd_pu = 'DSP1' then '50'
                              when dd_pu = 'DSP3' then '80'
                              when dd_pu = 'DSP4' then '80'
                              when dd_pu = 'MEDIA1' then '5'
                              when dd_pu = 'MEDIA2' then '5'
                              when dd_pu = 'MP10' then '30'
                              when dd_pu = 'USP1' then '49'
                              when dd_pu = 'USP4' then '23'
                              else 'Not Set'
                            end;

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dd_pugroupe_target = case
                               when dd_pu_groupe = 'DSP FEDBATCH' then '160'
                               when dd_pu_groupe = 'DSP PERFUSION' then '50'
                               when dd_pu_groupe = 'SUP FEDBATCH' then '110'
                               when dd_pu_groupe = 'USP FEDBATCH' then '61'
                               when dd_pu_groupe = 'USP PERFUSION' then '55'
                               when dd_pu_groupe = 'SUP PERFUSION' then '10'
                               else 'Not Set'
                             end;

UPDATE tmp_fact_ekependingreview tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_ekependingreview tmp;

DELETE FROM fact_ekependingreview;

INSERT INTO fact_ekependingreview(
fact_ekependingreviewid,
dd_run,
dd_mo,
dd_mo_status,
dd_review_status,
dd_batch,
dd_material_id,
dd_material_desc,
dd_mbr_var,
dd_mbr_ver,
dim_dateidstart_date,
dim_dateidend_date,
dd_date_status,
dim_dateidlast_sign,
dd_pu,
dd_product_code,
ct_pu_target,
dd_pu_desc,
dd_pu_groupe,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pulevel_target,
dd_pugroupe_target
)
SELECT fact_ekependingreviewid,
       dd_run,
       dd_mo,
       dd_mo_status,
       dd_review_status,
       dd_batch,
       dd_material_id,
       dd_material_desc,
       dd_mbr_var,
       dd_mbr_ver,
       dim_dateidstart_date,
       dim_dateidend_date,
       dd_date_status,
       dim_dateidlast_sign,
       dd_pu,
       dd_product_code,
       ct_pu_target,
       dd_pu_desc,
       dd_pu_groupe,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_pulevel_target,
       dd_pugroupe_target
FROM tmp_fact_ekependingreview;

DROP TABLE IF EXISTS tmp_fact_ekependingreview;