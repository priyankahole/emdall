/**********************************************************************************/
/* 6 March 2014    		  Cornelia 	 add ct_qtyuoe 	  */
/* 30 Dec 2013    		  Cornelia 	 update dim_customer 	  */
/* 24 Oct 2013    1.11		  Issam	 	 New fields AFKO.VORUE and AUFK.VAPLZ 	  */
/* 7 Sep 2013     1.5		  Lokesh	 Exchange rate, currency and amt changes */
/*  28 Aug 2013   1.4	      Shanthi    New Field WIP Qty from AUFM		  */
/**********************************************************************************/
Drop table if exists pGlobalCurrency_po_43;

Create table pGlobalCurrency_po_43(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_43(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_43
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

Create table fact_productionorder_tmp_43 as
SELECT dd_ordernumber,dd_orderitemno
FROM fact_productionorder ;

Create table max_holder_43
as Select ifnull(max(fact_productionorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder;

DROP TABLE IF EXISTS TMP_001_productionorder;
CREATE TABLE TMP_001_productionorder
AS SELECT p.*,convert(varchar(5),'') AUFK_WAERS from fact_productionorder p where 1=2;


INSERT INTO TMP_001_productionorder(fact_productionorderid,
				 Dim_DateidRelease,
                                 Dim_DateidBOMExplosion,
                                 Dim_DateidLastScheduling,
                                 Dim_DateidTechnicalCompletion,
                                 Dim_DateidBasicStart,
                                 Dim_DateidScheduledRelease,
                                 Dim_DateidScheduledFinish,
                                 Dim_DateidScheduledStart,
                                 Dim_DateidActualStart,
                                 Dim_DateidConfirmedOrderFinish,
                                 Dim_DateidActualHeaderFinish,
                                 Dim_DateidActualRelease,
                                 Dim_DateidActualItemFinish,
                                 Dim_DateidPlannedOrderDelivery,
                                 Dim_DateidRoutingTransfer,
                                 Dim_DateidBasicFinish,
                                 Dim_ControllingAreaid,
                                 Dim_ProfitCenterId,
                                 Dim_tasklisttypeid,
                                 Dim_PartidHeader,
                                 Dim_bomstatusid,
                                 Dim_bomusageid,
                                 dim_productionschedulerid,
                                 Dim_ProcurementID,
                                 Dim_SpecialProcurementID,
                                 Dim_UnitOfMeasureid,
                                 Dim_PartidItem,
                                 Dim_AccountCategoryid,
                                 Dim_StockTypeid,
                                 Dim_StorageLocationid,
                                 Dim_Plantid,
                                 dim_specialstockid,
                                 Dim_ConsumptionTypeid,
                                 Dim_SalesOrgid,
                                 Dim_PurchaseOrgid,
                                 Dim_Currencyid,
                                 Dim_Companyid,
                                 Dim_ordertypeid,
                                 dim_productionorderstatusid,
                                 dim_productionordermiscid,
                                 Dim_BuildTypeid,
                                 dd_ordernumber,
                                 dd_orderitemno,
                                 dd_bomexplosionno,
                                 dd_plannedorderno,
                                 dd_SalesOrderNo,
                                 dd_SalesOrderItemNo,
                                 dd_SalesOrderDeliveryScheduleNo,
                                 ct_ConfirmedReworkQty,
                                 ct_ScrapQty,
                                 ct_OrderItemQty,
                                 ct_TotalOrderQty,
                                 ct_GRQty,
                                 ct_GRProcessingTime,
                                 amt_estimatedTotalCost,
                                 amt_ValueGR,
                                 dd_BOMLevel,
                                 dd_sequenceno,
                                 dd_ObjectNumber,
				 ct_ConfirmedScrapQty,
				 dd_OrderDescription,
                                 Dim_MrpControllerId,
				dim_currencyid_TRA,
				dim_currencyid_GBL,
				dd_OperationNumber,
				dd_WorkCenter,
				dim_customerid,
				AUFK_WAERS)
   SELECT max_holder_43.maxid + row_number() over(order by '') fact_productionorderid,
	  1   Dim_DateidRelease,
          1   Dim_DateidBOMExplosion,
          1   Dim_DateidLastScheduling,
          1   Dim_DateidTechnicalCompletion,
          1 Dim_DateidBasicStart,
          1   Dim_DateidScheduledRelease,
          1   Dim_DateidScheduledFinish,
          1   Dim_DateidScheduledStart,
          1   Dim_DateidActualStart,
          1   Dim_DateidConfirmedOrderFinish,
          1   Dim_DateidActualHeaderFinish,
          1   Dim_DateidActualRelease,
          1   Dim_DateidActualItemFinish,
          1   Dim_DateidPlannedOrderDelivery,
          1   Dim_DateidRoutingTransfer,
          1   Dim_DateidBasicFinish,
          1   Dim_ControllingAreaid,
          1 Dim_ProfitCenterId,
          1   Dim_tasklisttypeid,
          1   Dim_PartidHeader,
          1 Dim_bomstatusid,
          1   Dim_bomusageid,
          1   dim_productionschedulerid,
          1   Dim_ProcurementID,
          1   Dim_SpecialProcurementID,
          1   Dim_UnitOfMeasureid,
          1  Dim_PartidItem,
          1   Dim_AccountCategoryid,
          1   Dim_StockTypeid,
          1   Dim_StorageLocationid,
          1 Dim_Plantid,
          1   dim_specialstockid,
          1   Dim_ConsumptionTypeid,
          1  Dim_SalesOrgid,
          1  Dim_PurchaseOrgid,
          1  Dim_Currencyid,
          1  Dim_Companyid,
          1  Dim_productionordertypeid,
          1   dim_productionorderstatusid,
          1   dim_productionordermiscid,
          1   Dim_BuildTypeid,
          ifnull(AFKO_AUFNR,'Not Set') dd_ordernumber,
          AFPO_POSNR dd_orderitemno,
          ifnull(AFPO_SERNR, 'Not Set') dd_bomexplosionno,
          ifnull(AFPO_PLNUM,'Not Set') dd_plannedorderno,
          ifnull(AFPO_KDAUF,'Not Set') dd_SalesOrderNo,
          ifnull(AFPO_KDPOS,0) dd_SalesOrderItemNo,
          ifnull(AFPO_KDEIN,0) dd_SalesOrderDeliveryScheduleNo,
          AFKO_RMNGA ct_ConfirmedReworkQty,
          AFPO_PSAMG ct_ScrapQty,
          AFPO_PSMNG ct_OrderItemQty,
          AFKO_GAMNG ct_TotalOrderQty,
          AFPO_WEMNG ct_GRQty,
          AFPO_WEBAZ ct_GRProcessingTime,
          AUFK_USER4 amt_estimatedTotalCost,
          mhi.AFPO_WEWRT amt_ValueGR,
          AFKO_STUFE dd_BOMLevel,
          ifnull(AFKO_CY_SEQNR,'Not Set') dd_sequenceno,
          ifnull(AUFK_OBJNR,'Not Set') dd_ObjectNumber,
	  ifnull(AFKO_IASMG,0.000) ct_ConfirmedScrapQty,
	  ifnull(AUFK_KTEXT,'Not Set') dd_OrderDescription,
          1 Dim_MrpControllerId,
          1 Dim_Currencyid_TRA,
          1 dim_Currencyid_GBL,
	ifnull(AFKO_VORUE,'Not Set') dd_OperationNumber,
	ifnull(AUFK_VAPLZ,'Not Set') dd_WorkCenter,
	1 dim_customerid,
	mhi.AUFK_WAERS -- used in Dim_Currencyid_TRA
     FROM  max_holder_43,AFKO_AFPO_AUFK mhi,pGlobalCurrency_po_43
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_productionorder_tmp_43 po
                   WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
                         AND po.dd_orderitemno = mhi.AFPO_POSNR);

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_TRA = IFNULL(cur.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		LEFT JOIN Dim_Currency cur ON		
			cur.CurrencyCode = t1.AUFK_WAERS;

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_GBL = IFNULL(t2.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		CROSS JOIN (SELECT cr.Dim_Currencyid 
		            FROM Dim_Currency cr 
							INNER JOIN pGlobalCurrency_po_43 p
                  on cr.CurrencyCode = p.pGlobalCurrency) t2;

INSERT INTO fact_productionorder(fact_productionorderid,
				 Dim_DateidRelease,
                                 Dim_DateidBOMExplosion,
                                 Dim_DateidLastScheduling,
                                 Dim_DateidTechnicalCompletion,
                                 Dim_DateidBasicStart,
                                 Dim_DateidScheduledRelease,
                                 Dim_DateidScheduledFinish,
                                 Dim_DateidScheduledStart,
                                 Dim_DateidActualStart,
                                 Dim_DateidConfirmedOrderFinish,
                                 Dim_DateidActualHeaderFinish,
                                 Dim_DateidActualRelease,
                                 Dim_DateidActualItemFinish,
                                 Dim_DateidPlannedOrderDelivery,
                                 Dim_DateidRoutingTransfer,
                                 Dim_DateidBasicFinish,
                                 Dim_ControllingAreaid,
                                 Dim_ProfitCenterId,
                                 Dim_tasklisttypeid,
                                 Dim_PartidHeader,
                                 Dim_bomstatusid,
                                 Dim_bomusageid,
                                 dim_productionschedulerid,
                                 Dim_ProcurementID,
                                 Dim_SpecialProcurementID,
                                 Dim_UnitOfMeasureid,
                                 Dim_PartidItem,
                                 Dim_AccountCategoryid,
                                 Dim_StockTypeid,
                                 Dim_StorageLocationid,
                                 Dim_Plantid,
                                 dim_specialstockid,
                                 Dim_ConsumptionTypeid,
                                 Dim_SalesOrgid,
                                 Dim_PurchaseOrgid,
                                 Dim_Currencyid,
                                 Dim_Companyid,
                                 Dim_ordertypeid,
                                 dim_productionorderstatusid,
                                 dim_productionordermiscid,
                                 Dim_BuildTypeid,
                                 dd_ordernumber,
                                 dd_orderitemno,
                                 dd_bomexplosionno,
                                 dd_plannedorderno,
                                 dd_SalesOrderNo,
                                 dd_SalesOrderItemNo,
                                 dd_SalesOrderDeliveryScheduleNo,
                                 ct_ConfirmedReworkQty,
                                 ct_ScrapQty,
                                 ct_OrderItemQty,
                                 ct_TotalOrderQty,
                                 ct_GRQty,
                                 ct_GRProcessingTime,
                                 amt_estimatedTotalCost,
                                 amt_ValueGR,
                                 dd_BOMLevel,
                                 dd_sequenceno,
                                 dd_ObjectNumber,
				 ct_ConfirmedScrapQty,
				 dd_OrderDescription,
                                 Dim_MrpControllerId,
				dim_currencyid_TRA,
				dim_currencyid_GBL,
				dd_OperationNumber,
				dd_WorkCenter,
				dim_customerid)
   SELECT fact_productionorderid,
				 Dim_DateidRelease,
                                 Dim_DateidBOMExplosion,
                                 Dim_DateidLastScheduling,
                                 Dim_DateidTechnicalCompletion,
                                 Dim_DateidBasicStart,
                                 Dim_DateidScheduledRelease,
                                 Dim_DateidScheduledFinish,
                                 Dim_DateidScheduledStart,
                                 Dim_DateidActualStart,
                                 Dim_DateidConfirmedOrderFinish,
                                 Dim_DateidActualHeaderFinish,
                                 Dim_DateidActualRelease,
                                 Dim_DateidActualItemFinish,
                                 Dim_DateidPlannedOrderDelivery,
                                 Dim_DateidRoutingTransfer,
                                 Dim_DateidBasicFinish,
                                 Dim_ControllingAreaid,
                                 Dim_ProfitCenterId,
                                 Dim_tasklisttypeid,
                                 Dim_PartidHeader,
                                 Dim_bomstatusid,
                                 Dim_bomusageid,
                                 dim_productionschedulerid,
                                 Dim_ProcurementID,
                                 Dim_SpecialProcurementID,
                                 Dim_UnitOfMeasureid,
                                 Dim_PartidItem,
                                 Dim_AccountCategoryid,
                                 Dim_StockTypeid,
                                 Dim_StorageLocationid,
                                 Dim_Plantid,
                                 dim_specialstockid,
                                 Dim_ConsumptionTypeid,
                                 Dim_SalesOrgid,
                                 Dim_PurchaseOrgid,
                                 Dim_Currencyid,
                                 Dim_Companyid,
                                 Dim_ordertypeid,
                                 dim_productionorderstatusid,
                                 dim_productionordermiscid,
                                 Dim_BuildTypeid,
                                 dd_ordernumber,
                                 dd_orderitemno,
                                 dd_bomexplosionno,
                                 dd_plannedorderno,
                                 dd_SalesOrderNo,
                                 dd_SalesOrderItemNo,
                                 dd_SalesOrderDeliveryScheduleNo,
                                 ct_ConfirmedReworkQty,
                                 ct_ScrapQty,
                                 ct_OrderItemQty,
                                 ct_TotalOrderQty,
                                 ct_GRQty,
                                 ct_GRProcessingTime,
                                 amt_estimatedTotalCost,
                                 amt_ValueGR,
                                 dd_BOMLevel,
                                 dd_sequenceno,
                                 dd_ObjectNumber,
				 ct_ConfirmedScrapQty,
				 dd_OrderDescription,
                                 Dim_MrpControllerId,
				dim_currencyid_TRA,
				dim_currencyid_GBL,
				dd_OperationNumber,
				dd_WorkCenter,
				dim_customerid
     FROM TMP_001_productionorder;


Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

DELETE FROM fact_productionorder
WHERE EXISTS
          (SELECT 1
             FROM AFKO_AFPO_AUFK
            WHERE     AFKO_AUFNR = dd_ordernumber
                  AND AFPO_POSNR = dd_orderitemno
                  AND AFPO_XLOEK = 'X');
                  
DELETE FROM fact_productionorder
 WHERE EXISTS
          (SELECT 1
             FROM AFKO_AFPO_AUFK
            WHERE AFKO_AUFNR = dd_ordernumber AND AUFK_LOEKZ = 'X');


UPDATE fact_productionorder po
SET po.Dim_DateidRelease = dr.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AUFK_IDAT1 AND mhi.AUFK_BUKRS = dr.CompanyCode
  AND po.Dim_DateidRelease <> dr.dim_dateid;

UPDATE fact_productionorder po
SET po.Dim_DateidBOMExplosion = dr.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AFKO_AUFLD AND mhi.AUFK_BUKRS = dr.CompanyCode
  AND po.Dim_DateidBOMExplosion <> dr.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidLastScheduling = ls.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ls
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ls.DateValue = AFKO_TRMDT AND mhi.AUFK_BUKRS = ls.CompanyCode
  AND po.Dim_DateidLastScheduling <> ls.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidTechnicalCompletion = tc.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date tc
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tc.DateValue = AUFK_IDAT2 AND mhi.AUFK_BUKRS = tc.CompanyCode
  AND po.Dim_DateidTechnicalCompletion <> tc.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidBasicStart= bst.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bst
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bst.DateValue = AFKO_GSTRP AND mhi.AUFK_BUKRS = bst.CompanyCode
  AND po.Dim_DateidBasicStart <> bst.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledRelease = sr.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sr.DateValue = AFKO_FTRMS AND mhi.AUFK_BUKRS = sr.CompanyCode
  AND po.Dim_DateidScheduledRelease <> sr.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledFinish = sf.dim_dateid
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sf.DateValue = AFPO_DGLTS AND mhi.AUFK_BUKRS = sf.CompanyCode
  AND po.Dim_DateidScheduledFinish <> sf.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledStart = ss.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ss
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ss.DateValue = AFKO_GSTRS AND mhi.AUFK_BUKRS = ss.CompanyCode
  AND po.Dim_DateidScheduledStart <> ss.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualStart = ast.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ast
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ast.DateValue = AFKO_GSTRI AND mhi.AUFK_BUKRS = ast.CompanyCode
  AND po.Dim_DateidActualStart <> ast.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidConfirmedOrderFinish = cof.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date cof
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND cof.DateValue = AFKO_GETRI AND mhi.AUFK_BUKRS = cof.CompanyCode
  AND po.Dim_DateidConfirmedOrderFinish <> cof.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualHeaderFinish = ahf.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ahf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ahf.DateValue = AFKO_GLTRI AND mhi.AUFK_BUKRS = ahf.CompanyCode
  AND po.Dim_DateidActualHeaderFinish <> ahf.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualRelease = ar.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ar
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ar.DateValue = AFKO_FTRMI AND mhi.AUFK_BUKRS = ar.CompanyCode
  AND po.Dim_DateidActualRelease <> ar.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = aif.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode
  AND po.Dim_DateidActualItemFinish <> aif.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = aif.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode
  AND po.Dim_DateidActualItemFinish <> aif.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidPlannedOrderDelivery = pod.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date pod
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pod.DateValue = AFPO_LTRMP AND mhi.AUFK_BUKRS = pod.CompanyCode
  AND po.Dim_DateidPlannedOrderDelivery <> pod.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidRoutingTransfer = rt.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date rt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND rt.DateValue = AFKO_PLAUF AND mhi.AUFK_BUKRS = rt.CompanyCode
  AND po.Dim_DateidRoutingTransfer <> rt.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidBasicFinish = bf.dim_dateid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bf.DateValue = AFPO_DGLTP AND mhi.AUFK_BUKRS = bf.CompanyCode
  AND po.Dim_DateidBasicFinish <> bf.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_ControllingAreaid = ca.dim_controllingareaid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_ControllingArea ca
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ca.ControllingAreaCode = AUFK_KOKRS
  AND po.Dim_ControllingAreaid <> ca.dim_controllingareaid;

Drop table if exists dim_profitcenter_tmp_43;


Create table dim_profitcenter_tmp_43 as
Select  * from dim_profitcenter  ORDER BY ValidTo ASC;



UPDATE  fact_productionorder po
   SET po.Dim_ProfitCenterId = IFNULL(pc.Dim_ProfitCenterid,1)
FROM fact_productionorder po 
		INNER JOIN AFKO_AFPO_AUFK mhi ON po.dd_ordernumber = mhi.AFKO_AUFNR
									 AND po.dd_orderitemno = mhi.AFPO_POSNR
		LEFT JOIN dim_profitcenter_tmp_43 pc ON pc.ProfitCenterCode = AUFK_PRCTR
											AND pc.ControllingArea = AUFK_KOKRS
											AND pc.ValidTo >= AFKO_GSTRP;

drop table if exists dim_profitcenter_tmp_43;


UPDATE fact_productionorder po
   SET po.Dim_tasklisttypeid = tlt.Dim_tasklisttypeid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_tasklisttype tlt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tlt.TaskListTypeCode = AFKO_PLNTY 
  AND po.Dim_tasklisttypeid <> tlt.Dim_tasklisttypeid;

UPDATE fact_productionorder po
   SET po.Dim_PartidHeader = phdr.Dim_Partid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part phdr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND phdr.PartNumber = AFKO_STLBEZ AND phdr.Plant = mhi.AUFK_WERKS
  AND po.Dim_PartidHeader <> phdr.Dim_Partid;

UPDATE fact_productionorder po
   SET po.Dim_bomstatusid = bs.Dim_bomstatusid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomstatus bs
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bs.BOMStatusCode = AFKO_STLST 
  AND po.Dim_bomstatusid <> bs.Dim_bomstatusid;

UPDATE fact_productionorder po
   SET po.Dim_bomusageid = bu.Dim_bomusageid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomusage bu
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bu.BOMUsageCode = AFKO_STLAN 
  aND po.Dim_bomusageid <> bu.Dim_bomusageid;

UPDATE fact_productionorder po
   SET po.dim_productionschedulerid = ps.dim_productionschedulerid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionscheduler ps
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ps.ProductionScheduler = AFKO_FEVOR
  AND ps.Plant = mhi.AUFK_WERKS
  AND po.dim_productionschedulerid <> ps.dim_productionschedulerid ;

UPDATE fact_productionorder po
   SET po.Dim_ProcurementID = dpr.Dim_ProcurementID
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_procurement dpr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dpr.procurement = AFPO_BESKZ 
  AND po.Dim_ProcurementID <> dpr.Dim_ProcurementID;


UPDATE fact_productionorder po
   SET po.Dim_SpecialProcurementID = spr.Dim_SpecialProcurementID
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialprocurement spr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spr.specialprocurement = AFPO_PSOBS 
  AND po.Dim_SpecialProcurementID <> spr.Dim_SpecialProcurementID;

UPDATE fact_productionorder po
   SET po.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_UnitOfMeasure uom
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND uom.UOM = afpo_meins 
  AND po.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;

UPDATE fact_productionorder po
   SET po.Dim_PartidItem = pitem.Dim_Partid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part pitem
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pitem.PartNumber = mhi.AFPO_MATNR
       AND pitem.Plant = mhi.AFPO_DWERK 
       AND po.Dim_PartidItem <> pitem.Dim_Partid;

UPDATE fact_productionorder po
   SET po.Dim_AccountCategoryid = ac.Dim_AccountCategoryid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_accountcategory ac
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ac.Category = AFPO_KNTTP 
  AND po.Dim_AccountCategoryid <> ac.Dim_AccountCategoryid;

UPDATE fact_productionorder po
   SET po.Dim_StockTypeid = st.Dim_StockTypeid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_stocktype st
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND st.TypeCode = AFPO_INSMK 
  AND po.Dim_StockTypeid <> st.Dim_StockTypeid;

UPDATE fact_productionorder po
   SET po.Dim_StorageLocationid = sl.Dim_StorageLocationid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi, dim_storagelocation sl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND sl.LocationCode = AFPO_LGORT
       AND sl.Plant = AFPO_DWERK
       AND po.Dim_StorageLocationid <> sl.Dim_StorageLocationid;

UPDATE fact_productionorder po
   SET po.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_consumptiontype ct
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ct.ConsumptionCode = AFPO_KZVBR 
  AND po.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;

UPDATE fact_productionorder po
   SET po.dim_specialstockid = spt.dim_specialstockid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialstock spt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spt.specialstockindicator = AFPO_SOBKZ
  AND po.dim_specialstockid <> spt.dim_specialstockid;

UPDATE fact_productionorder po
   SET po.Dim_Plantid = dp.Dim_Plantid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_plant dp
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dp.PlantCode = mhi.afpo_dwerk
  ANd  po.Dim_Plantid <> dp.Dim_Plantid;

UPDATE fact_productionorder po
   SET po.dim_salesorgid = so.dim_salesorgid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_salesorg so,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dp.PlantCode = mhi.afpo_dwerk
       AND so.SalesOrgCode = dp.SalesOrg
       AND  po.dim_salesorgid <> so.dim_salesorgid;

UPDATE fact_productionorder po
   SET po.Dim_PurchaseOrgid = porg.Dim_PurchaseOrgid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_purchaseorg porg,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND porg.PurchaseOrgCode = dp.PurchOrg
       AND dp.PlantCode = mhi.AUFK_WERKS
       AND po.Dim_PurchaseOrgid <> porg.Dim_PurchaseOrgid;

	   /*
UPDATE fact_productionorder po
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Currency cur
   SET po.Dim_Currencyid = cur.Dim_Currencyid
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND cur.CurrencyCode = mhi.AUFK_WAERS */
       
UPDATE fact_productionorder po
   SET po.Dim_Companyid = dc.Dim_Companyid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Company dc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dc.CompanyCode = mhi.AUFK_BUKRS
       AND po.Dim_Companyid <> dc.Dim_Companyid;
	   
/* Dim_Currencyid is now the local curr */	   
UPDATE fact_productionorder po
   SET po.Dim_Currencyid = cur.Dim_Currencyid
FROM fact_productionorder po,dim_Company dc,
       dim_Currency cur
 WHERE     po.Dim_Companyid = dc.Dim_Companyid
 AND cur.CurrencyCode = dc.currency;	   


UPDATE fact_productionorder po
SET  ct_ConfirmedScrapQty = ifnull(AFKO_IASMG ,0.000)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND ct_ConfirmedScrapQty <> ifnull(AFKO_IASMG ,0.000);


UPDATE fact_productionorder po
   SET dd_OrderDescription = ifnull(AUFK_KTEXT,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dd_OrderDescription <>  ifnull(AUFK_KTEXT,'Not Set');
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = mrp.dim_MrpControllerid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_MrpController mrp, dim_plant pl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dim_plantid = pl.dim_plantid
       AND mhi.AFKO_DISPO IS  NOT NULL
       AND mrp.MRPController = mhi.AFKO_DISPO AND mrp.plant = pl.plantcode
       AND mrp.RowIsCurrent = 1
	AND po.dim_MrpControllerid <>  mrp.dim_MrpControllerid;       
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND mhi.AFKO_DISPO IS  NULL
	AND  po.dim_MrpControllerid <> 1; 

UPDATE fact_productionorder po
   SET po.Dim_ordertypeid = pot.Dim_productionordertypeid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_productionordertype pot
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND pot.typecode = AFPO_DAUAT AND pot.RowIsCurrent = 1
       AND po.Dim_ordertypeid <> pot.Dim_productionordertypeid;

UPDATE fact_productionorder po
   SET po.dim_productionordermiscid  = misc.dim_productionordermiscid
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionordermisc misc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND     CollectiveOrder = ifnull(AFKO_PRODNET, 'Not Set')
                  AND DeliveryComplete = ifnull(AFPO_ELIKZ, 'Not Set')
                  AND GRChangeIndicator = ifnull(AFPO_WEAED, 'Not Set')
                  AND GRIndicator = ifnull(AFPO_WEPOS, 'Not Set')
                  AND GRNonValue = ifnull(AFPO_WEUNB, 'Not Set')
                  AND NoAutoCost = ifnull(AFKO_NAUCOST, 'Not Set')
                  AND NoAutoSchedule = ifnull(AFKO_NAUTERM, 'Not Set')
                  AND NoCapacityRequirement = ifnull(AFKO_KBED, 'Not Set')
                  AND NonMRPMaterial = ifnull(AFPO_NDISR, 'Not Set')
                  AND NonMRPOrderItem = ifnull(AFPO_DNREL, 'Not Set')
                  AND NoPlannedCostCalculation = ifnull(AFKO_NOPCOST, 'Not Set')
                  AND OrderReleased = ifnull(AUFK_PHAS1, 'Not Set')
                  AND ReleasedToMRP = ifnull(AFPO_DFREI, 'Not Set')
                  AND SchedulingBreak = ifnull(AFKO_BREAKS, 'Not Set')
                  AND UnlimitedOverdelivery = ifnull(AFPO_UEBTK, 'Not Set')
                  AND ValueWorkRelevant = ifnull(AFKO_FLG_ARBEI, 'Not Set')
                  AND po.dim_productionordermiscid  <> misc.dim_productionordermiscid;

UPDATE fact_productionorder po
SET dd_bomexplosionno = AFPO_SERNR
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_bomexplosionno <>  AFPO_SERNR;

     


 UPDATE fact_productionorder po
   SET  dd_plannedorderno = ifnull(AFPO_PLNUM,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_plannedorderno <> ifnull(AFPO_PLNUM,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderNo = ifnull(AFPO_KDAUF,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderNo <> ifnull(AFPO_KDAUF,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderItemNo = AFPO_KDPOS
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderItemNo <> AFPO_KDPOS;



       UPDATE fact_productionorder po
   SET dd_SalesOrderDeliveryScheduleNo = AFPO_KDEIN
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_SalesOrderDeliveryScheduleNo <> AFPO_KDEIN;



       UPDATE fact_productionorder po
   SET ct_ConfirmedReworkQty = AFKO_RMNGA
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ConfirmedReworkQty <> AFKO_RMNGA;



       UPDATE fact_productionorder po
   SET ct_ScrapQty = AFPO_PSAMG
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ScrapQty  <> AFPO_PSAMG;



       UPDATE fact_productionorder po
   SET ct_OrderItemQty = AFPO_PSMNG
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_OrderItemQty <> AFPO_PSMNG;



       UPDATE fact_productionorder po
   SET ct_TotalOrderQty = AFKO_GAMNG
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_TotalOrderQty  <> AFKO_GAMNG;



       UPDATE fact_productionorder po
   SET ct_GRQty = AFPO_WEMNG
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRQty  <> AFPO_WEMNG;



       UPDATE fact_productionorder po
   SET ct_GRProcessingTime = AFPO_WEBAZ
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRProcessingTime <> AFPO_WEBAZ;



       UPDATE fact_productionorder po
   SET amt_estimatedTotalCost = AUFK_USER4
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND amt_estimatedTotalCost <>  AUFK_USER4;


       UPDATE fact_productionorder po
   SET amt_ValueGR = mhi.AFPO_WEWRT
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  amt_ValueGR <> mhi.AFPO_WEWRT;



       UPDATE fact_productionorder po
   SET Dim_BuildTypeid = (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END)
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  Dim_BuildTypeid <> (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END);


       UPDATE fact_productionorder po
   SET dd_BOMLevel = AFKO_STUFE
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_BOMLevel <> AFKO_STUFE;


       UPDATE fact_productionorder po
   SET dd_sequenceno = ifnull(AFKO_CY_SEQNR,'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_sequenceno <> ifnull(AFKO_CY_SEQNR,'Not Set');



UPDATE fact_productionorder po
   SET dd_ObjectNumber = ifnull(AUFK_OBJNR, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_ObjectNumber <> ifnull(AUFK_OBJNR, 'Not Set');


Drop table if exists subqry_po_001;

Create table subqry_po_001 as 
Select distinct dd_ObjectNumber,
CONVERT(varchar(20),'Not Set') Closed,
CONVERT(varchar(20),'Not Set') Delivered,
CONVERT(varchar(20),'Not Set') GoodsMovementPosted,
CONVERT(varchar(20),'Not Set') MaterialCommitted,
CONVERT(varchar(20),'Not Set') PreCosted,
CONVERT(varchar(20),'Not Set') SettlementRuleCreated, 
CONVERT(varchar(20),'Not Set') VariancesCalculated,
CONVERT(varchar(20),'Not Set') TechnicallyCompleted,
CONVERT(varchar(20),'Not Set') Created_col,
CONVERT(varchar(20),'Not Set') Released,
CONVERT(varchar(20),'Not Set') MaterialShortage,
CONVERT(varchar(20),'Not Set') PartPrinted,
CONVERT(varchar(20),'Not Set') PartiallyConfirmed,
CONVERT(varchar(20),'Not Set') InspectionLotAssigned,
CONVERT(varchar(20),'Not Set') MaterialAvailabilityNotChecked,
CONVERT(varchar(20),'Not Set') ResultsAnalysisCarriedOut
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
Where  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR;


Update subqry_po_001 k
Set k.Closed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' AND j.JEST_INACT is NULL;



Update subqry_po_001 k
Set k.Delivered = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0012' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.GoodsMovementPosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0321' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialCommitted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0340' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PreCosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0016' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.SettlementRuleCreated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0028' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.VariancesCalculated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0056' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.TechnicallyCompleted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Created_col = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0001' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Released = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialShortage = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0004' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartPrinted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0008' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartiallyConfirmed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0010' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.InspectionLotAssigned = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0281' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialAvailabilityNotChecked = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0420' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.ResultsAnalysisCarriedOut = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0082' AND j.JEST_INACT is NULL;

UPDATE fact_productionorder po
SET po.dim_productionorderstatusid = post.dim_productionorderstatusid
FROM fact_productionorder po,dim_productionorderstatus post,
     AFKO_AFPO_AUFK mhi,
     subqry_po_001 sq
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND sq.dd_ObjectNumber = po.dd_ObjectNumber
       AND post.Closed = sq.Closed
       AND post.Delivered = sq.Delivered
       AND post.GoodsMovementPosted = sq.GoodsMovementPosted
       AND post.MaterialCommitted =sq.MaterialCommitted
       AND post.PreCosted = sq.PreCosted
       AND post.SettlementRuleCreated =sq.SettlementRuleCreated 
       AND post.VariancesCalculated =sq.VariancesCalculated
       AND post.TechnicallyCompleted = sq.TechnicallyCompleted
       AND post."CREATED" = sq.Created_col
       AND post.Released = sq.Released
       AND post.MaterialShortage = sq.MaterialShortage
       AND post.PartPrinted = sq.PartPrinted
       AND post.PartiallyConfirmed = sq.PartiallyConfirmed
       AND post.InspectionLotAssigned = sq.InspectionLotAssigned
       AND post.MaterialAvailabilityNotChecked = sq.MaterialAvailabilityNotChecked
       AND post.ResultsAnalysisCarriedOut = sq.ResultsAnalysisCarriedOut;


UPDATE fact_productionorder po
SET amt_ExchangeRate = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_productionorder po
SET amt_ExchangeRate = ex.exchangeRate
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = AUFK_AEDAT
and fact_script_name = 'bi_populate_prodorder_fact' 
AND amt_ExchangeRate <> ex.exchangeRate ;


UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = 1
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = ex.exchangeRate 
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_43
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  current_date  /*AUFK_AEDAT - current date to be used for global rate*/
and fact_script_name = 'bi_populate_prodorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;

DROP TABLE IF EXISTS TMP_AUFM_UPDT;
CREATE TABLE TMP_AUFM_UPDT
AS
select AUFM_AUFNR, sum((case a.AUFM_SHKZG when 'S' then -1 else 1 end) * a.AUFM_DMBTR) sum_aufm
from AUFM a group by a.AUFM_AUFNR;

DROP TABLE IF EXISTS TMP_COSS_UPDT;
CREATE TABLE TMP_COSS_UPDT
AS
select COSS_OBJNR,sum(c.COSS_WOG001 + c.COSS_WOG002 + c.COSS_WOG003 + c.COSS_WOG004
                                    + c.COSS_WOG005 + c.COSS_WOG006 + c.COSS_WOG007 + c.COSS_WOG008
                                    + c.COSS_WOG009 + c.COSS_WOG010 + c.COSS_WOG011 + c.COSS_WOG012
                                    + c.COSS_WOG013 + c.COSS_WOG014 + c.COSS_WOG015 + c.COSS_WOG016) sum_cos
from coss c
where c.COSS_WRTTP = '04'
group by COSS_OBJNR;

UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(a.sum_aufm,0)
FROM fact_productionorder po
	LEFT JOIN TMP_AUFM_UPDT a ON po.dd_ordernumber = a.AUFM_AUFNR 
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(a.sum_aufm,0);

UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0)
FROM fact_productionorder po
	LEFT JOIN TMP_COSS_UPDT c ON 'OR' || po.dd_ordernumber = c.COSS_OBJNR
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0);

DROP TABLE IF EXISTS TMP_AUFM_UPDT;
DROP TABLE IF EXISTS TMP_COSS_UPDT;
						   
/* LK: 15 Sep 2013: aufm_dmbtr and coss amts are in local currency. So convert amts to transaction currency */
						   
UPDATE fact_productionorder po
SET amt_WIPBalance = ( amt_WIPBalance / amt_ExchangeRate )
WHERE amt_ExchangeRate <> 1;						   

DROP TABLE IF EXISTS FPO_TMP;
CREATE TABLE FPO_TMP
AS SELECT a.AUFM_AUFNR,sum(a.AUFM_MENGE) sm_aufmenge
from AUFM a
GROUP BY a.AUFM_AUFNR;

UPDATE fact_productionorder po
SET ct_WIPQty = ifnull(sm_aufmenge,0)
from fact_productionorder po, FPO_TMP a 
where a.AUFM_AUFNR = po.dd_ordernumber;
						 
UPDATE fact_productionorder po
SET dd_OperationNumber = ifnull(AFKO_VORUE, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_OperationNumber <>  ifnull(AFKO_VORUE, 'Not Set');

UPDATE fact_productionorder po
SET dd_WorkCenter = ifnull(AUFK_VAPLZ, 'Not Set')
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_WorkCenter <>  ifnull(AUFK_VAPLZ, 'Not Set');

DROP TABLE IF EXISTS tmp_salesorder;

CREATE TABLE tmp_salesorder AS
SELECT distinct dd_salesdocno,dd_Salesitemno,dim_customerid from fact_salesorder;

UPDATE fact_productionorder po
 SET po.Dim_customerid = fso.Dim_customerid
FROM  fact_productionorder po,tmp_salesorder fso
WHERE     po.dd_salesorderno = fso.dd_salesdocno
     AND po.dd_salesorderitemno = fso.dd_salesitemno
     AND ifnull(po.Dim_customerid, -1) <> fso.Dim_customerid;
     
DROP TABLE IF EXISTS tmp_salesorder;
     
UPDATE fact_productionorder po
SET dd_OperationNumber = 'Not Set' WHERE dd_OperationNumber IS NULL;

UPDATE fact_productionorder po
SET dd_WorkCenter = 'Not Set' WHERE dd_WorkCenter IS NULL;

DROP TABLE IF EXISTS FPO_TMP2;
CREATE TABLE FPO_TMP2
AS SELECT r.RESB_AUFNR,sum(r.RESB_ERFMG) sm_resberfmg
from RESB r
GROUP BY r.RESB_AUFNR;

UPDATE fact_productionorder po
SET ct_qtyuoe = ifnull(sm_resberfmg,0)
FROM fact_productionorder po,FPO_TMP2 r
WHERE r.RESB_AUFNR = po.dd_ordernumber;

/* MDG Part */

DROP TABLE IF EXISTS tmp_dim_mdg_partid;
CREATE TABLE tmp_dim_mdg_partid as
SELECT DISTINCT pr.dim_partid, md.dim_mdg_partid, md.partnumber
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18);

UPDATE fact_productionorder f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    dw_update_date = current_timestamp
FROM tmp_dim_mdg_partid tmp, fact_productionorder f
WHERE f.dim_partidheader = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);
	  
DROP TABLE IF EXISTS tmp_dim_mdg_partid;


UPDATE fact_productionorder f
SET f.amt_ExchangeRate_GBL = 1
WHERE ifnull(f.amt_ExchangeRate_GBL,-1) <> 1;


UPDATE fact_productionorder f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_productionorder f, dim_currency dc, csv_oprate r
WHERE f.dim_currencyid = dc.dim_currencyid
	AND dc.CurrencyCode = r.fromc
	AND r.toc = (SELECT ifnull((SELECT property_value FROM systemproperty WHERE property = 'customer.global.currency'), 'USD'))
	AND ifnull(f.amt_ExchangeRate_GBL,-1) <> ifnull(r.exrate,1);