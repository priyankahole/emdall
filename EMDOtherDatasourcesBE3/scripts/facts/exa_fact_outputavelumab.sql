/*****************************************************************************************************************/
/*   Script         : exa_fact_outputavelumab                                                                    */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 06 Mar 2019                                                                                */
/*   Description    : Populating script of fact_outputavelumab                                                   */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   06 Mar 2019         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_outputavelumab;
CREATE TABLE tmp_fact_outputavelumab
AS
SELECT *
FROM fact_outputavelumab
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_outputavelumab;
CREATE TABLE number_fountain_fact_outputavelumab LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_outputavelumab
SELECT 'fact_outputavelumab', ifnull(max(fact_outputavelumabid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_outputavelumab;


INSERT INTO tmp_fact_outputavelumab(
fact_outputavelumabid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
ct_bulk_ds_2ab_uplc_a_fucosylated,
ct_bulk_ds_2ab_uplc_g0,
ct_bulk_ds_2ab_uplc_g1,
ct_bulk_ds_2ab_uplc_g2,
ct_bulk_ds_2ab_uplc_high_mannose,
ct_bulk_ds_2ab_uplc_total_sialylated,
ct_bulk_ds_clarity,
ct_bulk_ds_deamidation,
ct_bulk_ds_electropphoretic_purity,
ct_bulk_ds_hcp,
ct_bulk_ds_hmw,
ct_bulk_ds_ice_acidic_group,
ct_bulk_ds_ice_basic_group,
ct_bulk_ds_ice_cluster_4,
ct_bulk_ds_ice_cluster_5,
ct_bulk_ds_ice_cluster_6,
ct_bulk_ds_ice_main_cluster,
ct_bulk_ds_lmw,
ct_bulk_ds_oxidation,
ct_bulk_ds_ph,
ct_bulk_ds_protein_titer
)
SELECT (SELECT max_id from number_fountain_fact_outputavelumab WHERE table_name = 'fact_outputavelumab') + ROW_NUMBER() over(order by '') AS fact_outputavelumabid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(ps_name, 'Not Set') as dd_ps_name,
       ifnull(ps_date, '0001-01-01') as dd_ps_date,
       1 as dim_dateid,
       ifnull(bulk_ds_2ab_uplc_a_fucosylated,0) as ct_bulk_ds_2ab_uplc_a_fucosylated,
       ifnull(bulk_ds_2ab_uplc_g0,0) as ct_bulk_ds_2ab_uplc_g0,
       ifnull(bulk_ds_2ab_uplc_g1,0) as ct_bulk_ds_2ab_uplc_g1,
       ifnull(bulk_ds_2ab_uplc_g2,0) as ct_bulk_ds_2ab_uplc_g2,
       ifnull(bulk_ds_2ab_uplc_high_mannose,0) as ct_bulk_ds_2ab_uplc_high_mannose,
       ifnull(bulk_ds_2ab_uplc_total_sialylated,0) as ct_bulk_ds_2ab_uplc_total_sialylated,
       ifnull(bulk_ds_clarity,0) as ct_bulk_ds_clarity,
       ifnull(bulk_ds_deamidation,0) as ct_bulk_ds_deamidation,
       ifnull(bulk_ds_electropphoretic_purity,0) as ct_bulk_ds_electropphoretic_purity,
       ifnull(bulk_ds_hcp,0) as ct_bulk_ds_hcp,
       ifnull(bulk_ds_hmw,0) as ct_bulk_ds_hmw,
       ifnull(bulk_ds_ice_acidic_group,0) as ct_bulk_ds_ice_acidic_group,
       ifnull(bulk_ds_ice_basic_group,0) as ct_bulk_ds_ice_basic_group,
       ifnull(bulk_ds_ice_cluster_4,0) as ct_bulk_ds_ice_cluster_4,
       ifnull(bulk_ds_ice_cluster_5,0) as ct_bulk_ds_ice_cluster_5,
       ifnull(bulk_ds_ice_cluster_6,0) as ct_bulk_ds_ice_cluster_6,
       ifnull(bulk_ds_ice_main_cluster,0) as ct_bulk_ds_ice_main_cluster,
       ifnull(bulk_ds_lmw,0) as ct_bulk_ds_lmw,
       ifnull(bulk_ds_oxidation,0) as ct_bulk_ds_oxidation,
       ifnull(bulk_ds_ph,0) as ct_bulk_ds_ph,
       ifnull(bulk_ds_protein_titer,0) as ct_bulk_ds_protein_titer
FROM new_output_avelumab;

/* Calculating the AVG of previous years */
DELETE FROM number_fountain_fact_outputavelumab WHERE table_name = 'fact_outputavelumab';

INSERT INTO number_fountain_fact_outputavelumab
SELECT 'fact_outputavelumab', ifnull(max(fact_outputavelumabid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_outputavelumab;

INSERT INTO tmp_fact_outputavelumab(
fact_outputavelumabid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
ct_bulk_ds_2ab_uplc_a_fucosylated,
ct_bulk_ds_2ab_uplc_g0,
ct_bulk_ds_2ab_uplc_g1,
ct_bulk_ds_2ab_uplc_g2,
ct_bulk_ds_2ab_uplc_high_mannose,
ct_bulk_ds_2ab_uplc_total_sialylated,
ct_bulk_ds_clarity,
ct_bulk_ds_deamidation,
ct_bulk_ds_electropphoretic_purity,
ct_bulk_ds_hcp,
ct_bulk_ds_hmw,
ct_bulk_ds_ice_acidic_group,
ct_bulk_ds_ice_basic_group,
ct_bulk_ds_ice_cluster_4,
ct_bulk_ds_ice_cluster_5,
ct_bulk_ds_ice_cluster_6,
ct_bulk_ds_ice_main_cluster,
ct_bulk_ds_lmw,
ct_bulk_ds_oxidation,
ct_bulk_ds_ph,
ct_bulk_ds_protein_titer
)
SELECT (SELECT max_id from number_fountain_fact_outputavelumab WHERE table_name = 'fact_outputavelumab') + ROW_NUMBER() over(order by '') AS fact_outputavelumabid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Average ' || substr(ps_date, 0, 4) as dd_ps_name,
       current_date as dd_ps_date,
       1 as dim_dateid,
       ifnull(AVG(bulk_ds_2ab_uplc_a_fucosylated), 0) as ct_bulk_ds_2ab_uplc_a_fucosylated,
       ifnull(AVG(bulk_ds_2ab_uplc_g0), 0) as ct_bulk_ds_2ab_uplc_g0,
       ifnull(AVG(bulk_ds_2ab_uplc_g1), 0) as ct_bulk_ds_2ab_uplc_g1,
       ifnull(AVG(bulk_ds_2ab_uplc_g2), 0) as ct_bulk_ds_2ab_uplc_g2,
       ifnull(AVG(bulk_ds_2ab_uplc_high_mannose), 0) as ct_bulk_ds_2ab_uplc_high_mannose,
       ifnull(AVG(bulk_ds_2ab_uplc_total_sialylated), 0) as ct_bulk_ds_2ab_uplc_total_sialylated,
       ifnull(AVG(bulk_ds_clarity), 0) as ct_bulk_ds_clarity,
       ifnull(AVG(bulk_ds_deamidation), 0) as ct_bulk_ds_deamidation,
       ifnull(AVG(bulk_ds_electropphoretic_purity), 0) as ct_bulk_ds_electropphoretic_purity,
       ifnull(AVG(bulk_ds_hcp), 0) as ct_bulk_ds_hcp,
       ifnull(AVG(bulk_ds_hmw), 0) as ct_bulk_ds_hmw,
       ifnull(AVG(bulk_ds_ice_acidic_group), 0) as ct_bulk_ds_ice_acidic_group,
       ifnull(AVG(bulk_ds_ice_basic_group), 0) as ct_bulk_ds_ice_basic_group,
       ifnull(AVG(bulk_ds_ice_cluster_4), 0) as ct_bulk_ds_ice_cluster_4,
       ifnull(AVG(bulk_ds_ice_cluster_5), 0) as ct_bulk_ds_ice_cluster_5,
       ifnull(AVG(bulk_ds_ice_cluster_6), 0) as ct_bulk_ds_ice_cluster_6,
       ifnull(AVG(bulk_ds_ice_main_cluster), 0) as ct_bulk_ds_ice_main_cluster,
       ifnull(AVG(bulk_ds_lmw), 0) as ct_bulk_ds_lmw,
       ifnull(AVG(bulk_ds_oxidation), 0) as ct_bulk_ds_oxidation,
       ifnull(AVG(bulk_ds_ph), 0) as ct_bulk_ds_ph,
       ifnull(AVG(bulk_ds_protein_titer), 0) as ct_bulk_ds_protein_titer
FROM new_output_avelumab
GROUP BY substr(ps_date, 0, 4);

UPDATE tmp_fact_outputavelumab tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_outputavelumab tmp;

UPDATE tmp_fact_outputavelumab tmp
SET tmp.dim_dateid = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_outputavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_ps_date;

TRUNCATE TABLE fact_outputavelumab;
INSERT INTO fact_outputavelumab(
fact_outputavelumabid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
ct_bulk_ds_2ab_uplc_a_fucosylated,
ct_bulk_ds_2ab_uplc_g0,
ct_bulk_ds_2ab_uplc_g1,
ct_bulk_ds_2ab_uplc_g2,
ct_bulk_ds_2ab_uplc_high_mannose,
ct_bulk_ds_2ab_uplc_total_sialylated,
ct_bulk_ds_clarity,
ct_bulk_ds_deamidation,
ct_bulk_ds_electropphoretic_purity,
ct_bulk_ds_hcp,
ct_bulk_ds_hmw,
ct_bulk_ds_ice_acidic_group,
ct_bulk_ds_ice_basic_group,
ct_bulk_ds_ice_cluster_4,
ct_bulk_ds_ice_cluster_5,
ct_bulk_ds_ice_cluster_6,
ct_bulk_ds_ice_main_cluster,
ct_bulk_ds_lmw,
ct_bulk_ds_oxidation,
ct_bulk_ds_ph,
ct_bulk_ds_protein_titer
)
SELECT fact_outputavelumabid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_ps_name,
       dd_ps_date,
       dim_dateid,
       ct_bulk_ds_2ab_uplc_a_fucosylated,
       ct_bulk_ds_2ab_uplc_g0,
       ct_bulk_ds_2ab_uplc_g1,
       ct_bulk_ds_2ab_uplc_g2,
       ct_bulk_ds_2ab_uplc_high_mannose,
       ct_bulk_ds_2ab_uplc_total_sialylated,
       ct_bulk_ds_clarity,
       ct_bulk_ds_deamidation,
       ct_bulk_ds_electropphoretic_purity,
       ct_bulk_ds_hcp,
       ct_bulk_ds_hmw,
       ct_bulk_ds_ice_acidic_group,
       ct_bulk_ds_ice_basic_group,
       ct_bulk_ds_ice_cluster_4,
       ct_bulk_ds_ice_cluster_5,
       ct_bulk_ds_ice_cluster_6,
       ct_bulk_ds_ice_main_cluster,
       ct_bulk_ds_lmw,
       ct_bulk_ds_oxidation,
       ct_bulk_ds_ph,
       ct_bulk_ds_protein_titer
FROM tmp_fact_outputavelumab;

DROP TABLE IF EXISTS tmp_fact_outputavelumab;

DELETE FROM emd586.fact_outputavelumab;
INSERT INTO emd586.fact_outputavelumab SELECT * FROM fact_outputavelumab;