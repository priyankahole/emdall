/*****************************************************************************************************************/
/*   Script         : bi_populate_trackwisebatch_fact                                                            */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 09 Oct 2017                                                                                */
/*   Description    : Populating script of fact_trackwisebatch                                                   */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   09 Oct 2017         CristianT      1.0          Creating the script.                                        */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS number_fountain_trackwisebatch;
CREATE TABLE number_fountain_trackwisebatch LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_trackwisebatch;
CREATE TABLE tmp_fact_trackwisebatch LIKE fact_trackwisebatch INCLUDING DEFAULTS INCLUDING IDENTITY;


INSERT INTO number_fountain_trackwisebatch
SELECT 'fact_trackwisebatch', ifnull(max(fact_trackwisebatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_trackwisebatch;


INSERT INTO tmp_fact_trackwisebatch(
fact_trackwisebatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pr_id,
dd_seq_no,
dd_product,
dd_item_code_no,
dd_batch_no,
dd_location_of_batches,
dd_region
)
SELECT (SELECT max_id from number_fountain_trackwisebatch WHERE table_name = 'fact_trackwisebatch') + ROW_NUMBER() over(order by '') AS fact_trackwisebatchid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp1.batch1_pr_id, 0) as dd_pr_id,
       ifnull(grp1.batch1_seq_no, 0) as dd_seq_no,
       ifnull(grp1.batch1_product,'Not Set') as dd_product,
       ifnull(grp1.batch1_item_code_no,'Not Set') as dd_item_code_no,
       ifnull(grp1.batch1_batch_no,'Not Set') as dd_batch_no,
       ifnull(grp1.batch1_location_of_batches,'Not Set') as dd_location_of_batches,
       'Europe' as dd_region
FROM mv_xtr_grp1_rpt_batch grp1
UNION ALL
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp2.batch2_pr_id, 0) as dd_pr_id,
       ifnull(grp2.batch2_seq_no, 0) as dd_seq_no,
       ifnull(grp2.batch2_product,'Not Set') as dd_product,
       ifnull(grp2.batch2_item_code_no,'Not Set') as dd_item_code_no,
       ifnull(grp2.batch2_batch_no,'Not Set') as dd_batch_no,
       ifnull(grp2.batch2_location_of_batches,'Not Set') as dd_location_of_batches,
       'China' as dd_region
FROM mv_xtr_grp2_rpt_batch grp2
UNION ALL
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp3.batch3_pr_id, 0) as dd_pr_id,
       ifnull(grp3.batch3_seq_no, 0) as dd_seq_no,
       ifnull(grp3.batch3_product,'Not Set') as dd_product,
       ifnull(grp3.batch3_item_code_no,'Not Set') as dd_item_code_no,
       ifnull(grp3.batch3_batch_no,'Not Set') as dd_batch_no,
       ifnull(grp3.batch3_location_of_batches,'Not Set') as dd_location_of_batches,
       'Brazil & Uruguay' as dd_region
FROM mv_xtr_grp3_rpt_batch grp3
UNION ALL
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp4.batch4_pr_id, 0) as dd_pr_id,
       ifnull(grp4.batch4_seq_no, 0) as dd_seq_no,
       ifnull(grp4.batch4_product,'Not Set') as dd_product,
       ifnull(grp4.batch4_item_code_no,'Not Set') as dd_item_code_no,
       ifnull(grp4.batch4_batch_no,'Not Set') as dd_batch_no,
       ifnull(grp4.batch4_location_of_batches,'Not Set') as dd_location_of_batches,
       'Mexico' as dd_region
FROM mv_xtr_grp4_rpt_batch grp4
     ) t;

UPDATE tmp_fact_trackwisebatch tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_trackwisebatch tmp;


TRUNCATE TABLE fact_trackwisebatch;

INSERT INTO fact_trackwisebatch(
fact_trackwisebatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pr_id,
dd_seq_no,
dd_product,
dd_item_code_no,
dd_batch_no,
dd_location_of_batches,
dd_region
)
SELECT fact_trackwisebatchid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_pr_id,
       dd_seq_no,
       dd_product,
       dd_item_code_no,
       dd_batch_no,
       dd_location_of_batches,
       dd_region
FROM tmp_fact_trackwisebatch;

DROP TABLE IF EXISTS tmp_fact_trackwisebatch;

TRUNCATE TABLE emd586.fact_trackwisebatch;

INSERT INTO emd586.fact_trackwisebatch
SELECT *
FROM fact_trackwisebatch;