/*****************************************************************************************************************/
/*   Script         : exa_fact_mangooverallstatus                                                                */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 14 Aug 2018                                                                                */
/*   Description    : Populating script of fact_mangooverallstatus                                               */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   14 Aug 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_mangooverallstatus;
CREATE TABLE tmp_fact_mangooverallstatus
AS
SELECT *
FROM fact_mangooverallstatus
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_mangooverallstatus;
CREATE TABLE number_fountain_fact_mangooverallstatus LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_mangooverallstatus
SELECT 'fact_mangooverallstatus', ifnull(max(fact_mangooverallstatusid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_mangooverallstatus;


INSERT INTO tmp_fact_mangooverallstatus(
fact_mangooverallstatusid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_object_name,
dd_original_status,
dd_new_status,
dd_status_change_description,
dd_original_version,
dd_new_version,
dd_server_date_and_time,
dim_dateidserver,
dd_user_login_name,
dd_user_address,
dd_user_name,
dim_qualityusersiduser,
dd_event_name,
dd_author_site,
dd_author_user_login_name,
dim_qualityusersidauthor,
dd_title,
dd_document_type,
dd_document_subtype,
dd_document_unit,
dd_english_title,
dd_rn,
dd_author,
dd_r_version_label,
dd_planned_effective_date,
dim_dateidplannedeffective
)
SELECT (SELECT max_id from number_fountain_fact_mangooverallstatus WHERE table_name = 'fact_mangooverallstatus') + ROW_NUMBER() over(order by '') AS fact_mangooverallstatusid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(osd.object_name, 'Not Set') as dd_object_name,
       ifnull(osd.original_status, 'Not Set') as dd_original_status,
       ifnull(osd.new_status, 'Not Set') as dd_new_status,
       ifnull(osd.status_change_description, 'Not Set') as dd_status_change_description,
       ifnull(osd.original_version, 'Not Set') as dd_original_version,
       ifnull(osd.new_version, 'Not Set') as dd_new_version,
       ifnull(osd.server_date_and_time, '0001-01-01 00:00:00') as dd_server_date_and_time,
       1 as dim_dateidserver,
       ifnull(osd.user_login_name, 'Not Set') as dd_user_login_name,
       ifnull(osd.user_address, 'Not Set') as dd_user_address,
       ifnull(osd.user_name, 'Not Set') as dd_user_name,
       1 as dim_qualityusersiduser,
       ifnull(osd.event_name, 'Not Set') as dd_event_name,
       ifnull(osd.author_site, 'Not Set') as dd_author_site,
       ifnull(osd.user_login_name_mainview, 'Not Set') as dd_author_user_login_name,
       1 as dim_qualityusersidauthor,
       ifnull(osd.title, 'Not Set') as dd_title,
       ifnull(osd.document_type, 'Not Set') as dd_document_type,
       ifnull(osd.document_subtype, 'Not Set') as dd_document_subtype,
       ifnull(osd.document_unit, 'Not Set') as dd_document_unit,
       ifnull(osd.english_title, 'Not Set') as dd_english_title,
       ifnull(osd.rn, 'Not Set') as dd_rn,
       ifnull(replace(osd.authors, ' (legacy)', ''), 'Not Set') as dd_author,
       ifnull(osd.r_version_label, 'Not Set') as dd_r_version_label,
       ifnull(osd.planned_effective_date, '0001-01-01 00:00:00') as dd_planned_effective_date,
       1 as dim_dateidplannedeffective
FROM overall_status_per_document osd;

UPDATE tmp_fact_mangooverallstatus f
SET dim_dateidserver = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangooverallstatus f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_server_date_and_time)
      AND f.dim_dateidserver <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangooverallstatus f
SET dim_dateidplannedeffective = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangooverallstatus f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_planned_effective_date)
      AND f.dim_dateidplannedeffective <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangooverallstatus f
SET dim_qualityusersiduser = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangooverallstatus f,
     dim_qualityusers d
WHERE f.dd_user_login_name = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersiduser  <> d.dim_qualityusersid;

UPDATE tmp_fact_mangooverallstatus f
SET dim_qualityusersiduser = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangooverallstatus f,
     dim_qualityusers d
WHERE upper(substr(f.dd_user_address, 0, instr(f.dd_user_address, '@', 1)-1)) = upper(substr(d.mail, 0, instr(d.mail, '@', 1)-1))
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersiduser = 1
      AND f.dim_qualityusersiduser <> d.dim_qualityusersid;

UPDATE tmp_fact_mangooverallstatus f
SET dim_qualityusersidauthor = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangooverallstatus f,
     dim_qualityusers d
WHERE upper(f.dd_author_user_login_name) = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersidauthor  <> d.dim_qualityusersid;

UPDATE tmp_fact_mangooverallstatus f
SET dd_author_user_login_name = ifnull(m_id.user_id, 'Not Set')
FROM mango_missing_author_ids m_id,
     tmp_fact_mangooverallstatus f
WHERE lower(m_id.AUTHOR) = lower(f.dd_author)
      AND f.dim_qualityusersiduser = 1;

UPDATE tmp_fact_mangooverallstatus f
SET dim_qualityusersidauthor = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangooverallstatus f,
     dim_qualityusers d
WHERE upper(f.dd_author_user_login_name) = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersidauthor <> d.dim_qualityusersid;

DELETE FROM fact_mangooverallstatus;

INSERT INTO fact_mangooverallstatus(
fact_mangooverallstatusid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_object_name,
dd_original_status,
dd_new_status,
dd_status_change_description,
dd_original_version,
dd_new_version,
dd_server_date_and_time,
dim_dateidserver,
dd_user_login_name,
dd_user_address,
dd_user_name,
dim_qualityusersiduser,
dd_event_name,
dd_author_site,
dd_author_user_login_name,
dim_qualityusersidauthor,
dd_title,
dd_document_type,
dd_document_subtype,
dd_document_unit,
dd_english_title,
dd_rn,
dd_author,
dd_r_version_label,
dd_planned_effective_date,
dim_dateidplannedeffective
)
SELECT fact_mangooverallstatusid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_object_name,
       dd_original_status,
       dd_new_status,
       dd_status_change_description,
       dd_original_version,
       dd_new_version,
       dd_server_date_and_time,
       dim_dateidserver,
       dd_user_login_name,
       dd_user_address,
       dd_user_name,
       dim_qualityusersiduser,
       dd_event_name,
       dd_author_site,
       dd_author_user_login_name,
       dim_qualityusersidauthor,
       dd_title,
       dd_document_type,
       dd_document_subtype,
       dd_document_unit,
       dd_english_title,
       dd_rn,
       dd_author,
       dd_r_version_label,
       dd_planned_effective_date,
       dim_dateidplannedeffective
FROM tmp_fact_mangooverallstatus;

DROP TABLE IF EXISTS tmp_fact_mangooverallstatus;
DROP TABLE IF EXISTS number_fountain_fact_mangooverallstatus;
