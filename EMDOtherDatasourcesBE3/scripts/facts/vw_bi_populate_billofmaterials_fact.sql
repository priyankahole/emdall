/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 1 Jun 2013 */
/*   Description    : Stored Proc bi_populate_billofmaterials_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   17 Sep 2013     Lokesh    1.1 				 Currency and Exchange Rate changes								 */
/*   1 Jun 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_pGlobalCurrency_bom;
CREATE TABLE tmp_pGlobalCurrency_bom ( pGlobalCurrency CHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_bom VALUES ( 'USD' );

UPDATE tmp_pGlobalCurrency_bom t
SET t.pGlobalCurrency = ifNULL(s.property_value,'USD')
FROM tmp_pGlobalCurrency_bom t
CROSS JOIN (SELECT property_value
           FROM systemproperty 
   WHERE  property = 'customer.global.currency' ) s
WHERE t.pGlobalCurrency <> ifNULL(s.property_value,'USD') ;


/* 	 Append pGlobalCurrency to tmp_getExchangeRate1 */	
  DROP TABLE IF EXISTS tmp_getExchangeRate1_with_globalvar;
  CREATE TABLE tmp_getExchangeRate1_with_globalvar AS
  SELECT er.*, gbl.pGlobalCurrency
  FROM 
  tmp_getExchangeRate1 er
  INNER JOIN tmp_pGlobalCurrency_bom gbl ON er.pToCurrency = gbl.pGlobalCurrency
  ;

/* MODIFY fact_bom_tmp_populate TO TRUNCATED */


DROP TABLE IF EXISTS fact_bom_tmp_populate;
CREATE TABLE fact_bom_tmp_populate
like  fact_bom  INCLUDING DEFAULTS INCLUDING IDENTITY;

ALTER TABLE fact_bom_tmp_populate
add  PRIMARY KEY (fact_bomid);

/* Lokesh : Not converting this update. The table is just truncated before this update. Confirm this with Rahul/Shanthi. 
	Either the truncate or the update should be removed  */
/*	
UPDATE fact_bom_tmp_populate bom, STKO hdr, STPO item, dim_bomstatus bs,dim_bomcategory bc
   SET Dim_CreatedOnDateid =
          ifnull(
             (SELECT dr.dim_dateid
                FROM dim_date dr
               WHERE dr.DateValue = STPO_ANDAT AND 'Not Set' = dr.CompanyCode),
             1),
       Dim_ChangedOnDateid =
          ifnull(
             (SELECT be.dim_dateid
                FROM dim_date be
               WHERE be.DateValue = STPO_AEDAT AND 'Not Set' = be.CompanyCode),
             1),
       Dim_ValidFromDateid =
          ifnull(
             (SELECT ls.dim_dateid
                FROM dim_date ls
               WHERE ls.DateValue = STPO_DATUV AND 'Not Set' = ls.CompanyCode),
             1),
       Dim_BOMComponentId =
          ifnull(
             (SELECT pitem.Dim_Partid
                FROM dim_part pitem
               WHERE     pitem.PartNumber = STPO_IDNRK
                     AND pitem.Plant = ifnull(STPO_PSWRK,STKO_WRKAN)
                     AND pitem.RowIsCurrent = 1),
             1),
       bom.Dim_bomstatusid = bs.Dim_bomstatusid,
       Dim_BaseUOMId =
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1),
                 1),
       Dim_ComponentUOMId =
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = STPO_MEINS AND uom.RowIsCurrent = 1),
                 1),
       Dim_StorageLocationid =
          ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = STPO_LGORT

                     AND sl.RowIsCurrent = 1 LIMIT 1),
             1),
       bom.Dim_IssuingPlantId =
          ifnull((SELECT dp.Dim_PlantId
                    FROM dim_plant dp
                   WHERE dp.PlantCode = STPO_PSWRK AND dp.RowIsCurrent = 1),
                 1),
       bom.Dim_PurchasingOrgId =
          ifnull(
             (SELECT porg.Dim_PurchaseOrgId
                FROM Dim_PurchaseOrg porg
               WHERE     porg.PurchaseOrgCode = STPO_EKORG

                     AND porg.RowIsCurrent = 1 LIMIT 1),
             1),
       bom.Dim_Currencyid =
          ifnull((SELECT cur.Dim_Currencyid
                    FROM Dim_Currency cur
                   WHERE cur.CurrencyCode = STPO_WAERS),
                 1),
       Dim_PurchasingGroupId =
          ifnull((SELECT pg.Dim_PurchaseGroupId
                    FROM Dim_PurchaseGroup pg
                   WHERE pg.PurchaseGroup = STPO_EKGRP AND pg.RowIsCurrent = 1),
                  1),
       bom.dd_BomItemNo = ifnull(item.stpo_posnr,'Not Set'),
       dd_BOMPredecessorNodeNo = ifnull(STPO_VGKNT, 0),
       dd_CreatedBy = ifnull(STPO_ANNAM, 'Not Set'),
       dd_ChangedBy = ifnull(STPO_AENAM, 'Not Set'),
       ct_ComponentScrapinPercent = STPO_AUSCH,
       ct_AvgMatPurityinPercent = STPO_CSSTR,
       ct_LeadTimeOffset = STPO_NLFZT,
       ct_BaseQty = STKO_BMENG,
       ct_ComponentQty = STPO_MENGE,
       amt_Price = STPO_PREIS,
       amt_PriceUnit = ifnull(STPO_PEINH,1),
       dd_PartNumber = 'Not Set',
       dd_ComponentNumber = ifnull(STPO_IDNRK, 'Not Set'),
       bom.Dim_BomItemCategoryId = ifnull(( SELECT bic.Dim_BomItemCategoryId FROM dim_bomitemcategory bic
                                           WHERE bic.ItemCategory = STPO_POSTP
                                           AND bic.RowIsCurrent = 1), 1),
       bom.Dim_VendorId = ifnull(( SELECT dv.Dim_VendorId FROM dim_Vendor dv
                                           WHERE dv.VendorNumber = STPO_LIFNR
                                           AND dv.RowIsCurrent = 1), 1),
       bom.dd_BomItemCounter = STPO_STPOZ
 WHERE     bom.dd_BomNumber = hdr.STKO_STLNR
       AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
       AND bc.Category = STPO_STLTY
       AND bc.RowIsCurrent = 1
       AND bom.dd_Alternative = hdr.STKO_STLAL
       AND hdr.STKO_STLNR = item.STPO_STLNR
       AND hdr.STKO_STLTY = item.STPO_STLTY
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1*/

/* Insert 1 */



delete from NUMBER_FOUNTAIN where table_name = 'fact_bom_tmp_populate';
INSERT INTO NUMBER_FOUNTAIN
select 'fact_bom_tmp_populate',ifnull(max(fact_bomid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_bom_tmp_populate;

/* Changes made to the temporary table based on the fields needed in the update statements for new rows. */
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_BMEIN VARCHAR(20);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_STLNR VARCHAR(10);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_STLTY VARCHAR(10);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_WRKAN VARCHAR(4);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_MEINS VARCHAR(3);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_PSWRK VARCHAR(4);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_AEDAT DATE;
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_ANDAT DATE;
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_WAERS VARCHAR(5);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_EKORG VARCHAR(4);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_LGORT VARCHAR(4);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_EKGRP VARCHAR(3);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_MATKL VARCHAR(9);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_DATUV DATE;
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_POSTP VARCHAR(1);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STPO_LIFNR VARCHAR(10);

   
INSERT INTO fact_bom_tmp_populate(ct_AvgMatPurityinPercent,
                                 ct_BaseQty,
                                 ct_ComponentQty,
                                 ct_ComponentScrapinPercent,
                                 ct_LeadTimeOffset,
                                 amt_Price,
                                 amt_PriceUnit,
                                 dd_BomItemNo,
                                 dd_BOMItemNodeNo,
                                 dd_BomNumber,
                                 Dim_BOMCategoryId,
                                 dd_BOMPredecessorNodeNo,
                                 dd_ChangedBy,
                                 dd_CreatedBy,
                                 dd_PartNumber,
                                 dd_ComponentNumber,
                                 dd_Alternative,
                                 Dim_BaseUOMId,
                                 Dim_ComponentUOMId,
                                 Dim_BOMComponentId,
                                 Dim_ChangedOnDateid,
                                 Dim_CreatedOnDateid,
                                 Dim_CurrencyId,
                                 Dim_IssuingPlantId,
                                 Dim_PurchasingOrgId,
                                 Dim_StorageLocationId,
                                 Dim_BomStatusId,
                                 Dim_PurchasingGroupId,
                                 Dim_MaterialGroupId,
                                 Dim_ValidFromDateid,
                                 Dim_BomItemCategoryId,
                                 Dim_VendorId,
                                 dd_BomItemCounter,
								 fact_bomid,
								 Dim_Currencyid_GBL,
								 amt_ExchangeRate_GBL,
								 STKO_BMEIN,		-- Used when updating new added rows
								 STKO_STLNR,		-- Used when updating new added rows
								 STKO_STLTY,		-- Used when updating new added rows
								 STKO_WRKAN,		-- Used when updating new added rows
								 STPO_MEINS,		-- Used when updating new added rows
								 STPO_PSWRK,		-- Used when updating new added rows
								 STPO_AEDAT,		-- Used when updating new added rows
								 STPO_ANDAT,		-- Used when updating new added rows
								 STPO_WAERS,		-- Used when updating new added rows
								 STPO_EKORG,		-- Used when updating new added rows
								 STPO_LGORT,		-- Used when updating new added rows
								 STPO_EKGRP,		-- Used when updating new added rows
								 STPO_MATKL,		-- Used when updating new added rows
								 STPO_DATUV,		-- Used when updating new added rows
								 STPO_POSTP,		-- Used when updating new added rows
								 STPO_LIFNR			-- Used when updating new added rows
								 )
   SELECT STPO_CSSTR ct_AvgMatPurityinPercent,
          STKO_BMENG ct_BaseQty,
          STPO_MENGE ct_ComponentQty,
          STPO_AUSCH ct_ComponentScrapinPercent,
          STPO_NLFZT ct_LeadTimeOffset,
          STPO_PREIS amt_Price,
          ifnull(STPO_PEINH,1) amt_PriceUnit,
          ifnull(stpo_posnr,'Not Set') dd_BomItemNo,
          ifnull(STPO_STLKN, 0) dd_BOMItemNodeNo,
          STPO_STLNR dd_BomNumber,
          ifnull(bc.Dim_BomCategoryId, 1) ,
          ifnull(STPO_VGKNT, 0) dd_BOMPredecessorNodeNo,
          ifnull(STPO_AENAM, 'Not Set') dd_ChangedBy,
          ifnull(STPO_ANNAM, 'Not Set') dd_CreatedBy,
          'Not Set' dd_PartNumber,
          ifnull(STPO_IDNRK, 'Not Set') dd_ComponentNumber,
          ifnull(STKO_STLAL,'Not Set') dd_Alternative,
          cast(1 as bigint) Dim_BaseUOMId,
          cast(1 as bigint) Dim_ComponentUOMId,
          cast(1 as bigint) Dim_BOMComponentId,
          cast(1 as bigint) Dim_ChangedOnDateid,
          cast(1 as bigint) Dim_CreatedOnDateid,
          cast(1 as bigint) Dim_Currencyid_TRA,
          cast(1 as bigint) Dim_IssuingPlantId,
          cast(1 as bigint) Dim_PurchasingOrgId,
          cast(1 as bigint) Dim_StorageLocationid,
          bs.Dim_bomstatusid Dim_bomstatusid,
          cast(1 as bigint) Dim_PurchasingGroupId,
          cast(1 as bigint) Dim_MaterialGroupid,
          cast(1 as bigint) Dim_ValidFromDateid,
          cast(1 as bigint) Dim_BomItemCategoryId,
          cast(1 as bigint) Dim_VendorId,
          STPO_STPOZ dd_BomItemCounter,
		   (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_bom_tmp_populate') + row_number() over (order by '') fact_bomid,   
		  cast(1 as bigint) Dim_Currencyid_GBL,  
		  1.00 amt_ExchangeRate_GBL,
		  hdr.STKO_BMEIN,		-- Used when updating new added rows
		  hdr.STKO_STLNR,		-- Used when updating new added rows
		  hdr.STKO_STLTY,		-- Used when updating new added rows
		  hdr.STKO_WRKAN,		-- Used when updating new added rows
		  item.STPO_MEINS,		-- Used when updating new added rows
		  item.STPO_PSWRK,		-- Used when updating new added rows  
		  item.STPO_AEDAT, 		-- Used when updating new added rows
		  item.STPO_ANDAT,		-- Used when updating new added rows
		  item.STPO_WAERS,		-- Used when updating new added rows
		  item.STPO_EKORG,		-- Used when updating new added rows
		  item.STPO_LGORT,		-- Used when updating new added rows
		  item.STPO_EKGRP,		-- Used when updating new added rows
		  item.STPO_MATKL,		-- Used when updating new added rows
		  item.STPO_DATUV,		-- Used when updating new added rows
		  item.STPO_POSTP,		-- Used when updating new added rows
		  item.STPO_LIFNR		-- Used when updating new added rows
     FROM STKO hdr
          INNER JOIN STPO item
             ON hdr.STKO_STLNR = item.STPO_STLNR
                AND hdr.STKO_STLTY = item.STPO_STLTY
          INNER JOIN dim_bomstatus bs
             ON bs.BOMStatusCode = ifnull(STKO_STLST, 'Not Set')
                AND bs.RowIsCurrent = 1
          INNER JOIN dim_bomcategory bc
             ON bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_bom_tmp_populate bom
                   WHERE bom.dd_BomNumber = hdr.STKO_STLNR
                         AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
                         AND bc.Category = STPO_STLTY
                         AND STPO_STLTY = STKO_STLTY);

/* Update Statements for new added rows */

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(uom.Dim_UnitOfMeasureid,1) Dim_BaseUOMId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_unitofmeasure uom ON uom.UOM = T1.STKO_BMEIN
		WHERE uom.RowIsCurrent = 1) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_BaseUOMId = SRC.Dim_BaseUOMId
WHERE FA.Dim_BaseUOMId <> SRC.Dim_BaseUOMId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(uom.Dim_UnitOfMeasureid,1) Dim_ComponentUOMId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_unitofmeasure uom ON uom.UOM = T1.STPO_MEINS
		WHERE uom.RowIsCurrent = 1) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ComponentUOMId = SRC.Dim_ComponentUOMId
WHERE FA.Dim_ComponentUOMId <> SRC.Dim_ComponentUOMId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(pitem.Dim_Partid,1) Dim_BOMComponentId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_part pitem ON pitem.PartNumber = T1.dd_ComponentNumber
                     					 AND pitem.Plant = ifnull(T1.STPO_PSWRK,T1.STKO_WRKAN)
		WHERE pitem.RowIsCurrent = 1) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_BOMComponentId = SRC.Dim_BOMComponentId
WHERE FA.Dim_BOMComponentId <> SRC.Dim_BOMComponentId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(be.dim_dateid,1) Dim_ChangedOnDateid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_date be ON be.DateValue = T1.STPO_AEDAT 
									  AND 'Not Set' = be.CompanyCode ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ChangedOnDateid = SRC.Dim_ChangedOnDateid
WHERE FA.Dim_ChangedOnDateid <> SRC.Dim_ChangedOnDateid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(be.dim_dateid,1) Dim_CreatedOnDateid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_date be ON be.DateValue = T1.STPO_ANDAT 
									  AND 'Not Set' = be.CompanyCode ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_CreatedOnDateid = SRC.Dim_CreatedOnDateid
WHERE FA.Dim_CreatedOnDateid <> SRC.Dim_CreatedOnDateid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(cur.Dim_Currencyid,1) Dim_Currencyid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_currency cur ON cur.CurrencyCode = T1.STPO_WAERS ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid = SRC.Dim_Currencyid
WHERE FA.Dim_Currencyid <> SRC.Dim_Currencyid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(dp.Dim_PlantId,1) Dim_IssuingPlantId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_plant dp ON dp.PlantCode = T1.STPO_PSWRK 
		WHERE dp.RowIsCurrent = 1 
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_IssuingPlantId = SRC.Dim_IssuingPlantId
WHERE FA.Dim_IssuingPlantId <> SRC.Dim_IssuingPlantId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(porg.Dim_PurchaseOrgId,1) Dim_PurchasingOrgId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN Dim_PurchaseOrg porg ON  porg.PurchaseOrgCode = T1.STPO_EKORG
		WHERE porg.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_PurchasingOrgId = SRC.Dim_PurchasingOrgId
WHERE FA.Dim_PurchasingOrgId <> SRC.Dim_PurchasingOrgId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(sl.Dim_StorageLocationid,1) Dim_StorageLocationid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_storagelocation sl ON sl.LocationCode = T1.STPO_LGORT  AND sl.Plant = ifnull(T1.STPO_PSWRK,T1.STKO_WRKAN)
		WHERE sl.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_StorageLocationid = SRC.Dim_StorageLocationid
WHERE FA.Dim_StorageLocationid <> SRC.Dim_StorageLocationid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(pg.Dim_PurchaseGroupId,1) Dim_PurchasingGroupId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN Dim_PurchaseGroup pg ON pg.PurchaseGroup = T1.STPO_EKGRP
		WHERE pg.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_PurchasingGroupId = SRC.Dim_PurchasingGroupId
WHERE FA.Dim_PurchasingGroupId <> SRC.Dim_PurchasingGroupId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(mg.Dim_MaterialGroupid,1) Dim_MaterialGroupid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN Dim_MaterialGroup mg ON mg.MaterialGroupCode = T1.STPO_MATKL
		WHERE mg.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_MaterialGroupid = SRC.Dim_MaterialGroupid
WHERE FA.Dim_MaterialGroupid <> SRC.Dim_MaterialGroupid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(ls.dim_dateid,1) Dim_ValidFromDateid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_date ls ON ls.DateValue = T1.STPO_DATUV 
									  AND 'Not Set' = ls.CompanyCode ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ValidFromDateid = SRC.Dim_ValidFromDateid
WHERE FA.Dim_ValidFromDateid <> SRC.Dim_ValidFromDateid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(bic.Dim_BomItemCategoryId,1) Dim_BomItemCategoryId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_bomitemcategory bic ON bic.ItemCategory = T1.STPO_POSTP
        WHERE bic.RowIsCurrent = 1 ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_BomItemCategoryId = SRC.Dim_BomItemCategoryId
WHERE FA.Dim_BomItemCategoryId <> SRC.Dim_BomItemCategoryId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(dv.Dim_VendorId,1) Dim_VendorId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_vendor dv ON dv.VendorNumber = T1.STPO_LIFNR
        WHERE dv.RowIsCurrent = 1 ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_VendorId = SRC.Dim_VendorId
WHERE FA.Dim_VendorId <> SRC.Dim_VendorId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT T1.fact_bomid, IFNULL(cur.dim_currencyid,1) Dim_Currencyid_GBL
		FROM fact_bom_tmp_populate T1 
				CROSS JOIN tmp_pGlobalCurrency_bom gbl
				LEFT JOIN dim_currency cur ON cur.CurrencyCode = gbl.pGlobalCurrency) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid_GBL = SRC.Dim_Currencyid_GBL
WHERE FA.Dim_Currencyid_GBL <> SRC.Dim_Currencyid_GBL;

MERGE INTO fact_bom_tmp_populate FA
USING (
		SELECT T1.fact_bomid,
			   ifnull(z.exchangeRate,1) exchangeRate
		FROM fact_bom_tmp_populate T1
				LEFT JOIN tmp_getExchangeRate1_with_globalvar z 
							ON      z.pFromCurrency = T1.STPO_WAERS
								and z.fact_script_name = 'bi_populate_billofmaterials_fact' 
								and z.pDate = CURRENT_DATE ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.amt_ExchangeRate_GBL = FA.amt_ExchangeRate_GBL;

/* END Update Statements for new added rows */

/* Delete columns used when updating new records */
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_BMEIN;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_STLNR;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_STLTY;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_WRKAN;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_MEINS;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_PSWRK;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_AEDAT;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_ANDAT;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_WAERS;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_EKORG;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_LGORT;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_EKGRP;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_MATKL;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_DATUV;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_POSTP;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STPO_LIFNR;

						 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_bomid),0) from fact_bom_tmp_populate)
where table_name = 'fact_bom_tmp_populate';			

/* Update local curr. Tran/Global curr already populated in insert */
/* Dim_PlantID not populated above */
/* Use Dim_IssuingPlantId. Later override with STKO_WRKAN (Dim_PlantID) to get local currency where applicable*/

UPDATE fact_bom_tmp_populate bom
SET 
	 bom.Dim_Currencyid = cur.Dim_Currencyid
FROM fact_bom_tmp_populate bom,
STKO hdr,
STPO item,
dim_bomstatus bs,
dim_bomcategory bc,
dim_plant dp,
dim_company c,
dim_currency cur
WHERE 
	 hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY
AND bs.BOMStatusCode = ifnull(STKO_STLST, 'Not Set') AND bs.RowIsCurrent = 1
AND bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1
AND bom.dd_BomNumber = hdr.STKO_STLNR
AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
AND bc.Category = STPO_STLTY
AND STPO_STLTY = STKO_STLTY
AND bom.Dim_IssuingPlantId = dp.Dim_PlantId
AND dp.companycode = c.companycode
AND  c.currency = cur.CurrencyCode;


/* Update local exchange rate. GBL exchange rate already populated in insert */
UPDATE fact_bom_tmp_populate bom
SET 
	 bom.amt_ExchangeRate = z.exchangeRate
FROM 
fact_bom_tmp_populate bom,
STKO hdr,STPO item,
dim_bomstatus bs,
dim_bomcategory bc,
dim_plant dp,
dim_company c,
tmp_getExchangeRate1 z
WHERE 
	 hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY
AND bs.BOMStatusCode = ifnull(STKO_STLST, 'Not Set') AND bs.RowIsCurrent = 1
AND bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1
AND bom.dd_BomNumber = hdr.STKO_STLNR
AND dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
AND bc.Category = STPO_STLTY
AND STPO_STLTY = STKO_STLTY
AND bom.Dim_IssuingPlantId = dp.Dim_PlantId
AND dp.companycode = c.companycode
AND z.pFromCurrency  = STPO_WAERS AND z.fact_script_name = 'bi_populate_billofmaterials_fact'  
AND z.pToCurrency = c.currency AND z.pDate = STPO_AEDAT;


/* Update 2*/			

DROP TABLE IF EXISTS TMP1_STKO;
CREATE TABLE TMP1_STKO
as
SELECT s.ROWID as rid,s.* FROM STKO s;

DROP TABLE IF EXISTS TMP2_STKO;
CREATE TABLE TMP2_STKO
as
SELECT DISTINCT hdr.ROWID rid,hdr.*
FROM STKO hdr,stpo,dim_Bomcategory bomc
WHERE stpo.STPO_STLTY = hdr.STKO_STLTY
AND bomc.Category = STPO_STLTY
AND bomc.RowIsCurrent = 1		
AND stpo.STPO_STLNR =  hdr.STKO_STLNR;

MERGE INTO TMP1_STKO T1
USING 
(
	SELECT t2.*
	FROM TMP2_STKO t2
) SRC
ON T1.rid = SRC.rid
WHEN MATCHED THEN DELETE;

 
/* Update column Dim_CreatedOnDateid */

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_CreatedOnDateid = 1
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_CreatedOnDateid,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
SET bom.Dim_CreatedOnDateid = dr.dim_dateid
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
,dim_date dr
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
  AND dr.DateValue = STKO_ANDAT AND 'Not Set' = dr.CompanyCode
AND IFNULL(bom.Dim_CreatedOnDateid,-1) <> IFNULL(dr.dim_dateid,-2);


/* Update column Dim_ChangedOnDateid */

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_ChangedOnDateid = 1
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_ChangedOnDateid,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
SET bom.Dim_ChangedOnDateid = be.dim_dateid
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
,dim_date be
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
  AND be.DateValue = STKO_AEDAT AND 'Not Set' = be.CompanyCode
AND IFNULL(bom.Dim_ChangedOnDateid,-1) <> IFNULL(be.dim_dateid,-2);


/* Update column Dim_ValidFromDateid */

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_ValidFromDateid = 1
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_ValidFromDateid,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
SET bom.Dim_ValidFromDateid = ls.dim_dateid
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
,dim_date ls
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
  AND ls.DateValue = STKO_DATUV AND 'Not Set' = ls.CompanyCode
AND IFNULL(bom.Dim_ValidFromDateid,-1) <> IFNULL(ls.dim_dateid,-2);
	 
	 
UPDATE fact_bom_tmp_populate bom
SET Dim_BOMComponentId = 1	
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
AND bom.dd_Alternative = STKO_STLAL
AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
AND bc.category = STKO_STLTY
AND bc.RowIsCurrent = 1
AND bs.BOMStatusCode = STKO_STLST
AND bs.RowIsCurrent = 1
AND IFNULL(bom.dim_BOMComponentId,-1) <> 1	 ;
	 
	 
/* Update column Dim_BaseUOMId */

UPDATE fact_bom_tmp_populate bom
SET Dim_BaseUOMId = 1
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.dim_BaseUOMId,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
SET Dim_BaseUOMId = uom.Dim_UnitOfMeasureid
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
,dim_unitofmeasure uom
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
 AND uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1
AND IFNULL(bom.Dim_BaseUOMId,-1) <> IFNULL(uom.Dim_UnitOfMeasureid,-2);


/* Update column bom.Dim_PlantId */

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_PlantId = 1
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_PlantId,-1) <> 1;


UPDATE fact_bom_tmp_populate bom
SET bom.Dim_PlantId = dp.Dim_PlantId
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
,dim_plant dp
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
 AND dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
AND IFNULL(bom.Dim_PlantId,-1) <> IFNULL(dp.Dim_PlantId,-2);


/* Update local curr and exchg rate as previous query updated plantid */

/* Use STKO_WRKAN (plant id) to get local currency */

UPDATE fact_bom_tmp_populate bom
SET 
	 Dim_Currencyid = cur.Dim_Currencyid
FROM fact_bom_tmp_populate bom,
TMP1_STKO hdr,
dim_bomstatus bs,
dim_bomcategory bc,
dim_plant dp,
dim_company c,
dim_currency cur
WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
 AND dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
 AND dp.companycode = c.companycode AND  c.currency = cur.CurrencyCode;


/* Update local exchange rate.  */
/* Use Dim_CurrencyId_TRA(from curr),  plant currency (to curr : retrieved using STKO_WRKAN ),STKO_AEDAT(pDate) */

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT  T1.fact_bomid,ifnull(zz.exchangeRate,1) exchangeRate
		FROM 
		fact_bom_tmp_populate T1
				INNER JOIN TMP1_STKO hdr ON T1.dd_BomNumber = hdr.STKO_STLNR
       							 AND T1.dd_Alternative = hdr.STKO_STLAL
				INNER JOIN dim_bomstatus bs ON bs.BOMStatusCode = hdr.STKO_STLST
				INNER JOIN dim_bomcategory bc ON bc.category = hdr.STKO_STLTY AND T1.Dim_BomcategoryId = bc.Dim_BomcategoryId
				INNER JOIN dim_plant dp ON dp.PlantCode = hdr.STKO_WRKAN
				INNER JOIN dim_company c ON dp.companycode = c.companycode
				LEFT JOIN (	SELECT z.exchangeRate, z.pToCurrency, z.pDate
							FROM fact_bom_tmp_populate bom1
									INNER JOIN dim_currency cur ON  bom1.Dim_CurrencyId_TRA = cur.Dim_CurrencyId
									LEFT JOIN tmp_getExchangeRate1_with_globalvar z ON z.pFromCurrency  = cur.CurrencyCode
							WHERE 
							z.fact_script_name = 'bi_populate_billofmaterials_fact'
							) zz ON zz.pToCurrency = c.currency
	 				 			 AND zz.pDate = hdr.STKO_AEDAT
		WHERE bc.RowIsCurrent = 1   
		AND   bs.RowIsCurrent = 1
		AND   dp.RowIsCurrent = 1
		) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.amt_ExchangeRate = SRC.exchangeRate
WHERE FA.amt_ExchangeRate <> SRC.exchangeRate;


/* Update remaining columns. These did not have inner sub-queries in mysql */
	 
UPDATE fact_bom_tmp_populate bom
SET bom.Dim_bomstatusid = bs.Dim_bomstatusid
FROM 
fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_bomstatusid ,-1) <> IFNULL( bs.Dim_bomstatusid,-2);

UPDATE fact_bom_tmp_populate bom
SET Dim_ComponentUOMId = 1
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND Dim_ComponentUOMId  <> 1;

UPDATE fact_bom_tmp_populate bom
SET Dim_StorageLocationid = 1
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND bom.Dim_StorageLocationid <> 1;

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_IssuingPlantId = 1
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_IssuingPlantId ,-1) <> 1;

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_PurchasingOrgId = 1
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND bom.Dim_PurchasingOrgId <> 1;

/* LK: Commented out as this is populated above */
/*
UPDATE fact_bom_tmp_populate bom
FROM TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
SET bom.Dim_Currencyid = 1
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND bom.Dim_Currencyid <> 1 */

UPDATE fact_bom_tmp_populate bom
SET dd_BOMItemNodeNo = 0
FROM fact_bom_tmp_populate bom,
TMP1_STKO hdr, 
dim_bomstatus bs,
dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(dd_BOMItemNodeNo ,-1) <> 0;

UPDATE fact_bom_tmp_populate bom
SET
dd_BomItemNo = 'Not Set'
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND ifnull(bom.dd_BomItemNo,'xx') <> 'Not Set';

UPDATE fact_bom_tmp_populate bom
SET dd_BOMPredecessorNodeNo = 0
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_BOMPredecessorNodeNo ,-1) <> 0;

UPDATE fact_bom_tmp_populate bom
SET
dd_CreatedBy = ifnull(STKO_ANNAM, 'Not Set')
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_CreatedBy ,'xx') <> ifnull(STKO_ANNAM, 'Not Set');

UPDATE fact_bom_tmp_populate bom
SET
dd_ChangedBy = ifnull(STKO_AENAM, 'Not Set')
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_ChangedBy ,'xx') <> ifnull(STKO_AENAM, 'Not Set');

UPDATE fact_bom_tmp_populate bom
SET ct_AvgMatPurityinPercent = 0.00
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_AvgMatPurityinPercent ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
SET
ct_LeadTimeOffset = 0.00
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_LeadTimeOffset ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
SET ct_BaseQty = STKO_BMENG
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_BaseQty ,-1) <> IFNULL( STKO_BMENG,-2);

UPDATE fact_bom_tmp_populate bom
SET ct_ComponentQty = 0.00
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.ct_ComponentQty ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
SET amt_Price = 0.00
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.amt_Price ,-1) <> 0.00;

UPDATE fact_bom_tmp_populate bom
SET amt_PriceUnit = 1.00
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.amt_PriceUnit ,-1) <> 1.00;

UPDATE fact_bom_tmp_populate bom
SET
dd_PartNumber = 'Not Set'
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_PartNumber ,'xx') <> 'Not Set';

UPDATE fact_bom_tmp_populate bom
SET
dd_ComponentNumber = 'Not Set'
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.dd_ComponentNumber ,'xx') <> 'Not Set';

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_VendorId = 1
FROM fact_bom_tmp_populate bom,TMP1_STKO hdr, dim_bomstatus bs,dim_bomcategory bc
WHERE     bom.dd_BomNumber = STKO_STLNR
 AND bom.dd_Alternative = STKO_STLAL
 AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
 AND bc.category = STKO_STLTY
 AND bc.RowIsCurrent = 1
 AND bs.BOMStatusCode = STKO_STLST
 AND bs.RowIsCurrent = 1
AND IFNULL(bom.Dim_VendorId ,-1) <> 1;
	 
/* End of Update 2 */	 
						 
/* Insert 2*/

/* Changes made to the temporary table based on the fields needed in the update statements for new rows. */
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_BMEIN VARCHAR(20);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_WRKAN VARCHAR(4);
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_AEDAT DATE;
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_ANDAT DATE;
ALTER TABLE fact_bom_tmp_populate ADD COLUMN STKO_DATUV DATE;

					 
INSERT INTO fact_bom_tmp_populate(ct_AvgMatPurityinPercent,
                                 ct_BaseQty,
                                 ct_ComponentQty,
                                 ct_ComponentScrapinPercent,
                                 ct_LeadTimeOffset,
                                 amt_Price,
                                 amt_PriceUnit,
                                 dd_BomItemNo,
                                 dd_BOMItemNodeNo,
                                 dd_BomNumber,
                                 Dim_BomCategoryId,
                                 dd_Alternative,
                                 dd_BOMPredecessorNodeNo,
                                 dd_ChangedBy,
                                 dd_CreatedBy,
                                 dd_PartNumber,
                                 dd_ComponentNumber,
                                 Dim_PlantId,
                                 Dim_BaseUOMId,
                                 Dim_ComponentUOMId,
                                 Dim_BOMComponentId,
                                 Dim_ChangedOnDateid,
                                 Dim_CreatedOnDateid,
                                 Dim_CurrencyId,
                                 Dim_IssuingPlantId,
                                 Dim_PurchasingOrgId,
                                 Dim_StorageLocationId,
                                 Dim_BomStatusId,
                                 Dim_PurchasingGroupId,
                                 Dim_MaterialGroupId,
                                 Dim_ValidFromDateid,
                                 Dim_BomItemCategoryId,
                                 Dim_VendorId,
								 fact_bomid,
								 Dim_Currencyid_TRA,
								 Dim_Currencyid_GBL,
								 STKO_BMEIN,		-- Used when updating new added rows
								 STKO_WRKAN,		-- Used when updating new added rows
								 STKO_AEDAT,		-- Used when updating new added rows
								 STKO_ANDAT,		-- Used when updating new added rows
								 STKO_DATUV			-- Used when updating new added rows	
								 )
   SELECT 0.00 ct_AvgMatPurityinPercent,
          STKO_BMENG ct_BaseQty,	/* decimal - ok */
          0.00 ct_ComponentQty,
          0.00 ct_ComponentScrapinPercent,
          0 ct_LeadTimeOffset,
          0.00 amt_Price,
          1.00 amt_PriceUnit,
		  0 dd_BomItemNo,			/* 'Not Set' dd_BomItemNo, in mysql	*/
          0 dd_BOMItemNodeNo,
          STKO_STLNR dd_BomNumber,	/* varchar - ok */
          bc.Dim_BomCategoryId Dim_BomCategoryId,	/* int - ok */
          STKO_STLAL dd_Alternative,	/* varchar - ok */
          0 dd_BOMPredecessorNodeNo,
          ifnull(STKO_AENAM, 'Not Set') dd_ChangedBy,
          ifnull(STKO_ANNAM, 'Not Set') dd_CreatedBy,
          'Not Set' dd_PartNumber,
          'Not Set' dd_ComponentNumber,
          cast (1 as bigint) Dim_PlantId,
          cast (1 as bigint) Dim_BaseUOMId,
          cast (1 as bigint) Dim_ComponentUOMId,
          cast (1 as bigint) Dim_BOMComponentId,
          cast (1 as bigint) Dim_ChangedOnDateid,
          cast (1 as bigint) Dim_CreatedOnDateid,
		  cast (1 as bigint) Dim_Currencyid,	/* Local currency */
          cast (1 as bigint) Dim_IssuingPlantId,
          cast (1 as bigint) Dim_PurchasingOrgId,
          cast (1 as bigint) Dim_StorageLocationid,
          bs.Dim_bomstatusid Dim_bomstatusid,
          cast (1 as bigint) Dim_PurchasingGroupId,
          cast (1 as bigint) Dim_MaterialGroupid,
          cast (1 as bigint) Dim_ValidFromDateid,
          cast (1 as bigint) Dim_BomItemCategoryId,
          cast (1 as bigint) Dim_VendorId,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_bom_tmp_populate') + row_number() over (order by '') fact_bomid,
		   cast (1 as bigint) Dim_Currencyid_TRA,		  	/* Keep default value */
		   cast (1 as bigint) Dim_Currencyid_GBL,
		   hdr.STKO_BMEIN,		-- Used when updating new added rows
		   hdr.STKO_WRKAN,		-- Used when updating new added rows
		   hdr.STKO_AEDAT,		-- Used when updating new added rows
		   hdr.STKO_ANDAT,		-- Used when updating new added rows
		   hdr.STKO_DATUV		-- Used when updating new added rows	
					
     FROM    TMP1_STKO hdr
          INNER JOIN
             dim_bomstatus bs
          ON bs.BOMStatusCode = ifnull(STKO_STLST, 0)
             AND bs.RowIsCurrent = 1
          INNER JOIN dim_bomcategory bc
             ON bc.Category = STKO_STLTY AND bc.RowIsCurrent = 1
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_bom_tmp_populate bom
                   WHERE     bom.dd_BomNumber = hdr.STKO_STLNR
                         AND bom.dd_Alternative = hdr.STKO_STLAL
                         AND bc.Category = hdr.STKO_STLTY
                         AND bc.RowIsCurrent = 1);

/* Update Statements for new added rows */

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(dp.Dim_PlantId,1) Dim_PlantId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_plant dp ON dp.PlantCode = T1.STKO_WRKAN
		WHERE dp.RowIsCurrent = 1 
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_PlantId = SRC.Dim_PlantId
WHERE FA.Dim_PlantId <> SRC.Dim_PlantId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(uom.Dim_UnitOfMeasureid,1) Dim_BaseUOMId
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_unitofmeasure uom ON uom.UOM = T1.STKO_BMEIN
		WHERE uom.RowIsCurrent = 1) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_BaseUOMId = SRC.Dim_BaseUOMId
WHERE FA.Dim_BaseUOMId <> SRC.Dim_BaseUOMId;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(be.dim_dateid,1) Dim_ChangedOnDateid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_date be ON be.DateValue = T1.STKO_AEDAT 
									  AND 'Not Set' = be.CompanyCode ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ChangedOnDateid = SRC.Dim_ChangedOnDateid
WHERE FA.Dim_ChangedOnDateid <> SRC.Dim_ChangedOnDateid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(be.dim_dateid,1) Dim_CreatedOnDateid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_date be ON be.DateValue = T1.STKO_ANDAT 
									  AND 'Not Set' = be.CompanyCode ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_CreatedOnDateid = SRC.Dim_CreatedOnDateid
WHERE FA.Dim_CreatedOnDateid <> SRC.Dim_CreatedOnDateid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(t.Dim_Currencyid,1) Dim_Currencyid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN (	SELECT dp.PlantCode, cur.Dim_Currencyid
							FROM  dim_plant dp
									INNER JOIN dim_company c ON dp.companycode = c.companycode
									INNER JOIN dim_currency cur ON c.currency = cur.CurrencyCode
							WHERE dp.RowIsCurrent = 1
						   ) t ON t.PlantCode = T1.STKO_WRKAN 
		) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid = SRC.Dim_Currencyid
WHERE FA.Dim_Currencyid <> SRC.Dim_Currencyid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT 
		T1.fact_bomid, ifnull(ls.dim_dateid,1) Dim_ValidFromDateid
		FROM fact_bom_tmp_populate T1
				LEFT JOIN dim_date ls ON ls.DateValue = T1.STKO_DATUV 
									  AND 'Not Set' = ls.CompanyCode ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ValidFromDateid = SRC.Dim_ValidFromDateid
WHERE FA.Dim_ValidFromDateid <> SRC.Dim_ValidFromDateid;

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT T1.fact_bomid, IFNULL(cur.dim_currencyid,1) Dim_Currencyid_GBL
		FROM fact_bom_tmp_populate T1 
				CROSS JOIN tmp_pGlobalCurrency_bom gbl
				LEFT JOIN dim_currency cur ON cur.CurrencyCode = gbl.pGlobalCurrency) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid_GBL = SRC.Dim_Currencyid_GBL
WHERE FA.Dim_Currencyid_GBL <> SRC.Dim_Currencyid_GBL;

/* END Update Statements for new added rows */

/* Delete columns used when updating new records */
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_BMEIN;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_WRKAN;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_AEDAT;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_ANDAT;
ALTER TABLE fact_bom_tmp_populate DROP COLUMN STKO_DATUV;

									
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_bomid),0) from fact_bom_tmp_populate)
where table_name = 'fact_bom_tmp_populate';											


/* Delete 1 */

DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_1;
CREATE TABLE TMP_DEL_fact_bom_tmp_populate_1
AS
SELECT bom.*
FROM fact_bom_tmp_populate bom,STKO k , STPO p
WHERE     k.STKO_STLNR = p.STPO_STLNR
AND k.STKO_STLTY = p.STPO_STLTY
AND k.STKO_STLAL = bom.dd_Alternative
AND k.STKO_STLNR = bom.dd_BomNumber
AND p.STPO_STLKN = bom.dd_BomItemNodeNo
AND p.STPO_LKENZ = 'X';

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT * FROM TMP_DEL_fact_bom_tmp_populate_1 ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN DELETE;


DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_2;
CREATE TABLE TMP_DEL_fact_bom_tmp_populate_2
AS
SELECT distinct bom.*
FROM fact_bom_tmp_populate bom,STKO k
WHERE     k.STKO_STLNR = dd_BomNumber
AND k.STKO_STLAL = dd_Alternative
AND k.STKO_LOEKZ = 'X';

MERGE INTO fact_bom_tmp_populate FA
USING ( SELECT * FROM TMP_DEL_fact_bom_tmp_populate_2 ) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN DELETE;


/* Update column Dim_MaterialPlantId */

UPDATE fact_bom_tmp_populate bom
SET Dim_MaterialPlantId = 1
FROM  fact_bom_tmp_populate bom, MAST m, dim_bomusage bu, dim_bomcategory bc
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
AND bom.Dim_MaterialPlantId <> 1;


/*UPDATE fact_bom_tmp_populate bom
SET Dim_MaterialPlantId = p.Dim_PlantId
FROM  fact_bom_tmp_populate bom, MAST m, dim_bomusage bu, dim_bomcategory bc
 ,dim_plant p
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
 AND p.PlantCode = MAST_WERKS AND p.RowIsCurrent = 1
AND bom.Dim_MaterialPlantId <> p.Dim_PlantId*/

drop table if exists tmp_mast;
create table tmp_mast as select    MAST_STLAN , MAST_STLNR,MAST_STLAL,MAST_WERKS , row_number() over (partition by   MAST_STLNR,MAST_STLAL order by '') rowno from MAST;

UPDATE fact_bom_tmp_populate bom
SET Dim_MaterialPlantId = p.Dim_PlantId
FROM  fact_bom_tmp_populate bom, tmp_MAST m, dim_bomusage bu, dim_bomcategory bc,dim_plant p
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
 AND p.PlantCode = MAST_WERKS AND p.RowIsCurrent = 1
AND m.rowno = 1
AND bom.Dim_MaterialPlantId <> p.Dim_PlantId;


/* Update column Dim_PartId */

UPDATE fact_bom_tmp_populate bom
SET Dim_PartId = 1
FROM fact_bom_tmp_populate bom, MAST m, dim_bomusage bu, dim_bomcategory bc
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
AND bom.Dim_PartId <> 1;

DROP TABLE IF EXISTS TMP_UPDT_BOM;
CREATE TABLE TMP_UPDT_BOM
AS
SELECT distinct MAST_STLNR,MAST_STLAL,MAST_STLAN,MAST_MATNR,MAST_WERKS, row_number() over(partition by MAST_STLNR,MAST_STLAL order by '') rono FROM MAST;

UPDATE fact_bom_tmp_populate bom
SET Dim_PartId = pt.Dim_PartId
FROM fact_bom_tmp_populate bom, TMP_UPDT_BOM m, dim_bomusage bu, dim_bomcategory bc
 ,dim_part pt
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
 AND pt.PartNumber = m.MAST_MATNR AND pt.Plant = m.MAST_WERKS AND pt.RowIsCurrent = 1
 AND m.rono = 1
AND bom.Dim_PartId <> pt.Dim_PartId;

DROP TABLE IF EXISTS TMP_UPDT_BOM;

DROP TABLE IF EXISTS TMP_UPDT_MAST_BOM;
CREATE TABLE TMP_UPDT_MAST_BOM
AS
SELECT m.MAST_STLNR,m.MAST_STLAL,max(m.MAST_MATNR) MAST_MATNR FROM MAST m GROUP BY m.MAST_STLNR,m.MAST_STLAL;

UPDATE fact_bom_tmp_populate bom
SET  bom.dd_PartNumber = ifnull(m.MAST_MATNR,'Not Set')
FROM  fact_bom_tmp_populate bom, TMP_UPDT_MAST_BOM m, dim_bomcategory bc
WHERE m.MAST_STLNR = bom.dd_BomNumber
 AND m.MAST_STLAL = bom.dd_Alternative
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'M'
AND IFNULL( bom.dd_PartNumber ,'xx') <> ifnull(m.MAST_MATNR,'Not Set');

DROP TABLE IF EXISTS TMP_UPDT_MAST_BOM;

DROP TABLE IF EXISTS TMP_UPDT_MAST_BOM;
CREATE TABLE TMP_UPDT_MAST_BOM
AS
SELECT m.MAST_STLNR,m.MAST_STLAL,max(m.MAST_STLAN) MAST_STLAN FROM MAST m GROUP BY m.MAST_STLNR,m.MAST_STLAL;

UPDATE fact_bom_tmp_populate bom
SET bom.Dim_BomUsageId = bu.Dim_bomusageid
FROM  fact_bom_tmp_populate bom, TMP_UPDT_MAST_BOM m, dim_bomusage bu, dim_bomcategory bc
WHERE m.MAST_STLNR = bom.dd_BomNumber
 AND m.MAST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'M'
AND bom.Dim_BomUsageId <> bu.Dim_bomusageid;

DROP TABLE IF EXISTS TMP_UPDT_MAST_BOM;
/* Done till here */

/* Update from PRST : Category = P */


/* Update Dim_MaterialPlantId */
UPDATE    fact_bom_tmp_populate bom 
SET bom.Dim_MaterialPlantId  = 1
FROM fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
WHERE p.PRST_STLNR = bom.dd_BomNumber
          AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P';	

UPDATE    fact_bom_tmp_populate bom 
SET bom.Dim_MaterialPlantId  = pl.Dim_PlantId
FROM fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
,dim_plant pl
WHERE p.PRST_STLNR = bom.dd_BomNumber
          AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P'
AND pl.PlantCode = p.PRST_WERKS
AND pl.RowIsCurrent = 1
AND bom.Dim_MaterialPlantId  <> pl.Dim_PlantId;	


/* Update Dim_PartId */
UPDATE    fact_bom_tmp_populate bom 
SET bom.Dim_PartId  =  1
FROM  fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P';	

UPDATE    fact_bom_tmp_populate bom 
SET bom.Dim_PartId  = pt.Dim_PartId
FROM fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
,dim_part pt
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P'
AND pt.PartNumber = p.PRST_MATNR
AND pt.Plant = p.PRST_WERKS
AND pt.RowIsCurrent = 1
AND bom.Dim_PartId  <> pt.Dim_PartId;	


UPDATE    fact_bom_tmp_populate bom
SET  bom.dd_PartNumber = ifnull(p.PRST_MATNR,'Not Set')
FROM fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
WHERE p.PRST_STLNR = bom.dd_BomNumber
 AND p.PRST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'P'
AND IFNULL( bom.dd_PartNumber ,'xx') <> ifnull(p.PRST_MATNR,'Not Set');

UPDATE    fact_bom_tmp_populate bom
SET bom.Dim_BomUsageId = bu.Dim_bomusageid
FROM fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
WHERE p.PRST_STLNR = bom.dd_BomNumber
 AND p.PRST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'P'
AND bom.Dim_BomUsageId <> bu.Dim_bomusageid;

UPDATE    fact_bom_tmp_populate bom
SET bom.dd_WBSElement = ifnull(p.PRST_PSPNR,0)
FROM fact_bom_tmp_populate bom, PRST p,dim_bomusage bu,dim_bomcategory bc
WHERE p.PRST_STLNR = bom.dd_BomNumber
 AND p.PRST_STLAL = bom.dd_Alternative
 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bc.Category = 'P'
AND IFNULL(bom.dd_WBSElement ,-1) <> ifnull(p.PRST_PSPNR,0);

/* End of Update from PRST : Category = P */


DROP TABLE IF EXISTS TMP_UPDATE_BOM_MSC;
CREATE TABLE TMP_UPDATE_BOM_MSC
AS
SELECT STPO_STLNR,STPO_STLKN,STPO_STLTY,STPO_FMENG,STPO_NETAU,STPO_SCHGT,STPO_BEIKZ,STPO_RVREL,STPO_SANFE,STPO_SANIN,STPO_SANKA,STPO_REKRI,STPO_REKRS,STPO_CADPO,row_number () over(partition by STPO_STLNR,STPO_STLKN,STPO_STLTY order by '') rono
FROM STPO;

UPDATE       fact_bom_tmp_populate bom
SET bom.Dim_BillOfMaterialMiscid = bomi.Dim_BillOfMaterialMiscid
FROM  fact_bom_tmp_populate bom, dim_bomcategory bc,TMP_UPDATE_BOM_MSC p, Dim_BillOfMaterialMisc bomi
WHERE bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND   bom.dd_BomNumber = p.STPO_STLNR
AND bom.dd_BomItemNodeNo = p.STPO_STLKN
AND bc.Category = p.STPO_STLTY
AND  bomi.FixedQuantity = ifnull(p.STPO_FMENG, 'Not Set')
AND bomi.Net = ifnull(p.STPO_NETAU, 'Not Set')
AND bomi.BulkMaterial = ifnull(p.STPO_SCHGT, 'Not Set')
AND bomi.MaterialProvision = ifnull(p.STPO_BEIKZ, 'Not Set')
AND bomi.ItemRelevantToSales = ifnull(p.STPO_RVREL, 'Not Set')
AND bomi.ItemRelevantToProduction = ifnull(p.STPO_SANFE, 'Not Set')
AND bomi.ItemRelevantForPlantMaint = ifnull(p.STPO_SANIN, 'Not Set')
AND bomi.ItemRelevantToEngg = ifnull(p.STPO_SANKA, 'Not Set')
AND bomi.BOMIsRecursive = ifnull(p.STPO_REKRI, 'Not Set')
AND bomi.RecursivenessAllowed = ifnull(p.STPO_REKRS, 'Not Set')
AND bomi.CAD = ifnull(p.STPO_CADPO, 'Not Set')
AND p.rono = 1
AND   bom.Dim_BillOfMaterialMiscid <> bomi.Dim_BillOfMaterialMiscid ;

DROP TABLE IF EXISTS TMP_UPDATE_BOM_MSC;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate;

CREATE TABLE tmp_FactBOMValidToDate
AS
SELECT   distinct dim_bomcategoryid, dd_BomNumber,dd_BomItemNo,dt.dateValue as ValidFromDate,'1 Jan 9999' as ValidToDate
FROM fact_bom_tmp_populate inner join dim_date dt on dt.dim_dateid = dim_validfromdateid 
where dim_validfromdateid <> 1;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate_1;

CREATE TABLE tmp_FactBOMValidToDate_1 AS
SELECT distinct b1.dim_bomcategoryid, b1.dd_BomNumber,b1.dd_BomItemNo ,b2.validfromdate, (dt.datevalue - INTERVAL '1' DAY) ValidToDate FROM 
tmp_FactBOMValidToDate b2, fact_bom_tmp_populate b1,dim_date dt
WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid 
and b1.dd_BomNumber = b2.dd_BomNumber 
and b1.dd_BomItemNo = b2.dd_BomItemNo
AND dt.dim_dateid = b1.dim_validfromdateid
AND b2.validfromdate < dt.datevalue;

MERGE INTO fact_bom_tmp_populate f
USING (SELECT fact_bomid,max(dt.dim_dateid) dim_dateidvalidto
	FROM fact_bom_tmp_populate b1, tmp_FactBOMValidToDate_1 b2, dim_date dt,dim_Date dfrd
	WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid
		AND b1.dd_BomNumber = b2.dd_BomNumber
		AND b1.dd_BomItemNo = b2.dd_BomItemNo
		AND dfrd.dim_dateid = b1.dim_validfromdateid
		AND dfrd.datevalue = b2.validfromdate
		AND dfrd.companycode = 'Not Set'
		AND dt.datevalue = b2.ValidToDate
		AND dt.companycode = 'Not Set'
		AND b1.dim_dateidvalidto <> dt.dim_dateid
	GROUP BY fact_bomid) src
ON f.fact_bomid = src.fact_bomid
WHEN MATCHED THEN UPDATE SET f.dim_dateidvalidto = src.dim_dateidvalidto;

DROP TABLE IF EXISTS TMP_UPDT_BOM_DD;
CREATE TABLE TMP_UPDT_BOM_DD
AS
SELECT dim_dateid FROM dim_date dt WHERE dt.DateValue = '1900-01-01' AND dt.CompanyCode = 'Not Set';

UPDATE fact_bom_tmp_populate b1
SET b1.dim_dateidvalidto = ifnull(dt.Dim_DateId,1)
FROM fact_bom_tmp_populate b1
  LEFT JOIN TMP_UPDT_BOM_DD dt ON 1 = 1
WHERE (b1.dim_dateidvalidto = 1 OR b1.dim_dateidvalidto IS NULL);

DROP TABLE IF EXISTS TMP_UPDT_BOM_DD;


 UPDATE fact_bom_tmp_populate b 
SET dim_compprodhierarchyid = dim_producthierarchyid
from fact_bom_tmp_populate b, dim_producthierarchy dph, dim_part dpr  
  WHERE dph.ProductHierarchy = dpr.ProductHierarchy
        AND dpr.Dim_Partid = b.Dim_BOMComponentId;

 UPDATE fact_bom_tmp_populate
    SET dim_compprodhierarchyid = 1
  WHERE dim_compprodhierarchyid IS NULL;

 UPDATE fact_bom_tmp_populate b 
SET dim_parentpartprodhierarchyid = dim_producthierarchyid
from fact_bom_tmp_populate b, dim_producthierarchy dph, dim_part dpr
  WHERE dph.ProductHierarchy = dpr.ProductHierarchy
        AND dpr.Dim_Partid = b.Dim_partId;

 UPDATE fact_bom_tmp_populate
    SET dim_parentpartprodhierarchyid = 1
  WHERE dim_parentpartprodhierarchyid IS NULL;


DROP TABLE IF EXISTS tmp_FactBOMValidToDate_1;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate;


/* Drop original table and rename staging table to orig table name */
drop table if exists fact_bom;
rename table fact_bom_tmp_populate to fact_bom;

DROP TABLE IF EXISTS tmp_FactBOMValidToDate;

/* These 2 procs are called after this */
/* CALL bi_populate_bom_level() */
/* CALL bi_process_bom_fact() */

DROP TABLE IF EXISTS tmp_pGlobalCurrency_bom;
DROP TABLE IF EXISTS tmp_getExchangeRate1_with_globalvar;
DROP TABLE IF EXISTS TMP1_STKO;
DROP TABLE IF EXISTS TMP2_STKO;
DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_1;
DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_2;
