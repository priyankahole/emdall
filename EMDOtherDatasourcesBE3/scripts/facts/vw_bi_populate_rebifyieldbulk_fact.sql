/******************************************************************************************************************/
/*   Script         : bi_populate_rebifyieldbulk_fact                                                             */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_rebifyieldbulk                                                    */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   10 Feb 2017         CristianT       1.1               Calculating the AVG of previous years                  */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_rebifyieldbulk;
CREATE TABLE tmp_fact_rebifyieldbulk
AS
SELECT *
FROM fact_rebifyieldbulk
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldbulk', ifnull(max(fact_rebifyieldbulkid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldbulk;


INSERT INTO tmp_fact_rebifyieldbulk(
fact_rebifyieldbulkid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifbulklotid,
ct_bulk_prot_hplc,
ct_yieldpap1bulk,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk') + ROW_NUMBER() over(order by '') as fact_rebifyieldbulkid,
       'G1CC' as dd_type,
       ifnull(parameter_set_name ,'not set') as dd_paramater_set_name,
       ifnull(parameter_set_date ,'not set') as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ifnull(rebif_bulk_lot_id ,'not set') as dd_rebifbulklotid,
       ifnull(bulk_bulk_prot_hplc, 0) as ct_bulk_prot_hplc,
       ifnull(yield_pap1_bulk, 0) as ct_yieldpap1bulk,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_yield_pap1_bulk_g1
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldbulk', ifnull(max(fact_rebifyieldbulkid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldbulk;

INSERT INTO tmp_fact_rebifyieldbulk(
fact_rebifyieldbulkid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifbulklotid,
ct_bulk_prot_hplc,
ct_yieldpap1bulk,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk') + ROW_NUMBER() over(order by '') as fact_rebifyieldbulkid,
       'G2SFM' as dd_type,
       ifnull(parameter_set_name ,'not set') as dd_paramater_set_name,
       ifnull(parameter_set_date ,'not set') as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ifnull(rebif_bulk_lot_id ,'not set') as dd_rebifbulklotid,
       ifnull(bulk_bulk_prot_hplc, 0) as ct_bulk_prot_hplc,
       ifnull(yield_pap1_bulk, 0) as ct_yieldpap1bulk,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_yield_pap1_bulk_g2
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR;

UPDATE tmp_fact_rebifyieldbulk tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_rebifyieldbulk tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = substr(tmp.dd_parameter_set_date, 0, 10);


/* 10 Feb 2016 CristianT Start: Calculating the AVG of previous years */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldbulk', ifnull(max(fact_rebifyieldbulkid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldbulk;

INSERT INTO tmp_fact_rebifyieldbulk(
fact_rebifyieldbulkid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifbulklotid,
ct_bulk_prot_hplc,
ct_yieldpap1bulk,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk') + ROW_NUMBER() over(order by '') as fact_rebifyieldbulkid,
       'G1CC' as dd_type,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_PARAMATER_SET_NAME,
       current_date as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_rebifbulklotid,
       ifnull(avg(bulk_bulk_prot_hplc), 0) as ct_bulk_prot_hplc,
       ifnull(avg(yield_pap1_bulk), 0) as ct_yieldpap1bulk,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_yield_pap1_bulk_g1 tmp
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldbulk', ifnull(max(fact_rebifyieldbulkid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldbulk;

INSERT INTO tmp_fact_rebifyieldbulk(
fact_rebifyieldbulkid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifbulklotid,
ct_bulk_prot_hplc,
ct_yieldpap1bulk,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldbulk') + ROW_NUMBER() over(order by '') as fact_rebifyieldbulkid,
       'G2SFM' as dd_type,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_PARAMATER_SET_NAME,
       current_date as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_rebifbulklotid,
       ifnull(avg(bulk_bulk_prot_hplc), 0) as ct_bulk_prot_hplc,
       ifnull(avg(yield_pap1_bulk), 0) as ct_yieldpap1bulk,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_yield_pap1_bulk_g2 tmp
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));

UPDATE tmp_fact_rebifyieldbulk tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_rebifyieldbulk tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_parameter_set_date
      AND tmp.dim_dateidsetdate = 1;
/* 10 Feb 2016 CristianT End */

UPDATE tmp_fact_rebifyieldbulk tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_rebifyieldbulk tmp;

TRUNCATE TABLE fact_rebifyieldbulk;

INSERT INTO fact_rebifyieldbulk(
fact_rebifyieldbulkid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifbulklotid,
ct_bulk_prot_hplc,
ct_yieldpap1bulk,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_rebifyieldbulkid,
       dd_type,
       dd_paramater_set_name,
       dd_parameter_set_date,
       dim_dateidsetdate,
       dd_rebifbulklotid,
       ct_bulk_prot_hplc,
       ct_yieldpap1bulk,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_rebifyieldbulk;

DROP TABLE IF EXISTS tmp_fact_rebifyieldbulk;

TRUNCATE TABLE emd586.fact_rebifyieldbulk;

INSERT INTO emd586.fact_rebifyieldbulk(
fact_rebifyieldbulkid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifbulklotid,
ct_bulk_prot_hplc,
ct_yieldpap1bulk,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_rebifyieldbulkid,
       dd_type,
       dd_paramater_set_name,
       dd_parameter_set_date,
       dim_dateidsetdate,
       dd_rebifbulklotid,
       ct_bulk_prot_hplc,
       ct_yieldpap1bulk,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM fact_rebifyieldbulk;