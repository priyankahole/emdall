delete from fact_artworksno;
insert into fact_artworksno(
  fact_artworksnoid,
  dd_artwork_id,
  dd_ppif_version_tree_root_obj,
  dd_ppif_id,dd_ppif_product_family,
  dd_ppif_erp_product_code,dd_ppif_version,
  dd_ppif_is_current_version,
  dd_ppif_erp_product_designation,
  dd_ppif_status,
  dd_ppif_country,
  dd_ppif_country_region,
  dd_component_version_tree_root,
  dd_component_id,
  dd_component_erp_code,
  dd_component_product_family,
  dd_component_version,
  dd_component_is_current_version,
  dd_component_erp_designation,
  dd_component_type,
  dd_component_company_logos,
  dd_component_anti_counterfeiting,
  dd_component_status,
  dd_component_doc_status,
  dd_component_packaging_site,
  dd_component_pprf_type_of_request,
  dim_component_creation_dateid,
  dim_comp_last_modified_dateid,
  dd_comp_lra_change_control_id,
  dd_comp_lra_other_change_control,
  dim_ppif_impl_date_at_pack_siteid,
  dim_ppif_target_market_dateid,
  dd_mpprf_id,dd_mpprf_no,
  dd_change_control,
  dd_mpprf_status,
  dd_mpprf_is_current_version,
  dd_wf_workflow_id,
  dd_wf_status,
  dim_wf_start_dateid,
  dd_wf_initiator,
  dd_wf_initiator_role,
  dd_wf_initiator_country,
  dim_aw_ha_approval_dateid,
  dim_aw_est_approval_dateid,
  dim_pprf_filling_req_start_dateid,
  dim_pprf_filling_req_end_dateid,
  dim_pprf_filling_req_1stsignoff_dateid,
  dd_pprf_approval_ps_name,
  dim_pprf_approval_ps_start_dateid,
  dim_pprf_approval_ps_end_dateid,
  dd_pprf_approval_lra_name,
  dim_pprf_approval_lra_start_dateid,
  dim_pprf_approval_lra_end_dateid,
  dd_pprf_acceptance_co_name,
  dim_pprf_acceptance_co_start_dateid,
  dim_pprf_acceptance_co_end_dateid,
  dim_pprf_aborted_dateid,
  dim_pprf_requested_deadlineid,
  dd_pprf_variation_approval_dateid,
  dd_art_prod_de_name,
  dim_art_prod_de_start_dateid,
  dim_art_prod_de_end_dateid,
  dim_art_prod_de_1stsignoff_dateid,
  ct_art_prod_de_signoff_count,
  dim_art_checking_co_start_dateid,
  dim_art_checking_co_end_dateid,
  dim_art_checking_co_1stsignoff_dateid,
  dd_art_approval_plh_name,
  dim_art_approval_plh_start_dateid,
  dim_art_approval_plh_end_dateid,
  dd_art_approval_ps_name,
  dim_art_approval_ps_start_dateid,
  dim_art_approval_ps_end_dateid,
  dd_art_approval_lra_name,
  dim_art_approval_lra_start_dateid,
  dim_art_approval_lra_end_dateid,
  dd_art_accepted_co_name,
  dim_art_acceptance_co_start_dateid,
  dim_art_acceptance_co_end_dateid,
  dim_art_impl_dateid,
  dim_art_target_impl_dateid,
  dim_art_released_dateid,
  dd_art_released_version,
  dd_wf_pprf_start_mm_yyyy,
  dd_wf_right_first_time,
  ct_wf_lt1,
  ct_wf_lt2,
  ct_wf_lt3,
  ct_wf_lt4,
  ct_wf_lt5,
  ct_wf_lt6,
  ct_wf_lt7,
  ct_wf_total_lt_art_mgnt_sys,
  ct_wf_lt_implementation,
  dd_activities_name,
  dd_activities_sent_by,
  dim_activities_date_sent,
  dd_activities_task_name,
  dd_activities_task_subject,
  ct_pprffilledstep1,     
  ct_pprfapprovedstep1,   
  ct_pprfapprovedstep2,   
  ct_pprfacceptedstep1,   
  ct_draftedstep1,        
  ct_checkedstep1,        
  ct_artworkacceptedstep1,
  ct_artworkacceptedstep2,
  ct_pprfapprovedmultistep,
  ct_artworkacceptedmultistep,
  ct_packagepprffilleddays,
  ct_packagepprfapproveddays,
  ct_packagedrafteddays,
  ct_packageawaccepteddays,
  dim_packagepprffilleddateid,  
  dim_packagepprfapproveddateid,
  dim_packagedrafteddateid,     
  dim_packageawaccepteddateid,
  dim_pprfapprovedmultistepdateid,
  dim_artworkacceptedmultistepdateid,
  dd_packagepprffilledflag,
  dd_packagepprfapprovedflag,
  dd_packagedraftedflag,
  dd_packageawacceptedflag,
  dd_pprffilledlead,
  dd_package1lead,
  dd_pprfapprovalspslead,
  dd_pprfapprovalslralead,
  dd_pprfapprovalslead,
  dd_package2lead,
  dd_pprfacceptancecolead,
  dd_artproddeenddatelead,
  dd_package3lead,
  dd_artcheckcoenddatelead,
  dd_artapprovalpslead,
  dd_artapprovallralead,
  dd_artapprovalpsandlralead,
  dd_package4lead,
  dd_ppiffillinglead,
  dd_package5lead,
  dd_pscapprovallead,
  dd_package6lead,
  dd_lraapprovallead,
  dd_package7lead,
  dd_siteqappifreleaselead,
  dd_package8lead,
  dd_Package9lead,
  dd_totallead,
  ct_pprffilledstep1vslead,
  ct_pprfapprovedstep1vslead,
  ct_pprfapprovedstep2vslead,
  ct_pprfacceptedstep1vslead,
  ct_draftedstep1vslead,
  ct_checkedstep1vslead,
  ct_artworkacceptedstep1vslead,
  ct_artworkacceptedstep2vslead,
  ct_pprfapprovedmultistepvslead,
  ct_artworkacceptedmultistepvslead,
  ct_packagepprffilleddaysvslead,
  ct_packagepprfapproveddaysvslead,
  ct_packagedrafteddaysvslead,
  ct_packageawaccepteddaysvslead,
  dd_pprfapprovedmultistepflag,
  dd_artworkacceptedmultistepflag,
  dd_pprffilledstep1flag,
  dd_pprfapprovedstep1flag,
  dd_pprfapprovedstep2flag,
  dd_pprfacceptedstep1flag,
  dd_draftedstep1flag,
  dd_checkedstep1flag,
  dd_artworkacceptedstep1flag,
  dd_artworkacceptedstep2flag,
  dim_packagereleaseddateid,
  ct_packagereleaseddays,
  dd_packagereleasedflag,
  ct_packagereleaseddaysvslead,
  dim_basedateid,
  dd_packagesteps,
  dd_nextpackagesteps,
  dd_componentstatussteps,
  dd_nextcomponentstatussteps,
  dd_tasksteps,
  dd_nexttasksteps,
  ct_artreleasedstep1,
  ct_artreleasedstepdaysvslead,
  dd_artreleasedlead,
  dd_component_type_of_change,
  dd_component_erp_code_old,
  dd_component_erp_code_new,
  dd_component_local_product_name,
  dd_actual_mockup,
  dd_pprf_pprf_opening_date,
  dd_pprf_deadline_details,
  dd_ppif_filling_co_end_dt,
  dd_ppif_approval_ps_end_dt,
  dd_ppif_approval_lra_end_dt,
  dd_ppif_release_sqa_end_dt,
  dim_ppif_filling_co_end_dateid,
  dim_ppif_approval_ps_end_dateid,
  dim_ppif_approval_lra_end_dateid,
  dim_ppif_release_sqa_end_dateid,
  dim_pprf_pprf_opening_dateid,
  dim_global_status_dateid,
  dd_globalstatuslead,
  ct_numberofdaysinparkinglot,
  ct_numberofdaystocompletion,
  ct_globalstatusdays,
  ct_globalstatusdaysvslead,
  dd_globalstatusflag)
select
  (select dim_projectsourceid * multiplier 
	 from dim_projectsource) + row_number() over (order by '') as fact_artworksnoid ,
  ifnull(wkf.artwork_id,'Not Set') as DD_ARTWORK_ID,
  ifnull(wkf.ppif_version_tree_root_obj,'Not Set') as DD_PPIF_VERSION_TREE_ROOT_OBJ,
  ifnull(wkf.ppif_id,'Not Set') as DD_PPIF_ID,
  ifnull(wkf.ppif_product_family,'Not Set') as DD_PPIF_PRODUCT_FAMILY,
  ifnull(wkf.ppif_erp_product_code,'Not Set') as DD_PPIF_ERP_PRODUCT_CODE,
  ifnull(wkf.ppif_version,'Not Set') as DD_PPIF_VERSION,
  ifnull(wkf.ppif_is_current_version,'Not Set') as DD_PPIF_IS_CURRENT_VERSION,
  ifnull(wkf.ppif_erp_product_designation,'Not Set') as DD_PPIF_ERP_PRODUCT_DESIGNATION,
  ifnull(wkf.ppif_status,'Not Set') as DD_PPIF_STATUS,
  ifnull(wkf.ppif_country,'Not Set') as DD_PPIF_COUNTRY,
  ifnull(wkf.ppif_country_region,'Not Set') as DD_PPIF_COUNTRY_REGION,
  ifnull(wkf.component_version_tree_root,'Not Set') as DD_COMPONENT_VERSION_TREE_ROOT,
  ifnull(wkf.component_id,'Not Set') as DD_COMPONENT_ID,
  ifnull(wkf.component_erp_code,'Not Set') as DD_COMPONENT_ERP_CODE,
  ifnull(wkf.component_product_family,'Not Set') as DD_COMPONENT_PRODUCT_FAMILY,
  ifnull(wkf.component_version,'Not Set') as DD_COMPONENT_VERSION,
  ifnull(wkf.component_is_current_version,'Not Set') as DD_COMPONENT_IS_CURRENT_VERSION,
  ifnull(wkf.component_erp_designation,'Not Set') as DD_COMPONENT_ERP_DESIGNATION,
  ifnull(wkf.component_type,'Not Set') as DD_COMPONENT_TYPE,
  ifnull(wkf.component_company_logos,'Not Set') as DD_COMPONENT_COMPANY_LOGOS,
  ifnull(wkf.component_anti_counterfeiting,'Not Set') as DD_COMPONENT_ANTI_COUNTERFEITING,
  ifnull(wkf.component_status,'Not Set') as DD_COMPONENT_STATUS,
  ifnull(wkf.component_doc_status,'Not Set') as DD_COMPONENT_DOC_STATUS,
  ifnull(wkf.component_packaging_site,'Not Set') as DD_COMPONENT_PACKAGING_SITE,
  ifnull(wkf.component_pprf_type_of_request,'Not Set') as DD_COMPONENT_PPRF_TYPE_OF_REQUEST,
  cast(1 as decimal(36,0))  as DIM_COMPONENT_CREATION_DATEID,
  cast(1 as decimal(36,0))  as DIM_COMP_LAST_MODIFIED_DATEID,
  ifnull(wkf.comp_lra_change_control_id,'Not Set') as DD_COMP_LRA_CHANGE_CONTROL_ID,
  ifnull(wkf.comp_lra_other_change_control,'Not Set') as DD_COMP_LRA_OTHER_CHANGE_CONTROL,
  cast(1 as decimal(36,0))  as DIM_PPIF_IMPL_DATE_AT_PACK_SITEID,
  cast(1 as decimal(36,0))  as DIM_PPIF_TARGET_MARKET_DATEID,
  ifnull(wkf.mpprf_id,'Not Set') as DD_MPPRF_ID,
  ifnull(wkf.mpprf_no,'Not Set') as DD_MPPRF_NO,
  ifnull(wkf.change_control,'Not Set') as DD_CHANGE_CONTROL,
  ifnull(wkf.mpprf_status,'Not Set') as DD_MPPRF_STATUS,
  ifnull(wkf.mpprf_is_current_version,'Not Set') as DD_MPPRF_IS_CURRENT_VERSION,
  ifnull(wkf.wf_workflow_id,'Not Set') as DD_WF_WORKFLOW_ID,
  ifnull(wkf.wf_status,'Not Set') as DD_WF_STATUS,
  cast(1 as decimal(36,0))  as DIM_WF_START_DATEID,
  ifnull(wkf.wf_initiator,'Not Set') as DD_WF_INITIATOR,
  ifnull(wkf.wf_initiator_role,'Not Set') as DD_WF_INITIATOR_ROLE,
  ifnull(wkf.wf_initiator_country,'Not Set') as DD_WF_INITIATOR_COUNTRY,
  cast(1 as decimal(36,0))  as DIM_AW_HA_APPROVAL_DATEID,
  cast(1 as decimal(36,0))  as DIM_AW_EST_APPROVAL_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_FILLING_REQ_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_FILLING_REQ_END_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_FILLING_REQ_1STSIGNOFF_DATEID,
  ifnull(wkf.pprf_approval_ps_name,'Not Set') as DD_PPRF_APPROVAL_PS_NAME,
  cast(1 as decimal(36,0))  as DIM_PPRF_APPROVAL_PS_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_APPROVAL_PS_END_DATEID,
  ifnull(wkf.pprf_approval_lra_name,'Not Set') as DD_PPRF_APPROVAL_LRA_NAME,
  cast(1 as decimal(36,0))  as DIM_PPRF_APPROVAL_LRA_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_APPROVAL_LRA_END_DATEID,
  ifnull(wkf.pprf_acceptance_co_name,'Not Set') as DD_PPRF_ACCEPTANCE_CO_NAME,
  cast(1 as decimal(36,0))  as DIM_PPRF_ACCEPTANCE_CO_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_ACCEPTANCE_CO_END_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_ABORTED_DATEID,
  cast(1 as decimal(36,0))  as DIM_PPRF_REQUESTED_DEADLINEID,
  cast(1 as decimal(36,0))  as DD_PPRF_VARIATION_APPROVAL_DATEID,
  ifnull(wkf.art_prod_de_name,'Not Set') as DD_ART_PROD_DE_NAME,
  cast(1 as decimal(36,0))  as DIM_ART_PROD_DE_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_PROD_DE_END_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_PROD_DE_1STSIGNOFF_DATEID,
  ifnull(wkf.art_prod_de_signoff_count,0) as CT_ART_PROD_DE_SIGNOFF_COUNT,
  cast(1 as decimal(36,0))  as DIM_ART_CHECKING_CO_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_CHECKING_CO_END_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_CHECKING_CO_1STSIGNOFF_DATEID,
  ifnull(wkf.art_approval_plh_name,'Not Set') as DD_ART_APPROVAL_PLH_NAME,
  cast(1 as decimal(36,0))  as DIM_ART_APPROVAL_PLH_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_APPROVAL_PLH_END_DATEID,
  ifnull(wkf.art_approval_ps_name,'Not Set') as DD_ART_APPROVAL_PS_NAME,
  cast(1 as decimal(36,0))  as DIM_ART_APPROVAL_PS_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_APPROVAL_PS_END_DATEID,
  ifnull(wkf.art_approval_lra_name,'Not Set') as DD_ART_APPROVAL_LRA_NAME,
  cast(1 as decimal(36,0))  as DIM_ART_APPROVAL_LRA_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_APPROVAL_LRA_END_DATEID,
  ifnull(wkf.art_accepted_co_name,'Not Set') as DD_ART_ACCEPTED_CO_NAME,
  cast(1 as decimal(36,0))  as DIM_ART_ACCEPTANCE_CO_START_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_ACCEPTANCE_CO_END_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_IMPL_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_TARGET_IMPL_DATEID,
  cast(1 as decimal(36,0))  as DIM_ART_RELEASED_DATEID,
  ifnull(wkf.art_released_version,'Not Set') as DD_ART_RELEASED_VERSION,
  ifnull(wkf.wf_pprf_start_mm_yyyy,'Not Set') as DD_WF_PPRF_START_MM_YYYY,
  ifnull(wkf.wf_right_first_time,0) as DD_WF_RIGHT_FIRST_TIME,
  ifnull(wkf.wf_lt1,0) as CT_WF_LT1,
  ifnull(wkf.wf_lt2,0) as CT_WF_LT2,
  ifnull(wkf.wf_lt3,0) as CT_WF_LT3,
  ifnull(wkf.wf_lt4,0) as CT_WF_LT4,
  ifnull(wkf.wf_lt5,0) as CT_WF_LT5,
  ifnull(wkf.wf_lt6,0) as CT_WF_LT6,
  ifnull(wkf.wf_lt7,0) as CT_WF_LT7,
  ifnull(wkf.wf_total_lt_art_mgnt_sys,0) as CT_WF_TOTAL_LT_ART_MGNT_SYS,
  ifnull(wkf.wf_lt_implementation,0) as CT_WF_LT_IMPLEMENTATION,
  ifnull(wkf.activities_name,'Not Set') as DD_ACTIVITIES_NAME,
  ifnull(wkf.activities_sent_by,'Not Set') as DD_ACTIVITIES_SENT_BY,
  cast(1 as decimal(36,0))  as DIM_ACTIVITIES_DATE_SENT,
  ifnull(wkf.activities_task_name,'Not Set') as DD_ACTIVITIES_TASK_NAME,
  ifnull(wkf.activities_task_subject,'Not Set') as DD_ACTIVITIES_TASK_SUBJECT,
  0 AS ct_pprffilledstep1,     
  0 AS ct_pprfapprovedstep1,   
  0 AS ct_pprfapprovedstep2,   
  0 AS ct_pprfacceptedstep1,   
  0 AS ct_draftedstep1,        
  0 AS ct_checkedstep1,        
  0 AS ct_artworkacceptedstep1,
  0 AS ct_artworkacceptedstep2,
  0 AS ct_pprfapprovedmultistep,
  0 AS ct_artworkacceptedmultistep,
  0 AS ct_packagepprffilleddays,
  0 AS ct_packagepprfapproveddays,
  0 AS ct_packagedrafteddays,
  0 AS ct_packageawaccepteddays,
  CAST(1 AS BIGINT) AS dim_packagepprffilleddateid,  
  CAST(1 AS BIGINT) AS dim_packagepprfapproveddateid,
  CAST(1 AS BIGINT) AS dim_packagedrafteddateid,     
  CAST(1 AS BIGINT) AS dim_packageawaccepteddateid,
  CAST(1 AS BIGINT) AS dim_pprfapprovedmultistepdateid,
  CAST(1 AS BIGINT) AS dim_artworkacceptedmultistepdateid,
  999 AS dd_packagepprffilledflag,
  999 AS dd_packagepprfapprovedflag,
  999 AS dd_packagedraftedflag,
  999 AS dd_packageawacceptedflag,
  'Not Set' AS dd_pprffilledlead,
  'Not Set' AS dd_package1lead,
  'Not Set' AS dd_pprfapprovalspslead,
  'Not Set' AS dd_pprfapprovalslralead,
  'Not Set' AS dd_pprfapprovalslead,
  'Not Set' AS dd_package2lead,
  'Not Set' AS dd_pprfacceptancecolead,
  'Not Set' AS dd_artproddeenddatelead,
  'Not Set' AS dd_package3lead,
  'Not Set' AS dd_artcheckcoenddatelead,
  'Not Set' AS dd_artapprovalpslead,
  'Not Set' AS dd_artapprovallralead,
  'Not Set' AS dd_artapprovalpsandlralead,
  'Not Set' AS dd_package4lead,
  'Not Set' AS dd_ppiffillinglead,
  'Not Set' AS dd_package5lead,
  'Not Set' AS dd_pscapprovallead,
  'Not Set' AS dd_package6lead,
  'Not Set' AS dd_lraapprovallead,
  'Not Set' AS dd_package7lead,
  'Not Set' AS dd_siteqappifreleaselead,
  'Not Set' AS dd_package8lead,
  'Not Set' AS dd_Package9lead,
  'Not Set' AS dd_totallead,
  0 AS ct_pprffilledstep1vslead,
  0 AS ct_pprfapprovedstep1vslead,
  0 AS ct_pprfapprovedstep2vslead,
  0 AS ct_pprfacceptedstep1vslead,
  0 AS ct_draftedstep1vslead,
  0 AS ct_checkedstep1vslead,
  0 AS ct_artworkacceptedstep1vslead,
  0 AS ct_artworkacceptedstep2vslead,
  0 AS ct_pprfapprovedmultistepvslead,
  0 AS ct_artworkacceptedmultistepvslead,
  0 AS ct_packagepprffilleddaysvslead,
  0 AS ct_packagepprfapproveddaysvslead,
  0 AS ct_packagedrafteddaysvslead,
  0 AS ct_packageawaccepteddaysvslead,
  999 AS dd_pprfapprovedmultistepflag,
  999 AS dd_artworkacceptedmultistepflag,
  999 AS dd_pprffilledstep1flag,
  999 AS dd_pprfapprovedstep1flag,
  999 AS dd_pprfapprovedstep2flag,
  999 AS dd_pprfacceptedstep1flag,
  999 AS dd_draftedstep1flag,
  999 AS dd_checkedstep1flag,
  999 AS dd_artworkacceptedstep1flag,
  999 AS dd_artworkacceptedstep2flag,
  CAST(1 AS BIGINT) AS dim_packagereleaseddateid,
  0 AS ct_packagereleaseddays,
  999 AS dd_packagereleasedflag,
  0 AS ct_packagereleaseddaysvslead,
  CAST(1 AS BIGINT) AS dim_basedateid,
  'Not Set' AS dd_packagesteps,
  'Not Set' AS dd_nextpackagesteps,
  'Not Set' AS dd_componentstatussteps,
  'Not Set' AS dd_nextcomponentstatussteps,
  'Not Set' AS dd_tasksteps,
  'Not Set' AS dd_nexttasksteps,
  0 AS ct_artreleasedstep1,
  0 AS ct_artreleasedstepdaysvslead,
  'Not Set' AS dd_artreleasedlead,
  ifnull(wkf.component_type_of_change,'Not Set') AS dd_component_type_of_change,
  ifnull(wkf.component_erp_code_old,'Not Set') AS dd_component_erp_code_old,
  ifnull(wkf.component_erp_code_new,'Not Set') AS dd_component_erp_code_new,
  ifnull(wkf.component_local_product_name,'Not Set') AS dd_component_local_product_name,
  ifnull(wkf.actual_mockup,'Not Set') AS dd_actual_mockup,
  ifnull(wkf.pprf_pprf_opening_date,'Not Set') AS dd_pprf_pprf_opening_date,
  ifnull(wkf.pprf_deadline_details,'Not Set') AS dd_pprf_deadline_details,
  ifnull(wkf.ppif_filling_co_end_dt,'Not Set') AS dd_ppif_filling_co_end_dt,
  ifnull(wkf.ppif_approval_ps_end_dt,'Not Set') AS dd_ppif_approval_ps_end_dt,
  ifnull(wkf.ppif_approval_lra_end_dt,'Not Set') AS dd_ppif_approval_lra_end_dt,
  ifnull(wkf.ppif_release_sqa_end_dt,'Not Set') AS dd_ppif_release_sqa_end_dt,
  CAST(1 AS BIGINT) AS dim_ppif_filling_co_end_dateid,
  CAST(1 AS BIGINT) AS dim_ppif_approval_ps_end_dateid,
  CAST(1 AS BIGINT) AS dim_ppif_approval_lra_end_dateid,
  CAST(1 AS BIGINT) AS dim_ppif_release_sqa_end_dateid,
  CAST(1 AS BIGINT) AS dim_pprf_pprf_opening_dateid,
  CAST(1 AS BIGINT) AS dim_global_status_dateid,
  'Not Set' AS dd_globalstatuslead,
  0 AS ct_numberofdaysinparkinglot,
  0 AS ct_numberofdaystocompletion,
  0 AS ct_globalstatusdays,
  0 AS ct_globalstatusdaysvslead,
  999 AS dd_globalstatusflag
from WKF_PPIF_COMPONENT AS wkf;

UPDATE fact_artworksno AS f
SET f.dim_ppif_filling_co_end_dateid = ifnull(dd.dim_dateid,1)
FROM fact_artworksno AS f, dim_date AS dd
WHERE left(f.dd_ppif_filling_co_end_dt, 10) = dd.datevalue
  AND dd.companycode = 'Not Set';

UPDATE fact_artworksno AS f
SET f.dim_ppif_approval_ps_end_dateid = ifnull(dd.dim_dateid,1)
FROM fact_artworksno AS f, dim_date AS dd
WHERE left(f.dd_ppif_approval_ps_end_dt, 10) = dd.datevalue
  AND dd.companycode = 'Not Set';

UPDATE fact_artworksno AS f
SET f.dim_ppif_approval_lra_end_dateid = ifnull(dd.dim_dateid,1)
FROM fact_artworksno AS f, dim_date AS dd
WHERE left(f.dd_ppif_approval_lra_end_dt, 10) = dd.datevalue
  AND dd.companycode = 'Not Set';

UPDATE fact_artworksno AS f
SET f.dim_ppif_release_sqa_end_dateid = ifnull(dd.dim_dateid,1)
FROM fact_artworksno AS f, dim_date AS dd
WHERE left(f.dd_ppif_release_sqa_end_dt, 10) = dd.datevalue
  AND dd.companycode = 'Not Set';

UPDATE fact_artworksno AS f
SET f.dim_pprf_pprf_opening_dateid = ifnull(dd.dim_dateid,1)
FROM fact_artworksno AS f, dim_date AS dd
WHERE left(f.dd_pprf_pprf_opening_date, 10) = dd.datevalue
  AND dd.companycode = 'Not Set';

update fact_artworksno f
 set DIM_COMPONENT_CREATION_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.component_creation_date= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
	and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_COMP_LAST_MODIFIED_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.comp_last_modified_date= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPIF_IMPL_DATE_AT_PACK_SITEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.ppif_impl_date_at_pack_site= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPIF_TARGET_MARKET_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.ppif_target_market_date= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_WF_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.wf_start_date= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_AW_HA_APPROVAL_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.aw_ha_approval_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_AW_EST_APPROVAL_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.aw_est_approval_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_FILLING_REQ_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_filling_req_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_FILLING_REQ_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_filling_req_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_FILLING_REQ_1STSIGNOFF_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_filling_req_1stsignoff_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_APPROVAL_PS_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_approval_ps_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_APPROVAL_PS_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_approval_ps_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_APPROVAL_LRA_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_approval_lra_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_APPROVAL_LRA_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_approval_lra_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_ACCEPTANCE_CO_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_acceptance_co_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_ACCEPTANCE_CO_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_acceptance_co_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_ABORTED_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_aborted_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_PPRF_REQUESTED_DEADLINEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_requested_deadline= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DD_PPRF_VARIATION_APPROVAL_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.pprf_variation_approval_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_PROD_DE_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_prod_de_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_PROD_DE_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_prod_de_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_PROD_DE_1STSIGNOFF_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_prod_de_1stsignoff_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_CHECKING_CO_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_checking_co_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_CHECKING_CO_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_checking_co_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_CHECKING_CO_1STSIGNOFF_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_checking_co_1stsignoff_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_APPROVAL_PLH_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_approval_plh_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_APPROVAL_PLH_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_approval_plh_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_APPROVAL_PS_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_approval_ps_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_APPROVAL_PS_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_approval_ps_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_APPROVAL_LRA_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_approval_lra_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_APPROVAL_LRA_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_approval_lra_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_ACCEPTANCE_CO_START_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_acceptance_co_start_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_ACCEPTANCE_CO_END_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_acceptance_co_end_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_IMPL_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_impl_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_TARGET_IMPL_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_target_impl_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ART_RELEASED_DATEID = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.art_released_dt= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;

update fact_artworksno f
 set DIM_ACTIVITIES_DATE_SENT = ifnull(dim_dateid,1)
from fact_artworksno f, wkf_ppif_component wkf, dim_Date dt
where wkf.activities_date_sent= dt.datevalue
 and wkf.PPIF_ERP_PRODUCT_CODE = f.DD_PPIF_ERP_PRODUCT_CODE
 and wkf.PPIF_COUNTRY = f.DD_PPIF_COUNTRY
 and wkf.COMPONENT_ERP_CODE = f.DD_COMPONENT_ERP_CODE
and wkf.artwork_id = f.DD_ARTWORK_ID;


/* BEGIN - base date logic */

DROP TABLE IF EXISTS tmp_basedatelogic;
CREATE TABLE tmp_basedatelogic
AS
SELECT
  fact_artworksnoid,
  dim_aw_ha_approval_dateid,
  dim_art_impl_dateid,
  CAST(1 AS BIGINT) AS dim_basedateid,
  CAST('0001-01-01' AS DATE) AS minus180days,
  CAST(1 AS BIGINT) AS dim_minus180daysdateid
FROM 
  fact_artworksno;

UPDATE tmp_basedatelogic AS t
SET t.minus180days = 
  CASE 
    WHEN dt.datevalue > '0001-06-30' THEN dt.datevalue - 180
    ELSE '0001-01-01'
  END
FROM tmp_basedatelogic AS t, dim_date as dt
WHERE t.dim_art_impl_dateid = dt.dim_dateid;

UPDATE tmp_basedatelogic AS t
SET t.dim_minus180daysdateid = dt.dim_dateid
FROM tmp_basedatelogic AS t, dim_date as dt
WHERE dt.datevalue = t.minus180days
  AND dt.companycode = 'Not Set';

UPDATE tmp_basedatelogic AS t
SET t.dim_basedateid = 
  CASE 
    WHEN dim_aw_ha_approval_dateid = 1 AND dim_art_impl_dateid = 1
      THEN 1
    WHEN dim_aw_ha_approval_dateid = 1 AND dim_art_impl_dateid <> 1
      THEN dim_minus180daysdateid
    WHEN dim_aw_ha_approval_dateid <> 1
      THEN dim_aw_ha_approval_dateid
    ELSE 1
  END;

UPDATE fact_artworksno AS f
SET f.dim_basedateid = t.dim_basedateid
FROM fact_artworksno AS f, tmp_basedatelogic AS t
WHERE t.fact_artworksnoid = f.fact_artworksnoid;

/* END - base date logic */


/* BEGIN - update lead times from csv file */

UPDATE fact_artworksno AS f
SET f.dd_pprffilledlead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PPRF FILLED');

UPDATE fact_artworksno AS f
SET f.dd_package1lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 1');

UPDATE fact_artworksno AS f
SET f.dd_pprfapprovalspslead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PPRF APPROVALS PS');

UPDATE fact_artworksno AS f
SET f.dd_pprfapprovalslralead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PPRF APPROVALS LRA');

UPDATE fact_artworksno AS f
SET f.dd_pprfapprovalslead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PPRF APPROVALS');

UPDATE fact_artworksno AS f
SET f.dd_package2lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 2');

UPDATE fact_artworksno AS f
SET f.dd_pprfacceptancecolead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PPRF ACCEPTANCE CO');

UPDATE fact_artworksno AS f
SET f.dd_artproddeenddatelead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'ART PROD DE END DATE');

UPDATE fact_artworksno AS f
SET f.dd_package3lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 3');

UPDATE fact_artworksno AS f
SET f.dd_artcheckcoenddatelead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'ART CHECK CO END DATE');

UPDATE fact_artworksno AS f
SET f.dd_artapprovalpslead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'ART APPROVAL PS');

UPDATE fact_artworksno AS f
SET f.dd_artapprovallralead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'ART APPROVAL LRA');

UPDATE fact_artworksno AS f
SET f.dd_artapprovalpsandlralead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'ART APPROVAL PS & LRA');

UPDATE fact_artworksno AS f
SET f.dd_package4lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 4');

UPDATE fact_artworksno AS f
SET f.dd_ppiffillinglead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PPIF FILLING');

UPDATE fact_artworksno AS f
SET f.dd_artreleasedlead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'ART RELEASED');

UPDATE fact_artworksno AS f
SET f.dd_package5lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 5');

UPDATE fact_artworksno AS f
SET f.dd_pscapprovallead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PSC APPROVAL');

UPDATE fact_artworksno AS f
SET f.dd_package6lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 6');

UPDATE fact_artworksno AS f
SET f.dd_lraapprovallead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'LRA APPROVAL');

UPDATE fact_artworksno AS f
SET f.dd_package7lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 7');

UPDATE fact_artworksno AS f
SET f.dd_siteqappifreleaselead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'SITE QA PPIF RELEASE');

UPDATE fact_artworksno AS f
SET f.dd_package8lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 8');

UPDATE fact_artworksno AS f
SET f.dd_Package9lead = 
  (SELECT art_lead 
  FROM csv_artwork_leadtimes 
  WHERE UPPER(art_step) = 'PACKAGE 9');

UPDATE fact_artworksno AS f
SET f.dd_totallead = 
  CAST(
    CAST(dd_package1lead AS INTEGER) +
    CAST(dd_package2lead AS INTEGER) +
    CAST(dd_package3lead AS INTEGER) +
    CAST(dd_package4lead AS INTEGER) +
    CAST(dd_package5lead AS INTEGER)
  AS VARCHAR(7));

UPDATE fact_artworksno AS f
SET f.dd_globalstatuslead = f.dd_totallead;
/* END - update lead times from csv file */



/* BEGIN - update step number of days */

UPDATE fact_artworksno AS f
SET f.ct_pprffilledstep1 = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - d1.businessdaysseqno
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_basedateid = d1.dim_dateid
  AND f.dim_pprf_filling_req_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_pprfapprovedstep1 = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_filling_req_end_dateid = d1.dim_dateid
  AND f.dim_pprf_approval_ps_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_pprfapprovedstep2 =
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_filling_req_end_dateid = d1.dim_dateid
  AND f.dim_pprf_approval_lra_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_pprfacceptedstep1 =  
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END - 
  CASE 
    WHEN d2.businessdaysseqno > d3.businessdaysseqno THEN d2.businessdaysseqno
    ELSE
      CASE 
        WHEN d3.businessdaysseqno = 1 THEN 
          (SELECT dt.businessdaysseqno 
          FROM dim_date AS dt 
          WHERE dt.datevalue = CURRENT_DATE
          AND dt.companycode = 'Not Set')
        ELSE d3.businessdaysseqno
      END
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2, dim_date AS d3
WHERE f.dim_pprf_acceptance_co_end_dateid = d1.dim_dateid
  AND f.dim_pprf_approval_ps_end_dateid = d2.dim_dateid
  AND f.dim_pprf_approval_lra_end_dateid = d3.dim_dateid;

UPDATE fact_artworksno AS f 
SET f.ct_draftedstep1 = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_acceptance_co_end_dateid = d1.dim_dateid
  AND f.dim_art_prod_de_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f 
SET f.ct_checkedstep1 =
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_art_prod_de_end_dateid = d1.dim_dateid
  AND f.dim_art_checking_co_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f 
SET f.ct_artworkacceptedstep1 =
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_art_checking_co_end_dateid = d1.dim_dateid
  AND f.dim_art_approval_ps_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f 
SET f.ct_artworkacceptedstep2 =
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_art_checking_co_end_dateid = d1.dim_dateid
  AND f.dim_art_approval_lra_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_artreleasedstep1 = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - d1.businessdaysseqno
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_basedateid = d1.dim_dateid
  AND f.dim_art_released_dateid = d2.dim_dateid;

/* END - update step number of days */



/* BEGIN - update multistep number of days */

UPDATE fact_artworksno AS f
SET f.ct_pprfapprovedmultistep = 
  CASE
    WHEN d2.datevalue = '0001-01-01' OR d3.datevalue = '0001-01-01' THEN  
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    WHEN d2.datevalue >= d3.datevalue THEN
      d2.businessdaysseqno
    ELSE
      d3.businessdaysseqno
  END -
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2, dim_date AS d3
WHERE f.dim_pprf_filling_req_end_dateid = d1.dim_dateid
  AND f.dim_pprf_approval_ps_end_dateid = d2.dim_dateid
  AND f.dim_pprf_approval_lra_end_dateid = d3.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_artworkacceptedmultistep = 
  CASE
    WHEN d2.datevalue = '0001-01-01' OR d3.datevalue = '0001-01-01' THEN  
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    WHEN d2.datevalue >= d3.datevalue THEN
      d2.businessdaysseqno
    ELSE
      d3.businessdaysseqno
  END -
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2, dim_date AS d3
WHERE f.dim_art_checking_co_end_dateid = d1.dim_dateid
  AND f.dim_art_approval_ps_end_dateid = d2.dim_dateid
  AND f.dim_art_approval_lra_end_dateid = d3.dim_dateid;

/* END - update multistep number of days */



/* BEGIN - update multi step dates */

UPDATE fact_artworksno AS f
SET f.dim_pprfapprovedmultistepdateid =
  CASE
    WHEN d1.datevalue = '0001-01-01' OR d2.datevalue = '0001-01-01' THEN  
      (SELECT dd.dim_dateid
      FROM dim_date AS dd
      WHERE dd.datevalue = '0001-01-01'
      AND dd.companycode = 'Not Set')
    WHEN d1.datevalue >= d2.datevalue THEN
      d1.dim_dateid
    ELSE
      d2.dim_dateid
  END 
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_approval_ps_end_dateid = d1.dim_dateid
  AND f.dim_pprf_approval_lra_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.dim_artworkacceptedmultistepdateid =
  CASE
    WHEN d1.datevalue = '0001-01-01' OR d2.datevalue = '0001-01-01' THEN  
      (SELECT dd.dim_dateid
      FROM dim_date AS dd
      WHERE dd.datevalue = '0001-01-01'
      AND dd.companycode = 'Not Set')
    WHEN d1.datevalue >= d2.datevalue THEN
      d1.dim_dateid
    ELSE
      d2.dim_dateid
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_art_approval_ps_end_dateid = d1.dim_dateid
  AND f.dim_art_approval_lra_end_dateid = d2.dim_dateid;

/* END - update multi step dates */



/* BEGIN - update package dates */

UPDATE fact_artworksno AS f
SET f.dim_packagepprffilleddateid = f.dim_pprf_filling_req_end_dateid;

UPDATE fact_artworksno AS f
SET f.dim_packagepprfapproveddateid =
  CASE
    WHEN d1.datevalue = '0001-01-01' OR d2.datevalue = '0001-01-01' THEN  
      (SELECT dd.dim_dateid
      FROM dim_date AS dd
      WHERE dd.datevalue = '0001-01-01'
      AND dd.companycode = 'Not Set')
    WHEN d1.datevalue >= d2.datevalue THEN
      d1.dim_dateid
    ELSE
      d2.dim_dateid
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_approval_ps_end_dateid = d1.dim_dateid
  AND f.dim_pprf_approval_lra_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.dim_packagedrafteddateid =
  CASE
    WHEN d1.datevalue = '0001-01-01' OR d2.datevalue = '0001-01-01' THEN  
      (SELECT dd.dim_dateid
      FROM dim_date AS dd
      WHERE dd.datevalue = '0001-01-01'
      AND dd.companycode = 'Not Set')
    WHEN d1.datevalue >= d2.datevalue THEN
      d1.dim_dateid
    ELSE
      d2.dim_dateid
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_acceptance_co_end_dateid = d1.dim_dateid
  AND f.dim_art_prod_de_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.dim_packageawaccepteddateid =
  CASE
    WHEN d1.datevalue = '0001-01-01' OR 
      d2.datevalue = '0001-01-01' OR 
      d3.datevalue = '0001-01-01' THEN  
        (SELECT dd.dim_dateid
        FROM dim_date AS dd
        WHERE dd.datevalue = '0001-01-01'
        AND dd.companycode = 'Not Set')
    WHEN d1.datevalue >= d2.datevalue AND d1.datevalue >= d3.datevalue THEN
      d1.dim_dateid
    WHEN d2.datevalue >= d3.datevalue AND d2.datevalue >= d1.datevalue THEN
      d2.dim_dateid
    WHEN d3.datevalue >= d1.datevalue AND d3.datevalue >= d2.datevalue THEN
      d3.dim_dateid
    ELSE d1.dim_dateid
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2, dim_date AS d3
WHERE f.dim_art_checking_co_end_dateid = d1.dim_dateid
  AND f.dim_art_approval_ps_end_dateid = d2.dim_dateid
  AND f.dim_art_approval_lra_end_dateid = d3.dim_dateid;

UPDATE fact_artworksno AS f
SET f.dim_packagereleaseddateid =
  CASE
    WHEN d1.datevalue = '0001-01-01' OR d2.datevalue = '0001-01-01' THEN  
      (SELECT dd.dim_dateid
      FROM dim_date AS dd
      WHERE dd.datevalue = '0001-01-01'
      AND dd.companycode = 'Not Set')
    WHEN d1.datevalue >= d2.datevalue THEN
      d1.dim_dateid
    ELSE
      d2.dim_dateid
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_artworkacceptedmultistepdateid = d1.dim_dateid
  AND f.dim_art_released_dateid = d2.dim_dateid;

/* END - update package dates */



/* BEGIN - update package number of days */

UPDATE fact_artworksno AS f
SET f.ct_packagepprffilleddays = f.ct_pprffilledstep1;

UPDATE fact_artworksno AS f
SET f.ct_packagepprfapproveddays =
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_packagepprffilleddateid = d1.dim_dateid
  AND f.dim_packagepprfapproveddateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_packagedrafteddays = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_packagepprfapproveddateid = d1.dim_dateid
  AND f.dim_packagedrafteddateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_packageawaccepteddays = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_packagedrafteddateid = d1.dim_dateid
  AND f.dim_packageawaccepteddateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_packagereleaseddays = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_packageawaccepteddateid = d1.dim_dateid
  AND f.dim_packagereleaseddateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_globalstatusdays = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_basedateid = d1.dim_dateid
  AND f.dim_packagereleaseddateid = d2.dim_dateid;

/* END - update package number of days */



/* BEGIN - update step, multistep and package logic flags */

UPDATE fact_artworksno AS f
SET f.dd_packagepprffilledflag = 
  CASE
    WHEN ct_packagepprffilleddays > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PACKAGE 1') THEN 0
    WHEN ct_packagepprffilleddays >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 1')
      AND ct_packagepprffilleddays <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 1')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_packagepprfapprovedflag = 
  CASE
    WHEN ct_packagepprfapproveddays > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PACKAGE 2') THEN 0
    WHEN ct_packagepprfapproveddays >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 2')
      AND ct_packagepprfapproveddays <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 2')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_packagedraftedflag = 
  CASE
    WHEN ct_packagedrafteddays > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PACKAGE 3') THEN 0
    WHEN ct_packagedrafteddays >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 3')
      AND ct_packagedrafteddays <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 3')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_packageawacceptedflag = 
  CASE
    WHEN ct_packageawaccepteddays > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PACKAGE 4') THEN 0
    WHEN ct_packageawaccepteddays >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 4')
      AND ct_packageawaccepteddays <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 4')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_packagereleasedflag = 
  CASE
    WHEN ct_packagereleaseddays > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PACKAGE 5') THEN 0
    WHEN ct_packagereleaseddays >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 5')
      AND ct_packagereleaseddays <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PACKAGE 5')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_pprfapprovedmultistepflag = 
  CASE
    WHEN ct_pprfapprovedmultistep > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PPRF APPROVALS') THEN 0
    WHEN ct_pprfapprovedmultistep >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF APPROVALS')
      AND ct_pprfapprovedmultistep <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF APPROVALS')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_artworkacceptedmultistepflag = 
  CASE
    WHEN ct_artworkacceptedmultistep > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'ART APPROVAL PS & LRA') THEN 0
    WHEN ct_artworkacceptedmultistep >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART APPROVAL PS & LRA')
      AND ct_artworkacceptedmultistep <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART APPROVAL PS & LRA')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_pprffilledstep1flag = 
  CASE
    WHEN ct_pprffilledstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PPRF FILLED') THEN 0
    WHEN ct_pprffilledstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF FILLED')
      AND ct_pprffilledstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF FILLED')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_pprfapprovedstep1flag = 
  CASE
    WHEN ct_pprfapprovedstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PPRF APPROVALS PS') THEN 0
    WHEN ct_pprfapprovedstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF APPROVALS PS')
      AND ct_pprfapprovedstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF APPROVALS PS')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_pprfapprovedstep2flag = 
  CASE
    WHEN ct_pprfapprovedstep2 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PPRF APPROVALS LRA') THEN 0
    WHEN ct_pprfapprovedstep2 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF APPROVALS LRA')
      AND ct_pprfapprovedstep2 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF APPROVALS LRA')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_pprfacceptedstep1flag = 
  CASE
    WHEN ct_pprfacceptedstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'PPRF ACCEPTANCE CO') THEN 0
    WHEN ct_pprfacceptedstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF ACCEPTANCE CO')
      AND ct_pprfacceptedstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'PPRF ACCEPTANCE CO')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_draftedstep1flag = 
  CASE
    WHEN ct_draftedstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'ART PROD DE END DATE') THEN 0
    WHEN ct_draftedstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART PROD DE END DATE')
      AND ct_draftedstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART PROD DE END DATE')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_checkedstep1flag = 
  CASE
    WHEN ct_checkedstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'ART CHECK CO END DATE') THEN 0
    WHEN ct_checkedstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART CHECK CO END DATE')
      AND ct_checkedstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART CHECK CO END DATE')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_artworkacceptedstep1flag = 
  CASE
    WHEN ct_artworkacceptedstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'ART APPROVAL PS') THEN 0
    WHEN ct_artworkacceptedstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART APPROVAL PS')
      AND ct_artworkacceptedstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART APPROVAL PS')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_artworkacceptedstep2flag = 
  CASE
    WHEN ct_artworkacceptedstep2 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'ART APPROVAL LRA') THEN 0
    WHEN ct_artworkacceptedstep2 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART APPROVAL LRA')
      AND ct_artworkacceptedstep2 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART APPROVAL LRA')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_artreleasedstepflag = 
  CASE
    WHEN ct_artreleasedstep1 > 
      (SELECT art_lead 
      FROM csv_artwork_leadtimes 
      WHERE UPPER(art_step) = 'ART RELEASED') THEN 0
    WHEN ct_artreleasedstep1 >= 0.7 * (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART RELEASED')
      AND ct_artreleasedstep1 <= (SELECT art_lead FROM csv_artwork_leadtimes WHERE UPPER(art_step) = 'ART RELEASED')
      THEN 1
    ELSE 2
  END      
;

UPDATE fact_artworksno AS f
SET f.dd_globalstatusflag = 
  CASE
    WHEN ct_globalstatusdays > CAST(dd_globalstatuslead AS INTEGER) THEN 0
    WHEN ct_globalstatusdays >= 0.7 * CAST(dd_globalstatuslead AS INTEGER)
      AND ct_globalstatusdays <= CAST(dd_globalstatuslead AS INTEGER)
      THEN 1
    ELSE 2
  END      
;
/* END - update step, multistep and package logic flags */



/* BEGIN - update flags against component logic */

UPDATE fact_artworksno AS f
SET 
  f.dd_packagedraftedflag = 999,
  f.dd_packageawacceptedflag = 999,
  f.dd_artworkacceptedstep2flag = 999,
  f.dd_artworkacceptedstep1flag = 999,
  f.dd_checkedstep1flag = 999,
  f.dd_draftedstep1flag = 999,
  f.dd_pprfacceptedstep1flag = 999,
  f.dd_artworkacceptedmultistepflag = 999,
  f.dd_packagereleasedflag = 999,
  f.dd_artreleasedstepflag = 999
WHERE UPPER(f.dd_component_status) IN 
  ('PPRF FILLED',
  'REQUESTED');

UPDATE fact_artworksno AS f
SET 
  f.dd_packageawacceptedflag = 999,
  f.dd_artworkacceptedstep2flag = 999,
  f.dd_artworkacceptedstep1flag = 999,
  f.dd_checkedstep1flag = 999,
  f.dd_artworkacceptedmultistepflag = 999,
  f.dd_packagereleasedflag = 999,
  f.dd_artreleasedstepflag = 999
WHERE UPPER(f.dd_component_status) IN 
  ('PPRF APPROVED',
  'PPRF ACCEPTED',
  'IN PROCESS');

UPDATE fact_artworksno AS f
SET 
  f.dd_packagereleasedflag = 999,
  f.dd_artreleasedstepflag = 999
WHERE UPPER(f.dd_component_status) IN 
  ('DRAFTED',
  'CHECKED',
  'AW ACCEPTANCE',
  'PARKING LOT IN',
  'PARKING LOT OUT');

/* END - update flags against component logic */



/* BEGIN - days vs lead measures */

UPDATE fact_artworksno AS f
SET f.ct_pprffilledstep1vslead = 
  f.ct_pprffilledstep1 - CAST(f.dd_pprffilledlead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_pprfapprovedstep1vslead = 
  f.ct_pprfapprovedstep1 - CAST(f.dd_pprfapprovalspslead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_pprfapprovedstep2vslead = 
  f.ct_pprfapprovedstep2 - CAST(f.dd_pprfapprovalslralead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_pprfacceptedstep1vslead = 
  f.ct_pprfacceptedstep1 - CAST(f.dd_pprfacceptancecolead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_draftedstep1vslead = 
  f.ct_draftedstep1 - CAST(f.dd_artproddeenddatelead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_checkedstep1vslead = 
  f.ct_checkedstep1 - CAST(f.dd_artcheckcoenddatelead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_artworkacceptedstep1vslead = 
  f.ct_artworkacceptedstep1 - CAST(f.dd_artapprovalpslead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_artworkacceptedstep2vslead = 
  f.ct_artworkacceptedstep2 - CAST(f.dd_artapprovallralead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_pprfapprovedmultistepvslead = 
  f.ct_pprfapprovedmultistep - CAST(f.dd_pprfapprovalslead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_artworkacceptedmultistepvslead = 
  f.ct_artworkacceptedmultistep - 
  CAST(f.dd_artapprovalpsandlralead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_packagepprffilleddaysvslead = 
  f.ct_packagepprffilleddays - CAST(f.dd_package1lead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_packagepprfapproveddaysvslead = 
  f.ct_packagepprfapproveddays - CAST(f.dd_package2lead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_packagedrafteddaysvslead = 
  f.ct_packagedrafteddays - CAST(f.dd_package3lead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_packageawaccepteddaysvslead = 
  f.ct_packageawaccepteddays - CAST(f.dd_package4lead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_packagereleaseddaysvslead = 
  f.ct_packagereleaseddays - CAST(f.dd_package5lead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_artreleasedstepdaysvslead = 
  f.ct_artreleasedstep1 - CAST(f.dd_artreleasedlead AS INTEGER);

UPDATE fact_artworksno AS f
SET f.ct_globalstatusdaysvslead = 
  f.ct_globalstatusdays - CAST(f.dd_globalstatuslead AS INTEGER);

/* END - days vs lead measures */


/* BEGIN - (next) packages, (next) components, (next) task logic */

UPDATE fact_artworksno AS f
SET f.dd_packagesteps = 
  CASE 
    WHEN upper(f.dd_component_status) = 'REQUESTED'
      THEN 'Package PPRF Filled'
    WHEN upper(f.dd_component_status) = 'IN PROCESS'
      THEN 'Package PPRF Approved'
    WHEN upper(f.dd_component_status) = 'PARKING LOT IN'
      THEN 'Package Drafted'
    WHEN upper(f.dd_component_status) = 'PARKING LOT OUT'
      THEN 'Package Drafted'
    WHEN upper(f.dd_component_status) = 'AW ACCEPTANCE'
      THEN 'Package AW Accepted'
    WHEN upper(f.dd_component_status) = 'PPIF INITIATED'
      THEN 'Package AW Accepted'
    WHEN upper(f.dd_component_status) = 'PPRF FILLED'
      THEN 'Package PPRF Filled'
    WHEN upper(f.dd_component_status) = 'PPRF APPROVED'
      THEN 'Package PPRF Approved'
    WHEN upper(f.dd_component_status) = 'PPRF ACCEPTED'
      THEN 'Package Drafted'
    WHEN upper(f.dd_component_status) = 'DRAFTED'
      THEN 'Package Drafted'
    WHEN upper(f.dd_component_status) = 'CHECKED'
      THEN 'Package AW Accepted'
    WHEN upper(f.dd_component_status) = 'ARTWORK ACCEPTED'
      THEN 'Package AW Accepted'
    WHEN upper(f.dd_component_status) = 'RELEASED'
      THEN 'Package Released'
    ELSE 'Not Set'
  END
;


UPDATE fact_artworksno AS f
SET f.dd_nextpackagesteps = 
  CASE 
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF FILLED'
      THEN 'Package PPRF Approved'
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF APPROVED'
      THEN 'Package Drafted'
    WHEN upper(dd_packagesteps) = 'PACKAGE DRAFTED'
      THEN 'Package AW Accepted'
    WHEN upper(dd_packagesteps) = 'PACKAGE AW ACCEPTED'
      THEN 'Package Released'
    WHEN upper(dd_packagesteps) = 'PACKAGE RELEASED'
      THEN ''
    ELSE 'Not Set'
  END
;

UPDATE fact_artworksno AS f
SET f.dd_componentstatussteps = 
  CASE 
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF FILLED'
      THEN 'PPRF Filled'
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF APPROVED'
      THEN 'PPRF Approved'
    WHEN upper(dd_packagesteps) = 'PACKAGE DRAFTED'
      THEN CONCAT('PPRF Accepted ','** ','Drafted')
    WHEN upper(dd_packagesteps) = 'PACKAGE AW ACCEPTED'
      THEN CONCAT('Checked ','** ','Artwork Accepted')
    WHEN upper(dd_packagesteps) = 'PACKAGE RELEASED'
      THEN 'Released'
    ELSE 'Not Set'
  END
;

UPDATE fact_artworksno AS f
SET f.dd_nextcomponentstatussteps = 
  CASE 
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF FILLED'
      THEN 'PPRF Approved'
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF APPROVED'
      THEN 'PPRF Accepted'
    WHEN upper(dd_packagesteps) = 'PACKAGE DRAFTED'
      THEN CONCAT('Drafted ','** ','Checked')
    WHEN upper(dd_packagesteps) = 'PACKAGE AW ACCEPTED'
      THEN CONCAT('Artwork Accepted ','** ','Released')
    WHEN upper(dd_packagesteps) = 'PACKAGE RELEASED'
      THEN ''
    ELSE 'Not Set'
  END
;

UPDATE fact_artworksno AS f
SET f.dd_tasksteps = 
  CASE 
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF FILLED'
      THEN 'PPRF Filling Req End Date'
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF APPROVED'
      THEN CONCAT('PPRF Approval PS End Date ','** ','PPRF Approval LRA End Date')
    WHEN upper(dd_packagesteps) = 'PACKAGE DRAFTED'
      THEN CONCAT('PPRF Acceptance Co End Date ','** ',' Art Prod DE End Date')
    WHEN upper(dd_packagesteps) = 'PACKAGE AW ACCEPTED'
      THEN CONCAT('Art Checking Co End Date ','** ','Art Approval PS End Date ','** ','Art Approval LRA End Date')
    WHEN upper(dd_packagesteps) = 'PACKAGE RELEASED'
      THEN 'Art Released Date'
    ELSE 'Not Set'
  END
;

UPDATE fact_artworksno AS f
SET f.dd_nexttasksteps = 
  CASE 
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF FILLED'
      THEN CONCAT('PPRF Approval PS End Date ','** ','PPRF Approval LRA End Date')
    WHEN upper(dd_packagesteps) = 'PACKAGE PPRF APPROVED'
      THEN 'PPRF Acceptance Co End Date'
    WHEN upper(dd_packagesteps) = 'PACKAGE DRAFTED'
      THEN CONCAT('Art Prod DE End Date ','** ','Art Checking Co End Date')
    WHEN upper(dd_packagesteps) = 'PACKAGE AW ACCEPTED'
      THEN CONCAT('Art Approval PS End Date ','** ','Art Approval LRA End Date ','** ','Art Released Date')
    WHEN upper(dd_packagesteps) = 'PACKAGE RELEASED'
      THEN ''
    ELSE 'Not Set'
  END
;

/* END - (next) packages, (next) components, (next) task logic */



/* BEGIN - global status logic */

DROP TABLE IF EXISTS tmp_globalstatusdatelogic;
CREATE TABLE tmp_globalstatusdatelogic
AS
SELECT
  fact_artworksnoid,
  dim_basedateid,
  CAST(1 AS BIGINT) AS dim_global_status_dateid,
  CAST('0001-01-01' AS DATE) AS plus30days,
  CAST(1 AS BIGINT) AS dim_plus30daysdateid
FROM 
  fact_artworksno;
  
UPDATE tmp_globalstatusdatelogic AS t
SET t.plus30days = 
  CASE 
    WHEN dt.datevalue <= '9999-12-01' THEN dt.datevalue + 30
    ELSE '0001-01-01'
  END
FROM tmp_globalstatusdatelogic AS t, dim_date as dt
WHERE t.dim_basedateid = dt.dim_dateid;

UPDATE tmp_globalstatusdatelogic AS t
SET t.dim_plus30daysdateid = dt.dim_dateid
FROM tmp_globalstatusdatelogic AS t, dim_date as dt
WHERE dt.datevalue = t.plus30days
  AND dt.companycode = 'Not Set';
  
UPDATE tmp_globalstatusdatelogic AS t
SET t.dim_global_status_dateid = 
  CASE 
    WHEN dim_basedateid = 1
      THEN (SELECT dim_dateid FROM dim_date WHERE datevalue = CURRENT_DATE AND companycode = 'Not Set')
    ELSE dim_plus30daysdateid
  END;

UPDATE fact_artworksno AS f
SET f.dim_global_status_dateid = t.dim_global_status_dateid
FROM fact_artworksno AS f, tmp_globalstatusdatelogic AS t
WHERE t.fact_artworksnoid = f.fact_artworksnoid;
/* END - global status logic */


/* BEGIN - Average Number of Completion */ 

UPDATE fact_artworksno AS f
SET f.ct_numberofdaysinparkinglot = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_art_approval_plh_start_dateid = d1.dim_dateid
  AND f.dim_art_approval_plh_end_dateid = d2.dim_dateid;

UPDATE fact_artworksno AS f
SET f.ct_numberofdaystocompletion = 
  CASE 
    WHEN d2.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d2.businessdaysseqno
  END - 
  CASE 
    WHEN d1.businessdaysseqno = 1 THEN 
      (SELECT dt.businessdaysseqno 
      FROM dim_date AS dt 
      WHERE dt.datevalue = CURRENT_DATE
      AND dt.companycode = 'Not Set')
    ELSE d1.businessdaysseqno
  END -
  f.ct_numberofdaysinparkinglot
FROM fact_artworksno AS f, dim_date AS d1, dim_date AS d2 
WHERE f.dim_pprf_filling_req_start_dateid = d1.dim_dateid  
  AND f.dim_art_released_dateid = d2.dim_dateid;

/* END - Average Number of Completion */
