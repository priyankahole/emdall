/******************************************************************************************************************/
/*   Script         : bi_populate_gamed_fact                                                                      */
/*   Author         : Cornelia                                                                                    */
/*   Created On     : 07 Mar 2017                                                                                 */
/*   Description    : Populating script of fact_gamed                                                             */
/*********************************************Change History*******************************************************/
/*   Date                By             Version      Desc                                                         */
/*   07 Mar 2017         Cornelia       1.0          Creating the script.                                         */
/*   04 Apr 2017         CristianT      1.1          Fix for Total Qty, Scrap Qty, Duration Prod AIM              */
/*   06 Apr 2017         CristianT      1.2          Adding dummy rows for Rate & Quality loss status             */
/*   13 Apr 2017         Cornelia       1.3          Add prdsts_duration in Hours:Minutes:Secconds                */
/*   29 Aug 2018         CristianT      1.4          Adding Remark field                                          */
/*   05 Oct 2018         CristianT      1.5          APP-10489 Requirements for Darmstadt                         */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_oee_prod_status_extract;
CREATE TABLE tmp_oee_prod_status_extract
AS
SELECT *
FROM oee_prod_status_extract
WHERE 1 = 0;

INSERT INTO tmp_oee_prod_status_extract(
SITE_NAME,
MACHINE_ID,
MACHINE_NAME,
PSHIFT_ID,
SHIFT_NR,
DATI_SHIFT,
STATUS_CLS_NAME,
STATUS_CLS_DESC,
STATUS_NAME,
STATUS_DESC,
PRDSTS_DATI_FROM,
PRDSTS_DATI_TO,
ORDER_ID,
ORDER_NUMBER,
ORDER_DESCRIPTION,
TOOL_ID,
PART_ID,
PART_NAME,
PART_DESCRIPTION,
FORMAT_ID,
FORMAT_NAME,
FORMAT_DESCRIPTION,
PRDSTS_DURATION,
PRODUCTION_DURATION,
LOSS_TYPE,
AVAIL_LOSS_DURATION,
USE_LOSS_DURATION,
ALL_LOSSES,
AVAILABILITY_LOSSES,
REMARK
)
SELECT 
SITE_NAME,
MACHINE_ID,
MACHINE_NAME,
PSHIFT_ID,
SHIFT_NR,
DATI_SHIFT,
STATUS_CLS_NAME,
STATUS_CLS_DESC,
STATUS_NAME,
STATUS_DESC,
PRDSTS_DATI_FROM,
PRDSTS_DATI_TO,
ORDER_ID,
ORDER_NUMBER,
ORDER_DESCRIPTION,
TOOL_ID,
PART_ID,
PART_NAME,
PART_DESCRIPTION,
FORMAT_ID,
FORMAT_NAME,
FORMAT_DESCRIPTION,
PRDSTS_DURATION,
PRODUCTION_DURATION,
LOSS_TYPE,
AVAIL_LOSS_DURATION,
USE_LOSS_DURATION,
ALL_LOSSES,
AVAILABILITY_LOSSES,
REMARK
FROM oee_prod_status_extract
UNION
SELECT 
SITE_NAME,
MACHINE_ID,
MACHINE_NAME,
PSHIFT_ID,
SHIFT_NR,
DATI_SHIFT,
STATUS_CLS_NAME,
STATUS_CLS_DESC,
STATUS_NAME,
STATUS_DESC,
PRDSTS_DATI_FROM,
PRDSTS_DATI_TO,
ORDER_ID,
ORDER_NUMBER,
ORDER_DESCRIPTION,
TOOL_ID,
PART_ID,
PART_NAME,
PART_DESCRIPTION,
FORMAT_ID,
FORMAT_NAME,
FORMAT_DESCRIPTION,
PRDSTS_DURATION,
PRODUCTION_DURATION,
LOSS_TYPE,
AVAIL_LOSS_DURATION,
USE_LOSS_DURATION,
ALL_LOSSES,
AVAILABILITY_LOSSES,
REMARK
FROM oee_prod_status_extract_prev_years;

DROP TABLE IF EXISTS tmp_oee_prod_sum_extract;
CREATE TABLE tmp_oee_prod_sum_extract
AS
SELECT *
FROM oee_prod_sum_extract
WHERE 1 = 0;

INSERT INTO tmp_oee_prod_sum_extract(
SITE_NAME,
MACHINE_ID,
MACHINE_NAME,
PSHIFT_ID,
SHIFT_NR,
DATI_SHIFT,
SHIFT_BEGIN,
SHIFT_END,
ORDER_ID,
ORDER_NUMBER,
ORDER_DESCRIPTION,
TOOL_ID,
PART_ID,
PART_NAME,
PART_DESCRIPTION,
FORMAT_ID,
FORMAT_NAME,
FORMAT_DESCRIPTION,
TOTAL_QUANTITY,
SCRAP_QUANTITY,
DURATION_PROD_AIM,
CADENCE_DURATION,
CADENCE_NOMINALE
)
SELECT 
SITE_NAME,
MACHINE_ID,
MACHINE_NAME,
PSHIFT_ID,
SHIFT_NR,
DATI_SHIFT,
SHIFT_BEGIN,
SHIFT_END,
ORDER_ID,
ORDER_NUMBER,
ORDER_DESCRIPTION,
TOOL_ID,
PART_ID,
PART_NAME,
PART_DESCRIPTION,
FORMAT_ID,
FORMAT_NAME,
FORMAT_DESCRIPTION,
TOTAL_QUANTITY,
SCRAP_QUANTITY,
DURATION_PROD_AIM,
CADENCE_DURATION,
CADENCE_NOMINALE
FROM oee_prod_sum_extract
UNION
SELECT 
SITE_NAME,
MACHINE_ID,
MACHINE_NAME,
PSHIFT_ID,
SHIFT_NR,
DATI_SHIFT,
SHIFT_BEGIN,
SHIFT_END,
ORDER_ID,
ORDER_NUMBER,
ORDER_DESCRIPTION,
TOOL_ID,
PART_ID,
PART_NAME,
PART_DESCRIPTION,
FORMAT_ID,
FORMAT_NAME,
FORMAT_DESCRIPTION,
TOTAL_QUANTITY,
SCRAP_QUANTITY,
DURATION_PROD_AIM,
CADENCE_DURATION,
CADENCE_NOMINALE
FROM oee_prod_sum_extract_prev_years;

DROP TABLE IF EXISTS tmp_fact_gamed;
CREATE TABLE tmp_fact_gamed
AS
SELECT *
FROM fact_gamed
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_gamed';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_gamed',IFNULL(MAX(fact_gamedID),0)
FROM tmp_fact_gamed;

INSERT INTO tmp_fact_gamed( 
fact_gamedid,
dd_site_name,
dd_machine_id,
dd_machine_name,
dd_pshift_id,
dd_shift_nr,
dd_dati_shift,
dd_status_cls_name,
dd_status_cls_desc,
dd_status_name,
dd_status_desc,
dd_prdsts_dati_from,
dd_prdsts_dati_to,
dd_order_id,
dd_order_number,
dd_order_description,
dd_tool_id,
dd_part_id,
dd_part_name,
dd_part_description,
dd_format_id,
dd_format_name,
dd_format_description,
ct_prdsts_duration,
ct_production_duration_stg,
dd_loss_type,
ct_avail_loss_duration_stg,
ct_use_loss_duration_stg,
dd_all_losses,
dd_availability_losses,
dd_shift_begin,
dd_shift_end,
ct_duration_prod_aim,
ct_total_quantity,
ct_scrap_quantity,
ct_cadence_duration,
ct_cadence_nominale,
amt_exchangerate_gbl,
amt_exchangerate,
DW_INSERT_DATE,
DW_UPDATE_DATE,
dim_projectsourceid,
dim_date_dati_shiftid,
dim_date_prdsts_dati_toid,
dim_prdsts_dati_fromid,
dim_shift_beginid,
dim_shift_endid,
dd_remark
)
SELECT 
(select ifnull(max_id,1) 
 from number_fountain 
 where table_name = 'tmp_fact_gamed') + row_number() over(order by '') AS fact_gamedID,
ifnull(st.site_name,'Not Set') as dd_site_name,
ifnull(substr(st.machine_id,0,6),0) as dd_machine_id,
ifnull(st.machine_name,'Not Set')as dd_machine_name,
ifnull(substr(st.pshift_id,0,6),0) as dd_pshift_id,
ifnull(st.shift_nr ,0) as dd_shift_nr,
ifnull(st.dati_shift,'0001-01-01') as dd_dati_shift,
ifnull(st.status_cls_name, 'Not Set') as dd_status_cls_name ,
ifnull(st.status_cls_desc, 'Not Set') as dd_status_cls_desc ,
ifnull(st.status_name ,'Not Set') as dd_status_name,
ifnull(st.status_desc ,'Not Set') as dd_status_desc,
ifnull(st.prdsts_dati_from ,'0001-01-01') as dd_prdsts_dati_from,
ifnull(st.prdsts_dati_to, '0001-01-01') as dd_prdsts_dati_to,
ifnull(st.order_id ,0) as dd_order_id,         
ifnull(st.order_number, 'Not Set') as dd_order_number,  
ifnull(st.order_description, 'Not Set') as dd_order_description,
ifnull(st.tool_id ,0) as  dd_tool_id,
ifnull(st.part_id ,0) as dd_part_id,
ifnull(st.part_name, 'Not Set') as dd_part_name,  
ifnull(st.part_description, 'Not Set') as dd_part_description,
ifnull(st.format_id ,0) as dd_format_id,
ifnull(st.format_name, 'Not Set') as dd_format_name,  
ifnull(st.format_description, 'Not Set') as dd_format_description,
ifnull(st.prdsts_duration ,0) as ct_prdsts_duration,
ifnull(st.production_duration ,0) as ct_production_duration_stg,
ifnull(st.loss_type,'Not Set') as dd_loss_type,
ifnull(st.avail_loss_duration,0) as ct_avail_loss_duration_stg,
ifnull(st.use_loss_duration ,0) as ct_use_loss_duration_stg,
ifnull(st.all_losses,'Not Set') as  dd_all_losses,
ifnull(st.availability_losses,'Not Set') as dd_availability_losses,
'0001-01-01' as dd_shift_begin, /* su */
'0001-01-01' as  dd_shift_end, /* su */
0 as ct_duration_prod_aim, /* su */
0 as ct_total_quantity, /* su */
0 as ct_scrap_quantity, /* su */
0 as ct_cadence_duration,  /* su */
0 as ct_cadence_nominale,  /* su */
1 as amt_exchangerate_gbl,
1 as amt_exchangerate,
current_timestamp as DW_INSERT_DATE, 
current_timestamp as DW_UPDATE_DATE, 
1 as dim_projectsourceid,
1 as dim_date_dati_shiftid,
1 as dim_date_prdsts_dati_toid,
1 as dim_prdsts_dati_fromid,
1 as dim_shift_beginid ,
1 as dim_shift_endid,
ifnull(st.remark, 'Not Set') as dd_remark
FROM  tmp_oee_prod_status_extract st ;

-- updates for Date dimensions 

UPDATE tmp_fact_gamed F
SET F.dim_date_dati_shiftid = D.DIM_DATEID
FROM  tmp_fact_gamed F, DIM_DATE D 
WHERE to_date(f.dd_dati_shift) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_date_dati_shiftid  <> D.DIM_DATEID;

UPDATE tmp_fact_gamed F
SET F.dim_date_prdsts_dati_toid = D.DIM_DATEID
FROM  tmp_fact_gamed F, DIM_DATE D 
WHERE to_date(f.dd_prdsts_dati_to) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_date_prdsts_dati_toid  <> D.DIM_DATEID;

UPDATE tmp_fact_gamed F
SET F.dim_prdsts_dati_fromid = D.DIM_DATEID
FROM  tmp_fact_gamed F, DIM_DATE D 
WHERE to_date(f.dd_prdsts_dati_from) = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_prdsts_dati_fromid  <> D.DIM_DATEID;

DELETE FROM fact_gamed;

INSERT INTO fact_gamed(
fact_gamedid,
dd_site_name,
dd_machine_id,
dd_machine_name,
dd_pshift_id,
dd_shift_nr,
dd_dati_shift,
dd_status_cls_name,
dd_status_cls_desc,
dd_status_name,
dd_status_desc,
dd_prdsts_dati_from,
dd_prdsts_dati_to,
dd_order_id,
dd_order_number,
dd_order_description,
dd_tool_id,
dd_part_id,
dd_part_name,
dd_part_description,
dd_format_id,
dd_format_name,
dd_format_description,
ct_prdsts_duration,
ct_production_duration_stg,
dd_loss_type,
ct_avail_loss_duration_stg,
ct_use_loss_duration_stg,
dd_all_losses,
dd_availability_losses,
dd_shift_begin,
dd_shift_end,
ct_duration_prod_aim,
ct_total_quantity,
ct_scrap_quantity,
ct_cadence_duration,
ct_cadence_nominale,
amt_exchangerate_gbl,
amt_exchangerate,
DW_INSERT_DATE,
DW_UPDATE_DATE,
dim_projectsourceid,
dim_date_dati_shiftid,
dim_date_prdsts_dati_toid,
dim_prdsts_dati_fromid,
dim_shift_beginid,
dim_shift_endid,
dd_remark
)
SELECT fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       dd_status_name,
       dd_status_desc,
       dd_prdsts_dati_from,
       dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       ct_prdsts_duration,
       ct_production_duration_stg,
       dd_loss_type,
       ct_avail_loss_duration_stg,
       ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       dd_shift_begin,
       dd_shift_end,
       ct_duration_prod_aim,
       ct_total_quantity,
       ct_scrap_quantity,
       ct_cadence_duration,
       ct_cadence_nominale,
       amt_exchangerate_gbl,
       amt_exchangerate,
       DW_INSERT_DATE,
       DW_UPDATE_DATE,
       dim_projectsourceid,
       dim_date_dati_shiftid,
       dim_date_prdsts_dati_toid,
       dim_prdsts_dati_fromid,
       dim_shift_beginid,
       dim_shift_endid,
       dd_remark
FROM tmp_fact_gamed;

DROP TABLE IF EXISTS tmp_fact_gamed;


/* Losses calculation */
DROP TABLE IF EXISTS Losses_calculation;
CREATE TABLE Losses_calculation
AS 
SELECT dd_site_name,
       dd_machine_name,
       dim_date_dati_shiftid,
       dd_shift_nr,
       dd_status_cls_name , 
       dd_status_cls_desc ,
       dd_order_id,
       sum(ct_prdsts_duration) as DurationInSeconds
FROM fact_gamed f,
     dim_date dt
WHERE f.dim_date_dati_shiftid = dt.dim_dateid
GROUP BY dd_site_name,dd_machine_name,dim_date_dati_shiftid,dd_shift_nr,dd_status_cls_name, dd_status_cls_desc, dt.datevalue,dd_order_id;


UPDATE fact_gamed f
SET f.ct_DurationInSeconds = ifnull(l.DurationInSeconds,0)
FROM fact_gamed f, Losses_calculation l
WHERE f.dd_site_name = l.dd_site_name
      AND f.dd_machine_name = l.dd_machine_name
      AND f.dim_date_dati_shiftid = l.dim_date_dati_shiftid
      AND f.dd_shift_nr = l.dd_shift_nr
      AND f.dd_status_cls_name = l.dd_status_cls_name
      AND f.dd_status_cls_desc = l.dd_status_cls_desc
      AND f.dd_order_id = l.dd_order_id
      AND f.ct_DurationInSeconds <> ifnull(l.DurationInSeconds,0);

-- edited for new structure

/* 06 Apr 2017 CristianT Start: Adding dummy rows for Rate & Quality loss status */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_gamed', ifnull(max(fact_gamedid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_gamed;

DROP TABLE IF EXISTS tmp_distinctmachinerows;
CREATE TABLE tmp_distinctmachinerows
AS
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed') + ROW_NUMBER() over(order by '') AS fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dim_date_dati_shiftid,
       dd_order_id,
	   dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       'Rate & Quality Loss' as dd_status_cls_name,
       'Rate & Quality Loss' as dd_status_cls_desc,
        dd_remark
FROM fact_gamed
GROUP BY dd_site_name,
         dd_machine_id,
         dd_machine_name,
         dd_pshift_id,
         dd_shift_nr,
         dd_dati_shift,
         dim_date_dati_shiftid,
         dd_order_id,
		 dd_order_number,
         dd_order_description,
         dd_tool_id,
         dd_part_id,
         dd_part_name,
         dd_part_description,
         dd_format_id,
         dd_format_name,
         dd_format_description,
         dd_remark;
         
INSERT INTO fact_gamed(
fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       dd_status_name,
       dd_status_desc,
       dd_prdsts_dati_from,
       dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       ct_prdsts_duration,
       ct_production_duration_stg,
       dd_loss_type,
       ct_avail_loss_duration_stg,
       ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       dd_shift_begin,
       dd_shift_end,
       ct_duration_prod_aim,
       ct_total_quantity,
       ct_scrap_quantity,
       ct_cadence_duration,
       ct_cadence_nominale,
       amt_exchangerate_gbl,
       amt_exchangerate,
       DW_INSERT_DATE,
       DW_UPDATE_DATE,
       dim_projectsourceid,
       dim_date_dati_shiftid,
       dim_date_prdsts_dati_toid,
       dim_prdsts_dati_fromid,
       dim_shift_beginid,
       dim_shift_endid,
       dd_remark
)
SELECT fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
	   dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       'Not Set' as dd_status_name,
       'Not Set' as dd_status_desc,
       '0001-01-01' as dd_prdsts_dati_from,
       '0001-01-01' as dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       0 as ct_prdsts_duration,
       0 as ct_production_duration_stg,
       'Not Set' as dd_loss_type,
       0 as ct_avail_loss_duration_stg,
       0 as ct_use_loss_duration_stg,
       'All Losses' as dd_all_losses,
       'Availability Losses' as dd_availability_losses,
       '0001-01-01' as dd_shift_begin,
       '0001-01-01' as dd_shift_end,
       0 as ct_duration_prod_aim,
       0 as ct_total_quantity,
       0 as ct_scrap_quantity,
       0 as ct_cadence_duration,
       0 as ct_cadence_nominale,
       0 as amt_exchangerate_gbl,
       0 as amt_exchangerate,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_projectsourceid,
       dim_date_dati_shiftid,
       1 as dim_date_prdsts_dati_toid,
       1 as dim_prdsts_dati_fromid,
       1 as dim_shift_beginid,
       1 as dim_shift_endid,
       dd_remark
FROM tmp_distinctmachinerows;
         
/* select rows from sum table that have order_id null and cannot be joined to fact_gamed*/
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_gamed', ifnull(max(fact_gamedid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_gamed;

DROP TABLE IF EXISTS tmp_dummyorderrows;
CREATE TABLE tmp_dummyorderrows
AS
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed') + ROW_NUMBER() over(order by '') AS fact_gamedid,
	   site_name as dd_site_name,
       machine_id as dd_machine_id,
       machine_name as dd_machine_name,
       pshift_id as dd_pshift_id,
       shift_nr	as dd_shift_nr,
       dati_shift as dd_dati_shift,
       dt.dim_dateid as dim_date_dati_shiftid,	   					
       ifnull(order_id,0) as dd_order_id,
	   ifnull(order_number,'Not Set') as dd_order_number,
       ifnull(order_description,'Not Set') as	 dd_order_description,	
       ifnull(tool_id,0) as dd_tool_id,
       ifnull(part_id,0) as dd_part_id,
       ifnull(part_name,'Not Set') as	 dd_part_name,
       ifnull(part_description,'Not Set') as dd_part_description,	
       ifnull(format_id,0) as	 dd_format_id,
       ifnull(format_name,'Not Set') as dd_format_name,
       ifnull(format_description,'Not Set') as dd_format_description,	
       'Rate & Quality Loss' as dd_status_cls_name,
       'Rate & Quality Loss' as dd_status_cls_desc
FROM tmp_oee_prod_sum_extract sm, dim_date dt
WHERE dt.datevalue = dati_shift
AND dt.companycode = 'Not Set'
AND order_id is null
GROUP BY site_name,
         machine_id,
         machine_name,
         pshift_id,
         shift_nr,
         dati_shift,
         dt.dim_dateid,
         order_id,
		 order_number,
         order_description,
         tool_id,
         part_id,
         part_name,
         part_description,
         format_id,
         format_name,
         format_description;


INSERT INTO fact_gamed(
fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       dd_status_name,
       dd_status_desc,
       dd_prdsts_dati_from,
       dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       ct_prdsts_duration,
       ct_production_duration_stg,
       dd_loss_type,
       ct_avail_loss_duration_stg,
       ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       dd_shift_begin,
       dd_shift_end,
       ct_duration_prod_aim,
       ct_total_quantity,
       ct_scrap_quantity,
       ct_cadence_duration,
       ct_cadence_nominale,
       amt_exchangerate_gbl,
       amt_exchangerate,
       DW_INSERT_DATE,
       DW_UPDATE_DATE,
       dim_projectsourceid,
       dim_date_dati_shiftid,
       dim_date_prdsts_dati_toid,
       dim_prdsts_dati_fromid,
       dim_shift_beginid,
       dim_shift_endid
)
SELECT fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
	   dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       'Not Set' as dd_status_name,
       'Not Set' as dd_status_desc,
       '0001-01-01' as dd_prdsts_dati_from,
       '0001-01-01' as dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       0 as ct_prdsts_duration,
       0 as ct_production_duration_stg,
       'Not Set' as dd_loss_type,
       0 as ct_avail_loss_duration_stg,
       0 as ct_use_loss_duration_stg,
       'All Losses' as dd_all_losses,
       'Availability Losses' as dd_availability_losses,
       '0001-01-01' as dd_shift_begin,
       '0001-01-01' as dd_shift_end,
       0 as ct_duration_prod_aim,
       0 as ct_total_quantity,
       0 as ct_scrap_quantity,
       0 as ct_cadence_duration,
       0 as ct_cadence_nominale,
       0 as amt_exchangerate_gbl,
       0 as amt_exchangerate,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_projectsourceid,
       dim_date_dati_shiftid,
       1 as dim_date_prdsts_dati_toid,
       1 as dim_prdsts_dati_fromid,
       1 as dim_shift_beginid,
       1 as dim_shift_endid

FROM tmp_dummyorderrows;

DROP TABLE IF EXISTS tmp_distinctmachinerows;
DROP TABLE IF EXISTS tmp_dummyorderrows;

/* Adding dummy rows for dd_status_cls_name = Production when this are not available for a date/site/machine/shift. This is to fix Duration% with Rate & Loss measure */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_gamed', ifnull(max(fact_gamedid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_gamed;

DROP TABLE IF EXISTS tmp_gamed_dummyproductionrows;
CREATE TABLE tmp_gamed_dummyproductionrows
AS 
SELECT dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dim_date_dati_shiftid,
       dd_order_id,
	   dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description
FROM fact_gamed tmp
WHERE tmp.dd_status_cls_desc = 'Production'
      AND tmp.dd_status_cls_name = 'PROD'
      AND tmp.dd_all_losses = 'All Losses'
      AND tmp.dd_availability_losses = 'Availability Losses'
GROUP BY dd_site_name,
         dd_machine_id,
         dd_machine_name,
         dd_pshift_id,
         dd_shift_nr,
         dd_dati_shift,
         dim_date_dati_shiftid,
         dd_order_id,
	     dd_order_number,
         dd_order_description,
         dd_tool_id,
         dd_part_id,
         dd_part_name,
         dd_part_description,
         dd_format_id,
         dd_format_name,
         dd_format_description;


drop table if exists tmp_dummyproductionrows;
CREATE TABLE tmp_dummyproductionrows
AS
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed') + ROW_NUMBER() over(order by '') AS fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dim_date_dati_shiftid,
       dd_order_id,
	   dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       'Production' as dd_status_cls_desc,
       'PROD' as dd_status_cls_name,
       'All Losses' as dd_all_losses,
       'Availability Losses' as dd_availability_losses
FROM fact_gamed fg
WHERE 1 = 1
      AND NOT EXISTS (SELECT 1
                      FROM tmp_gamed_dummyproductionrows tmp
                      WHERE tmp.dd_site_name = fg.dd_site_name
                            AND tmp.dd_machine_id = fg.dd_machine_id
                            AND tmp.dd_machine_name = fg.dd_machine_name
                            AND tmp.dd_pshift_id = fg.dd_pshift_id
                            AND tmp.dd_shift_nr = fg.dd_shift_nr
                            AND tmp.dd_dati_shift = fg.dd_dati_shift
                            AND tmp.dim_date_dati_shiftid = fg.dim_date_dati_shiftid
                            AND tmp.dd_order_id = fg.dd_order_id)
GROUP BY dd_site_name,
         dd_machine_id,
         dd_machine_name,
         dd_pshift_id,
         dd_shift_nr,
         dd_dati_shift,
         dim_date_dati_shiftid,
         dd_order_id,
	     dd_order_number,
         dd_order_description,
         dd_tool_id,
         dd_part_id,
         dd_part_name,
         dd_part_description,
         dd_format_id,
         dd_format_name,
         dd_format_description;

INSERT INTO fact_gamed(
fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       dd_status_name,
       dd_status_desc,
       dd_prdsts_dati_from,
       dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       ct_prdsts_duration,
       ct_production_duration_stg,
       dd_loss_type,
       ct_avail_loss_duration_stg,
       ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       dd_shift_begin,
       dd_shift_end,
       ct_duration_prod_aim,
       ct_total_quantity,
       ct_scrap_quantity,
       ct_cadence_duration,
       ct_cadence_nominale,
       amt_exchangerate_gbl,
       amt_exchangerate,
       DW_INSERT_DATE,
       DW_UPDATE_DATE,
       dim_projectsourceid,
       dim_date_dati_shiftid,
       dim_date_prdsts_dati_toid,
       dim_prdsts_dati_fromid,
       dim_shift_beginid,
       dim_shift_endid
)
SELECT fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
	   dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       'Not Set' as dd_status_name,
       'Not Set' as dd_status_desc,
       '0001-01-01' as dd_prdsts_dati_from,
       '0001-01-01' as dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       0 as ct_prdsts_duration,
       0 as ct_production_duration_stg,
       'Not Set' as dd_loss_type,
       0 as ct_avail_loss_duration_stg,
       0 as ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       '0001-01-01' as dd_shift_begin,
       '0001-01-01' as dd_shift_end,
       0 as ct_duration_prod_aim,
       0 as ct_total_quantity,
       0 as ct_scrap_quantity,
       0 as ct_cadence_duration,
       0 as ct_cadence_nominale,
       0 as amt_exchangerate_gbl,
       0 as amt_exchangerate,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_projectsourceid,
       dim_date_dati_shiftid,
       1 as dim_date_prdsts_dati_toid,
       1 as dim_prdsts_dati_fromid,
       1 as dim_shift_beginid,
       1 as dim_shift_endid

FROM tmp_dummyproductionrows;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_gamed', ifnull(max(fact_gamedid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_gamed;

drop table if exists tmp_dummyproductionorderrows;
CREATE TABLE tmp_dummyproductionorderrows
AS
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_gamed') + ROW_NUMBER() over(order by '') AS fact_gamedid,
	   site_name as dd_site_name,
       machine_id as dd_machine_id,
       machine_name as dd_machine_name,
       pshift_id as dd_pshift_id,
       shift_nr	as dd_shift_nr,
       dati_shift as dd_dati_shift,
       dt.dim_dateid as dim_date_dati_shiftid,	   					
       ifnull(order_id,0) as dd_order_id,
	   ifnull(order_number,'Not Set') as dd_order_number,
       ifnull(order_description,'Not Set') as	 dd_order_description,	
       ifnull(tool_id,0) as dd_tool_id,
       ifnull(part_id,0) as dd_part_id,
       ifnull(part_name,'Not Set') as	 dd_part_name,
       ifnull(part_description,'Not Set') as dd_part_description,	
       ifnull(format_id,0) as	 dd_format_id,
       ifnull(format_name,'Not Set') as dd_format_name,
       ifnull(format_description,'Not Set') as dd_format_description,
       'Production' as dd_status_cls_desc,
       'PROD' as dd_status_cls_name,
       'All Losses' as dd_all_losses,
       'Availability Losses' as dd_availability_losses
FROM tmp_oee_prod_sum_extract sm, dim_date dt
WHERE dt.datevalue = dati_shift
AND dt.companycode = 'Not Set'
AND sm.order_id IS NULL
AND NOT EXISTS (SELECT 1
                      FROM tmp_gamed_dummyproductionrows tmp
                      WHERE tmp.dd_site_name = sm.site_name
                            AND tmp.dd_machine_id = sm.machine_id
                            AND tmp.dd_machine_name = sm.machine_name
                            AND tmp.dd_pshift_id = sm.pshift_id
                            AND tmp.dd_shift_nr = sm.shift_nr
                            AND tmp.dd_dati_shift = sm.dati_shift
                            AND tmp.dim_date_dati_shiftid = dt.dim_dateid)
GROUP BY site_name,
         machine_id,
         machine_name,
         pshift_id,
         shift_nr,
         dati_shift,
         dt.dim_dateid,
         order_id,
		 order_number,
         order_description,
         tool_id,
         part_id,
         part_name,
         part_description,
         format_id,
         format_name,
         format_description;

INSERT INTO fact_gamed(
fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
       dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       dd_status_name,
       dd_status_desc,
       dd_prdsts_dati_from,
       dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       ct_prdsts_duration,
       ct_production_duration_stg,
       dd_loss_type,
       ct_avail_loss_duration_stg,
       ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       dd_shift_begin,
       dd_shift_end,
       ct_duration_prod_aim,
       ct_total_quantity,
       ct_scrap_quantity,
       ct_cadence_duration,
       ct_cadence_nominale,
       amt_exchangerate_gbl,
       amt_exchangerate,
       DW_INSERT_DATE,
       DW_UPDATE_DATE,
       dim_projectsourceid,
       dim_date_dati_shiftid,
       dim_date_prdsts_dati_toid,
       dim_prdsts_dati_fromid,
       dim_shift_beginid,
       dim_shift_endid,
       dd_remark
)
SELECT fact_gamedid,
       dd_site_name,
       dd_machine_id,
       dd_machine_name,
       dd_pshift_id,
       dd_shift_nr,
	   dd_dati_shift,
       dd_status_cls_name,
       dd_status_cls_desc,
       'Not Set' as dd_status_name,
       'Not Set' as dd_status_desc,
       '0001-01-01' as dd_prdsts_dati_from,
       '0001-01-01' as dd_prdsts_dati_to,
       dd_order_id,
       dd_order_number,
       dd_order_description,
       dd_tool_id,
       dd_part_id,
       dd_part_name,
       dd_part_description,
       dd_format_id,
       dd_format_name,
       dd_format_description,
       0 as ct_prdsts_duration,
       0 as ct_production_duration_stg,
       'Not Set' as dd_loss_type,
       0 as ct_avail_loss_duration_stg,
       0 as ct_use_loss_duration_stg,
       dd_all_losses,
       dd_availability_losses,
       '0001-01-01' as dd_shift_begin,
       '0001-01-01' as dd_shift_end,
       0 as ct_duration_prod_aim,
       0 as ct_total_quantity,
       0 as ct_scrap_quantity,
       0 as ct_cadence_duration,
       0 as ct_cadence_nominale,
       0 as amt_exchangerate_gbl,
       0 as amt_exchangerate,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       1 as dim_projectsourceid,
       dim_date_dati_shiftid,
       1 as dim_date_prdsts_dati_toid,
       1 as dim_prdsts_dati_fromid,
       1 as dim_shift_beginid,
       1 as dim_shift_endid,
       'Not Set' as dd_remark
FROM tmp_dummyproductionorderrows;


DROP TABLE IF EXISTS tmp_dummyproductionrows;
DROP TABLE IF EXISTS tmp_dummyproductionorderrows;

-------edited 
/* 06 Apr 2017 CristianT End */

/* 04 Apr 2017 CristianT Start: Fix for Total Qty, Scrap Qty, Duration Prod AIM */
/* All this values will be populated on rows with rate & quality status */
DROP TABLE IF EXISTS tmp_oee_groupedvalues;
CREATE TABLE tmp_oee_groupedvalues
AS
SELECT site_name,
       machine_id,
       pshift_id,
       dati_shift,
       IFNULL(order_id,0) AS order_id,
       sum(ifnull(total_quantity, 0)) 		as total_quantity,
       sum(ifnull(scrap_quantity, 0)) 		as scrap_quantity,
       sum(ifnull(duration_prod_aim, 0)) 	as duration_prod_aim,
	   sum(ifnull(cadence_duration,0)) 		as cadence_duration,
	   sum(ifnull(cadence_nominale,0)) 		as cadence_nominale
FROM tmp_oee_prod_sum_extract
GROUP BY site_name,
         machine_id,
         pshift_id,
         dati_shift,
         order_id;

DROP TABLE IF EXISTS tmp_gamed_minid;
CREATE TABLE tmp_gamed_minid
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       fct.dd_order_id as dd_order_id,
       min(fct.fact_gamedid) as fact_gamedid
FROM fact_gamed fct,
     dim_date dt
WHERE 	fct.dim_date_dati_shiftid = dt.dim_dateid
		and fct.dd_status_cls_desc = 'Rate & Quality Loss'
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue,
         fct.dd_order_id;

UPDATE fact_gamed f
SET f.ct_scrap_quantity = ifnull(qt.scrap_quantity,0)
FROM fact_gamed f,
	 tmp_oee_groupedvalues qt,
     tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_scrap_quantity <> ifnull(qt.scrap_quantity,0);
      

UPDATE fact_gamed f
SET f.ct_total_quantity = ifnull(qt.total_quantity,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_total_quantity <> ifnull(qt.total_quantity,0);	
	

UPDATE fact_gamed f
SET f.ct_duration_prod_aim = ifnull(qt.duration_prod_aim,0)
FROM fact_gamed f, 
	tmp_oee_groupedvalues qt,
    tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_duration_prod_aim <> ifnull(qt.duration_prod_aim,0);	


/* 04 Apr 2017 CristianT End */


UPDATE fact_gamed f
SET f.ct_cadence_duration = ifnull(qt.cadence_duration,0)
FROM fact_gamed f,
	 tmp_oee_groupedvalues qt,
     tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_cadence_duration <> ifnull(qt.cadence_duration,0);	


UPDATE fact_gamed f
SET f.ct_cadence_nominale = ifnull(qt.cadence_nominale,0)
FROM fact_gamed f,
	 tmp_oee_groupedvalues qt,
     tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_cadence_nominale <> ifnull(qt.cadence_nominale,0);	
            

---edited
/* 07 Apr 2017 CristianT Start: Change of logic to be applied for Duration % measure when dd_status_cls_desc = Production or dd_status_cls_desc = Rate and Quality */
/* CristianT Details of this logic: Based on the feedback from Chandu and Priyanka Duration % measure for dd_status_cls_desc = Production should match the OEE % value.
In order to achieve this i created 3 aditional columns to get the values from oee_prod_sum_extract table and to reproduce the OEE logic
*/
DROP TABLE IF EXISTS tmp_prod_minid;
CREATE TABLE tmp_prod_minid
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       fct.dd_order_id as dd_order_id,
       min(fct.fact_gamedid) as fact_gamedid
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc = 'Production'
      AND fct.dd_availability_losses <> 'Not Set'
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue,
         fct.dd_order_id;

UPDATE fact_gamed f
SET f.ct_scrap_quantity_prod_duration = ifnull(qt.scrap_quantity,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_scrap_quantity_prod_duration <> ifnull(qt.scrap_quantity,0);	

UPDATE fact_gamed f
SET f.ct_total_quantity_prod_duration = ifnull(qt.total_quantity,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_total_quantity_prod_duration <> ifnull(qt.total_quantity,0);	

UPDATE fact_gamed f
SET f.ct_aim_prod_duration = ifnull(qt.duration_prod_aim,0)
FROM fact_gamed f,
		tmp_oee_groupedvalues qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.site_name
      AND minid.dd_machine_id = qt.machine_id
      AND minid.dd_pshift_id = qt.pshift_id
      AND minid.datevalue = qt.dati_shift
      AND minid.dd_order_id = qt.order_id
      AND f.ct_aim_prod_duration <> ifnull(qt.duration_prod_aim,0);	

/* Calculating the total of ct_avail_loss_duration_stg and ct_production_duration_stg at site/machine/shift/date level to use in the Duration % logic when dd_status_cls_desc = Production */
DROP TABLE IF EXISTS tmp_aggregated_sum;
CREATE TABLE tmp_aggregated_sum
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       fct.dd_order_id as dd_order_id,
       sum(ct_avail_loss_duration_stg) as ct_total_avail_loss_duration,
       sum(ct_production_duration_stg) as ct_total_production_duration
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue,
         fct.dd_order_id;

UPDATE fact_gamed f
SET f.ct_total_avail_loss_duration = ifnull(qt.ct_total_avail_loss_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND minid.dd_order_id = qt.dd_order_id
      AND f.ct_total_avail_loss_duration <> ifnull(qt.ct_total_avail_loss_duration,0);	
      
UPDATE fact_gamed f
SET f.ct_total_production_duration = ifnull(qt.ct_total_production_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_prod_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND minid.dd_order_id = qt.dd_order_id
      AND f.ct_total_production_duration <> ifnull(qt.ct_total_production_duration,0);

/* For Rate and Loss status Duration % measure logic should be (Duration% when dd_status_cls_desc = Production) - OEE% value */
/* Added ct_prdsts_duration_rateloss to get the value from ct_prdsts_duration when dd_status_cls_desc = Production and put it on Rate and Loss lines */
/* Calculating the total of ct_avail_loss_duration_stg and ct_production_duration_stg at site/machine/shift/date level to use in the Duration % logic when dd_status_cls_desc = Rate and loss */
DROP TABLE IF EXISTS tmp_prod_rateloss;
CREATE TABLE tmp_prod_rateloss
AS
SELECT fct.dd_site_name as dd_site_name,
       fct.dd_machine_id as dd_machine_id,
       fct.dd_pshift_id as dd_pshift_id,
       fct.dim_date_dati_shiftid as dim_date_dati_shiftid,
       dt.datevalue as datevalue,
       fct.dd_order_id as dd_order_id,
       sum(fct.ct_prdsts_duration) as ct_prdsts_duration_rateloss
FROM fact_gamed fct,
     dim_date dt
WHERE fct.dim_date_dati_shiftid = dt.dim_dateid
      AND fct.dd_status_cls_desc = 'Production'
GROUP BY fct.dd_site_name,
         fct.dd_machine_id,
         fct.dd_pshift_id,
         fct.dim_date_dati_shiftid,
         dt.datevalue,
         fct.dd_order_id;

UPDATE fact_gamed f
SET f.ct_prdsts_duration_rateloss = ifnull(qt.ct_prdsts_duration_rateloss,0)
FROM fact_gamed f,
		tmp_prod_rateloss qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND minid.dd_order_id = qt.dd_order_id
      AND f.ct_prdsts_duration_rateloss <> ifnull(qt.ct_prdsts_duration_rateloss,0);

UPDATE fact_gamed f
SET f.ct_total_avail_loss_duration_rateloss = ifnull(qt.ct_total_avail_loss_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND minid.dd_order_id = qt.dd_order_id
      AND f.ct_total_avail_loss_duration_rateloss <> ifnull(qt.ct_total_avail_loss_duration,0);

UPDATE fact_gamed f
SET f.ct_total_production_duration_rateloss = ifnull(qt.ct_total_production_duration,0)
FROM fact_gamed f,
		tmp_aggregated_sum qt,
     	tmp_gamed_minid minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND minid.dd_site_name = qt.dd_site_name
      AND minid.dd_machine_id = qt.dd_machine_id
      AND minid.dd_pshift_id = qt.dd_pshift_id
      AND minid.datevalue = qt.datevalue
      AND minid.dd_order_id = qt.dd_order_id
      AND f.ct_total_production_duration_rateloss <> ifnull(qt.ct_total_production_duration,0);
	  
DROP TABLE IF EXISTS tmp_prod_rateloss;
DROP TABLE IF EXISTS tmp_aggregated_sum;	  
DROP TABLE IF EXISTS tmp_prod_minid;
DROP TABLE IF EXISTS tmp_oee_groupedvalues;
DROP TABLE IF EXISTS tmp_gamed_minid;
/* 07 Apr 2017 CristianT End */



/* Total duration percents by machine */
DROP TABLE IF EXISTS total_duration;   
CREATE TABLE total_duration
AS 
SELECT dd_machine_name,dd_machine_id,dim_date_dati_shiftid,
      sum(ct_prdsts_duration) as total_duration,
	  count(dd_machine_name) as ct_no_machine
FROM fact_gamed,
    dim_date dt
WHERE 1 = 1
     and dt.dim_dateid = dim_date_dati_shiftid
GROUP BY dd_machine_name,dd_machine_id,dim_date_dati_shiftid;


UPDATE fact_gamed f 
SET f.ct_prdts_duration_total = t.total_duration
FROM fact_gamed f, total_duration t
WHERE f.dd_machine_name = t.dd_machine_name
      AND f.dd_machine_id = t.dd_machine_id
      AND f.dim_date_dati_shiftid = t.dim_date_dati_shiftid;

DROP TABLE IF EXISTS duration_by_machine;
CREATE TABLE duration_by_machine
AS 
SELECT f.dd_machine_name,
       f.dd_machine_id,
       f.dim_date_dati_shiftid,
       sum(case when t.ct_no_machine = 0 then 0 else t.total_duration/t.ct_no_machine end) as duration_by_machine
FROM fact_gamed f, total_duration t
WHERE f.dd_machine_name = t.dd_machine_name
      AND f.dd_machine_id = t.dd_machine_id
      AND f.dim_date_dati_shiftid = t.dim_date_dati_shiftid
GROUP BY f.dd_machine_name,f.dd_machine_id,f.dim_date_dati_shiftid;


UPDATE fact_gamed f 
SET f.ct_duration_by_machine = t.duration_by_machine
FROM fact_gamed f , duration_by_machine t
WHERE f.dd_machine_name = t.dd_machine_name
      AND f.dd_machine_id = t.dd_machine_id
      AND f.dim_date_dati_shiftid = t.dim_date_dati_shiftid;

DROP TABLE IF EXISTS duration_by_machine;
DROP TABLE IF EXISTS total_duration;   

UPDATE fact_gamed f
set dd_batch = dd_order_description
from fact_gamed f
where ct_total_quantity<>0
and dd_order_description <>'Not Set';

/* Target Values*/

drop table if exists min_target_machine;

CREATE TABLE min_target_machine AS
SELECT 
MIN(fact_gamedid) AS fact_gamedid,
dd_site_name,
dd_machine_name,
dd_dati_shift,
target_month,
target_ytd
from fact_gamed
join target_machine_level on site = dd_site_name
	and machine = dd_machine_name
	and dati_shift  = dd_dati_shift
group by dd_site_name,
		dd_machine_name,
		dd_dati_shift,
		target_month,
		target_ytd
order by dd_site_name,
		dd_machine_name,
		dd_dati_shift;

UPDATE fact_gamed f
SET f.ct_target_month = ifnull(minid.target_month,0)
FROM fact_gamed f,
     	min_target_machine minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND f.ct_target_month <> ifnull(minid.target_month,0);

UPDATE fact_gamed f
SET f.ct_target_ytd = ifnull(minid.target_ytd,0)
FROM fact_gamed f,
     	min_target_machine minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND f.ct_target_ytd <> ifnull(minid.target_ytd,0);

drop table if exists min_target_site;

CREATE TABLE min_target_site AS
SELECT 
MIN(fact_gamedid) AS fact_gamedid,
dd_site_name,
dd_dati_shift,
target_month,
target_ytd
from fact_gamed
join target_site_level on site = dd_site_name
	and dati_shift  = dd_dati_shift
group by dd_site_name,
		dd_dati_shift,
		target_month,
		target_ytd
order by dd_site_name,
		dd_dati_shift;


UPDATE fact_gamed f
SET f.ct_target_month_site = ifnull(minid.target_month,0)
FROM fact_gamed f,
     	min_target_site minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND f.ct_target_month_site <> ifnull(minid.target_month,0);

UPDATE fact_gamed f
SET f.ct_target_ytd_site = ifnull(minid.target_ytd,0)
FROM fact_gamed f,
     	min_target_site minid
WHERE f.fact_gamedid = minid.fact_gamedid
      AND f.ct_target_ytd_site <> ifnull(minid.target_ytd,0);
      
/* 12.10.2017 Cristian C: creating filter for Setup Mean Time based on file oee_machine_setup_codes */

update fact_gamed 
set dd_setupmeantimefilter = 'Y'
from fact_gamed f, oee_machine_setup_codes sc
WHERE f.dd_site_name = sc.site_name
  AND f.dd_status_cls_name = sc.status_cls_name
  AND (f.dd_status_name = sc.status_name or sc.status_name = '*')
	AND dd_setupmeantimefilter <> 'Y';

--update Y rows that have a batch, because batches have values on the dummy rows that do not contain information on dd_status_name 
update fact_gamed 
set dd_setupmeantimefilter = 'Y'
from fact_gamed f
WHERE
ct_total_quantity<>0
and dd_order_description <>'Not Set'
AND dd_setupmeantimefilter <> 'Y';
/* 12.10.2017 Cristian C: END*/

/* 08.01.2018 Cristian C: add calendar week Monday - Sunday */

UPDATE fact_gamed F
SET dd_calweekMS = 
case when dd_dati_shift = '0001-01-01' 
then 'Not Set'
	when month(dd_dati_shift) =1 and week(dd_dati_shift)in(51, 52, 53) 
	then concat(year(dd_dati_shift)-1,lpad(week(dd_dati_shift),2,'0'))
	else concat(year(dd_dati_shift),lpad(week(dd_dati_shift),2,'0')) 
	end
FROM fact_gamed F
WHERE 
dd_calweekMS <> 
case when dd_dati_shift = '0001-01-01' 
then 'Not Set'
	when month(dd_dati_shift) =1 and week(dd_dati_shift)in(51, 52, 53) 
	then concat(year(dd_dati_shift)-1,lpad(week(dd_dati_shift),2,'0'))
	else concat(year(dd_dati_shift),lpad(week(dd_dati_shift),2,'0')) 
	end;
	
/* 08.01.2018 Cristian C: add calendar week Monday - Sunday */


drop table if exists min_target_machine;
drop table if exists min_target_site;
DROP TABLE IF EXISTS tmp_oee_prod_status_extract;
DROP TABLE IF EXISTS tmp_oee_prod_sum_extract;

/* 05 Oct 2018 CristianT Start: APP-10489 Requirements for Darmstadt */
/* Logic for Technical Losses By Month request by Oliver Kaerst - Point 12 in the user story */
DROP TABLE IF EXISTS tmp_calculatelosses;
CREATE TABLE tmp_calculatelosses
AS
SELECT min(fact_gamedid) as fact_gamedid,
       fg.dd_site_name,
       fg.dd_machine_name,
       fg.dd_format_name,
       fg.dd_shift_nr,
       dt.datevalue,
       sum(ct_prdsts_duration) as ct_total_day_duration,
       ifnull(sum(case when dd_status_cls_desc = 'Organisational Losses' then fg.ct_prdsts_duration end), 0) as ct_organisational_losses_day_duration,
       ifnull(sum(case when dd_status_cls_desc = 'Technical Losses' then fg.ct_prdsts_duration end), 0) as ct_technical_losses_day_duration,
       ifnull(sum(case when dd_status_cls_desc = 'Production' then fg.ct_prdsts_duration end), 0) as ct_production_day_duration,
       ifnull(sum(case when dd_status_cls_desc = 'Functional Losses' then fg.ct_prdsts_duration end), 0) as ct_functional_losses_day_duration,
       ifnull(sum(case when dd_status_cls_desc = 'Change Overs' then fg.ct_prdsts_duration end), 0) as ct_change_overs_day_duration
FROM fact_gamed fg,
     dim_date dt 
WHERE 1 = 1
      AND fg.dim_date_dati_shiftid = dt.dim_dateid 
      AND fg.dd_loss_type <> 'UL'
      AND ct_prdsts_duration <> 0
GROUP BY fg.dd_site_name,
         fg.dd_machine_name,
         fg.dd_format_name,
         fg.dd_shift_nr,
         dt.datevalue,
         fg.dd_status_cls_desc;

UPDATE fact_gamed fg
SET ct_total_day_duration = ifnull(tmp.ct_total_day_duration, 0),
    ct_organisational_losses_day_duration = ifnull(tmp.ct_organisational_losses_day_duration, 0),
    ct_technical_losses_day_duration = ifnull(tmp.ct_technical_losses_day_duration, 0),
    ct_production_day_duration = ifnull(tmp.ct_production_day_duration, 0),
    ct_functional_losses_day_duration = ifnull(tmp.ct_functional_losses_day_duration, 0),
    ct_change_overs_day_duration = ifnull(tmp.ct_change_overs_day_duration, 0)
FROM fact_gamed fg,
     tmp_calculatelosses tmp
WHERE fg.fact_gamedid = tmp.fact_gamedid;

DROP TABLE IF EXISTS tmp_calculatelosses;

/* 05 Oct 2018 CristianT End */

DELETE FROM emd586.fact_gamed;

INSERT INTO emd586.fact_gamed
SELECT *
FROM fact_gamed;
