
DELETE FROM number_fountain m
WHERE  m.table_name = 'fact_logisticcost_snapshot';

INSERT INTO number_fountain
SELECT 'fact_logisticcost_snapshot',
       IFNULL(Max(f.fact_logisticcost_snapshotid), IFNULL(
       (SELECT s.dim_projectsourceid *
               s.multiplier
        FROM   dim_projectsource s), 1)
       )
FROM  fact_logisticcost_snapshot f;

insert into fact_logisticcost_snapshot (fact_logisticcost_snapshotid, dim_dateidstartdate, dd_planning_item_id, dim_jda_locationid, dim_jda_productid, dd_cool_ambient, dd_grpvolumes, ct_packs_cf, dim_projectsourceid, dw_insert_date, dw_update_date, dd_cmg_id, ct_eq_units_cf, ct_packs_ambient_cf, ct_packs_cool_cf, dd_cost_type, dim_legalentityid, amt_freight_cost, amt_wh_cost, amt_demand_cost, ct_ambient_factor, ct_cool_factor, ct_freight_ratio, ct_wh_ratio, ct_ord_ratio, ct_eq_actual_total_cf, ct_count_key, amt_demand_monthly_cost, amt_wh_monthly_cost, amt_freight_monthly_cost, amt_freight_op_monthly_cost, amt_wh_op_monthly_cost, amt_demand_op_monthly_cost, dd_devgroup_flag, dim_gausscosttypeid, ct_packs_op, ct_eq_units_op, ct_packs_ambient_op, ct_packs_cool_op, ct_eq_actual_total_op, ct_eq_units, amt_wh_expected, amt_freight_expected, amt_demand_expected, amt_packs_cf, amt_packs_op, dd_flag_actfcst, dd_maxidflag, dim_dateidlastactmth, dd_ytdactual, amt_wh_corrected, amt_freight_corrected, amt_demand_corrected, ct_packs_opfrozen, ct_packs_ambient_opfrozen, ct_packs_cool_opfrozen, ct_eq_units_opfrozen, amt_packs_opfrozen, ct_eq_actual_total_opfrozen, ct_eq_units_frozen, amt_wh_expected_frozen, amt_freight_expected_frozen, amt_demand_expected_frozen, dd_year_flag)
select (select max_id from number_fountain where table_name = 'fact_jda_demandforecast') + row_number() over(order by '') as fact_logisticcost_snapshotid
    ,dim_dateidstartdate
    ,dd_planning_item_id
    ,dim_jda_locationid
    ,dim_jda_productid
    ,dd_cool_ambient
    ,dd_grpvolumes
    ,ct_packs_cf
    ,dim_projectsourceid
    ,dw_insert_date
    ,dw_update_date
    ,dd_cmg_id
    ,ct_eq_units_cf
    ,ct_packs_ambient_cf
    ,ct_packs_cool_cf
    ,dd_cost_type
    ,dim_legalentityid
    ,amt_freight_cost
    ,amt_wh_cost
    ,amt_demand_cost
    ,ct_ambient_factor
    ,ct_cool_factor
    ,ct_freight_ratio
    ,ct_wh_ratio
    ,ct_ord_ratio
    ,ct_eq_actual_total_cf
    ,ct_count_key
    ,amt_demand_monthly_cost
    ,amt_wh_monthly_cost
    ,amt_freight_monthly_cost
    ,amt_freight_op_monthly_cost
    ,amt_wh_op_monthly_cost
    ,amt_demand_op_monthly_cost
    ,dd_devgroup_flag
    ,dim_gausscosttypeid
    ,ct_packs_op
    ,ct_eq_units_op
    ,ct_packs_ambient_op
    ,ct_packs_cool_op
    ,ct_eq_actual_total_op
    ,ct_eq_units
    ,amt_wh_expected
    ,amt_freight_expected
    ,amt_demand_expected
    ,amt_packs_cf
    ,amt_packs_op
    ,dd_flag_actfcst
    ,dd_maxidflag
    ,dim_dateidlastactmth
    ,dd_ytdactual
    ,amt_wh_corrected
    ,amt_freight_corrected
    ,amt_demand_corrected
    ,ct_packs_opfrozen
    ,ct_packs_ambient_opfrozen
    ,ct_packs_cool_opfrozen
    ,ct_eq_units_opfrozen
    ,amt_packs_opfrozen
    ,ct_eq_actual_total_opfrozen
    ,ct_eq_units_frozen
    ,amt_wh_expected_frozen
    ,amt_freight_expected_frozen
    ,amt_demand_expected_frozen
    ,dd_year_flag
from fact_logisticcost;

update fact_logisticcost_snapshot f
	set f.dim_dateidsnapshot  = (select dim_dateid from dim_date where datevalue = current_Date and companycode = 'Not Set')
	where f.dim_dateidsnapshot = 1;
