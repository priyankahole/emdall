/*****************************************************************************************************************/
/*   Script         : exa_fact_mangoprintstatus                                                                  */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 14 Aug 2018                                                                                */
/*   Description    : Populating script of fact_mangoprintstatus                                                 */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   14 Aug 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_mangoprintstatus;
CREATE TABLE tmp_fact_mangoprintstatus
AS
SELECT *
FROM fact_mangoprintstatus
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_mangoprintstatus;
CREATE TABLE number_fountain_fact_mangoprintstatus LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_mangoprintstatus
SELECT 'fact_mangoprintstatus', ifnull(max(fact_mangoprintstatusid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_mangoprintstatus;


INSERT INTO tmp_fact_mangoprintstatus(
fact_mangoprintstatusid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_object_name,
dd_title,
dd_document_type,
dd_document_subtype,
dd_document_unit,
dd_page_range_and_reason,
dd_receipient_generated_copy,
dd_id_of_copy_generated,
dd_version_number,
dd_event_name,
dd_serverdateandtime,
dim_dateidserver,
dd_user_name,
dd_user_login_name,
dim_qualityusersiduser,
dd_author_site
)
SELECT (SELECT max_id from number_fountain_fact_mangoprintstatus WHERE table_name = 'fact_mangoprintstatus') + ROW_NUMBER() over(order by '') AS fact_mangoprintstatusid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(vfp.object_name, 'Not Set') as dd_object_name,
       ifnull(vfp.title, 'Not Set') as dd_title,
       ifnull(vfp.document_type, 'Not Set') as dd_document_type,
       ifnull(vfp.document_subtype, 'Not Set') as dd_document_subtype,
       ifnull(vfp.document_unit, 'Not Set') as dd_document_unit,
       ifnull(vfp.page_range_and_reason, 'Not Set') as dd_page_range_and_reason,
       ifnull(vfp.receipient_generated_copy, 'Not Set') as dd_receipient_generated_copy,
       ifnull(vfp.id_of_copy_generated, 'Not Set') as dd_id_of_copy_generated,
       ifnull(vfp.version_number, 'Not Set') as dd_version_number,
       ifnull(vfp.event_name, 'Not Set') as dd_event_name,
       ifnull(vfp.serverdateandtime, '0001-01-01 00:00:00') as dd_serverdateandtime,
       1 as dim_dateidserver,
       ifnull(vfp.user_name, 'Not Set') as dd_user_name,
       ifnull(vfp.user_login_name, 'Not Set') as dd_user_login_name,
       1 as dim_qualityusersiduser,
       ifnull(vfp.author_site, 'Not Set') as dd_author_site
FROM view_for_prints vfp;

UPDATE tmp_fact_mangoprintstatus f
SET dim_dateidserver = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoprintstatus f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_serverdateandtime)
      AND f.dim_dateidserver <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangoprintstatus f
SET dim_qualityusersiduser = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangoprintstatus f,
     dim_qualityusers d
WHERE f.dd_user_login_name = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersiduser  <> d.dim_qualityusersid;


DELETE FROM fact_mangoprintstatus;

INSERT INTO fact_mangoprintstatus(
fact_mangoprintstatusid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_object_name,
dd_title,
dd_document_type,
dd_document_subtype,
dd_document_unit,
dd_page_range_and_reason,
dd_receipient_generated_copy,
dd_id_of_copy_generated,
dd_version_number,
dd_event_name,
dd_serverdateandtime,
dim_dateidserver,
dd_user_name,
dd_user_login_name,
dim_qualityusersiduser,
dd_author_site
)
SELECT fact_mangoprintstatusid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_object_name,
       dd_title,
       dd_document_type,
       dd_document_subtype,
       dd_document_unit,
       dd_page_range_and_reason,
       dd_receipient_generated_copy,
       dd_id_of_copy_generated,
       dd_version_number,
       dd_event_name,
       dd_serverdateandtime,
       dim_dateidserver,
       dd_user_name,
       dd_user_login_name,
       dim_qualityusersiduser,
       dd_author_site
FROM tmp_fact_mangoprintstatus;

DROP TABLE IF EXISTS tmp_fact_mangoprintstatus;
DROP TABLE IF EXISTS number_fountain_fact_mangoprintstatus;
