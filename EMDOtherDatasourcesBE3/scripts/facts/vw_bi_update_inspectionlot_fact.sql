
DROP TABLE IF EXISTS tmp_dim_profitcenter;
CREATE TABLE tmp_dim_profitcenter 
AS	  
SELECT  dim_profitcenterid,
			  ControllingArea,
			  ProfitCenterCode,
			  MIN(ValidTo) as min_validto
             FROM dim_profitcenter,
			      qals
            WHERE  ControllingArea = QALS_KOKRS
              AND ProfitCenterCode = QALS_PRCTR
              AND ValidTo >= QALS_ERSTELDAT
              AND RowIsCurrent = 1
              GROUP BY  dim_profitcenterid,
			  ControllingArea,
			  ProfitCenterCode ;

/*
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa	   
   SET Dim_AccountCategoryid =
          (SELECT dim_accountcategoryid
             FROM dim_accountcategory ac
            WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set')
                  AND ac.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   and ifnull(Dim_AccountCategoryid,1) <>ifnull((SELECT dim_accountcategoryid FROM dim_accountcategory ac WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set') AND ac.RowIsCurrent = 1),2)*/

UPDATE fact_inspectionlot il
SET il.Dim_AccountCategoryid = ifnull(ac.dim_accountcategoryid,1)
FROM fact_inspectionlot il
	inner join qals q on  il.dd_inspectionlotno = q.QALS_PRUEFLOS	
        left join dim_accountcategory ac on ac.Category = ifnull(q.QALS_KNTTP, 'Not Set')
	                                and ac.RowIsCurrent = 1
 WHERE il.Dim_AccountCategoryid <> ac.dim_accountcategoryid;				  


   
	
/*	   
UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_costcenterid = IFNULL(
          (SELECT dim_costcenterid
             FROM dim_costcenter cc
            WHERE     cc.Code = ifnull(QALS_KOSTL, 'Not Set')
                  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
                  AND cc.RowIsCurrent = 1),1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_costcenterid,1)<> ifnull((SELECT dim_costcenterid FROM dim_costcenter cc WHERE cc.Code = ifnull(QALS_KOSTL, 'Not Set')  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set') AND cc.RowIsCurrent = 1),2)
*/

UPDATE fact_inspectionlot il
SET  il.dim_costcenterid = IFNULL(cc.dim_costcenterid, 'Not Set')
FROM 
    fact_inspectionlot il,
	qals q,
	dim_costcenter cc
WHERE   cc.Code = ifnull(QALS_KOSTL, 'Not Set')
    AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
    AND cc.RowIsCurrent = 1
    AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	AND  il.dim_costcenterid <> IFNULL(cc.dim_costcenterid, 'Not Set');
		  
		  
		  
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET        dim_dateidinspectionend = ifnull((SELECT dim_dateid
                   FROM dim_date ie
                  WHERE ie.DateValue = QALS_PAENDTERM
                        AND ie.CompanyCode = dp.CompanyCode
						AND  QALS_PAENDTERM IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidinspectionend,1) <> ifnull((SELECT dim_dateid FROM dim_date ie  WHERE ie.DateValue = QALS_PAENDTERM  AND ie.CompanyCode = dp.CompanyCode AND  QALS_PAENDTERM IS NOT NULL), 2)*/
	   

	   
	  
	   
MERGE INTO fact_inspectionlot fact
USING (SELECT fact_inspectionlotid, 
              ifnull(ie.dim_dateid,1) dim_dateidinspectionend,
			  ifnull(dis.dim_dateid,1) dim_dateidinspectionstart,
			  ifnull(kd.dim_dateid,1) dim_dateidkeydate,
			  ifnull(lcd.dim_dateid,1) dim_dateidlotcreated,
			  ifnull(pd.dim_dateid,1) dim_dateidposting
	   FROM (SELECT fact_inspectionlotid, dp.CompanyCode, 
	                q.QALS_PAENDTERM,       -- using for dim_dateidinspectionend
					q.QALS_PASTRTERM,		-- using for dim_dateidinspectionstart
					q.QALS_GUELTIGAB,       -- using for dim_dateidkeydate
					q.QALS_ENSTEHDAT,        -- using for dim_dateidlotcreated
					q.QALS_BUDAT            -- using for dim_dateidposting
             FROM fact_inspectionlot il 
					INNER JOIN qals q ON il.dd_inspectionlotno = q.QALS_PRUEFLOS 
					INNER JOIN dim_plant dp ON il.dim_plantid = dp.dim_plantid) t1 
		    LEFT JOIN dim_date ie  ON ie.DateValue = t1.QALS_PAENDTERM AND ie.CompanyCode = t1.CompanyCode
			LEFT JOIN dim_date dis ON dis.DateValue = t1.QALS_PASTRTERM AND dis.CompanyCode = t1.CompanyCode
			LEFT JOIN dim_date kd  ON kd.DateValue = t1.QALS_GUELTIGAB AND kd.CompanyCode = t1.CompanyCode
			LEFT JOIN dim_date lcd ON lcd.DateValue = t1.QALS_ENSTEHDAT AND lcd.CompanyCode = t1.CompanyCode
			LEFT JOIN dim_date pd ON pd.DateValue = t1.QALS_BUDAT AND pd.CompanyCode = t1.CompanyCode) src
ON fact.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE SET fact.dim_dateidinspectionend = src.dim_dateidinspectionend, 
                             fact.dim_dateidinspectionstart = src.dim_dateidinspectionstart,
							 fact.dim_dateidkeydate = src.dim_dateidkeydate,
							 fact.dim_dateidlotcreated = src.dim_dateidlotcreated,
							 fact.dim_dateidposting = src.dim_dateidposting;
							 
							 


	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_dateidinspectionstart = ifnull((SELECT dim_dateid
                   FROM dim_date dis
                  WHERE dis.DateValue = QALS_PASTRTERM
                        AND dis.CompanyCode = dp.CompanyCode
						AND QALS_PASTRTERM IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidinspectionstart,1) <> ifnull((SELECT dim_dateid  FROM dim_date dis  WHERE dis.DateValue = QALS_PASTRTERM   AND dis.CompanyCode = dp.CompanyCode AND QALS_PASTRTERM IS NOT NULL), 2)*/
	   




	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa						
    SET  dim_dateidkeydate = ifnull((SELECT dim_dateid
                   FROM dim_date kd
                  WHERE kd.DateValue = QALS_GUELTIGAB
                        AND kd.CompanyCode = dp.CompanyCode
						AND QALS_GUELTIGAB IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidkeydate,1) <> ifnull((SELECT dim_dateid FROM dim_date kd WHERE kd.DateValue = QALS_GUELTIGAB  AND kd.CompanyCode = dp.CompanyCode	AND QALS_GUELTIGAB IS NOT NULL), 2)*/
	   

	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_dateidlotcreated = ifnull(
                   (SELECT dim_dateid
                      FROM dim_date lcd
                     WHERE lcd.DateValue = QALS_ENSTEHDAT
                           AND lcd.CompanyCode = dp.CompanyCode
						   AND QALS_ENSTEHDAT IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidlotcreated,1) <> ifnull((SELECT dim_dateid FROM dim_date lcd WHERE lcd.DateValue = QALS_ENSTEHDAT  AND lcd.CompanyCode = dp.CompanyCode  AND QALS_ENSTEHDAT IS NOT NULL), 2)*/
	   
	   

	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_dateidposting = ifnull((SELECT dim_dateid
                   FROM dim_date pd
                  WHERE pd.DateValue = QALS_BUDAT
                        AND pd.companycode = dp.CompanyCode
						AND QALS_BUDAT IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(dim_dateidposting,1) = ifnull((SELECT dim_dateid  FROM dim_date pd WHERE pd.DateValue = QALS_BUDAT  AND pd.companycode = dp.CompanyCode	AND QALS_BUDAT IS NOT NULL), 2)
	   
	  QALS_AENDERDAT dim_dateidrecordchanged,
      QALS_ERSTELDAT dim_dateidrecordcreated
*/






	
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
     SET  dim_documenttypetextid =
          (SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.Type = ifnull(QALS_BLART, 'Not Set')
                  AND dtt.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il	   
SET  il.dim_documenttypetextid = dtt.dim_documenttypetextid
FROM  fact_inspectionlot il,
      qals q,
      dim_documenttypetext dtt
WHERE     dtt.Type = ifnull(QALS_BLART, 'Not Set')
      AND dtt.RowIsCurrent = 1	
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS	  
	  AND ifnull(il.dim_documenttypetextid,1) <> ifnull(dtt.dim_documenttypetextid,2);
	   
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_inspectionlotmiscid =
          (SELECT dim_inspectionlotmiscid
             FROM dim_inspectionlotmisc
            WHERE     AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
                  AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
                  AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
                  AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
                  AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
                  AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
                  AND ShotTermInspectionComplete =
                        ifnull(QALS_STAT14, 'Not Set')
                  AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
                  AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
                  AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set'))
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS */
	   
	
UPDATE fact_inspectionlot il
SET  il.dim_inspectionlotmiscid = di.dim_inspectionlotmiscid
FROM qals q,
	 dim_inspectionlotmisc di,
	 fact_inspectionlot il
WHERE    AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
     AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
     AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
     AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
     AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
     AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
     AND ShotTermInspectionComplete = ifnull(QALS_STAT14, 'Not Set')
     AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
     AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
     AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set')
     AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	 AND ifnull(il.dim_inspectionlotmiscid,1) <> ifnull(di.dim_inspectionlotmiscid,2);

	
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
   SET dim_inspectionlotoriginid =
          (SELECT dim_inspectionlotoriginid
             FROM dim_inspectionlotorigin ilo
            WHERE ilo.InspectionLotOriginCode =
                     ifnull(QALS_HERKUNFT, 'Not Set')
                  AND ilo.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET il.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
FROM 
     fact_inspectionlot il,
     dim_inspectionlotorigin ilo,
     qals q
WHERE    ilo.InspectionLotOriginCode =ifnull(QALS_HERKUNFT, 'Not Set')
     AND ilo.RowIsCurrent = 1
     AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	 AND ifnull(il.dim_inspectionlotoriginid,1) <> ifnull(ilo.dim_inspectionlotoriginid,2);
     

 
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectionlotstoragelocationid = ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation isl
                  WHERE     isl.LocationCode = QALS_LAGORTVORG
                        AND isl.RowIsCurrent = 1
                        AND isl.Plant = dp.PlantCode
						AND QALS_LAGORTVORG IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET dim_inspectionlotstoragelocationid = ifnull(isl.dim_storagelocationid, 1)
FROM 
     fact_inspectionlot il,
	 qals q,
	 dim_plant dp,
     dim_storagelocation isl
WHERE      il.dd_inspectionlotno = q.QALS_PRUEFLOS
       AND il.dim_plantid = dp.dim_plantid
	   AND q.QALS_LAGORTVORG IS NOT NULL
       AND isl.LocationCode = q.QALS_LAGORTVORG
       AND isl.RowIsCurrent = 1
	   AND isl.Plant = dp.PlantCode
	   AND ifnull(dim_inspectionlotstoragelocationid,1) <> ifnull(isl.dim_storagelocationid, 2);





	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_inspectionsamplestatusid =
          (SELECT dim_inspectionsamplestatusid
             FROM dim_inspectionsamplestatus iss
            WHERE iss.statuscode = ifnull(QALS_LVS_STIKZ, 'Not Set')
                  AND iss.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET il.dim_inspectionsamplestatusid = iss.dim_inspectionsamplestatusid
FROM fact_inspectionlot il,
     dim_inspectionsamplestatus iss,
	 qals q
WHERE      iss.statuscode = ifnull(QALS_LVS_STIKZ, 'Not Set')
       AND iss.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(il.dim_inspectionsamplestatusid,1) <> ifnull(iss.dim_inspectionsamplestatusid,2);
	   



	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectionseverityid =
          (SELECT dim_inspectionseverityid
             FROM dim_inspectionseverity isvr
            WHERE isvr.InspectionSeverityCode =
                     ifnull(QALS_PRSCHAERFE, 0)
                  AND isvr.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/

UPDATE fact_inspectionlot il
SET il.dim_inspectionseverityid = isvr.dim_inspectionseverityid
FROM   
	   fact_inspectionlot il,
       dim_inspectionseverity isvr,
       qals q
WHERE isvr.InspectionSeverityCode = ifnull(QALS_PRSCHAERFE, 0)
       AND isvr.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(il.dim_inspectionseverityid,1) <> ifnull(isvr.dim_inspectionseverityid,2);



	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectionstageid =
          (SELECT dim_inspectionstageid
             FROM dim_inspectionstage istg
            WHERE istg.InspectionStageCode = QALS_PRSTUFE
                  AND istg.InspectionStageRuleCode =
                        ifnull(QALS_DYNREGEL, 'Not Set')
                  AND istg.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   

UPDATE fact_inspectionlot il
SET il.dim_inspectionstageid = istg.dim_inspectionstageid
FROM   fact_inspectionlot il,
       qals q,
	   dim_inspectionstage istg
 WHERE istg.InspectionStageCode = q.QALS_PRSTUFE
       AND istg.InspectionStageRuleCode = ifnull(QALS_DYNREGEL, 'Not Set')
       AND istg.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(il.dim_inspectionstageid,1) <> ifnull(istg.dim_inspectionstageid,2);



	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_inspectiontypeid =
          (SELECT dim_inspectiontypeid
             FROM dim_inspectiontype ityp
            WHERE ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
                  AND ityp.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   

	  
	  
UPDATE fact_inspectionlot il
SET il.dim_inspectiontypeid = ifnull(ityp.dim_inspectiontypeid,1)
FROM   
       fact_inspectionlot il,
       qals q,
	   dim_inspectiontype ityp
WHERE     ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
      AND ityp.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND IFNULL(il.dim_inspectiontypeid,1) <> ifnull(ityp.dim_inspectiontypeid,2);



	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_itemcategoryid =
          (SELECT dim_itemcategoryid
             FROM dim_itemcategory icc
            WHERE icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
                  AND icc.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	 
UPDATE fact_inspectionlot il
SET il.dim_itemcategoryid = ifnull(icc.dim_itemcategoryid,1)
FROM   fact_inspectionlot il,
       dim_itemcategory icc,
       qals q
WHERE     icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
      AND icc.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(il.dim_itemcategoryid,1) <> ifnull(icc.dim_itemcategoryid,2);

	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_lotunitofmeasureid =
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure luom
            WHERE luom.UOM = ifnull(QALS_MENGENEINH, 'Not Set')
                  AND luom.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/

UPDATE fact_inspectionlot il	   
SET il.dim_lotunitofmeasureid = ifnull(luom.dim_unitofmeasureid,1)  
FROM   fact_inspectionlot il,
       dim_unitofmeasure luom,
       qals q
WHERE     luom.UOM = ifnull(QALS_MENGENEINH, 'Not Set')
      AND luom.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(il.dim_lotunitofmeasureid,1) <> ifnull(luom.dim_unitofmeasureid,2);



	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_manufacturerid =
          (SELECT dim_vendorid
             FROM dim_vendor dv
            WHERE dv.VendorNumber = ifnull(QALS_HERSTELLER, 'Not Set')
                  AND dv.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET  dim_manufacturerid = ifnull(dv.dim_vendorid,1)
FROM fact_inspectionlot il,
     dim_vendor dv,
	 qals q
WHERE     dv.VendorNumber = ifnull(QALS_HERSTELLER, 'Not Set')
      AND dv.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(dim_manufacturerid,1) <> ifnull(dv.dim_vendorid,2);



	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_ObjectCategoryid =
          (SELECT dim_ObjectCategoryid
             FROM dim_ObjectCategory oc
            WHERE oc.ObjectCategoryCode = ifnull(QALS_OBTYP, 'Not Set')
                  AND oc.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET  il.dim_ObjectCategoryid = oc.dim_ObjectCategoryid
FROM fact_inspectionlot il,
     dim_ObjectCategory oc,
     qals q
WHERE oc.ObjectCategoryCode = ifnull(QALS_OBTYP, 'Not Set')
      AND oc.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(il.dim_ObjectCategoryid,1) <> ifnull(oc.dim_ObjectCategoryid,2);

	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_purchaseorgid =
          (SELECT dim_purchaseorgid
             FROM dim_purchaseorg po
            WHERE po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
                  AND po.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il	   
SET il.dim_purchaseorgid = ifnull(po.dim_purchaseorgid,1)
FROM   fact_inspectionlot il,
       qals q,
	   dim_purchaseorg po
WHERE     po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
      AND po.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(il.dim_purchaseorgid,1) <> ifnull(po.dim_purchaseorgid,2);


	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_routeid =
          (SELECT dim_routeid
             FROM dim_route r
            WHERE r.RouteCode = ifnull(QALS_LS_ROUTE, 'Not Set')
                  AND r.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il	   
SET il.dim_routeid = ifnull(r.dim_routeid,1)
FROM fact_inspectionlot il,
     qals q,
	 dim_route r
WHERE     r.RouteCode = ifnull(QALS_LS_ROUTE, 'Not Set')
      AND r.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(il.dim_routeid,1) <> ifnull(r.dim_routeid,2);
	  
	  
	  
	  
	  
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_sampleunitofmeasureid =
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure uom
            WHERE uom.UOM = ifnull(QALS_EINHPROBE, 'Not Set')
                  AND uom.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	
UPDATE fact_inspectionlot il
SET il.dim_sampleunitofmeasureid = uom.dim_unitofmeasureid
FROM   fact_inspectionlot il,
       dim_unitofmeasure uom,
       qals q
WHERE     uom.UOM = ifnull(q.QALS_EINHPROBE, 'Not Set')
      AND uom.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	   AND ifnull(il.dim_sampleunitofmeasureid,1) <> ifnull(uom.dim_unitofmeasureid,2);

	
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_specialstockid =
          (SELECT dim_specialstockid
             FROM dim_specialstock ss
            WHERE ss.specialstockindicator = ifnull(QALS_SOBKZ, 'Not Set')
                  AND ss.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET il.dim_specialstockid =  ss.dim_specialstockid
FROM fact_inspectionlot il,
     qals q,
     dim_specialstock ss
WHERE     ss.specialstockindicator = ifnull(QALS_SOBKZ, 'Not Set')
      AND ss.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(il.dim_specialstockid,1) <>  ifnull(ss.dim_specialstockid,2);



	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_statusprofileid =
          (SELECT dim_statusprofileid
             FROM dim_statusprofile sp
            WHERE sp.StatusProfileCode = ifnull(QALS_STSMA, 'Not Set')
                  AND sp.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   

UPDATE fact_inspectionlot il	   
SET il.dim_statusprofileid = sp.dim_statusprofileid	
FROM fact_inspectionlot il,
     dim_statusprofile sp,
     qals q
WHERE    sp.StatusProfileCode = ifnull(q.QALS_STSMA, 'Not Set')
     AND sp.RowIsCurrent = 1
     AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	 AND ifnull(il.dim_statusprofileid,1) <> ifnull(sp.dim_statusprofileid,2);
	   
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET  dim_storagelocationid = ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation sl
                  WHERE sl.LocationCode = ifnull(QALS_LAGORTCHRG, 'Not Set')
                        AND sl.Plant = dp.PlantCode
                        AND sl.RowIsCurrent = 1
						AND QALS_LAGORTCHRG IS NOT NULL), 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/

UPDATE fact_inspectionlot il
SET  il.dim_storagelocationid = ifnull(sl.dim_storagelocationid, 1)   
FROM dim_storagelocation sl,
     fact_inspectionlot il,
	 qals q,
     dim_plant dp,
     dim_part dpa
WHERE sl.LocationCode = ifnull(q.QALS_LAGORTCHRG, 'Not Set')
      AND sl.Plant = dp.PlantCode
      AND sl.RowIsCurrent = 1
	  AND q.QALS_LAGORTCHRG IS NOT NULL
	  AND dp.PlantCode = q.QALS_WERK
      AND dp.RowIsCurrent = 1
      AND dpa.PartNumber = q.QALS_MATNR
      AND dpa.Plant = dp.PlantCode
      AND dpa.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(il.dim_storagelocationid,1) <> ifnull(sl.dim_storagelocationid, 2);
	   

	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_tasklisttypeid =
          (SELECT dim_tasklisttypeid
             FROM dim_tasklisttype tlt
            WHERE tlt.TaskListTypeCode = ifnull(QALS_PLNTY, 'Not Set')
                  AND tlt.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il
SET il.dim_tasklisttypeid = tlt.dim_tasklisttypeid
FROM fact_inspectionlot il,
     dim_tasklisttype tlt,
	 qals q
WHERE     tlt.TaskListTypeCode = ifnull(QALS_PLNTY, 'Not Set')
      AND tlt.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(il.dim_tasklisttypeid,1) <> ifnull(tlt.dim_tasklisttypeid,2);


	   

	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_tasklistusageid =
          (SELECT dim_tasklistusageid
             FROM dim_tasklistusage tlu
            WHERE tlu.UsageCode = ifnull(QALS_PPLVERW, 'Not Set')
                  AND tlu.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   

UPDATE fact_inspectionlot il	   
SET il.dim_tasklistusageid = tlu.dim_tasklistusageid   
FROM dim_tasklistusage tlu,
     fact_inspectionlot il,
	 qals q
WHERE     tlu.UsageCode = ifnull(QALS_PPLVERW, 'Not Set')
      AND tlu.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
	  AND ifnull(il.dim_tasklistusageid,1) <> ifnull(tlu.dim_tasklistusageid,2);




	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_vendorid =
          (SELECT dim_vendorid
             FROM dim_vendor dv1
            WHERE dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
                  AND dv1.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il	   
SET il.dim_vendorid = dv1.dim_vendorid
FROM dim_vendor dv1,
     fact_inspectionlot il,   
	 qals q
WHERE     dv1.VendorNumber = ifnull(q.QALS_LIFNR, 'Not Set')
      AND dv1.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(il.dim_vendorid,1) <> ifnull(dv1.dim_vendorid,2);

	  
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_warehousenumberid =
          (SELECT dim_warehouseid
             FROM dim_warehousenumber wn
            WHERE wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
                  AND wn.RowIsCurrent = 1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/

UPDATE fact_inspectionlot il	   
SET il.dim_warehousenumberid = wn.dim_warehouseid
FROM fact_inspectionlot il,
     dim_warehousenumber wn,
	 qals q
WHERE     wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
      AND wn.RowIsCurrent = 1
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS 
	  AND ifnull(il.dim_warehousenumberid,1) <> ifnull(wn.dim_warehouseid,2);
	  
	  
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_controllingareaid =
          ifnull((SELECT ca.dim_controllingareaid
	     FROM dim_controllingarea ca
            WHERE ca.ControllingAreaCode = QALS_KOKRS),1)
	WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/

UPDATE fact_inspectionlot il	   
SET il.dim_controllingareaid = ifnull(ca.dim_controllingareaid,1)
FROM   fact_inspectionlot il,
       dim_controllingarea ca,
       qals q
WHERE     ca.ControllingAreaCode = q.QALS_KOKRS
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(il.dim_controllingareaid,1) <> ifnull(ca.dim_controllingareaid,2);



	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q,
       dim_plant dp,
       dim_part dpa
    SET dim_profitcenterid =
          ifnull((SELECT  pc.dim_profitcenterid
             FROM dim_profitcenter pc,
				tmp_dim_profitcenter tmp_pc
            WHERE  pc.ControllingArea = QALS_KOKRS
               AND pc.ProfitCenterCode = QALS_PRCTR
              AND pc.ValidTo >= QALS_ERSTELDAT
              AND pc.RowIsCurrent = 1
			  AND tmp_pc.ControllingArea = QALS_KOKRS
              AND tmp_pc.ProfitCenterCode = QALS_PRCTR 
			  AND pc.ValidTo = tmp_pc.min_validto), 1),
	dd_ObjectNumber = ifnull(QALS_OBJNR,'Not Set')
 WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS*/
	   
UPDATE fact_inspectionlot il	   
SET il.dim_profitcenterid = ifnull(pc.dim_profitcenterid,1),
    dd_ObjectNumber = ifnull(QALS_OBJNR,'Not Set')
FROM fact_inspectionlot il,
     dim_profitcenter pc,
     tmp_dim_profitcenter tmp_pc,
     qals q
WHERE     pc.ControllingArea = q.QALS_KOKRS
      AND pc.ProfitCenterCode = q.QALS_PRCTR
      AND pc.ValidTo >= QALS_ERSTELDAT
      AND pc.RowIsCurrent = 1
	  AND tmp_pc.ControllingArea = q.QALS_KOKRS
      AND tmp_pc.ProfitCenterCode = q.QALS_PRCTR 
	  AND pc.ValidTo = tmp_pc.min_validto
      AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(il.dim_profitcenterid,1) <> ifnull(pc.dim_profitcenterid,2);

	   
DROP TABLE IF EXISTS tmp_dim_profitcenter;



/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_actualinspectedqty = ifnull(QALS_LMENGEPR,0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_actualinspectedqty,-1) <> ifnull(QALS_LMENGEPR,0)*/

UPDATE fact_inspectionlot il
SET ct_actualinspectedqty = ifnull(QALS_LMENGEPR,0)
FROM fact_inspectionlot il,
     qals q
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(ct_actualinspectedqty,-1) <> ifnull(QALS_LMENGEPR,0);

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_actuallotqty = ifnull(QALS_LMENGEIST,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_actuallotqty,-1) <> ifnull(QALS_LMENGEIST,0)*/

UPDATE fact_inspectionlot il
SET ct_actuallotqty = ifnull(QALS_LMENGEIST,0)
FROM fact_inspectionlot il,
     qals q
WHERE      il.dd_inspectionlotno = q.QALS_PRUEFLOS
       AND ifnull(ct_actuallotqty,-1) <> ifnull(QALS_LMENGEIST,0);	 
	 

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_blockedqty = ifnull(QALS_LMENGE04,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_blockedqty,-1) <> ifnull(QALS_LMENGE04,0)*/

UPDATE fact_inspectionlot il
SET ct_blockedqty = ifnull(QALS_LMENGE04,0) 
FROM  fact_inspectionlot il,
      qals q
WHERE     il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(ct_blockedqty,-1) <> ifnull(QALS_LMENGE04,0);



/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_inspectionlotqty = ifnull(QALS_LOSMENGE,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_inspectionlotqty,-1) <> ifnull(QALS_LOSMENGE,0)*/

UPDATE fact_inspectionlot il
SET ct_inspectionlotqty = ifnull(QALS_LOSMENGE,0)
FROM   fact_inspectionlot il,
       qals q
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
       AND ifnull(ct_inspectionlotqty,-1) <> ifnull(QALS_LOSMENGE,0);


/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_postedqty = ifnull(QALS_LMENGEZUB,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_postedqty,-1) <> ifnull(QALS_LMENGEZUB,0)*/

UPDATE fact_inspectionlot il
SET ct_postedqty = ifnull(QALS_LMENGEZUB,0)
FROM   fact_inspectionlot il,
       qals q
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
       AND ifnull(ct_postedqty,-1) <> ifnull(QALS_LMENGEZUB,0);

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_qtydefective = ifnull(QALS_LMENGESCH,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_qtydefective,-1) <> ifnull(QALS_LMENGESCH,0)*/

UPDATE fact_inspectionlot il
SET ct_qtydefective = ifnull(QALS_LMENGESCH,0) 
FROM   fact_inspectionlot il,
       qals q
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
       AND ifnull(ct_qtydefective,-1) <> ifnull(QALS_LMENGESCH,0);	   
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_qtyreturned = ifnull(QALS_LMENGE07,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_qtyreturned,-1) <> ifnull(QALS_LMENGE07,0)*/

UPDATE fact_inspectionlot il   
SET ct_qtyreturned = ifnull(QALS_LMENGE07,0) 
FROM   fact_inspectionlot il,
       qals q
WHERE     il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(ct_qtyreturned,-1) <> ifnull(QALS_LMENGE07,0);
   
/* UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_reserveqty = ifnull(QALS_LMENGE05,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_reserveqty,-1) <> ifnull(QALS_LMENGE05,0)*/

UPDATE fact_inspectionlot il 
SET ct_reserveqty = ifnull(QALS_LMENGE05,0) 
FROM   fact_inspectionlot il,
       qals q 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
       AND ifnull(ct_reserveqty,-1) <> ifnull(QALS_LMENGE05,0);
	   
	   

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_sampleqty = ifnull(QALS_LMENGE03,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_sampleqty,-1) <> ifnull(QALS_LMENGE03,0)*/

UPDATE fact_inspectionlot il 
SET ct_sampleqty = ifnull(QALS_LMENGE03,0)  
FROM   fact_inspectionlot il,
       qals q 
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(ct_sampleqty,-1) <> ifnull(QALS_LMENGE03,0);
	   
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_samplesizeqty = ifnull(QALS_GESSTICHPR,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_samplesizeqty,-1) <> ifnull(QALS_GESSTICHPR,0)*/

UPDATE fact_inspectionlot il   
SET ct_samplesizeqty = ifnull(QALS_GESSTICHPR,0) 
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(ct_samplesizeqty,-1) <> ifnull(QALS_GESSTICHPR,0);



/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_scrapqty = ifnull(QALS_LMENGE02,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_scrapqty,-1) <> ifnull(QALS_LMENGE02,0)*/

UPDATE fact_inspectionlot il
SET ct_scrapqty = ifnull(QALS_LMENGE02,0)
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(ct_scrapqty,-1) <> ifnull(QALS_LMENGE02,0);
	 

 
/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET ct_unrestrictedqty = ifnull(QALS_LMENGE01,0) 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(ct_unrestrictedqty,-1) <> ifnull(QALS_LMENGE01,0)*/

UPDATE fact_inspectionlot il  
SET ct_unrestrictedqty = ifnull(QALS_LMENGE01,0)
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(ct_unrestrictedqty,-1) <> ifnull(QALS_LMENGE01,0);

/* UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_batchno = ifnull(QALS_CHARG, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_batchno,'-1') <> ifnull(QALS_CHARG, 'Not Set')*/

UPDATE fact_inspectionlot il
SET dd_batchno = ifnull(QALS_CHARG, 'Not Set')
FROM  fact_inspectionlot il,
      qals q
WHERE     il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(dd_batchno,'-1') <> ifnull(QALS_CHARG, 'Not Set');
	  

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_changedby = ifnull(QALS_AENDERER, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_changedby,'-1') <> ifnull(QALS_AENDERER, 'Not Set')*/

UPDATE fact_inspectionlot il
SET dd_changedby = ifnull(QALS_AENDERER, 'Not Set')
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_changedby,'-1') <> ifnull(QALS_AENDERER, 'Not Set');

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_createdby = ifnull(QALS_ERSTELLER, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_createdby,'-1') <> ifnull(QALS_ERSTELLER, 'Not Set')*/

UPDATE fact_inspectionlot il
SET dd_createdby = ifnull(QALS_ERSTELLER, 'Not Set')
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_createdby,'-1') <> ifnull(QALS_ERSTELLER, 'Not Set');


/* UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_documentitemno = ifnull(QALS_EBELP, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_documentitemno,-1) <> ifnull(QALS_EBELP, 0)*/

UPDATE fact_inspectionlot il
SET dd_documentitemno = ifnull(QALS_EBELP, 0)
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_documentitemno,-1) <> ifnull(QALS_EBELP, 0);

/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_documentno = ifnull(QALS_EBELN, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_documentno,'-1') <> ifnull(QALS_EBELN, 'Not Set')*/

UPDATE fact_inspectionlot il
SET dd_documentno = ifnull(QALS_EBELN, 'Not Set')
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_documentno,'-1') <> ifnull(QALS_EBELN, 'Not Set');	   
	   
	   
	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_MaterialDocItemNo = ifnull(QALS_ZEILE, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocItemNo,-1) <> ifnull(QALS_ZEILE, 0)*/

UPDATE fact_inspectionlot il
SET dd_MaterialDocItemNo = ifnull(QALS_ZEILE, 0)
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_MaterialDocItemNo,-1) <> ifnull(QALS_ZEILE, 0);



           
/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_MaterialDocNo = ifnull(QALS_MBLNR, 'Not Set') 
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocNo,'-1') <> ifnull(QALS_MBLNR, 'Not Set') */
UPDATE fact_inspectionlot il
SET dd_MaterialDocNo = ifnull(QALS_MBLNR, 'Not Set')         
FROM   fact_inspectionlot il,
       qals q
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocNo,'-1') <> ifnull(QALS_MBLNR, 'Not Set');


	   
/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_MaterialDocYear = ifnull(QALS_MJAHR, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_MaterialDocYear,-1) <> ifnull(QALS_MJAHR, 0)*/

UPDATE fact_inspectionlot il
SET dd_MaterialDocYear = ifnull(QALS_MJAHR, 0)
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_MaterialDocYear,-1) <> ifnull(QALS_MJAHR, 0);	   




/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_orderno = ifnull(QALS_AUFNR, 'Not Set')
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_orderno,'-1') <> ifnull(QALS_AUFNR, 'Not Set')*/  

UPDATE fact_inspectionlot il
SET dd_orderno = ifnull(QALS_AUFNR, 'Not Set')
FROM   fact_inspectionlot il,
       qals q
WHERE    il.dd_inspectionlotno = q.QALS_PRUEFLOS
     AND ifnull(dd_orderno,'-1') <> ifnull(QALS_AUFNR, 'Not Set');



/*UPDATE fact_inspectionlot il
	FROM
       qals q
   SET dd_ScheduleNo = ifnull(QALS_ETENR, 0)
WHERE  il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND ifnull(dd_ScheduleNo,-1) <> ifnull(QALS_ETENR, 0)*/

UPDATE fact_inspectionlot il
SET dd_ScheduleNo = ifnull(QALS_ETENR, 0)
FROM   fact_inspectionlot il,
       qals q
WHERE      il.dd_inspectionlotno = q.QALS_PRUEFLOS
      AND ifnull(dd_ScheduleNo,-1) <> ifnull(QALS_ETENR, 0);