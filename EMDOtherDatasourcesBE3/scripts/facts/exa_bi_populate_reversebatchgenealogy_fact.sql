/* Combine All Levels Into One Single Table */
drop table if exists tmp_fact_reverse_batchgenealogy;
create table tmp_fact_reverse_batchgenealogy like fact_reverse_batchgenealogy
including defaults including identity;


/* Level 0 - Stage */ 
/* Level 0 - Get All Material Movments For Any Given Batch */	/*10357684*/
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0;   
create table tmp_factmmbatchhier_rbg_mmg_tnn_level0 as
select distinct 
0 dd_levelbottomup, f.dd_batchnumber dd_level0batchno, dp.partnumber dd_level0partno, pl.plantcode dd_level0plant,
dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, pd.datevalue as dd_postingdate, f.ct_quantity, 
mt.movementtype dd_movementtype, mt.movementindicator dd_movementindicator, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem,
f.dd_materialdocno, f.dd_materialdocitemno, f.dd_materialdocyear, f.dim_plantid, f.dim_partid, f.dim_dateidpostingdate, 
f.dd_debitcreditid, f.dim_receivingplantid, f.dim_receivingpartid, 
dp.partnumber dd_receivingpartnumber, pl.plantcode dd_receivingplantcode, f.dd_receivingbatchnumber,
'Level0-ForAnyGivenBatch' as dd_sourcescriptstepname, 100001 as dd_sourcescriptstepsequence, 'Not Set' as dd_issuereceive
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl, dim_date pd, dim_part dpr, dim_plant plr
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dim_partid <> 1 and f.dim_plantid <> 1
and f.dim_receivingpartid = dpr.dim_partid and f.dim_receivingplantid = plr.dim_plantid
-- and dp.materialclasification in ('API','DP','FG')
and mt.movementtype in ('101','102','309','541','543','261','262','641','643','645','647');
-- and mt.movementtype not in ('601','930','931','932','933','934','935')
--and pd.datevalue >= '2015-01-01';

/* Level 0 - Get All Material Movments For Any Given Batch For 
Material Classifiation API and DP - Limit Data At Level 0*/	/*4291288*/
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp;   
create table tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp as
select distinct 
0 dd_levelbottomup, f.dd_batchnumber dd_level0batchno, dp.partnumber dd_level0partno, pl.plantcode dd_level0plant,
dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, pd.datevalue as dd_postingdate, f.ct_quantity, 
mt.movementtype dd_movementtype, mt.movementindicator dd_movementindicator, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem,
f.dd_materialdocno, f.dd_materialdocitemno, f.dd_materialdocyear, f.dim_plantid, f.dim_partid, f.dim_dateidpostingdate,
f.dd_debitcreditid, f.dim_receivingplantid, f.dim_receivingpartid, 
dp.partnumber dd_receivingpartnumber, pl.plantcode dd_receivingplantcode, f.dd_receivingbatchnumber,
'Level0-ForAnyGivenBatch-APIDP' as dd_sourcescriptstepname, 100001 as dd_sourcescriptstepsequence, 'Not Set' as dd_issuereceive
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd, dim_part dpr, dim_plant plr
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dim_partid <> 1 and f.dim_plantid <> 1
and f.dim_receivingpartid = dpr.dim_partid and f.dim_receivingplantid = plr.dim_plantid;
-- and dp.materialclasification in ('API','DP');
-- and pd.datevalue >= '2015-01-01';

/* Level 0 - Create Temp Table With All Movements for Movement Type 101 and Indicator B */	/*1338641*/
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant, 
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
dd_sourcescriptstepname, dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0
where dd_movementtype in ('101','102') and dd_movementindicator = 'B';
--and dd_postingdate >= '2015-01-01';

/* Level 0 - Create Temp Table With All Movements for Movement Type 101 and Indicator F */	/*2341265*/
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant, 
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
dd_sourcescriptstepname, dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0
where dd_movementtype in ('101','102') and dd_movementindicator = 'F';
-- and dd_postingdate >= '2015-01-01';

/* Level 0 - Create Temp Table With All Movements for Movement Type 309 and Debit Credit Indicator*/	/*427187*/
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant, 
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
dd_sourcescriptstepname, dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0
where dd_movementtype = '309' and dd_debitcreditid = 'Debit';
--and dd_postingdate >= '2015-01-01';


/* Level 1 - Stage */

/* Level 1 - Process Sub Contract Transfer */
/* Level 1 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 0 For API and DP Data */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconissue as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant, 
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
'Level1-SubContractIssue' as dd_sourcescriptstepname, 101001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp
where dd_movementtype in ('541','543') /* and dd_level0batchno = 'L44308' */;

/* Level 1 - Step 1b - Get Sub Contract Receive for Batches in Level1/Step1a */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconreceive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber, 
'Level1-SubContractReceive' as dd_sourcescriptstepname, 101002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 1 - Process Production Movement */
/* Level 1 - Step 2a - Get Production Issue for Any Given Batch From Level 0 For API and DP Data  */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordissue as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
'Level1-ProductionIssue' as dd_sourcescriptstepname, 101003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp
where dd_movementtype in ('261','262') /* and dd_level0batchno = 'WEOF36A' */;

/* Level 1 - Step 2b - Get Production Receive for Batches in Level1/Step2a */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordreceive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber, 
'Level1-ProductionReceive' as dd_sourcescriptstepname, 101004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 1 - Process MM Transfer (309) */
/* Level 1 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 0 For API and DP Data */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309issue as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant, 
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
'Level1-MMT309Issue' as dd_sourcescriptstepname, 101005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp
where dd_movementtype in ('309') and dd_debitcreditid = 'Credit';

/* Level 1 - Step 3b - Get MM Transfer Receive (309) for Batches in Level1/Step3a */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level1-MMT309Receive' as dd_sourcescriptstepname, 101006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 1 - Process Inter Company Stock Transfer */
/* Level 1 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 0 For API and DP Data */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomissue as
select distinct 
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant, 
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
'Level1-InterCompanyTransferIssue' as dd_sourcescriptstepname, 101007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp
where dd_movementtype in ('641','647','643','645') /* and dd_level0batchno = 'L44308' */;

/* Level 1 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level1/Step4a */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomreceive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber, 
'Level1-InterCompanyTransferReceive' as dd_sourcescriptstepname, 101008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
/* Level 1 - Combine All Transfers */
/* Level 1 - Step N - Union Sub Contract, Production, MM Transfer (309), Inter Company Transfer (643) - Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate, 
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber,
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordreceive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309receive 
union all 
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomreceive
);


/* Level 2 - Stage */

/* Level 2 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-SubContractIssue' as dd_sourcescriptstepname, 102001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 2 - Step 1b - Get Sub Contract Receive for Batches in Level2/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-SubContractReceive' as dd_sourcescriptstepname, 102002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 2 - Step 2a - Get Production Issue for Any Given Batch From Level 1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-ProductionIssue' as dd_sourcescriptstepname, 102003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 2 - Step 2b - Get Production Receive for Batches in Level2/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-ProductionReceive' as dd_sourcescriptstepname, 102004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 2 - Process MM Transfer (309) */
/* Level 2 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 1 For API and DP Data */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-MMT309Issue' as dd_sourcescriptstepname, 102005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive';

/* Level 2 - Step 3b - Get MM Transfer Receive (309) for Batches in Level2/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-MMT309Receive' as dd_sourcescriptstepname, 102006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 2 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-InterCompanyTransferIssue' as dd_sourcescriptstepname, 102007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 2 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level2/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level2-InterCompanyTransferReceive' as dd_sourcescriptstepname, 102008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
/* Level 2 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomreceive
);

/* Level 2 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level2_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomreceive
);

/* Level 3 - Stage */

/* Level 3 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-SubContractIssue' as dd_sourcescriptstepname, 103001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level2_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 3 - Step 1b - Get Sub Contract Receive for Batches in Level3/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-SubContractReceive' as dd_sourcescriptstepname, 103002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 3 - Step 2a - Get Production Issue for Any Given Batch From Level 2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-ProductionIssue' as dd_sourcescriptstepname, 103003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level2_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 3 - Step 2b - Get Production Receive for Batches in Level3/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-ProductionReceive' as dd_sourcescriptstepname, 103004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 3 - Process MM Transfer (309) */
/* Level 3 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-MMT309Issue' as dd_sourcescriptstepname, 103005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level2_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 3 - Step 3b - Get MM Transfer Receive (309) for Batches in Level3/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-MMT309Receive' as dd_sourcescriptstepname, 103006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;
		
/* Level 3 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-InterCompanyTransferIssue' as dd_sourcescriptstepname, 103007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level2_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 3 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level3/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level3-InterCompanyTransferReceive' as dd_sourcescriptstepname, 103008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;

		
/* Level 3 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomreceive
);

/* Level 3 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level3_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomreceive
);



/* Level 4 - Stage */

/* Level 4 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-SubContractIssue' as dd_sourcescriptstepname, 104001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level3_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 4 - Step 1b - Get Sub Contract Receive for Batches in Level4/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-SubContractReceive' as dd_sourcescriptstepname, 104002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 4 - Step 2a - Get Production Issue for Any Given Batch From Level 3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-ProductionIssue' as dd_sourcescriptstepname, 104003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level3_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 4 - Step 2b - Get Production Receive for Batches in Level4/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-ProductionReceive' as dd_sourcescriptstepname, 104004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 4 - Process MM Transfer (309) */
/* Level 4 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-MMT309Issue' as dd_sourcescriptstepname, 104005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level3_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 4 - Step 3b - Get MM Transfer Receive (309) for Batches in Level4/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-MMT309Receive' as dd_sourcescriptstepname, 104006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 4 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-InterCompanyTransferIssue' as dd_sourcescriptstepname, 104007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level3_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 4 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level4/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level4-InterCompanyTransferReceive' as dd_sourcescriptstepname, 104008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;

	
/* Level 4 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomreceive
);

/* Level 4 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level4_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomreceive
);


/* Level 5 - Stage */

/* Level 5 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-SubContractIssue' as dd_sourcescriptstepname, 105001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level4_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 5 - Step 1b - Get Sub Contract Receive for Batches in Level5/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-SubContractReceive' as dd_sourcescriptstepname, 105002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 5 - Step 2a - Get Production Issue for Any Given Batch From Level 4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-ProductionIssue' as dd_sourcescriptstepname, 105003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level4_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 5 - Step 2b - Get Production Receive for Batches in Level5/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-ProductionReceive' as dd_sourcescriptstepname, 105004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 5 - Process MM Transfer (309) */
/* Level 5 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-MMT309Issue' as dd_sourcescriptstepname, 105005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level4_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 5 - Step 3b - Get MM Transfer Receive (309) for Batches in Level5/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-MMT309Receive' as dd_sourcescriptstepname, 105006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 5 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-InterCompanyTransferIssue' as dd_sourcescriptstepname, 105007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level4_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 5 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level5/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level5-InterCompanyTransferReceive' as dd_sourcescriptstepname, 105008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;


/* Level 5 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomreceive
);

/* Level 5 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level5_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomreceive
);


/* Level 6 - Stage */

/* Level 6 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 5 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-SubContractIssue' as dd_sourcescriptstepname, 106001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level5_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 6 - Step 1b - Get Sub Contract Receive for Batches in Level6/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-SubContractReceive' as dd_sourcescriptstepname, 106002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 6 - Step 2a - Get Production Issue for Any Given Batch From Level 5 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-ProductionIssue' as dd_sourcescriptstepname, 106003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level5_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 6 - Step 2b - Get Production Receive for Batches in Level6/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-ProductionReceive' as dd_sourcescriptstepname, 106004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 6 - Process MM Transfer (309) */
/* Level 6 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 5 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-MMT309Issue' as dd_sourcescriptstepname, 106005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level5_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 6 - Step 3b - Get MM Transfer Receive (309) for Batches in Level6/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-MMT309Receive' as dd_sourcescriptstepname, 106006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;
		
/* Level 6 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 5 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-InterCompanyTransferIssue' as dd_sourcescriptstepname, 106007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level5_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 6 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level6/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level6-InterCompanyTransferReceive' as dd_sourcescriptstepname, 106008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;


		
/* Level 6 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomreceive
);

/* Level 6 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level6_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomreceive
);



/* Level 7 - Stage */

/* Level 7 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 6 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-SubContractIssue' as dd_sourcescriptstepname, 107001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level6_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 7 - Step 1b - Get Sub Contract Receive for Batches in Level7/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-SubContractReceive' as dd_sourcescriptstepname, 107002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 7 - Step 2a - Get Production Issue for Any Given Batch From Level 6 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-ProductionIssue' as dd_sourcescriptstepname, 107003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level6_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 7 - Step 2b - Get Production Receive for Batches in Level7/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-ProductionReceive' as dd_sourcescriptstepname, 107004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 7 - Process MM Transfer (309) */
/* Level 7 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 6 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-MMT309Issue' as dd_sourcescriptstepname, 107005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level6_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 7 - Step 3b - Get MM Transfer Receive (309) for Batches in Level7/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-MMT309Receive' as dd_sourcescriptstepname, 107006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 7 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 6 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-InterCompanyTransferIssue' as dd_sourcescriptstepname, 107007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level6_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 7 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level7/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level7-InterCompanyTransferReceive' as dd_sourcescriptstepname, 107008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;


		
/* Level 7 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomreceive
);

/* Level 7 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level7_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomreceive
);


/* Level 8 - Stage */

/* Level 8 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 7 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-SubContractIssue' as dd_sourcescriptstepname, 108001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level7_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 8 - Step 1b - Get Sub Contract Receive for Batches in Level8/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-SubContractReceive' as dd_sourcescriptstepname, 108002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 8 - Step 2a - Get Production Issue for Any Given Batch From Level 7 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-ProductionIssue' as dd_sourcescriptstepname, 108003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level7_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 8 - Step 2b - Get Production Receive for Batches in Level8/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-ProductionReceive' as dd_sourcescriptstepname, 108004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 8 - Process MM Transfer (309) */
/* Level 8 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 7 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-MMT309Issue' as dd_sourcescriptstepname, 108005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level7_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 8 - Step 3b - Get MM Transfer Receive (309) for Batches in Level8/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-MMT309Receive' as dd_sourcescriptstepname, 108006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 8 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 7 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-InterCompanyTransferIssue' as dd_sourcescriptstepname, 108007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level7_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 8 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level8/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level8-InterCompanyTransferReceive' as dd_sourcescriptstepname, 108008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		

		
/* Level 8 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomreceive
);

/* Level 8 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level8_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomreceive
);

/* Level 9 - Stage */

/* Level 9 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 8 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-SubContractIssue' as dd_sourcescriptstepname, 109001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level8_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 9 - Step 1b - Get Sub Contract Receive for Batches in Level9/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-SubContractReceive' as dd_sourcescriptstepname, 109002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 9 - Step 2a - Get Production Issue for Any Given Batch From Level 8 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-ProductionIssue' as dd_sourcescriptstepname, 109003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level8_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 9 - Step 2b - Get Production Receive for Batches in Level9/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-ProductionReceive' as dd_sourcescriptstepname, 109004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 9 - Process MM Transfer (309) */
/* Level 9 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 8 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-MMT309Issue' as dd_sourcescriptstepname, 109005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level8_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 9 - Step 3b - Get MM Transfer Receive (309) for Batches in Level9/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-MMT309Receive' as dd_sourcescriptstepname, 109006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 9 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 8 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-InterCompanyTransferIssue' as dd_sourcescriptstepname, 109007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level8_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 9 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level9/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level9-InterCompanyTransferReceive' as dd_sourcescriptstepname, 109008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		

		
/* Level 9 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomreceive
);

/* Level 9 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level9_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomreceive
);





/* Level 10 - Stage */

/* Level 10 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 9 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-SubContractIssue' as dd_sourcescriptstepname, 110001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level9_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 10 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-SubContractReceive' as dd_sourcescriptstepname, 110002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 10 - Step 2a - Get Production Issue for Any Given Batch From Level 9 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-ProductionIssue' as dd_sourcescriptstepname, 110003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level9_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 10 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-ProductionReceive' as dd_sourcescriptstepname, 110004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 10 - Process MM Transfer (309) */
/* Level 10 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 9 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-MMT309Issue' as dd_sourcescriptstepname, 110005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level9_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 10 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-MMT309Receive' as dd_sourcescriptstepname, 110006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 10 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 9 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-InterCompanyTransferIssue' as dd_sourcescriptstepname, 110007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level9_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 10 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level10-InterCompanyTransferReceive' as dd_sourcescriptstepname, 110008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 10 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomreceive
);

/* Level 10 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level10_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomreceive
);





/* Level 11 - Stage */

/* Level 11 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 10 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-SubContractIssue' as dd_sourcescriptstepname, 111001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level10_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 11 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-SubContractReceive' as dd_sourcescriptstepname, 111002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 11 - Step 2a - Get Production Issue for Any Given Batch From Level 10 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-ProductionIssue' as dd_sourcescriptstepname, 111003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level10_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 11 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-ProductionReceive' as dd_sourcescriptstepname, 111004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 11 - Process MM Transfer (309) */
/* Level 11 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 10 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-MMT309Issue' as dd_sourcescriptstepname, 111005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level10_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 11 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-MMT309Receive' as dd_sourcescriptstepname, 111006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 11 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 10 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-InterCompanyTransferIssue' as dd_sourcescriptstepname, 111007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level10_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 11 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level11-InterCompanyTransferReceive' as dd_sourcescriptstepname, 111008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 11 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomreceive
);

/* Level 11 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level11_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomreceive
);




/* Level 12 - Stage */

/* Level 12 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 11 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-SubContractIssue' as dd_sourcescriptstepname, 112001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level11_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 12 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-SubContractReceive' as dd_sourcescriptstepname, 112002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 12 - Step 2a - Get Production Issue for Any Given Batch From Level 11 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-ProductionIssue' as dd_sourcescriptstepname, 112003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level11_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 12 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-ProductionReceive' as dd_sourcescriptstepname, 112004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 12 - Process MM Transfer (309) */
/* Level 12 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 11 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-MMT309Issue' as dd_sourcescriptstepname, 112005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level11_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 12 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-MMT309Receive' as dd_sourcescriptstepname, 112006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 12 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 11 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-InterCompanyTransferIssue' as dd_sourcescriptstepname, 112007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level11_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 12 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level12-InterCompanyTransferReceive' as dd_sourcescriptstepname, 112008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 12 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomreceive
);

/* Level 12 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level12_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomreceive
);




/* Level 13 - Stage */

/* Level 13 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 12 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-SubContractIssue' as dd_sourcescriptstepname, 113001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level12_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 13 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-SubContractReceive' as dd_sourcescriptstepname, 113002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 13 - Step 2a - Get Production Issue for Any Given Batch From Level 12 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-ProductionIssue' as dd_sourcescriptstepname, 113003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level12_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 13 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-ProductionReceive' as dd_sourcescriptstepname, 113004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 13 - Process MM Transfer (309) */
/* Level 13 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 12 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-MMT309Issue' as dd_sourcescriptstepname, 113005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level12_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 13 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-MMT309Receive' as dd_sourcescriptstepname, 113006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 13 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 12 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-InterCompanyTransferIssue' as dd_sourcescriptstepname, 113007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level12_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 13 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level13-InterCompanyTransferReceive' as dd_sourcescriptstepname, 113008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 13 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomreceive
);

/* Level 13 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level13_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomreceive
);





/* Level 14 - Stage */

/* Level 14 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 13 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-SubContractIssue' as dd_sourcescriptstepname, 114001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level13_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 14 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-SubContractReceive' as dd_sourcescriptstepname, 114002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 14 - Step 2a - Get Production Issue for Any Given Batch From Level 13 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-ProductionIssue' as dd_sourcescriptstepname, 114003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level13_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 14 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-ProductionReceive' as dd_sourcescriptstepname, 114004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 14 - Process MM Transfer (309) */
/* Level 14 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 13 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-MMT309Issue' as dd_sourcescriptstepname, 114005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level13_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 14 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-MMT309Receive' as dd_sourcescriptstepname, 114006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 14 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 13 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-InterCompanyTransferIssue' as dd_sourcescriptstepname, 114007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level13_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 14 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level14-InterCompanyTransferReceive' as dd_sourcescriptstepname, 114008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 14 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomreceive
);

/* Level 14 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level14_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomreceive
);





/* Level 15 - Stage */

/* Level 15 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 14 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-SubContractIssue' as dd_sourcescriptstepname, 115001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level14_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 15 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-SubContractReceive' as dd_sourcescriptstepname, 115002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 15 - Step 2a - Get Production Issue for Any Given Batch From Level 14 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-ProductionIssue' as dd_sourcescriptstepname, 115003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level14_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 15 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-ProductionReceive' as dd_sourcescriptstepname, 115004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 15 - Process MM Transfer (309) */
/* Level 15 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 14 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-MMT309Issue' as dd_sourcescriptstepname, 115005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level14_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 15 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-MMT309Receive' as dd_sourcescriptstepname, 115006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 15 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 14 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-InterCompanyTransferIssue' as dd_sourcescriptstepname, 115007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level14_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 15 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level15-InterCompanyTransferReceive' as dd_sourcescriptstepname, 115008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 15 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomreceive
);

/* Level 15 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level15_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomreceive
);





/* Level 16 - Stage */

/* Level 16 - Step 1a - Get Sub Contract Issue for Any Given Batch From Level 15 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-SubContractIssue' as dd_sourcescriptstepname, 116001 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level15_receive l
where a.dd_movementtype in ('541','543') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 16 - Step 1b - Get Sub Contract Receive for Batches in Level10/Step1 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-SubContractReceive' as dd_sourcescriptstepname, 116002 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem;

/* Level 16 - Step 2a - Get Production Issue for Any Given Batch From Level 15 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-ProductionIssue' as dd_sourcescriptstepname, 116003 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level15_receive l
where a.dd_movementtype in ('261','262') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode; /* and dd_level0batchno = 'L44308' */

/* Level 16 - Step 2b - Get Production Receive for Batches in Level10/Step2 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-ProductionReceive' as dd_sourcescriptstepname, 116004 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101F_All a
where t.dd_productionordernumber = a.dd_productionordernumber;

/* Level 16 - Process MM Transfer (309) */
/* Level 16 - Step 3a - Get MM Transfer Issue (309) for Any Given Batch From Level 15 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309issue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309issue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-MMT309Issue' as dd_sourcescriptstepname, 116005 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level15_receive l
where a.dd_movementtype in ('309') and a.dd_debitcreditid = 'Credit'
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and a.dd_plantcode = l.dd_plantcode;

/* Level 16 - Step 3b - Get MM Transfer Receive (309) for Batches in Level10/Step3 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309receive as
select distinct 
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant, 
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate,
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-MMT309Receive' as dd_sourcescriptstepname, 116006 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309issue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_309D_All a
where t.dd_materialdocno = a.dd_materialdocno and t.dd_receivingpartnumber = a.dd_partnumber and
		t.dd_receivingplantcode = a.dd_plantcode and t.dd_receivingbatchnumber = a.dd_batchnumber ;

/* Level 16 - Step 4a - Get Inter Company Stock Transfer Issue for Any Given Batch From Level 15 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomissue;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomissue as
select distinct
l.dd_levelbottomup dd_levelbottomup, l.dd_level0batchno, l.dd_level0partno, l.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-InterCompanyTransferIssue' as dd_sourcescriptstepname, 116007 as dd_sourcescriptstepsequence, 'Issue' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level0 a, tmp_factmmbatchhier_rbg_mmg_tnn_level15_issuereceive l
where a.dd_movementtype in ('641','647','643','645') 
and a.dd_batchnumber = l.dd_batchnumber and a.dd_partnumber = l.dd_partnumber and 
a.dd_plantcode = l.dd_plantcode and l.dd_issuereceive = 'Receive'; /* and dd_level0batchno = 'L44308' */

/* Level 16 - Step 4b - Get Inter Company Stock Transfer Receive for Batches in Level10/Step4 */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomreceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomreceive as
select distinct
t.dd_levelbottomup + 1 dd_levelbottomup, t.dd_level0batchno, t.dd_level0partno, t.dd_level0plant,
a.dd_partnumber, a.dd_plantcode, a.dd_batchnumber, a.dd_postingdate, a.ct_quantity, 
a.dd_movementtype, a.dd_movementindicator, a.dd_productionordernumber, 
a.dd_purchaseorder, a.dd_purchaseorderitem,
a.dd_materialdocno, a.dd_materialdocitemno, a.dd_materialdocyear, a.dim_plantid, a.dim_partid, a.dim_dateidpostingdate, 
a.dd_debitcreditid, a.dim_receivingplantid, a.dim_receivingpartid, 
a.dd_receivingpartnumber, a.dd_receivingplantcode, a.dd_receivingbatchnumber,
'Level16-InterCompanyTransferReceive' as dd_sourcescriptstepname, 116008 as dd_sourcescriptstepsequence, 'Receive' as dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomissue t, tmp_factmmbatchhier_rbg_mmg_tnn_level0_101B_All a
where t.dd_purchaseorder = a.dd_purchaseorder and t.dd_purchaseorderitem = a.dd_purchaseorderitem
and t.dd_partnumber = a.dd_partnumber and t.dd_batchnumber = a.dd_batchnumber;
		
		
/* Level 16 - Step 5 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Issue Receive Material Movements */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_issuereceive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_issuereceive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordissue  
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309issue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomissue
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomreceive
);

/* Level 16 - Step 6 - Union Sub Contract, Production, MM Transfer, Inter Company Transfer Receive Material Movements For Next Step */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_receive;
create table tmp_factmmbatchhier_rbg_mmg_tnn_level16_receive as
select distinct
dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
dd_movementtype, dd_movementindicator, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem,
dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from (
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconreceive
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordreceive
union all  
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309receive 
union all
select * from tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomreceive
);










/* Combine All Levels Into One Single Table */
/*
drop table if exists tmp_fact_reverse_batchgenealogy
create table tmp_fact_reverse_batchgenealogy as
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp union all
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive union all
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level2_issuereceive union all
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level3_issuereceive union all 
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level4_issuereceive union all 
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level5_issuereceive union all 
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level6_issuereceive union all 
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from tmp_factmmbatchhier_rbg_mmg_tnn_level7_issuereceive union all 
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level8_issuereceive union all 
select 	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
		dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
		dd_movementtype, dd_movementindicator, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem,
		dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
		dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
		dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
		dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level9_issuereceive
*/
--DECIMAL(18,0)

/* Insert Data Into Temp Fact Table - Level 0 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp;


/* Insert Data Into Temp Fact Table - Level 1 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive;


/* Insert Data Into Temp Fact Table - Level 2 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level2_issuereceive;



/* Insert Data Into Temp Fact Table - Level 3 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level3_issuereceive;



/* Insert Data Into Temp Fact Table - Level 4 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level4_issuereceive;



/* Insert Data Into Temp Fact Table - Level 5 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level5_issuereceive;



/* Insert Data Into Temp Fact Table - Level 6 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level6_issuereceive;




/* Insert Data Into Temp Fact Table - Level 7 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level7_issuereceive;



/* Insert Data Into Temp Fact Table - Level 8 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level8_issuereceive;



/* Insert Data Into Temp Fact Table - Level 9 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level9_issuereceive;


/* Insert Data Into Temp Fact Table - Level 10 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level10_issuereceive;


/* Insert Data Into Temp Fact Table - Level 11 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level11_issuereceive;


/* Insert Data Into Temp Fact Table - Level 12 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level12_issuereceive;



/* Insert Data Into Temp Fact Table - Level 13 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level13_issuereceive;



/* Insert Data Into Temp Fact Table - Level 14 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level14_issuereceive;



/* Insert Data Into Temp Fact Table - Level 15 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level15_issuereceive;


/* Insert Data Into Temp Fact Table - Level 16 */
delete from number_fountain m where m.table_name = 'tmp_fact_reverse_batchgenealogy';

insert into number_fountain
select  'tmp_fact_reverse_batchgenealogy' as table_name, 
		ifnull(max(f.fact_reverse_batchgenealogyid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0)) as max_id
from tmp_fact_reverse_batchgenealogy f;

insert into tmp_fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid, 
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
)
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_reverse_batchgenealogy') 
	+ row_number() over(order by '') fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
from tmp_factmmbatchhier_rbg_mmg_tnn_level16_issuereceive;


/* Populate Fact Table With the combined data */
/*
truncate table fact_reverse_batchgenealogy_mmgrain_treenonodes
insert into fact_reverse_batchgenealogy_mmgrain_treenonodes 
select row_number() over(order by '' ) fact_reverse_batchgenealogyid,
t.*
FROM  tmp_fact_reverse_batchgenealogy t
*/

truncate table fact_reverse_batchgenealogy;
insert into fact_reverse_batchgenealogy
(	fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive 
) 
select 
	fact_reverse_batchgenealogyid,
	dd_levelbottomup, dd_level0batchno, dd_level0partno, dd_level0plant,
	dd_partnumber, dd_plantcode, dd_batchnumber, dd_postingdate, ct_quantity, 
	dd_movementtype, dd_movementindicator, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem,
	dd_materialdocno, dd_materialdocitemno, dd_materialdocyear, dim_plantid, dim_partid, dim_dateidpostingdate,
	dd_debitcreditid, dim_receivingplantid, dim_receivingpartid, 
	dd_receivingpartnumber, dd_receivingplantcode, dd_receivingbatchnumber, 
	dd_sourcescriptstepname, dd_sourcescriptstepsequence, dd_issuereceive
from  tmp_fact_reverse_batchgenealogy t;



/* Drop Temp Tables */
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_101b_all;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_101f_all;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_309d_all;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level0_apidp;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level1_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level2_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level3_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level4_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level5_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level6_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level7_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level8_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level9_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level10_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level11_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level12_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level13_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level14_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level15_subconreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_intcomreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_issuereceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309issue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_mmt309receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_prodordreceive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_receive;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconissue;
drop table if exists tmp_factmmbatchhier_rbg_mmg_tnn_level16_subconreceive;
drop table if exists tmp_fact_reverse_batchgenealogy;



