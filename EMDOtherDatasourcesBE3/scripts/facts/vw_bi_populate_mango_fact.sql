/******************************************************************************************************************/
/*   Script         : bi_populate_lims_fact                                                                       */
/*   Author         : CorneliaM                                                                                   */
/*   Created On     : 10 Feb 2017                                                                                 */
/*   Description    : Populating script of fact_mango                                                             */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   10 Feb 2017         CorneliaM       1.0               Creating the script.                                   */
/*   30 Oct 2017         CristianT       1.1               Changed the join logic with dim_qualityusers           */
/******************************************************************************************************************/

/* 24 Aug 2017 CristianT Start: Added specific number_fountain table for the autocommit property */

DROP TABLE IF EXISTS number_fountain_fact_mango;
CREATE TABLE number_fountain_fact_mango LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

/* 24 Aug 2017 CristianT End */

DROP TABLE IF EXISTS tmp_fact_mango;
CREATE TABLE tmp_fact_mango
AS
SELECT *
FROM fact_mango
WHERE 1 = 0;

DELETE FROM number_fountain_fact_mango WHERE table_name = 'tmp_fact_mango';	


INSERT INTO number_fountain_fact_mango
SELECT 'tmp_fact_mango',IFNULL(MAX(fact_mangoid),0)
FROM tmp_fact_mango;


INSERT INTO tmp_fact_mango(
fact_mangoid,
dim_qualityusersid,
dim_datedocumentstatusid,
dim_dateoriginalcreationid, 
dim_dateperiodicreviewstartid, 
dim_dateefectstartid, 
dim_dateperiodicreviewnotifyid, 
dd_periodicreviewinterval, 
dim_datelastreviewid, 
dim_datenextreviewid, 
dim_dateplannedeffectiveid, 
dd_englishtitle, 
dd_authorsite, 
dd_systemname, 
dim_datereviewid, 
dd_authors, 
dd_versionlabel, 
amt_exchangerate_gbl, 
amt_exchangerate, 
dim_currencyid_tra, 
dim_currencyid_gbl, 
dw_insert_date, 
dw_update_date, 
dim_projectsourceid, 
dd_robjectid, 
dd_object_name, 
dd_title, 
dd_document_type, 
dd_document_subtype, 
dd_document_unit, 
dd_legacy_document, 
dd_original_creation_date,
dd_document_status_date,
dd_periodic_review_start_date,
dd_effect_start,
dd_periodic_review_notify_date,
dd_last_review_date,
dd_next_review_date,
dd_planned_effective_date,
dd_review_date,
dd_document_status,
dd_status,
dd_source_view,
dd_user_login_name,
dd_user_address
)
SELECT IFNULL((select ifnull(m.max_id, 1) from number_fountain_fact_mango m where m.table_name = 'tmp_fact_mango'),0) + row_number() over(order by '') fact_mangoid,
       1 dim_qualityusersid,
       1 dim_datedocumentstatusid,
       1 DIM_DATEORIGINALCREATIONID, 
       1 DIM_DATEPERIODICREVIEWSTARTID, 
       1 DIM_DATEEFECTSTARTID, 
       1 DIM_DATEPERIODICREVIEWNOTIFYID, 
       IFNULL(RW.PERIODIC_REVIEW_INTERVAL,'Not Set'), 
       1 DIM_DATELASTREVIEWID, 
       1 DIM_DATENEXTREVIEWID, 
       1 DIM_DATEPLANNEDEFFECTIVEID, 
       IFNULL(RW.ENGLISH_TITLE,'Not Set'), 
       IFNULL(RW.AUTHOR_SITE,'Not Set'), 
       IFNULL(RW.SYSTEM_NAME,'Not Set'), 
       1 DIM_DATEREVIEWID, 
       IFNULL(RW.AUTHORS,'Not Set'), 
       IFNULL(RW.R_VERSION_LABEL,'Not Set'), 
       1 AMT_EXCHANGERATE_GBL, 
       1 AMT_EXCHANGERATE, 
       1 DIM_CURRENCYID_TRA, 
       1 DIM_CURRENCYID_GBL, 
       current_date DW_INSERT_DATE, 
       current_date DW_UPDATE_DATE, 
       1, 
       IFNULL(M.R_OBJECT_ID,'Not Set'), 
       IFNULL(M.OBJECT_NAME,'Not Set'), 
       IFNULL(RW.TITLE,'Not Set'), 
       IFNULL(RW.DOCUMENT_TYPE,'Not Set'), 
       IFNULL(RW.DOCUMENT_SUBTYPE,'Not Set'), 
       IFNULL(RW.DOCUMENT_UNIT,'Not Set'), 
       IFNULL(RW.LEGACY_DOCUMENT,'Not Set'),
       IFNULL(RW.original_creation_date,'0001-01-01') dd_original_creation_date,
       IFNULL(RW.DOCUMENT_STATUS_DATE,'0001-01-01') dd_DOCUMENT_STATUS_DATE,
       IFNULL(RW.PERIODIC_REVIEW_START_DATE,'0001-01-01') dd_PERIODIC_REVIEW_START_DATE,
       IFNULL(RW.EFFECT_START,'0001-01-01') dd_EFFECT_START,
       IFNULL(RW.PERIODIC_REVIEW_NOTIFY_DATE,'0001-01-01') dd_PERIODIC_REVIEW_NOTIFY_DATE,
       IFNULL(RW.LAST_REVIEW_DATE,'0001-01-01') dd_LAST_REVIEW_DATE,
       IFNULL(RW.NEXT_REVIEW_DATE,'0001-01-01') dd_NEXT_REVIEW_DATE,
       IFNULL(RW.PLANNED_EFFECTIVE_DATE,'0001-01-01') dd_PLANNED_EFFECTIVE_DATE,
       IFNULL(RW.REVIEW_DATE,'0001-01-01') dd_REVIEW_DATE,
       IFNULL(RW.DOCUMENT_STATUS,'Not Set'),
       IFNULL(RW.STATUS,'Not Set'),
       'Pending For Review' AS DD_SOURCE_VIEW,
       IFNULL(RW.USER_LOGIN_NAME,'Not Set') as dd_user_login_name,
       IFNULL(RW.USER_ADDRESS,'Not Set') as dd_user_address
FROM  MSV_MANGO_DOC_TO_REVIEW RW
LEFT JOIN  mv_dm_mainview m  ON 
RW.OBJECT_NAME  = M.OBJECT_NAME
and RW.authors  = M.authors
and RW.r_version_label  = M.r_version_label;

DELETE FROM number_fountain_fact_mango WHERE table_name = 'tmp_fact_mango';

INSERT INTO number_fountain_fact_mango
SELECT 'tmp_fact_mango',IFNULL(MAX(fact_mangoid),0)
FROM tmp_fact_mango;


INSERT INTO tmp_fact_mango(
fact_mangoid,
dim_qualityusersid,
dim_datedocumentstatusid,
dim_dateoriginalcreationid, 
dim_dateperiodicreviewstartid, 
dim_dateefectstartid, 
dim_dateperiodicreviewnotifyid, 
dd_periodicreviewinterval, 
dim_datelastreviewid, 
dim_datenextreviewid, 
dim_dateplannedeffectiveid, 
dd_englishtitle, 
dd_authorsite, 
dd_systemname, 
dim_datereviewid, 
dd_authors, 
dd_versionlabel, 
amt_exchangerate_gbl, 
amt_exchangerate, 
dim_currencyid_tra, 
dim_currencyid_gbl, 
dw_insert_date, 
dw_update_date, 
dim_projectsourceid, 
dd_robjectid, 
dd_object_name, 
dd_title, 
dd_document_type, 
dd_document_subtype, 
dd_document_unit, 
dd_legacy_document, 
dd_original_creation_date,
dd_document_status_date,
dd_periodic_review_start_date,
dd_effect_start,
dd_periodic_review_notify_date,
dd_last_review_date,
dd_next_review_date,
dd_planned_effective_date,
dd_review_date,
dd_document_status,
dd_status,
dd_source_view,
dd_user_login_name,
dd_user_address
)
SELECT IFNULL((select ifnull(m.max_id, 1) from number_fountain_fact_mango m where m.table_name = 'tmp_fact_mango'),0) + row_number() over(order by '') fact_mangoid,
       1 dim_qualityusersid,
       1 dim_datedocumentstatusid,
       1 DIM_DATEORIGINALCREATIONID, 
       1 DIM_DATEPERIODICREVIEWSTARTID, 
       1 DIM_DATEEFECTSTARTID, 
       1 DIM_DATEPERIODICREVIEWNOTIFYID, 
       IFNULL(LT.PERIODIC_REVIEW_INTERVAL,'Not Set'), 
       1 DIM_DATELASTREVIEWID, 
       1 DIM_DATENEXTREVIEWID, 
       1 DIM_DATEPLANNEDEFFECTIVEID, 
       IFNULL(LT.ENGLISH_TITLE,'Not Set'), 
       IFNULL(LT.AUTHOR_SITE,'Not Set'), 
       IFNULL(LT.SYSTEM_NAME,'Not Set'), 
       1 DIM_DATEREVIEWID, 
       IFNULL(LT.AUTHORS,'Not Set'), 
       IFNULL(LT.R_VERSION_LABEL,'Not Set'), 
       1 AMT_EXCHANGERATE_GBL, 
       1 AMT_EXCHANGERATE, 
       1 DIM_CURRENCYID_TRA, 
       1 DIM_CURRENCYID_GBL, 
       current_date DW_INSERT_DATE, 
       current_date DW_UPDATE_DATE, 
       1, 
       IFNULL(M.R_OBJECT_ID,'Not Set'), 
       IFNULL(M.OBJECT_NAME,'Not Set'), 
       IFNULL(LT.TITLE,'Not Set'), 
       IFNULL(LT.DOCUMENT_TYPE,'Not Set'), 
       IFNULL(LT.DOCUMENT_SUBTYPE,'Not Set'), 
       IFNULL(LT.DOCUMENT_UNIT,'Not Set'), 
       IFNULL(LT.LEGACY_DOCUMENT,'Not Set'),
       IFNULL(LT.original_creation_date,'0001-01-01') dd_original_creation_date,
       IFNULL(LT.DOCUMENT_STATUS_DATE,'0001-01-01') dd_DOCUMENT_STATUS_DATE,
       IFNULL(LT.PERIODIC_REVIEW_START_DATE,'0001-01-01') dd_PERIODIC_REVIEW_START_DATE,
       IFNULL(LT.EFFECT_START,'0001-01-01') dd_EFFECT_START,
       IFNULL(LT.PERIODIC_REVIEW_NOTIFY_DATE,'0001-01-01') dd_PERIODIC_REVIEW_NOTIFY_DATE,
       IFNULL(LT.LAST_REVIEW_DATE,'0001-01-01') dd_LAST_REVIEW_DATE,
       IFNULL(LT.NEXT_REVIEW_DATE,'0001-01-01') dd_NEXT_REVIEW_DATE,
       IFNULL(LT.PLANNED_EFFECTIVE_DATE,'0001-01-01') dd_PLANNED_EFFECTIVE_DATE,
       IFNULL(LT.REVIEW_DATE,'0001-01-01') dd_REVIEW_DATE,
       IFNULL(LT.DOCUMENT_STATUS,'Not Set'),
       IFNULL(LT.STATUS,'Not Set'),
       'LATE' AS DD_SOURCE_VIEW,
       IFNULL(LT.USER_LOGIN_NAME,'Not Set') as dd_user_login_name,
       IFNULL(LT.USER_ADDRESS,'Not Set') as dd_user_address
FROM  MSV_MANGO_DOC_LATE LT
LEFT JOIN  mv_dm_mainview m  ON 
LT.OBJECT_NAME  = M.OBJECT_NAME
and lt.authors  = M.authors
and lt.r_version_label  = M.r_version_label;


-- updates for Date dimensions 
/* 30 Oct 2017 CristianT Start: Changed the join logic with dim_qualityusers*/
/* Old logic is commented here
UPDATE tmp_fact_mango F
SET F.DIM_qualityusersid = D.DIM_qualityusersid
FROM tmp_fact_mango F
LEFT JOIN DIM_qualityusers D ON trim(upper(REPLACE(f.dd_authors,'(legacy)',''))) = trim(upper(replace(d.LN_FN,',',''))) AND d.rowiscurrent = 1
WHERE F.DIM_qualityusersid  <> D.DIM_qualityusersid
*/

/* Step 1: Using User Login Name */
UPDATE tmp_fact_mango f
SET f.dim_qualityusersid = d.dim_qualityusersid
FROM tmp_fact_mango f,
     dim_qualityusers d
WHERE f.dd_user_login_name = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersid  <> d.dim_qualityusersid;

/* Step 2: Using first part of the User Adress */
UPDATE tmp_fact_mango f
SET f.dim_qualityusersid = d.dim_qualityusersid
FROM tmp_fact_mango f,
     dim_qualityusers d
WHERE upper(substr(f.dd_user_address, 0, instr(f.dd_user_address, '@', 1)-1)) = upper(substr(d.mail, 0, instr(d.mail, '@', 1)-1))
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersid = 1
      AND f.dim_qualityusersid <> d.dim_qualityusersid;

/* 30 Oct 2017 CristianT End */


UPDATE tmp_fact_mango F
SET F.DIM_DATEORIGINALCREATIONID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_original_creation_date) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEORIGINALCREATIONID  <> D.DIM_DATEID;


UPDATE tmp_fact_mango F
SET F.DIM_DATEDOCUMENTSTATUSID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_DOCUMENT_STATUS_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEDOCUMENTSTATUSID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATEPERIODICREVIEWSTARTID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_PERIODIC_REVIEW_START_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEPERIODICREVIEWSTARTID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATEEFECTSTARTID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_EFFECT_START) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEEFECTSTARTID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATEPERIODICREVIEWNOTIFYID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_PERIODIC_REVIEW_NOTIFY_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEPERIODICREVIEWNOTIFYID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATELASTREVIEWID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_LAST_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATELASTREVIEWID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATENEXTREVIEWID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_NEXT_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATENEXTREVIEWID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATENEXTREVIEWID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_NEXT_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATENEXTREVIEWID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATEPLANNEDEFFECTIVEID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_PLANNED_EFFECTIVE_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEPLANNEDEFFECTIVEID  <> D.DIM_DATEID;

UPDATE tmp_fact_mango F
SET F.DIM_DATEREVIEWID = D.DIM_DATEID
FROM tmp_fact_mango F
JOIN DIM_DATE D ON to_date(f.dd_REVIEW_DATE) = D.datevalue 
AND d.CompanyCode  = 'Not Set'
WHERE F.DIM_DATEREVIEWID  <> D.DIM_DATEID;



UPDATE tmp_fact_mango tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_mango tmp;

TRUNCATE TABLE fact_mango;

INSERT INTO fact_mango(
fact_mangoid,
dim_qualityusersid,
dim_datedocumentstatusid,
dim_dateoriginalcreationid, 
dim_dateperiodicreviewstartid, 
dim_dateefectstartid, 
dim_dateperiodicreviewnotifyid, 
dd_periodicreviewinterval, 
dim_datelastreviewid, 
dim_datenextreviewid, 
dim_dateplannedeffectiveid, 
dd_englishtitle, 
dd_authorsite, 
dd_systemname, 
dim_datereviewid, 
dd_authors, 
dd_versionlabel, 
amt_exchangerate_gbl, 
amt_exchangerate, 
dim_currencyid_tra, 
dim_currencyid_gbl, 
dw_insert_date, 
dw_update_date, 
dim_projectsourceid, 
dd_robjectid, 
dd_object_name, 
dd_title, 
dd_document_type, 
dd_document_subtype, 
dd_document_unit, 
dd_legacy_document, 
dd_original_creation_date,
dd_document_status_date,
dd_periodic_review_start_date,
dd_effect_start,
dd_periodic_review_notify_date,
dd_last_review_date,
dd_next_review_date,
dd_planned_effective_date,
dd_review_date,
dd_document_status,
dd_status,
dd_source_view,
dd_user_login_name,
dd_user_address
)
SELECT fact_mangoid,
       dim_qualityusersid,
       dim_datedocumentstatusid,
       dim_dateoriginalcreationid, 
       dim_dateperiodicreviewstartid, 
       dim_dateefectstartid, 
       dim_dateperiodicreviewnotifyid, 
       dd_periodicreviewinterval, 
       dim_datelastreviewid, 
       dim_datenextreviewid, 
       dim_dateplannedeffectiveid, 
       dd_englishtitle, 
       dd_authorsite, 
       dd_systemname, 
       dim_datereviewid, 
       dd_authors, 
       dd_versionlabel, 
       amt_exchangerate_gbl, 
       amt_exchangerate, 
       dim_currencyid_tra, 
       dim_currencyid_gbl, 
       dw_insert_date, 
       dw_update_date, 
       dim_projectsourceid, 
       dd_robjectid, 
       dd_object_name, 
       dd_title, 
       dd_document_type, 
       dd_document_subtype, 
       dd_document_unit, 
       dd_legacy_document, 
       dd_original_creation_date,
       dd_document_status_date,
       dd_periodic_review_start_date,
       dd_effect_start,
       dd_periodic_review_notify_date,
       dd_last_review_date,
       dd_next_review_date,
       dd_planned_effective_date,
       dd_review_date,
       dd_document_status,
       dd_status,
       dd_source_view,
       dd_user_login_name,
       dd_user_address
FROM tmp_fact_mango;

DROP TABLE IF EXISTS tmp_fact_mango;

/* CristianT Start: Mask German people */
DROP TABLE IF EXISTS tmp_mangoauthorgerman;
CREATE TABLE tmp_mangoauthorgerman
AS
SELECT fact_mangoid
FROM fact_mango fct,
     dim_qualityusers dim
WHERE 1 = 1
      AND fct.dim_qualityusersid = dim.dim_qualityusersid
      AND dim.CMGNUMBER = '1500';

UPDATE fact_mango fct
SET fct.DD_AUTHORS = 'Anonymous'
FROM tmp_mangoauthorgerman tmp,
     fact_mango fct
WHERE fct.fact_mangoid = tmp.fact_mangoid;

DROP TABLE IF EXISTS tmp_mangoauthorgerman;

/* CristianT End: Mask German people */

TRUNCATE TABLE emd586.fact_mango;

INSERT INTO emd586.fact_mango
SELECT *
FROM fact_mango;