/*****************************************************************************************************************/
/*   Script         : bi_populate_trackwisehierarchy_fact                                                        */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 09 Sep 2017                                                                                */
/*   Description    : Populating script of fact_trackwisehierarchy                                               */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   09 Sep 2016         CristianT      1.0          Creating the script.                                        */
/*   08 Oct 2018         CristianT      1.1          Added hc_site for all 6 levels APP-10512                    */
/*   14 May 2019         CristianT      1.2          Changed the source of data to HDFS                          */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS number_fountain_trackwisehierarchy;
CREATE TABLE number_fountain_trackwisehierarchy LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_trackwisehierarchy;
CREATE TABLE tmp_fact_trackwisehierarchy LIKE fact_trackwisehierarchy INCLUDING DEFAULTS INCLUDING IDENTITY;


INSERT INTO number_fountain_trackwisehierarchy
SELECT 'fact_trackwisehierarchy', ifnull(max(fact_trackwisehierarchyid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_trackwisehierarchy;


INSERT INTO tmp_fact_trackwisehierarchy(
fact_trackwisehierarchyid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_l1_id,
dd_l1_parentid,
dd_l1_project,
dd_l1_site,
dd_l1_title,
dd_l1_state,
dd_l2_id,
dd_l2_parentid,
dd_l2_project,
dd_l2_site,
dd_l2_title,
dd_l2_state,
dd_l3_id,
dd_l3_parentid,
dd_l3_project,
dd_l3_site,
dd_l3_title,
dd_l3_state,
dd_l4_id,
dd_l4_parentid,
dd_l4_project,
dd_l4_site,
dd_l4_title,
dd_l4_state,
dd_l5_id,
dd_l5_parentid,
dd_l5_project,
dd_l5_site,
dd_l5_title,
dd_l5_state,
dd_l6_id,
dd_l6_parentid,
dd_l6_project,
dd_l6_site,
dd_l6_title,
dd_l6_state,
dd_l1_hc_site,
dd_l2_hc_site,
dd_l3_hc_site,
dd_l4_hc_site,
dd_l5_hc_site,
dd_l6_hc_site
)
SELECT (SELECT max_id from number_fountain_trackwisehierarchy WHERE table_name = 'fact_trackwisehierarchy') + ROW_NUMBER() over(order by '') AS fact_trackwisehierarchyid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(l1_id, 0) as dd_l1_id,
       ifnull(l1_parentid, 0) dd_l1_parentid,
       ifnull(l1_project, 'Not Set') dd_l1_project,
       ifnull(l1_site, 'Not Set') dd_l1_site,
       ifnull(l1_title, 'Not Set') dd_l1_title,
       ifnull(l1_state, 'Not Set') dd_l1_state,
       ifnull(l2_id, 0) as dd_l2_id,
       ifnull(l2_parentid, 0) as dd_l2_parentid,
       ifnull(l2_project, 'Not Set') dd_l2_project,
       ifnull(l2_site, 'Not Set') dd_l2_site,
       ifnull(l2_title, 'Not Set') dd_l2_title,
       ifnull(l2_state, 'Not Set') dd_l2_state,
       ifnull(l3_id, 0) as dd_l3_id,
       ifnull(l3_parentid, 0) as dd_l3_parentid,
       ifnull(l3_project, 'Not Set') dd_l3_project,
       ifnull(l3_site, 'Not Set') dd_l3_site,
       ifnull(l3_title, 'Not Set') dd_l3_title,
       ifnull(l3_state, 'Not Set') dd_l3_state,
       ifnull(l4_id, 0) as dd_l4_id,
       ifnull(l4_parentid, 0) as dd_l4_parentid,
       ifnull(l4_project, 'Not Set') dd_l4_project,
       ifnull(l4_site, 'Not Set') dd_l4_site,
       ifnull(l4_title, 'Not Set') dd_l4_title,
       ifnull(l4_state, 'Not Set') dd_l4_state,
       ifnull(l5_id, 0) as dd_l5_id,
       ifnull(l5_parentid, 0) as dd_l5_parentid,
       ifnull(l5_project, 'Not Set') dd_l5_project,
       ifnull(l5_site, 'Not Set') dd_l5_site,
       ifnull(l5_title, 'Not Set') dd_l5_title,
       ifnull(l5_state, 'Not Set') dd_l5_state,
       ifnull(l6_id, 0) as dd_l6_id,
       ifnull(l6_parentid, 0) as dd_l6_parentid,
       ifnull(l6_project, 'Not Set') dd_l6_project,
       ifnull(l6_site, 'Not Set') dd_l6_site,
       ifnull(l6_title, 'Not Set') dd_l6_title,
       ifnull(l6_state, 'Not Set') dd_l6_state,
       ifnull(l1_hc_site, 'Not Set') as dd_l1_hc_site,
       ifnull(l2_hc_site, 'Not Set') as dd_l2_hc_site,
       ifnull(l3_hc_site, 'Not Set') as dd_l3_hc_site,
       ifnull(l4_hc_site, 'Not Set') as dd_l4_hc_site,
       ifnull(l5_hc_site, 'Not Set') as dd_l5_hc_site,
       ifnull(l6_hc_site, 'Not Set') as dd_l6_hc_site
FROM tw_hierarchy tmp
     ) t;

UPDATE tmp_fact_trackwisehierarchy tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_trackwisehierarchy tmp;


TRUNCATE TABLE fact_trackwisehierarchy;

INSERT INTO fact_trackwisehierarchy(
fact_trackwisehierarchyid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_l1_id,
dd_l1_parentid,
dd_l1_project,
dd_l1_site,
dd_l1_title,
dd_l1_state,
dd_l2_id,
dd_l2_parentid,
dd_l2_project,
dd_l2_site,
dd_l2_title,
dd_l2_state,
dd_l3_id,
dd_l3_parentid,
dd_l3_project,
dd_l3_site,
dd_l3_title,
dd_l3_state,
dd_l4_id,
dd_l4_parentid,
dd_l4_project,
dd_l4_site,
dd_l4_title,
dd_l4_state,
dd_l5_id,
dd_l5_parentid,
dd_l5_project,
dd_l5_site,
dd_l5_title,
dd_l5_state,
dd_l6_id,
dd_l6_parentid,
dd_l6_project,
dd_l6_site,
dd_l6_title,
dd_l6_state,
dd_l1_hc_site,
dd_l2_hc_site,
dd_l3_hc_site,
dd_l4_hc_site,
dd_l5_hc_site,
dd_l6_hc_site
)
SELECT fact_trackwisehierarchyid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_l1_id,
       dd_l1_parentid,
       dd_l1_project,
       dd_l1_site,
       dd_l1_title,
       dd_l1_state,
       dd_l2_id,
       dd_l2_parentid,
       dd_l2_project,
       dd_l2_site,
       dd_l2_title,
       dd_l2_state,
       dd_l3_id,
       dd_l3_parentid,
       dd_l3_project,
       dd_l3_site,
       dd_l3_title,
       dd_l3_state,
       dd_l4_id,
       dd_l4_parentid,
       dd_l4_project,
       dd_l4_site,
       dd_l4_title,
       dd_l4_state,
       dd_l5_id,
       dd_l5_parentid,
       dd_l5_project,
       dd_l5_site,
       dd_l5_title,
       dd_l5_state,
       dd_l6_id,
       dd_l6_parentid,
       dd_l6_project,
       dd_l6_site,
       dd_l6_title,
       dd_l6_state,
       dd_l1_hc_site,
       dd_l2_hc_site,
       dd_l3_hc_site,
       dd_l4_hc_site,
       dd_l5_hc_site,
       dd_l6_hc_site
FROM tmp_fact_trackwisehierarchy;

DROP TABLE IF EXISTS tmp_fact_trackwisehierarchy;

TRUNCATE TABLE emd586.fact_trackwisehierarchy;

INSERT INTO emd586.fact_trackwisehierarchy
SELECT *
FROM fact_trackwisehierarchy;