
drop table if exists tmp_dayofprocessing_var;
create table tmp_dayofprocessing_var
as
select sysdate - 1 as processing_date from (select 1) a;

delete from number_fountain m where m.table_name = 'fact_salesorder_wsnap';

insert into number_fountain
select 	'fact_salesorder_wsnap',
	ifnull(max(fact_salesorder_wsnapid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_salesorder_wsnap d ;

drop table if exists tmp_loaded_datevalues;

create table tmp_loaded_datevalues
as
select distinct(fc.dim_dateid) as dim_dateid
from fact_salesorder_wsnap ws inner join dim_date_factory_calendar fc on ws.dim_dateidsnapshot = fc.dim_Dateid
where to_date(fc.datevalue) = to_date(sysdate-1);																											 /*weekly fc.datevalue >=decode(trim(to_char(to_date(sysdate),'Day')),'Sunday',sysdate,trunc(to_date(sysdate,'dd-mm-yy'),'iw')-1) and to_date(fc.datevalue) <= to_date(sysdate) and to_date(fc.datevalue) <= to_Date(sysdate)*/


DELETE FROM fact_salesorder_wsnap t
where t.dim_dateidsnapshot in (select dim_dateid from tmp_loaded_datevalues ) ;

insert into fact_salesorder_wsnap (
fact_salesorder_wsnapID
,DD_SALESDOCNO
,DD_SALESITEMNO
,DD_SCHEDULENO
,CT_SCHEDULEQTYSALESUNIT
,CT_CONFIRMEDQTY
,CT_CORRECTEDQTY
,CT_DELIVEREDQTY
,CT_ONHANDCOVEREDQTY
,AMT_UNITPRICE
,CT_PRICEUNIT
,AMT_SCHEDULETOTAL
,AMT_STDCOST
,AMT_TARGETVALUE
,CT_TARGETQTY
,AMT_EXCHANGERATE
,CT_OVERDLVRTOLERANCE
,CT_UNDERDLVRTOLERANCE
,DIM_DATEIDSALESORDERCREATED
,DIM_DATEIDSCHEDDELIVERYREQ
,DIM_DATEIDSCHEDDLVRREQPREV
,DIM_DATEIDSCHEDDELIVERY
,DIM_DATEIDGOODSISSUE
,DIM_DATEIDMTRLAVAIL
,DIM_DATEIDLOADING
,DIM_DATEIDTRANSPORT
,DIM_CURRENCYID
,DIM_PRODUCTHIERARCHYID
,DIM_PLANTID
,DIM_COMPANYID
,DIM_STORAGELOCATIONID
,DIM_SALESDIVISIONID
,DIM_SHIPRECEIVEPOINTID
,DIM_DOCUMENTCATEGORYID
,DIM_SALESDOCUMENTTYPEID
,DIM_SALESORGID
,DIM_CUSTOMERID
,DIM_DATEIDVALIDFROM
,DIM_DATEIDVALIDTO
,DIM_SALESGROUPID
,DIM_COSTCENTERID
,DIM_CONTROLLINGAREAID
,DIM_BILLINGBLOCKID
,DIM_TRANSACTIONGROUPID
,DIM_SALESORDERREJECTREASONID
,DIM_PARTID
,DIM_PARTSALESID
,DIM_SALESORDERHEADERSTATUSID
,DIM_SALESORDERITEMSTATUSID
,DIM_CUSTOMERGROUP1ID
,DIM_CUSTOMERGROUP2ID
,DIM_SALESORDERITEMCATEGORYID
,AMT_EXCHANGERATE_GBL
,CT_FILLQTY
,CT_ONHANDQTY
,DIM_DATEIDFIRSTDATE
,DIM_SCHEDULELINECATEGORYID
,DIM_SALESMISCID
,DD_ITEMRELFORDELV
,DIM_DATEIDSHIPMENTDELIVERY
,DIM_DATEIDACTUALGI
,DIM_DATEIDSHIPDLVRFILL
,DIM_DATEIDACTUALGIFILL
,DIM_PROFITCENTERID
,DIM_CUSTOMERIDSHIPTO
,DIM_DATEIDGUARANTEEDATE
,DIM_UNITOFMEASUREID
,DIM_DISTRIBUTIONCHANNELID
,DD_BATCHNO
,DD_CREATEDBY
,DIM_CUSTOMPARTNERFUNCTIONID
,DIM_DATEIDLOADINGFILL
,DIM_PAYERPARTNERFUNCTIONID
,DIM_BILLTOPARTYPARTNERFUNCTIONID
,CT_SHIPPEDAGNSTORDERQTY
,CT_CMLQTYRECEIVED
,DIM_CUSTOMPARTNERFUNCTIONID1
,DIM_CUSTOMPARTNERFUNCTIONID2
,DIM_CUSTOMERGROUPID
,DIM_SALESOFFICEID
,DIM_HIGHERCUSTOMERID
,DIM_HIGHERSALESORGID
,DIM_HIGHERDISTCHANNELID
,DIM_HIGHERSALESDIVID
,DD_SOLDTOHIERARCHYLEVEL
,DD_HIERARCHYTYPE
,DIM_TOPLEVELCUSTOMERID
,DIM_TOPLEVELDISTCHANNELID
,DIM_TOPLEVELSALESDIVID
,DIM_TOPLEVELSALESORGID
,CT_AFSUNALLOCATEDQTY
,AMT_AFSUNALLOCATEDVALUE
,CT_AFSALLOCATIONRQTY
,CT_CUMCONFIRMEDQTY
,CT_AFSALLOCATIONFQTY
,CT_CUMORDERQTY
,AMT_AFSONDELIVERYVALUE
,CT_SHIPPEDORBILLEDQTY
,AMT_SHIPPEDORBILLED
,DIM_CREDITREPRESENTATIVEID
,DD_CUSTOMERPONO
,CT_INCOMPLETEQTY
,AMT_INCOMPLETE
,DIM_DELIVERYBLOCKID
,DD_AFSSTOCKCATEGORY
,DD_AFSSTOCKTYPE
,DIM_DATEIDAFSCANCELDATE
,AMT_AFSNETPRICE
,AMT_AFSNETVALUE
,AMT_BASEPRICE
,DIM_MATERIALPRICEGROUP1ID
,AMT_CREDITHOLDVALUE
,AMT_DELIVERYBLOCKVALUE
,DIM_AFSSIZEID
,DIM_AFSREJECTIONREASONID
,DIM_DATEIDDLVRDOCCREATED
,AMT_CUSTOMEREXPECTEDPRICE
,CT_AFSALLOCATEDQTY
,CT_AFSONDELIVERYQTY
,DIM_OVERALLSTATUSCREDITCHECKID
,DIM_SALESDISTRICTID
,DIM_ACCOUNTASSIGNMENTGROUPID
,DIM_MATERIALGROUPID
,DIM_SALESDOCORDERREASONID
,AMT_SUBTOTAL3
,AMT_SUBTOTAL4
,DIM_DATEIDAFSREQDELIVERY
,AMT_DICOUNTACCRUALNETPRICE
,CT_AFSOPENQTY
,DIM_PURCHASEORDERTYPEID
,DIM_DATEIDQUOTATIONVALIDFROM
,DIM_DATEIDPURCHASEORDER
,DIM_DATEIDQUOTATIONVALIDTO
,DIM_AFSSEASONID
,DD_AFSDEPARTMENT
,DIM_DATEIDSOCREATED
,DIM_DATEIDSODOCUMENT
,DD_SUBSEQUENTDOCNO
,DD_SUBSDOCITEMNO
,DD_SUBSSCHEDULENO
,DIM_SUBSDOCCATEGORYID
,CT_AFSTOTALDRAWN
,DIM_DATEIDASLASTDATE
,DD_REFERENCEDOCUMENTNO
,DD_REQUIREMENTTYPE
,AMT_STDPRICE
,DIM_DATEIDNEXTDATE
,AMT_TAX
,DIM_DATEIDEARLIESTSOCANCELDATE
,DIM_DATEIDLATESTCUSTPOAGI
,DD_BUSINESSCUSTOMERPONO
,DIM_ROUTEID
,DIM_BILLINGDATEID
,DIM_CUSTOMERPAYMENTTERMSID
,DIM_SALESRISKCATEGORYID
,DD_CREDITREP
,DD_CREDITLIMIT
,DIM_CUSTOMERRISKCATEGORYID
,CT_FILLQTY_CRD
,CT_FILLQTY_PDD
,DIM_H1CUSTOMERID
,DIM_DATEIDSOITEMCHANGEDON
,DIM_SHIPPINGCONDITIONID
,DD_AFSALLOCATIONGROUPNO
,DIM_DATEIDREJECTION
,DD_CUSTOMERMATERIALNO
,DIM_BASEUOMID
,DIM_SALESUOMID
,AMT_UNITPRICEUOM
,DIM_DATEIDFIXEDVALUE
,DIM_PAYMENTTERMSID
,DIM_DATEIDNETDUEDATE
,DIM_MATERIALPRICEGROUP4ID
,DIM_MATERIALPRICEGROUP5ID
,AMT_SUBTOTAL3_ORDERQTY
,AMT_SUBTOTAL3INCUSTCONFIG_BILLING
,DIM_CUSTOMERMASTERSALESID
,DD_SALESORDERBLOCKED
,DD_SOCREATETIME
,DD_REQDELIVERYTIME
,DD_SOLINECREATETIME
,DD_DELIVERYTIME
,DD_PLANNEDGITIME
,DIM_DATEIDARPOSTING
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,DIM_CURRENCYID_STAT
,AMT_EXCHANGERATE_STAT
,DD_ARUNNUMBER
,DIM_AVAILABILITYCHECKID
,DD_IDOCNUMBER
,DIM_INCOTERMID
,DD_INTERNATIONALARTICLENO
,DD_RELEASERULE
,DIM_DELIVERYPRIORITYID
,DD_REFERENCEDOCITEMNO
,DIM_SALESORDERPARTNERID
,DD_PURCHASEREQUISITION
,DD_ITEMOFREQUISITION
,CT_AFSCALCULATEDOPENQTY
,AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE
,DD_DOCUMENTCONDITIONNO
,AMT_SUBTOTAL1
,AMT_SUBTOTAL2
,AMT_SUBTOTAL5
,AMT_SUBTOTAL6
,DD_HIGHLEVELITEM
,DD_PODOCUMENTNO
,DD_PODOCUMENTITEMNO
,DD_POSCHEDULENO
,DD_PRODORDERNO
,DD_PRODORDERITEMNO
,DIM_CUSTOMERCONDITIONGROUPS1ID
,DIM_CUSTOMERCONDITIONGROUPS2ID
,DIM_CUSTOMERCONDITIONGROUPS3ID
,CT_AFSOPENPOQTY
,CT_AFSINTRANSITQTY
,DIM_SCHEDULEDELIVERYBLOCKID
,DIM_CUSTOMERGROUP4ID
,DD_OPENCONFIRMATIONSEXISTS
,DD_CONDITIONNO
,DD_CREDITMGR
,DD_CLEAREDBLOCKEDSTS
,CT_AFSINRDDUNITS
,CT_AFSAFTERRDDUNITS
,DIM_AGREEMENTSID
,DIM_CHARGEBACKCONTRACTID
,DD_RO_MATERIALDESC
,DIM_CUSTOMERIDCBOWNER
,CT_COMMITTEDQTY
,DIM_DATEIDREQDELIVLINE
,DIM_MATERIALPRICINGGROUPID
,DIM_BILLINGBLOCKSALESDOCLVLID
,DD_DELIVERYINDICATOR
,DD_SHIPTOPARTYSTREET
,DIM_CUSTOMERCONDITIONGROUPS4ID
,DIM_CUSTOMERCONDITIONGROUPS5ID
,DD_PURCHASEORDERITEM
,DIM_MATERIALPRICEGROUP2ID
,DIM_PROJECTSOURCEID
,DD_BILLING_NO
,DIM_MDG_PARTID
,DIM_BWPRODUCTHIERARCHYID
,CT_FIRSTNONZEROCONFIRMQTY
,CT_VOLUME
,CT_NOTCONFIRMEDQTY
,DD_CUSTOMCUSTEXPPRICEINC
,DD_DOCUMENTFLOWGROUP
,DD_ORDERBY
,DD_CUSTOMPRICEMISSINGINC
,DIM_CUSTOMERENDUSERFORFTRADE
,DIM_DATEIDSNAPSHOT
,dim_accordinggidatemto_emd
,dim_dateidexpectedship_emd
,dim_dateidrequested_emd
,ct_baseuomratio
,ct_baseuomratiokg
,ct_countsalesdocitem
,ct_deliveryisfull
,ct_deliveryontime
,ct_deliveryontimecasual
,dd_ace_openorders
,dd_backorderflag
,dd_backorderreason
,dd_backorder_responsibility
,dd_deliveryisfull
,dd_deliveryontime
,dd_deliveryontimecasual
,dd_intercompflag
,dd_lsintercompflag
,dd_mercklsbackorderfilter
,dd_ora_active_hold_info
, dd_ora_attribute11
,dd_ora_line_on_hold
,dd_ora_tradsales_flag
,dim_backorder_dateid
,dim_commercialviewid
,dim_dateidactualgimerckls
,dim_dateidconfirmeddelivery_emd
,dim_clusterid
,dd_openqtycvrdbyplant
,dim_countryhierpsid
,dim_countryhierarid
,dim_gsamapimportid
,dim_date_enddateloadingid
,dd_openqtycvrdbymat
,dim_customer_shipto
,dim_productionschedulerid
,dd_inco1
,dd_inco2
,dd_ship_to_party
,dd_mtomts
,DIM_ADDRESSID
,dd_CompleteInSingleDelivery
)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_salesorder_wsnap')
          + row_number()
             over(order by '') fact_salesorder_wsnapID
,ifnull(f.DD_SALESDOCNO,'Not Set')
,ifnull(f.DD_SALESITEMNO,0)
,ifnull(f.DD_SCHEDULENO,0)
,ifnull(f.CT_SCHEDULEQTYSALESUNIT,0)
,ifnull(f.CT_CONFIRMEDQTY,0)
,ifnull(f.CT_CORRECTEDQTY,0)
,ifnull(f.CT_DELIVEREDQTY,0)
,ifnull(f.CT_ONHANDCOVEREDQTY,0)
,ifnull(f.AMT_UNITPRICE,0)
,ifnull(f.CT_PRICEUNIT,1)
,ifnull(f.AMT_SCHEDULETOTAL,0)
,ifnull(f.AMT_STDCOST,0)
,ifnull(f.AMT_TARGETVALUE,0)
,ifnull(f.CT_TARGETQTY,0)
,ifnull(f.AMT_EXCHANGERATE,1)
,ifnull(f.CT_OVERDLVRTOLERANCE,0)
,ifnull(f.CT_UNDERDLVRTOLERANCE,0)
,ifnull(f.DIM_DATEIDSALESORDERCREATED,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERYREQ,1)
,ifnull(f.DIM_DATEIDSCHEDDLVRREQPREV,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERY,1)
,ifnull(f.DIM_DATEIDGOODSISSUE,1)
,ifnull(f.DIM_DATEIDMTRLAVAIL,1)
,ifnull(f.DIM_DATEIDLOADING,1)
,ifnull(f.DIM_DATEIDTRANSPORT,1)
,ifnull(f.DIM_CURRENCYID,1)
,ifnull(f.DIM_PRODUCTHIERARCHYID,1)
,ifnull(f.DIM_PLANTID,1)
,ifnull(f.DIM_COMPANYID,1)
,ifnull(f.DIM_STORAGELOCATIONID,1)
,ifnull(f.DIM_SALESDIVISIONID,1)
,ifnull(f.DIM_SHIPRECEIVEPOINTID,1)
,ifnull(f.DIM_DOCUMENTCATEGORYID,1)
,ifnull(f.DIM_SALESDOCUMENTTYPEID,1)
,ifnull(f.DIM_SALESORGID,1)
,ifnull(f.DIM_CUSTOMERID,1)
,ifnull(f.DIM_DATEIDVALIDFROM,1)
,ifnull(f.DIM_DATEIDVALIDTO,1)
,ifnull(f.DIM_SALESGROUPID,0)
,ifnull(f.DIM_COSTCENTERID,1)
,ifnull(f.DIM_CONTROLLINGAREAID,0)
,ifnull(f.DIM_BILLINGBLOCKID,0)
,ifnull(f.DIM_TRANSACTIONGROUPID,0)
,ifnull(f.DIM_SALESORDERREJECTREASONID,0)
,ifnull(f.DIM_PARTID,1)
,ifnull(f.DIM_PARTSALESID,1)
,ifnull(f.DIM_SALESORDERHEADERSTATUSID,0)
,ifnull(f.DIM_SALESORDERITEMSTATUSID,0)
,ifnull(f.DIM_CUSTOMERGROUP1ID,0)
,ifnull(f.DIM_CUSTOMERGROUP2ID,0)
,ifnull(f.DIM_SALESORDERITEMCATEGORYID,1)
,ifnull(f.AMT_EXCHANGERATE_GBL,1)
,ifnull(f.CT_FILLQTY,0)
,ifnull(f.CT_ONHANDQTY,0)
,ifnull(f.DIM_DATEIDFIRSTDATE,1)
,ifnull(f.DIM_SCHEDULELINECATEGORYID,1)
,ifnull(f.DIM_SALESMISCID,1)
,ifnull(f.DD_ITEMRELFORDELV,'Not Set')
,ifnull(f.DIM_DATEIDSHIPMENTDELIVERY,1)
,ifnull(f.DIM_DATEIDACTUALGI,1)
,ifnull(f.DIM_DATEIDSHIPDLVRFILL,1)
,ifnull(f.DIM_DATEIDACTUALGIFILL,1)
,ifnull(f.DIM_PROFITCENTERID,1)
,ifnull(f.DIM_CUSTOMERIDSHIPTO,1)
,ifnull(f.DIM_DATEIDGUARANTEEDATE,1)
,ifnull(f.DIM_UNITOFMEASUREID,1)
,ifnull(f.DIM_DISTRIBUTIONCHANNELID,1)
,ifnull(f.DD_BATCHNO,'Not Set')
,ifnull(f.DD_CREATEDBY,'Not Set')
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID,1)
,ifnull(f.DIM_DATEIDLOADINGFILL,1)
,ifnull(f.DIM_PAYERPARTNERFUNCTIONID,1)
,ifnull(f.DIM_BILLTOPARTYPARTNERFUNCTIONID,1)
,ifnull(f.CT_SHIPPEDAGNSTORDERQTY,0)
,ifnull(f.CT_CMLQTYRECEIVED,0)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID1,1)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID2,1)
,ifnull(f.DIM_CUSTOMERGROUPID,1)
,ifnull(f.DIM_SALESOFFICEID,1)
,ifnull(f.DIM_HIGHERCUSTOMERID,1)
,ifnull(f.DIM_HIGHERSALESORGID,1)
,ifnull(f.DIM_HIGHERDISTCHANNELID,1)
,ifnull(f.DIM_HIGHERSALESDIVID,1)
,ifnull(f.DD_SOLDTOHIERARCHYLEVEL,0)
,ifnull(f.DD_HIERARCHYTYPE,'Not Set')
,ifnull(f.DIM_TOPLEVELCUSTOMERID,1)
,ifnull(f.DIM_TOPLEVELDISTCHANNELID,1)
,ifnull(f.DIM_TOPLEVELSALESDIVID,1)
,ifnull(f.DIM_TOPLEVELSALESORGID,1)
,ifnull(f.CT_AFSUNALLOCATEDQTY,0)
,ifnull(f.AMT_AFSUNALLOCATEDVALUE,0)
,ifnull(f.CT_AFSALLOCATIONRQTY,0)
,ifnull(f.CT_CUMCONFIRMEDQTY,0)
,ifnull(f.CT_AFSALLOCATIONFQTY,0)
,ifnull(f.CT_CUMORDERQTY,0)
,ifnull(f.AMT_AFSONDELIVERYVALUE,0)
,ifnull(f.CT_SHIPPEDORBILLEDQTY,0)
,ifnull(f.AMT_SHIPPEDORBILLED,0)
,ifnull(f.DIM_CREDITREPRESENTATIVEID,1)
,ifnull(f.DD_CUSTOMERPONO,'Not Set')
,ifnull(f.CT_INCOMPLETEQTY,0)
,ifnull(f.AMT_INCOMPLETE,0)
,ifnull(f.DIM_DELIVERYBLOCKID,1)
,ifnull(f.DD_AFSSTOCKCATEGORY,'Not Set')
,ifnull(f.DD_AFSSTOCKTYPE,'Not Set')
,ifnull(f.DIM_DATEIDAFSCANCELDATE,1)
,ifnull(f.AMT_AFSNETPRICE,0)
,ifnull(f.AMT_AFSNETVALUE,0)
,ifnull(f.AMT_BASEPRICE,0)
,ifnull(f.DIM_MATERIALPRICEGROUP1ID,1)
,ifnull(f.AMT_CREDITHOLDVALUE,0)
,ifnull(f.AMT_DELIVERYBLOCKVALUE,0)
,ifnull(f.DIM_AFSSIZEID,1)
,ifnull(f.DIM_AFSREJECTIONREASONID,1)
,ifnull(f.DIM_DATEIDDLVRDOCCREATED,1)
,ifnull(f.AMT_CUSTOMEREXPECTEDPRICE,0)
,ifnull(f.CT_AFSALLOCATEDQTY,0)
,ifnull(f.CT_AFSONDELIVERYQTY,0)
,ifnull(f.DIM_OVERALLSTATUSCREDITCHECKID,1)
,ifnull(f.DIM_SALESDISTRICTID,1)
,ifnull(f.DIM_ACCOUNTASSIGNMENTGROUPID,1)
,ifnull(f.DIM_MATERIALGROUPID,1)
,ifnull(f.DIM_SALESDOCORDERREASONID,1)
,ifnull(f.AMT_SUBTOTAL3,0)
,ifnull(f.AMT_SUBTOTAL4,0)
,ifnull(f.DIM_DATEIDAFSREQDELIVERY,1)
,ifnull(f.AMT_DICOUNTACCRUALNETPRICE,0)
,ifnull(f.CT_AFSOPENQTY,0)
,ifnull(f.DIM_PURCHASEORDERTYPEID,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDFROM,1)
,ifnull(f.DIM_DATEIDPURCHASEORDER,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDTO,1)
,ifnull(f.DIM_AFSSEASONID,1)
,ifnull(f.DD_AFSDEPARTMENT,'Not Set')
,ifnull(f.DIM_DATEIDSOCREATED,1)
,ifnull(f.DIM_DATEIDSODOCUMENT,1)
,ifnull(f.DD_SUBSEQUENTDOCNO,'Not Set')
,ifnull(f.DD_SUBSDOCITEMNO,0)
,ifnull(f.DD_SUBSSCHEDULENO,0)
,ifnull(f.DIM_SUBSDOCCATEGORYID,1)
,ifnull(f.CT_AFSTOTALDRAWN,0)
,ifnull(f.DIM_DATEIDASLASTDATE,1)
,ifnull(f.DD_REFERENCEDOCUMENTNO,'Not Set')
,ifnull(f.DD_REQUIREMENTTYPE,'Not Set')
,ifnull(f.AMT_STDPRICE,0)
,ifnull(f.DIM_DATEIDNEXTDATE,1)
,ifnull(f.AMT_TAX,0)
,ifnull(f.DIM_DATEIDEARLIESTSOCANCELDATE,1)
,ifnull(f.DIM_DATEIDLATESTCUSTPOAGI,1)
,ifnull(f.DD_BUSINESSCUSTOMERPONO,'Not Set')
,ifnull(f.DIM_ROUTEID,1)
,ifnull(f.DIM_BILLINGDATEID,1)
,ifnull(f.DIM_CUSTOMERPAYMENTTERMSID,1)
,ifnull(f.DIM_SALESRISKCATEGORYID,1)
,ifnull(f.DD_CREDITREP,'Not Set')
,ifnull(f.DD_CREDITLIMIT,0)
,ifnull(f.DIM_CUSTOMERRISKCATEGORYID,1)
,ifnull(f.CT_FILLQTY_CRD,0)
,ifnull(f.CT_FILLQTY_PDD,0)
,ifnull(f.DIM_H1CUSTOMERID,1)
,ifnull(f.DIM_DATEIDSOITEMCHANGEDON,1)
,ifnull(f.DIM_SHIPPINGCONDITIONID,1)
,ifnull(f.DD_AFSALLOCATIONGROUPNO,'Not Set')
,ifnull(f.DIM_DATEIDREJECTION,1)
,ifnull(f.DD_CUSTOMERMATERIALNO,'Not Set')
,ifnull(f.DIM_BASEUOMID,1)
,ifnull(f.DIM_SALESUOMID,1)
,ifnull(f.AMT_UNITPRICEUOM,0)
,ifnull(f.DIM_DATEIDFIXEDVALUE,1)
,ifnull(f.DIM_PAYMENTTERMSID,1)
,ifnull(f.DIM_DATEIDNETDUEDATE,1)
,ifnull(f.DIM_MATERIALPRICEGROUP4ID,1)
,ifnull(f.DIM_MATERIALPRICEGROUP5ID,1)
,ifnull(f.AMT_SUBTOTAL3_ORDERQTY,0)
,ifnull(f.AMT_SUBTOTAL3INCUSTCONFIG_BILLING,0)
,ifnull(f.DIM_CUSTOMERMASTERSALESID,1)
,ifnull(f.DD_SALESORDERBLOCKED,'Not Set')
,ifnull(f.DD_SOCREATETIME,'Not Set')
,ifnull(f.DD_REQDELIVERYTIME,'Not Set')
,ifnull(f.DD_SOLINECREATETIME,'Not Set')
,ifnull(f.DD_DELIVERYTIME,'Not Set')
,ifnull(f.DD_PLANNEDGITIME,'Not Set')
,ifnull(f.DIM_DATEIDARPOSTING,1)
,ifnull(f.DIM_CURRENCYID_TRA,1)
,ifnull(f.DIM_CURRENCYID_GBL,1)
,ifnull(f.DIM_CURRENCYID_STAT,1)
,ifnull(f.AMT_EXCHANGERATE_STAT,1)
,ifnull(f.DD_ARUNNUMBER,'Not Set')
,ifnull(f.DIM_AVAILABILITYCHECKID,1)
,ifnull(f.DD_IDOCNUMBER,'Not Set')
,ifnull(f.DIM_INCOTERMID,1)
,ifnull(f.DD_INTERNATIONALARTICLENO,'Not Set')
,ifnull(f.DD_RELEASERULE,'Not Set')
,ifnull(f.DIM_DELIVERYPRIORITYID,1)
,ifnull(f.DD_REFERENCEDOCITEMNO,0)
,ifnull(f.DIM_SALESORDERPARTNERID,1)
,ifnull(f.DD_PURCHASEREQUISITION,'Not Set')
,ifnull(f.DD_ITEMOFREQUISITION,0)
,ifnull(f.CT_AFSCALCULATEDOPENQTY,0)
,ifnull(f.AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE,0)
,ifnull(f.DD_DOCUMENTCONDITIONNO,'Not Set')
,ifnull(f.AMT_SUBTOTAL1,0)
,ifnull(f.AMT_SUBTOTAL2,0)
,ifnull(f.AMT_SUBTOTAL5,0)
,ifnull(f.AMT_SUBTOTAL6,0)
,ifnull(f.DD_HIGHLEVELITEM,0)
,ifnull(f.DD_PODOCUMENTNO,'Not Set')
,ifnull(f.DD_PODOCUMENTITEMNO,0)
,ifnull(f.DD_POSCHEDULENO,0)
,ifnull(f.DD_PRODORDERNO,'Not Set')
,ifnull(f.DD_PRODORDERITEMNO,0)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS1ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS2ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS3ID,1)
,ifnull(f.CT_AFSOPENPOQTY,0)
,ifnull(f.CT_AFSINTRANSITQTY,0)
,ifnull(f.DIM_SCHEDULEDELIVERYBLOCKID,1)
,ifnull(f.DIM_CUSTOMERGROUP4ID,1)
,ifnull(f.DD_OPENCONFIRMATIONSEXISTS,'Not Set')
,ifnull(f.DD_CONDITIONNO,'Not Set')
,ifnull(f.DD_CREDITMGR,'Not Set')
,ifnull(f.DD_CLEAREDBLOCKEDSTS,'Not Set')
,ifnull(f.CT_AFSINRDDUNITS,0)
,ifnull(f.CT_AFSAFTERRDDUNITS,0)
,ifnull(f.DIM_AGREEMENTSID,1)
,ifnull(f.DIM_CHARGEBACKCONTRACTID,1)
,ifnull(f.DD_RO_MATERIALDESC,'Not Set')
,ifnull(f.DIM_CUSTOMERIDCBOWNER,1)
,ifnull(f.CT_COMMITTEDQTY,0)
,ifnull(f.DIM_DATEIDREQDELIVLINE,1)
,ifnull(f.DIM_MATERIALPRICINGGROUPID,1)
,ifnull(f.DIM_BILLINGBLOCKSALESDOCLVLID,1)
,ifnull(f.DD_DELIVERYINDICATOR,'Not Set')
,ifnull(f.DD_SHIPTOPARTYSTREET,'Not Set')
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS4ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS5ID,1)
,ifnull(f.DD_PURCHASEORDERITEM,'0')
,ifnull(f.DIM_MATERIALPRICEGROUP2ID,1)
,ifnull(f.DIM_PROJECTSOURCEID,1)
,ifnull(f.DD_BILLING_NO,'Not Set')
,ifnull(f.DIM_MDG_PARTID,1)
,ifnull(f.DIM_BWPRODUCTHIERARCHYID,1)
,ifnull(f.CT_FIRSTNONZEROCONFIRMQTY,0)
,ifnull(f.CT_VOLUME,0)
,ifnull(f.CT_NOTCONFIRMEDQTY,0)
,ifnull(f.DD_CUSTOMCUSTEXPPRICEINC,'Not Set')
,ifnull(f.DD_DOCUMENTFLOWGROUP,'Not Set')
,ifnull(f.DD_ORDERBY,'Not Set')
,ifnull(f.DD_CUSTOMPRICEMISSINGINC,'Not Set')
,ifnull(f.DIM_CUSTOMERENDUSERFORFTRADE,1)
,ifnull((select dim_dateid from dim_date_factory_calendar where companycode = 'Not Set' and plantcode_Factory = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var)),1) DIM_DATEIDSNAPSHOT
,ifnull(dim_accordinggidatemto_emd,1)
,ifnull(dim_dateidexpectedship_emd,1)
,ifnull(dim_dateidrequested_emd,1)
,ifnull(ct_baseuomratio,0)
,ifnull(ct_baseuomratiokg,0)
,ifnull(ct_countsalesdocitem,0)
,ct_deliveryisfull
,ct_deliveryontime
,ct_deliveryontimecasual
,dd_ace_openorders
,ifnull(dd_backorderflag,'Not Set')
,dd_backorderreason
,dd_backorder_responsibility
,dd_deliveryisfull
,dd_deliveryontime
,dd_deliveryontimecasual
,dd_intercompflag
,dd_lsintercompflag
,dd_mercklsbackorderfilter
,ifnull(dd_ora_active_hold_info,0)
,ifnull(dd_ora_attribute11,0)
,ifnull(dd_ora_line_on_hold,0)
,ifnull(dd_ora_tradsales_flag,0)
,ifnull(dim_backorder_dateid,1)
,dim_commercialviewid
,dim_dateidactualgimerckls
,dim_dateidconfirmeddelivery_emd
,ifnull(dim_clusterid,1)
,ifnull(f.dd_openqtycvrdbyplant,'Not Set') dd_openqtycvrdbyplant
,ifnull(f.dim_countryhierpsid,1)
,ifnull(f.dim_countryhierarid,1)
,ifnull(f.dim_gsamapimportid,1)
,ifnull(f.dim_date_enddateloadingid,1)
,ifnull(f.dd_openqtycvrdbymat,'Not Set')
,ifnull(f.dim_customer_shipto,1)
,ifnull(f.dim_productionschedulerid,1)
,ifnull(f.dd_inco1,'Not Set')
,ifnull(f.dd_inco2,'Not Set')
,ifnull(f.dd_ship_to_party,'Not Set')
,ifnull(prt.mtomts,'Not Set')
,IFNULL(F.DIM_ADDRESSID,1)
,ifnull(f.dd_CompleteInSingleDelivery,'Not Set')
from fact_salesorder f
inner join dim_plant pl on f.dim_plantid = pl.dim_plantid and pl.plantcode <>'CH73'/* @Catalin Exclude CH73 plant as requested by client BI-5280*/
inner join dim_part prt on f.dim_partid = prt.dim_partid
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
inner join dim_date_factory_calendar mlsrd on (
CASE
  WHEN UPPER(uph.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN decode(f.dim_dateidexpectedship_emd,1,f.dim_dateidrequested_emd,f.dim_dateidexpectedship_emd)
  ELSE CASE WHEN TRIM(prt.mtomts) = 'MTO' THEN decode(f.dim_accordinggidatemto_emd,1,f.dim_dateidconfirmeddelivery_emd,f.dim_accordinggidatemto_emd)
            ELSE decode(f.dim_dateidexpectedship_emd,1,f.dim_dateidrequested_emd,f.dim_dateidexpectedship_emd)
       END
END
) = mlsrd.dim_dateid
inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
inner join dim_salesorderitemstatus its on f.dim_salesorderitemstatusid = its.dim_salesorderitemstatusid
where
 to_Date(mlsrd.datevalue) >= decode(trim(to_char(to_date(sysdate),'Day')),'Sunday',(TRUNC(TO_DATE(SYSDATE -1, 'dd-mm-yy'),'iw')- 1),trunc(to_date(sysdate,'dd-mm-yy'),'iw')-1)
 and TO_DATE(mlsrd.datevalue) < TO_DATE(sysdate)
 AND mlsrd.datevalue  <> '0001-01-01'
     AND f.DD_ACE_OPENORDERS = 'X'
     AND uph.businesssector = 'BS-02'
     AND concat(f.dd_salesdocno,f.dd_salesitemno) not in (select distinct concat(dd_salesdocno,dd_salesitemno) from fact_salesorder_wsnap f,dim_date dd
where f.dim_dateidsnapshot=dd.dim_dateid
and WeekDayAbbreviation='Sat' )
     AND f.dd_scheduleno=dd_min_scheduleno;

/*@Catalin BI-5021 add cct to snapshots with commercial view*/
drop table if exists tmp_upd_sales;
create table tmp_upd_sales as
select distinct f.fact_salesorder_wsnapid,f.dim_customerid non_cct, ff.dim_customerid cct_customerid
 from fact_salesorder_wsnap f
 inner join dim_customer c on f.dim_customerid = c.dim_customerid
                                                and  c.customernumber in
('0010001158'
,'0010037282'
,'0010058020'
,'0010070034'
,'0010072370'
,'0010072371'
,'0010072373'
,'0010072374'
,'0010072375'
,'0010072376'
,'0010072390'
)
 inner join fact_purchase p on f.dd_businesscustomerpono = p.dd_documentno
 inner join fact_salesorder_wsnap ff on p.dd_salesdocno = ff.dd_salesdocno;

update fact_salesorder_wsnap f
set f.dim_customerid = t.cct_customerid
from fact_salesorder_wsnap f
,tmp_upd_sales t, dim_bwproducthierarchy ph
where t.fact_salesorder_wsnapid =f.fact_salesorder_wsnapid
and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
and ph.businesssector = 'BS-02'
and t.non_cct = f.dim_customerid
and f.dim_customerid <> t.cct_customerid;

update fact_salesorder_wsnap f
	set f.dim_commercialviewid=ds.dim_commercialviewid
	from dim_commercialview ds, dim_customer a, dim_bwproducthierarchy b, fact_salesorder_wsnap f
	where ds.SOLDTOCOUNTRY=a.COUNTRY
	and ds.UPPERPRODHIER=b.BUSINESS
	and f.DIM_BWPRODUCTHIERARCHYID=b.DIM_BWPRODUCTHIERARCHYID
	and f.dim_customerid=a.dim_customerid
	and f.dim_commercialviewid <> ds.dim_commercialviewid;



update fact_salesorder_wsnap   f
set f.dim_clusterid = dc.dim_clusterid
from fact_salesorder_wsnap   f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> dc.dim_clusterid;

update emd586.fact_salesorder_wsnap f
set f.dim_clusterid = ff.dim_clusterid
from emd586.fact_salesorder_wsnap f,EMDTempoCC4.fact_salesorder_wsnap ff
where f.fact_salesorder_wsnapid = ff.fact_salesorder_wsnapid
	and f.dim_projectsourceid=1
	and f.dim_clusterid <> ff.dim_clusterid;

drop table if exists tmp_upd_sales;
drop table if exists tmp_dayofprocessing_var;
drop table if exists tmp_loaded_datevalues;


update fact_salesorder_wsnap f
set dd_deliveryisfull_new=dd_deliveryisfull
from fact_salesorder_wsnap;

update fact_salesorder_wsnap f
set dd_deliveryontimecasual_new=dd_deliveryontimecasual
from fact_salesorder_wsnap;

update fact_salesorder_wsnap f
set ct_deliveryisfull_new=ct_deliveryisfull
from fact_salesorder_wsnap;
