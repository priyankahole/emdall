/******************************************************************************************************************/
/*   Script         : bi_populate_gmsdevperbatch_fact                                                             */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 11 Nov 2017                                                                                 */
/*   Description    : Populating script of fact_gmsdevperbatch                                                    */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   11 Nov 2017         CristianT       1.0               Creating the script.                                   */
/******************************************************************************************************************/

DROP TABLE IF EXISTS number_fountain_fact_gmsdevperbatch;
CREATE TABLE number_fountain_fact_gmsdevperbatch LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_gmsdevperbatch;
CREATE TABLE tmp_fact_gmsdevperbatch
AS
SELECT *
FROM fact_gmsdevperbatch
WHERE 1 = 0;

DELETE FROM number_fountain_fact_gmsdevperbatch WHERE table_name = 'fact_gmsdevperbatch';

INSERT INTO number_fountain_fact_gmsdevperbatch
SELECT 'fact_gmsdevperbatch', ifnull(max(fact_gmsdevperbatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_gmsdevperbatch;

/* Counting the batches */
DROP TABLE IF EXISTS tmp_gmscountbatches;
CREATE TABLE tmp_gmscountbatches
AS 
SELECT lcd.monthstartdate,
       pl.manufacturingsite as manufacturingsite,
       case
         when pl.manufacturingsite = 'Aubonne (SWITZ)' then 'Switzerland, Aubonne'
         when pl.manufacturingsite = 'Bari (ITALY)' then 'Italy, Bari'
         when pl.manufacturingsite = 'China (Nantong)' then 'China, Nantong'
         when pl.manufacturingsite = 'Darmstadt (GERMANY)' then 'Germany, Darmstadt'
         when pl.manufacturingsite = 'Mexico City (MEXICO)' then 'Mexico, Mexico City'
         when pl.manufacturingsite = 'Mollet (SPAIN)' then 'Spain, Mollet'
         when pl.manufacturingsite = 'Rio (BRAZIL)' then 'Brazil, Rio'
         when pl.manufacturingsite = 'Semoy (FRANCE)' then 'France, Semoy'
         when pl.manufacturingsite = 'Tres Cantos (SPAIN)' then 'Spain, Tres Cantos'
         when pl.manufacturingsite = 'Uruguay (URUGUAY)' then 'Uruguay, Montevideo'
         when pl.manufacturingsite = 'Vevey (SWITZ)' then 'Switzerland, Vevey'
         else pl.manufacturingsite
       end site,
       COUNT(distinct f_il.dd_batchno ) as batchescount
FROM emd586.fact_inspectionlot AS f_il 
     INNER JOIN emd586.dim_inspectionlotstatus AS ils ON f_il.dim_inspectionlotstatusid = ils.dim_inspectionlotstatusid 
     INNER JOIN emd586.dim_date AS lcd ON f_il.dim_dateidlotcreated = lcd.dim_dateid 
     INNER JOIN emd586.dim_plant AS pl ON f_il.dim_plantid = pl.dim_plantid 
WHERE f_il.dd_inspectionlotno LIKE '4%'
      AND CASE WHEN ils.usagedecisionmade = 'X' THEN 'Yes' ELSE 'No' END = 'No'
      AND CASE WHEN ils.lotcancelled = 'X' THEN 'Yes' ELSE 'No' END = 'No'
      AND f_il.dim_dateidusagedecisionmade = 1
      AND pl.manufacturingsite <> 'Not Set'
      AND lcd.datevalue >= '2017-01-01'
GROUP BY lcd.monthstartdate,
         pl.manufacturingsite;


/* Counting the deviations */
DROP TABLE IF EXISTS tmp_gmscountdeviations;
CREATE TABLE tmp_gmscountdeviations
AS
SELECT dt.monthstartdate,
       tw.main1_site as site,
       count(*) as deviationcount,
       tw.main1_division as division
FROM mv_xtr_grp1_rpt_main tw,
     dim_date dt
WHERE tw.main1_project = 'Deviation'
      AND tw.main1_site in ('France, Semoy', 'Spain, Mollet','Spain, Tres Cantos','Uruguay, Montevideo','China, Nantong','Mexico, Mexico City','Brazil, Rio','Germany, Darmstadt','Switzerland, Vevey','Switzerland, Aubonne','Italy, Bari')
      AND tw.main1_date_opened = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tw.main1_date_opened >= '2017-01-01'
GROUP BY dt.monthstartdate, tw.main1_site, tw.main1_division
UNION ALL
SELECT dt.monthstartdate,
       tw.main2_site as site,
       count(*) as deviationcount,
       tw.main2_division as division
FROM mv_xtr_grp2_rpt_main tw,
     dim_date dt
WHERE tw.main2_project = 'Deviation'
      AND tw.main2_site in ('France, Semoy', 'Spain, Mollet','Spain, Tres Cantos','Uruguay, Montevideo','China, Nantong','Mexico, Mexico City','Brazil, Rio','Germany, Darmstadt','Switzerland, Vevey','Switzerland, Aubonne','Italy, Bari')
      AND tw.main2_date_opened = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tw.main2_date_opened >= '2017-01-01'
GROUP BY dt.monthstartdate, tw.main2_site, tw.main2_division
UNION ALL
SELECT dt.monthstartdate,
       tw.main3_site as site,
       count(*) as deviationcount,
       tw.main3_division as division
FROM mv_xtr_grp3_rpt_main tw,
     dim_date dt
WHERE tw.main3_project = 'Deviation'
      AND tw.main3_site in ('France, Semoy', 'Spain, Mollet','Spain, Tres Cantos','Uruguay, Montevideo','China, Nantong','Mexico, Mexico City','Brazil, Rio','Germany, Darmstadt','Switzerland, Vevey','Switzerland, Aubonne','Italy, Bari')
      AND tw.main3_date_opened = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tw.main3_date_opened >= '2017-01-01'
GROUP BY dt.monthstartdate, tw.main3_site, tw.main3_division
UNION ALL
SELECT dt.monthstartdate,
       tw.main4_site as site,
       count(*) as deviationcount,
       tw.main4_division as division
FROM mv_xtr_grp4_rpt_main tw,
     dim_date dt
WHERE tw.main4_project = 'Deviation'
      AND tw.main4_site in ('France, Semoy', 'Spain, Mollet','Spain, Tres Cantos','Uruguay, Montevideo','China, Nantong','Mexico, Mexico City','Brazil, Rio','Germany, Darmstadt','Switzerland, Vevey','Switzerland, Aubonne','Italy, Bari')
      AND tw.main4_date_opened = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tw.main4_date_opened >= '2017-01-01'
GROUP BY dt.monthstartdate, tw.main4_site, tw.main4_division;

/* Blending the data */

INSERT INTO tmp_fact_gmsdevperbatch(
fact_gmsdevperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_manufacturingsite,
dd_site,
dim_dateid,
ct_batchescount,
ct_deviationcount,
dd_division
)
SELECT (SELECT max_id from number_fountain_fact_gmsdevperbatch WHERE table_name = 'fact_gmsdevperbatch') + ROW_NUMBER() over(order by '') AS fact_gmsdevperbatchid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(btch.manufacturingsite, 'Not Set') as dd_manufacturingsite,
       ifnull(btch.site, 'Not Set') as dd_site,
       ifnull(dt.dim_dateid, 1) as dim_dateid,
       ifnull(btch.batchescount, 0) as ct_batchescount,
       ifnull(dev.deviationcount, 0) as ct_deviationcount,
       ifnull(dev.division, 'Not Set') as dd_division
FROM tmp_gmscountbatches btch
     INNER JOIN tmp_gmscountdeviations dev ON btch.monthstartdate = dev.monthstartdate AND btch.site = dev.site
     INNER JOIN dim_date dt ON dt.companycode = 'Not Set' AND dt.datevalue = dev.monthstartdate
     ) t;

UPDATE tmp_fact_gmsdevperbatch tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_gmsdevperbatch tmp;

DELETE FROM fact_gmsdevperbatch;

INSERT INTO fact_gmsdevperbatch (
fact_gmsdevperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_manufacturingsite,
dd_site,
dim_dateid,
ct_batchescount,
ct_deviationcount,
dd_division
)
SELECT fact_gmsdevperbatchid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_manufacturingsite,
       dd_site,
       dim_dateid,
       ct_batchescount,
       ct_deviationcount,
       dd_division
FROM tmp_fact_gmsdevperbatch;

DROP TABLE IF EXISTS tmp_fact_gmsdevperbatch;

DELETE FROM emd586.fact_gmsdevperbatch;
INSERT INTO emd586.fact_gmsdevperbatch
SELECT *
FROM fact_gmsdevperbatch;

