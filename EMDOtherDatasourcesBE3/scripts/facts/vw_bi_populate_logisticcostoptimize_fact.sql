drop table if exists tmp_lc_operations;
create table tmp_lc_operations as
select rs.stsc_recship_dest dest
	,rs.stsc_recship_source	sourc
	,cast('Not Set' as varchar(50)) as sourcing
	,ifnull(n.stsc_network_UDC_TRANSMODE_REPORTING,
			case upper(rs.stsc_recship_transMode)
				WHEN 'AIRFREIGHT' THEN 'AIRFREIGHT'
                WHEN 'SEAFREIGHT' THEN 'SEAFREIGHT'
                WHEN 'TRUCK' THEN 'TRUCK'
           	    ELSE 'GENERIC'
			end) as transMode
	,rs.stsc_recship_item as item
	,rs.STSC_RECSHIP_SCHEDSHIPDATE as SCHEDSHIPDATE
	,rs.STSC_RECSHIP_DEPARTUREDATE as DEPARTUREDATE
	,rs.STSC_RECSHIP_DELIVERYDATE as DELIVERYDATE
	,rs.STSC_RECSHIP_SCHEDARRIVDATE as SCHEDARRIVDATE
	,rs.STSC_RECSHIP_NEEDARRIVDATE as NEEDARRIVDATE
	,rs.STSC_RECSHIP_QTY as qty
	,rs.STSC_RECSHIP_NEEDSHIPDATE as NEEDSHIPDATE
	,CASE WHEN  I.stsc_item_UDC_STORAGECONDITION IN ('01','06','14','15','16','21','22','24','25','26','34','35','36','38','39','40','41','42','43','44','45','49','50','51','53')
    		THEN 'C'
   			 ELSE 'A'
  	END AS COOL_AMBIENT
	,ifnull(s.stsc_sourcing_udc_unitsperpallet,0) as unitsperpallet
	,n.STSC_NETWORK_UDC_VARCOSTPERPALLET as VARCOSTPERPALLET
	,n.STSC_NETWORK_UDC_VARCOSTPERPALLET_C as VARCOSTPERPALLET_C
	,s.STSC_SOURCING_UDC_TRANSFER_PRICE as transfer_price
	,i.stsc_item_udc_factor as factor
	,cast(0 as decimal(18,4)) as palletqty
	,cast(0 as decimal(18,4)) as palletqtyroundup
	,cast (1 as decimal(18,4)) shipmcostopt
	,cast (1 as decimal(18,4)) shipmcostnotopt
	,cast(0 as decimal(18,4)) shipmvaluetp
	,(rs.STSC_RECSHIP_DELIVERYDATE - rs.STSC_RECSHIP_DEPARTUREDATE) as leadtimedays
	,cast (1 as decimal(36,0)) dim_snapshotdateid
from stsc_recship rs,stsc_item i, stsc_loc l, stsc_network n, stsc_sourcing s
where rs.stsc_recship_dest = l.stsc_loc_loc
	and rs.stsc_recship_item = i.stsc_item_item
	and rs.stsc_recship_dest = n.stsc_network_dest
	and rs.stsc_recship_source = n.stsc_network_source
	and rs.stsc_recship_transMode = n.stsc_network_transMode
	and rs.stsc_recship_item = s.stsc_sourcing_item
	and rs.stsc_recship_dest = s.stsc_sourcing_dest
	and rs.stsc_recship_source = s.stsc_sourcing_source
	and ifnull(rs.stsc_recship_sourcing,'Not Set') = ifnull(s.stsc_sourcing_sourcing,'Not Set')
	and year(rs.stsc_recship_SCHEDSHIPDATE) <= year(current_date)+1
UNION
select vll.stsc_vehicleloadline_dest as dest
	,vll.stsc_vehicleloadline_source as sourc
	,cast('Not Set' as varchar(50)) as SOURCING
	,ifnull(n.stsc_network_UDC_TRANSMODE_REPORTING,
			case upper(vl.stsc_vehicleload_transMode)
				when 'AIRFREIGHT' then 'AIRFREIGHT'
		        when 'SEAFREIGHT' then 'SEAFREIGHT'
		        when'TRUCK' then 'TRUCK'
		        else 'GENERIC'
			end) as transMode
	,vll.stsc_vehicleloadline_item as item
	,vll.STSC_VEHICLELOADLINE_SCHEDSHIPDATE as SCHEDSHIPDATE
	,vl.STSC_VEHICLELOAD_SHIPDATE as DEPARTUREDATE
	,vl.STSC_VEHICLELOAD_ARRIVDATE as DELIVERYDATE
	,vll.STSC_VEHICLELOADLINE_SCHEDARRIVDATE as SCHEDARRIVDATE
	,vl.STSC_VEHICLELOAD_ARRIVDATE as NEEDARRIVDATE
	,vll.STSC_VEHICLELOADLINE_QTY as qty
	,vl.STSC_VEHICLELOAD_SHIPDATE as NEEDSHIPDATE
	,CASE WHEN  I.stsc_item_UDC_STORAGECONDITION IN ('01','06','14','15','16','21','22','24','25','26','34','35','36','38','39','40','41','42','43','44','45','49','50','51','53')
    		THEN 'C'
   			 ELSE 'A'
  	END AS COOL_AMBIENT
	,ifnull(so.stsc_sourcing_udc_unitsperpallet,0) as unitsperpallet
	,n.STSC_NETWORK_UDC_VARCOSTPERPALLET as VARCOSTPERPALLET
	,n.STSC_NETWORK_UDC_VARCOSTPERPALLET_C as VARCOSTPERPALLET_C
	,so.STSC_SOURCING_UDC_TRANSFER_PRICE as transfer_price
	,i.stsc_item_udc_factor as factor
	,cast(0 as decimal(18,4)) as palletqty
	,cast(0 as decimal(18,4)) as palletqtyroundup
	,cast (1 as decimal(18,4)) shipmcostopt
	,cast (1 as decimal(18,4)) shipmcostnotopt
	,cast(0 as decimal(18,4)) as shipmvaluetp
	,(vl.STSC_VEHICLELOAD_ARRIVDATE - vl.STSC_VEHICLELOAD_SHIPDATE) as leadtimedays
	,cast (1 as decimal(36,0)) dim_snapshotdateid
from stsc_vehicleload vl, stsc_vehicleloadline vll, stsc_loc l, stsc_item i, stsc_network n, stsc_sourcing so
where l.stsc_loc_loc = vll.stsc_vehicleloadline_dest
and i.stsc_item_item = vll.stsc_vehicleloadline_item
and N.stsc_network_DEST = vll.stsc_vehicleloadline_dest
and N.stsc_network_SOURCE = vll.stsc_vehicleloadline_source
and N.stsc_network_TRANSMODE = vl.stsc_vehicleload_transMode
and vl.stsc_vehicleload_loadid = vll.stsc_vehicleloadline_loadid
and vl.stsc_vehicleload_sourcestatus = 1
and vll.stsc_vehicleloadline_udc_interface_code_source is null
and so.stsc_sourcing_item = vll.stsc_vehicleloadline_item
and so.stsc_sourcing_DEST = vll.stsc_vehicleloadline_dest
and so.stsc_sourcing_SOURCE = vll.stsc_vehicleloadline_source
and so.stsc_sourcing_SOURCING = vll.stsc_vehicleloadline_SOURCING
and year(VLL.stsc_vehicleloadline_SCHEDSHIPDATE) <= year(current_date)+1;

drop table if exists tmp_jda_recship;
create table tmp_jda_recship as
select DEST
	,SOURC
	,SOURCING
	,TRANSMODE
	,ITEM
	,SCHEDSHIPDATE
	,DEPARTUREDATE
	,DELIVERYDATE
	,SCHEDARRIVDATE
	,NEEDARRIVDATE
	,sum(QTY) as QTY
	,NEEDSHIPDATE
	,COOL_AMBIENT
	,UNITSPERPALLET
	,VARCOSTPERPALLET
	,VARCOSTPERPALLET_C
	,TRANSFER_PRICE
	,FACTOR
	,sum(PALLETQTY) as PALLETQTY
	,sum(PALLETQTYROUNDUP) as PALLETQTYROUNDUP
	,sum(SHIPMCOSTOPT) as SHIPMCOSTOPT
	,sum(SHIPMCOSTNOTOPT) as SHIPMCOSTNOTOPT
	,sum(SHIPMVALUETP) as SHIPMVALUETP
	,LEADTIMEDAYS
	,DIM_SNAPSHOTDATEID
from tmp_lc_operations
group by DEST,SOURC,SOURCING,TRANSMODE,ITEM,SCHEDSHIPDATE,DEPARTUREDATE,DELIVERYDATE,SCHEDARRIVDATE,NEEDARRIVDATE,
	NEEDSHIPDATE,COOL_AMBIENT,UNITSPERPALLET,VARCOSTPERPALLET,VARCOSTPERPALLET_C,TRANSFER_PRICE,FACTOR,
	LEADTIMEDAYS,DIM_SNAPSHOTDATEID;

update tmp_jda_recship t
	SET sourcing = l.stsc_loc_descr
from tmp_jda_recship t, stsc_loc l
where t.sourc = l.stsc_loc_loc;

update tmp_jda_recship t
	set palletqty = (case when ifnull(t.unitsperpallet,0) <> 0 then t.qty/ifnull(t.unitsperpallet,0) else 0 end) ;

update tmp_jda_recship t
	set palletqtyroundup = ceil(case when ifnull(t.unitsperpallet,0) <> 0 then t.qty/ifnull(t.unitsperpallet,0) else 0 end);

update tmp_jda_recship
	set shipmcostopt = palletqty * (case when cool_ambient = 'C' then varcostperpallet_c else varcostperpallet end);

update tmp_jda_recship
	set shipmcostnotopt = palletqtyroundup  * (case when cool_ambient = 'C' then varcostperpallet_c else varcostperpallet end);

update tmp_jda_recship
	set shipmvaluetp = qty * transfer_price;

update tmp_jda_recship t
	set dim_snapshotdateid = (select dim_Dateid from dim_date where datevalue = current_Date);

delete from number_fountain m where m.table_name = 'fact_logisticcostoptimize';
insert into number_fountain
	select 'fact_logisticcostoptimize', ifnull(max(f.fact_logisticcostoptimizeid ),
									 ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from fact_logisticcostoptimize f;

update fact_logisticcostoptimize
		set dd_curentsnapshot_flag = 0
where dd_curentsnapshot_flag <> 0;

insert into fact_logisticcostoptimize (fact_logisticcostoptimizeid,dim_jda_itemid,dim_destlocid,dim_sourcelocid,dd_sourcing,
		dd_transmode,dim_schedshipdateid,dim_departuredateid,dim_deliverydateid,dim_schedarrivdateid,dim_needarrivdateid,
		dim_needshipdateid,ct_qty,dd_cool_ambient,ct_unitsperpallet,amt_varcostperpallet,amt_varcostperpallet_c,amt_transferprice,
		ct_palletqty,ct_palletqtyroundup,amt_shipmcostopt,amt_shipmcostnotopt,amt_shipmvaluetp,ct_leadtimedays,dim_snapshotdateid,dd_curentsnapshot_flag,
		DD_NULLUNITSPERPALLETFLAG)
select
		(select max_id from number_fountain where table_name = 'fact_logisticcostoptimize') + row_number() over(order by'') as fact_logisticcostoptimizeid
		,ifnull(di.dim_jda_itemid,1) as dim_jda_itemid
		,ifnull(dl1.dim_jda_locid,1) as dim_destlocid
		,ifnull(dl2.dim_jda_locid,1) as dim_sourcelocid
		,ifnull(tmp.sourcing,'Not Set') dd_sourcing
		,ifnull(tmp.transMode,'Not Set') as dd_transMode
		,ifnull(dt.dim_dateid,1) as dim_schedshipdateid
		,ifnull(dt1.dim_dateid,1) as dim_departuredateid
		,ifnull(dt2.dim_dateid,1) as dim_deliverydateid
		,ifnull(dt3.dim_dateid,1) as dim_schedarrivdateid
		,ifnull(dt4.dim_dateid,1) as dim_needarrivdateid
		,ifnull(dt5.dim_dateid,1) as dim_needshipdateid
		,ifnull(tmp.qty,0) as ct_qty
		,ifnull(tmp.cool_ambient,'Not Set') as dd_cool_ambient
		,ifnull(tmp.unitsperpallet,0) as ct_unitsperpallet
		,ifnull(tmp.VARCOSTPERPALLET,0) as amt_varcostperpallet
		,ifnull(tmp.VARCOSTPERPALLET_C,0) as amt_varcostperpallet_c
		,ifnull(tmp.transfer_price,0) as amt_transferprice
		,ifnull(tmp.palletqty,0) as ct_palletqty
		,ifnull(tmp.palletqtyroundup,0) as ct_palletqtyroundup
		,ifnull(tmp.shipmcostopt,0) amt_shipmcostopt
		,ifnull(tmp.shipmcostnotopt,0) amt_shipmcostnotopt
		,ifnull(tmp.shipmvaluetp,0) as amt_shipmvaluetp
		,ifnull(tmp.leadtimedays,0) as ct_leadtimedays
		,ifnull(dim_snapshotdateid,1) as dim_snapshotdateid
		,1 as dd_curentsnapshot_flag
		,(case when ifnull(tmp.unitsperpallet,0) = 0 then 'X'
					when ifnull(tmp.unitsperpallet,0) between 0 and 1 then 'X'
					when ifnull(tmp.unitsperpallet,0) = 1 then 'X'
					when ifnull(tmp.unitsperpallet,0) >= 999999 then 'X'
			else 'Not Set' end) as DD_NULLUNITSPERPALLETFLAG
from tmp_jda_recship tmp, dim_date dt, dim_jda_loc dl1, dim_jda_loc dl2, dim_jda_item di, dim_date dt1, dim_date dt2, dim_date dt3, dim_date dt4, dim_date dt5
where tmp.SCHEDSHIPDATE = dt.datevalue
		and tmp.DEPARTUREDATE = dt1.datevalue
		and tmp.DELIVERYDATE = dt2.datevalue
		and tmp.SCHEDARRIVDATE = dt3.datevalue
		and tmp.NEEDARRIVDATE = dt4.datevalue
		and tmp.NEEDSHIPDATE = dt5.datevalue
		and tmp.item = di.item
		and tmp.dest = dl1.loc
		and tmp.sourc = dl2.loc;

/*
drop table if exists tmp_countshipments
create table tmp_countshipments as
select distinct  DIM_SNAPSHOTDATEID,
	DIM_DEPARTUREDATEID,
	DIM_SOURCELOCID,
	DIM_DESTLOCID,
	count(DIM_JDA_ITEMID) over(partition by  DIM_SNAPSHOTDATEID,DIM_DEPARTUREDATEID, DIM_SOURCELOCID,d.country) as shipmentpercountry,
	count(distinct concat(d.country,DD_TRANSMODE)) over(partition by  DIM_SNAPSHOTDATEID,DIM_DEPARTUREDATEID,DIM_SOURCELOCID) as shipmentperday,
	count(DIM_DEPARTUREDATEID) over(partition by DIM_SNAPSHOTDATEID,DIM_DEPARTUREDATEID,DIM_SOURCELOCID) as ct_count_key
from fact_logisticcostoptimize f, dim_jda_loc d, dim_date snp
where f.dim_destlocid = d.dim_jda_locid
	and f.DIM_SNAPSHOTDATEID = snp.dim_dateid
	and snp.datevalue = current_date

update fact_logisticcostoptimize f
	set f.ct_shipmentpercountry = t.shipmentpercountry,
		f.ct_shipmentperday = t.shipmentperday,
		f.ct_count_key = t.ct_count_key
from fact_logisticcostoptimize f, tmp_countshipments t
where f.DIM_DEPARTUREDATEID = t.DIM_DEPARTUREDATEID
	and f.DIM_SOURCELOCID = t.DIM_SOURCELOCID
	and f.DIM_DESTLOCID = t.DIM_DESTLOCID
	and  f.DIM_SNAPSHOTDATEID = t.DIM_SNAPSHOTDATEID

drop table if exists tmp_weighted_12mth
create table tmp_weighted_12mth as
select f.DIM_SNAPSHOTDATEID
	,sum(ct_qty * ct_leadtimedays)/sum(ct_qty)  weighted_12mth
from fact_logisticcostoptimize f, dim_date dt, dim_date snp
where  f.DIM_SNAPSHOTDATEID = snp.dim_Dateid
	and dim_schedshipdateid = dt.dim_dateid
	and date_trunc('month',dt.datevalue) between date_trunc('month',current_date) and  date_trunc('month',current_date + interval '12' month)
	and snp.datevalue = current_Date
group by f.DIM_SNAPSHOTDATEID

update fact_logisticcostoptimize f
	set f.ct_weighted_12mth = t.weighted_12mth
from fact_logisticcostoptimize f, tmp_weighted_12mth t
where f.DIM_SNAPSHOTDATEID = t.DIM_SNAPSHOTDATEID*/

drop table if exists tmp_palletfillvspack_12mth;
create table tmp_palletfillvspack_12mth as
select DIM_SNAPSHOTDATEID
	,avg(case when ct_palletqtyroundup<>0  then (ct_palletqty/ct_palletqtyroundup) else 0 end) ct_palletfillvspack_12mth
from(
select f.DIM_SNAPSHOTDATEID
	,f.DIM_SOURCELOCID
	,sum(ct_palletqty) ct_palletqty
	,sum(ct_palletqtyroundup) ct_palletqtyroundup
from fact_logisticcostoptimize f, dim_jda_loc s, dim_date dt, dim_date snp
where DIM_SOURCELOCID = s.dim_jda_locid
	and dim_schedshipdateid = dt.dim_dateid
	and DIM_SNAPSHOTDATEID = snp.dim_dateid
	and date_trunc('month',dt.datevalue) between  date_trunc('month',current_date) and date_trunc('month',current_date + interval '12' month)
	and snp.datevalue = current_date
group by f.DIM_SNAPSHOTDATEID,DIM_SOURCELOCID )
group by DIM_SNAPSHOTDATEID;

update fact_logisticcostoptimize f
	set f.ct_palletfillvspack_12mth = t.ct_palletfillvspack_12mth
from fact_logisticcostoptimize f, tmp_palletfillvspack_12mth t
where f.DIM_SNAPSHOTDATEID = t.DIM_SNAPSHOTDATEID;

drop table if exists tmp_palletfillvspack_18mth;
create table tmp_palletfillvspack_18mth as
select DIM_SNAPSHOTDATEID
	,avg(case when ct_palletqtyroundup<>0 then ct_palletqty/ct_palletqtyroundup else 0 end) as ct_palletfillvspack_18mth
from(select f.DIM_SNAPSHOTDATEID
		,f.DIM_SOURCELOCID
		,sum(ct_palletqty) ct_palletqty
		,sum(ct_palletqtyroundup) ct_palletqtyroundup
	from fact_logisticcostoptimize f, dim_jda_loc s, dim_date dt, dim_date snp
	where DIM_SOURCELOCID = s.dim_jda_locid
		and dim_schedshipdateid = dt.dim_dateid
		and DIM_SNAPSHOTDATEID = snp.dim_dateid
		and date_trunc('month',dt.datevalue) between  date_trunc('month',current_date) and date_trunc('month',current_date + interval '18' month)
		and snp.datevalue = current_date
	group by f.DIM_SNAPSHOTDATEID,f.DIM_SOURCELOCID)
group by DIM_SNAPSHOTDATEID;

update fact_logisticcostoptimize f
	set f.ct_palletfillvspack_18mth = t.ct_palletfillvspack_18mth
from fact_logisticcostoptimize f, tmp_palletfillvspack_18mth t
where f.DIM_SNAPSHOTDATEID = t.DIM_SNAPSHOTDATEID;
