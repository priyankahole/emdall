/*****************************************************************************************************************/
/*   Script         : exa_fact_mangoperiodicreview                                                               */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 14 Aug 2018                                                                                */
/*   Description    : Populating script of fact_mangoperiodicreview                                              */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   14 Aug 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_mangoperiodicreview;
CREATE TABLE tmp_fact_mangoperiodicreview
AS
SELECT *
FROM fact_mangoperiodicreview
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_mangoperiodicreview;
CREATE TABLE number_fountain_fact_mangoperiodicreview LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_mangoperiodicreview
SELECT 'fact_mangoperiodicreview', ifnull(max(fact_mangoperiodicreviewid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_mangoperiodicreview;


INSERT INTO tmp_fact_mangoperiodicreview(
fact_mangoperiodicreviewid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_object_name,
dd_title,
dd_document_type,
dd_document_subtype,
dd_document_unit,
dd_original_creation_date,
dim_dateidorgcreation,
dd_document_status,
dd_document_status_date,
dim_dateiddocstatus,
dd_periodic_review_start_date,
dim_dateidperiodicreviewstart,
dd_effect_start,
dim_dateideffectstart,
dd_periodic_review_notify_date,
dim_dateidperiodicreviewnotify,
dd_periodic_review_interval,
dd_last_review_date,
dim_dateidlastreview,
dd_next_review_date,
dim_dateidnextreview,
dd_planned_effective_date,
dim_dateidplannedeffective,
dd_english_title,
dd_author_site,
dd_legacy_document,
dd_review_date,
dim_dateidreview,
dd_user_login_name,
dim_qualityusersiduser,
dd_authors,
dd_r_version_label,
dd_identified_due_date,
dim_dateididentifieddue
)
SELECT (SELECT max_id from number_fountain_fact_mangoperiodicreview WHERE table_name = 'fact_mangoperiodicreview') + ROW_NUMBER() over(order by '') AS fact_mangoperiodicreviewid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(dl.object_name, 'Not Set') as dd_object_name,
       ifnull(dl.title, 'Not Set') as dd_title,
       ifnull(dl.document_type, 'Not Set') as dd_document_type,
       ifnull(dl.document_subtype, 'Not Set') as dd_document_subtype,
       ifnull(dl.document_unit, 'Not Set') as dd_document_unit,
       ifnull(dl.original_creation_date, '0001-01-01 00:00:00') as dd_original_creation_date,
       1 as dim_dateidorgcreation,
       ifnull(dl.document_status, 'Not Set') as dd_document_status,
       ifnull(dl.document_status_date, '0001-01-01 00:00:00') as dd_document_status_date,
       1 as dim_dateiddocstatus,
       ifnull(dl.periodic_review_start_date, '0001-01-01 00:00:00') as dd_periodic_review_start_date,
       1 as dim_dateidperiodicreviewstart,
       ifnull(dl.effect_start, '0001-01-01 00:00:00') as dd_effect_start,
       1 as dim_dateideffectstart,
       ifnull(dl.periodic_review_notify_date, '0001-01-01 00:00:00') as dd_periodic_review_notify_date,
       1 as dim_dateidperiodicreviewnotify,
       ifnull(dl.periodic_review_interval, 'Not Set') as dd_periodic_review_interval,
       ifnull(dl.last_review_date, '0001-01-01 00:00:00') as dd_last_review_date,
       1 as dim_dateidlastreview,
       ifnull(dl.next_review_date, '0001-01-01 00:00:00') as dd_next_review_date,
       1 as dim_dateidnextreview,
       ifnull(dl.planned_effective_date, '0001-01-01 00:00:00') as dd_planned_effective_date,
       1 as dim_dateidplannedeffective,
       ifnull(dl.english_title, 'Not Set') as dd_english_title,
       ifnull(dl.author_site, 'Not Set') as dd_author_site,
       ifnull(dl.legacy_document, 'Not Set') as dd_legacy_document,
       ifnull(dl.review_date, '0001-01-01 00:00:00') as dd_review_date,
       1 as dim_dateidreview,
       ifnull(dl.user_login_name, 'Not Set') as dd_user_login_name,
       1 as dim_qualityusersiduser,
       ifnull(replace(dl.authors, ' (legacy)', ''), 'Not Set') as dd_authors,
       ifnull(dl.r_version_label, 'Not Set') as dd_r_version_label,
       ifnull(dl.identified_due_date, '0001-01-01 00:00:00') as dd_identified_due_date,
       1 as dim_dateididentifieddue
FROM documents_late dl;

UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidorgcreation = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_original_creation_date)
      AND f.dim_dateidorgcreation <> ifnull(dt.dim_dateid, 1);
	  
UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateiddocstatus = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_document_status_date)
      AND f.dim_dateiddocstatus <> ifnull(dt.dim_dateid, 1);
	   
UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidperiodicreviewstart = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_periodic_review_start_date)
      AND f.dim_dateidperiodicreviewstart <> ifnull(dt.dim_dateid, 1);
	   
UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateideffectstart = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_effect_start)
      AND f.dim_dateideffectstart <> ifnull(dt.dim_dateid, 1);
	   
UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidperiodicreviewnotify = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_periodic_review_notify_date)
      AND f.dim_dateidperiodicreviewnotify <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidlastreview = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_last_review_date)
      AND f.dim_dateidlastreview <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidnextreview = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_next_review_date)
      AND f.dim_dateidnextreview <> ifnull(dt.dim_dateid, 1);
	   
UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidplannedeffective = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_planned_effective_date)
      AND f.dim_dateidplannedeffective <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateidreview = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_review_date)
      AND f.dim_dateidreview <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangoperiodicreview f
SET dim_dateididentifieddue = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_identified_due_date)
      AND f.dim_dateididentifieddue <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_mangoperiodicreview f
SET dim_qualityusersiduser = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_qualityusers d
WHERE f.dd_user_login_name = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersiduser  <> d.dim_qualityusersid;

UPDATE tmp_fact_mangoperiodicreview f
SET dd_user_login_name = ifnull(m_id.user_id, 'Not Set')
FROM mango_missing_author_ids m_id,
     tmp_fact_mangoperiodicreview f
WHERE lower(m_id.AUTHOR) = lower(f.dd_authors)
      AND f.dim_qualityusersiduser = 1;

UPDATE tmp_fact_mangoperiodicreview f
SET dim_qualityusersiduser = ifnull(d.dim_qualityusersid, 1)
FROM tmp_fact_mangoperiodicreview f,
     dim_qualityusers d
WHERE f.dd_user_login_name = d.merck_uid
      AND d.rowiscurrent = 1
      AND f.dim_qualityusersiduser  <> d.dim_qualityusersid;

DELETE FROM fact_mangoperiodicreview;

INSERT INTO fact_mangoperiodicreview(
fact_mangoperiodicreviewid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_object_name,
dd_title,
dd_document_type,
dd_document_subtype,
dd_document_unit,
dd_original_creation_date,
dim_dateidorgcreation,
dd_document_status,
dd_document_status_date,
dim_dateiddocstatus,
dd_periodic_review_start_date,
dim_dateidperiodicreviewstart,
dd_effect_start,
dim_dateideffectstart,
dd_periodic_review_notify_date,
dim_dateidperiodicreviewnotify,
dd_periodic_review_interval,
dd_last_review_date,
dim_dateidlastreview,
dd_next_review_date,
dim_dateidnextreview,
dd_planned_effective_date,
dim_dateidplannedeffective,
dd_english_title,
dd_author_site,
dd_legacy_document,
dd_review_date,
dim_dateidreview,
dd_user_login_name,
dim_qualityusersiduser,
dd_authors,
dd_r_version_label,
dd_identified_due_date,
dim_dateididentifieddue
)
SELECT fact_mangoperiodicreviewid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_object_name,
       dd_title,
       dd_document_type,
       dd_document_subtype,
       dd_document_unit,
       dd_original_creation_date,
       dim_dateidorgcreation,
       dd_document_status,
       dd_document_status_date,
       dim_dateiddocstatus,
       dd_periodic_review_start_date,
       dim_dateidperiodicreviewstart,
       dd_effect_start,
       dim_dateideffectstart,
       dd_periodic_review_notify_date,
       dim_dateidperiodicreviewnotify,
       dd_periodic_review_interval,
       dd_last_review_date,
       dim_dateidlastreview,
       dd_next_review_date,
       dim_dateidnextreview,
       dd_planned_effective_date,
       dim_dateidplannedeffective,
       dd_english_title,
       dd_author_site,
       dd_legacy_document,
       dd_review_date,
       dim_dateidreview,
       dd_user_login_name,
       dim_qualityusersiduser,
       dd_authors,
       dd_r_version_label,
       dd_identified_due_date,
       dim_dateididentifieddue
FROM tmp_fact_mangoperiodicreview;

DROP TABLE IF EXISTS tmp_fact_mangoperiodicreview;
DROP TABLE IF EXISTS number_fountain_fact_mangoperiodicreview;
