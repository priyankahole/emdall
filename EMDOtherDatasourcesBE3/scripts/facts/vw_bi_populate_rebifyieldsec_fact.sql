/******************************************************************************************************************/
/*   Script         : bi_populate_rebifyieldsec_fact                                                              */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_rebifyieldsec                                                     */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   10 Feb 2017         CristianT       1.1               Calculating the AVG of previous years                  */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_rebifyieldsec;
CREATE TABLE tmp_fact_rebifyieldsec
AS
SELECT *
FROM fact_rebifyieldsec
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldsec', ifnull(max(fact_rebifyieldsecid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldsec;


INSERT INTO tmp_fact_rebifyieldsec(
fact_rebifyieldsecid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifseclotid,
ct_yieldpap1sec,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec') + ROW_NUMBER() over(order by '') as fact_rebifyieldsecid,
       'G1CC' as dd_type,
       ifnull(parameter_set_name ,'not set') as dd_paramater_set_name,
       ifnull(parameter_set_date ,'not set') as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ifnull(rebif_sec_lot_id ,'not set') as dd_rebifseclotid,
       ifnull(yield_pg2_sec, 0) as ct_yieldpap1sec,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_purif_yield_vev_g1
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldsec', ifnull(max(fact_rebifyieldsecid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldsec;

INSERT INTO tmp_fact_rebifyieldsec(
fact_rebifyieldsecid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifseclotid,
ct_yieldpap1sec,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec') + ROW_NUMBER() over(order by '') as fact_rebifyieldsecid,
       'G2SFM' as dd_type,
       ifnull(parameter_set_name ,'not set') as dd_paramater_set_name,
       ifnull(parameter_set_date ,'not set') as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ifnull(rebif_sec_lot_id ,'not set') as dd_rebifseclotid,
       ifnull(yield_pg2_sec, 0) as ct_yieldpap1sec,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_purif_yield_vev_g2
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR;

UPDATE tmp_fact_rebifyieldsec tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_rebifyieldsec tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = substr(tmp.dd_parameter_set_date, 0, 10);

/* 10 Feb 2016 CristianT Start: Calculating the AVG of previous years */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldsec', ifnull(max(fact_rebifyieldsecid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldsec;

INSERT INTO tmp_fact_rebifyieldsec(
fact_rebifyieldsecid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifseclotid,
ct_yieldpap1sec,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec') + ROW_NUMBER() over(order by '') as fact_rebifyieldsecid,
       'G1CC' as dd_type,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_PARAMATER_SET_NAME,
       current_date as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_rebifseclotid,
       ifnull(avg(yield_pg2_sec), 0) as ct_yieldpap1sec,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_purif_yield_vev_g1 tmp
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifyieldsec', ifnull(max(fact_rebifyieldsecid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifyieldsec;

INSERT INTO tmp_fact_rebifyieldsec(
fact_rebifyieldsecid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifseclotid,
ct_yieldpap1sec,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifyieldsec') + ROW_NUMBER() over(order by '') as fact_rebifyieldsecid,
       'G2SFM' as dd_type,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_PARAMATER_SET_NAME,
       current_date as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ' Average ' || YEAR(substr(tmp.parameter_set_date, 0, 10)) as dd_rebifseclotid,
       ifnull(avg(yield_pg2_sec), 0) as ct_yieldpap1sec,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM cc_purif_yield_vev_g2 tmp
WHERE parameter_set_date >= date_trunc('year', current_date) - interval '5' YEAR
GROUP BY YEAR(substr(tmp.parameter_set_date, 0, 10));

UPDATE tmp_fact_rebifyieldsec tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_rebifyieldsec tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_parameter_set_date
      AND tmp.dim_dateidsetdate = 1;
/* 10 Feb 2016 CristianT End */

UPDATE tmp_fact_rebifyieldsec tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_rebifyieldsec tmp;

TRUNCATE TABLE fact_rebifyieldsec;

INSERT INTO fact_rebifyieldsec(
fact_rebifyieldsecid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifseclotid,
ct_yieldpap1sec,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_rebifyieldsecid,
       dd_type,
       dd_paramater_set_name,
       dd_parameter_set_date,
       dim_dateidsetdate,
       dd_rebifseclotid,
       ct_yieldpap1sec,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_rebifyieldsec;

DROP TABLE IF EXISTS tmp_fact_rebifyieldsec;

TRUNCATE TABLE emd586.fact_rebifyieldsec;

INSERT INTO emd586.fact_rebifyieldsec(
fact_rebifyieldsecid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_rebifseclotid,
ct_yieldpap1sec,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_rebifyieldsecid,
       dd_type,
       dd_paramater_set_name,
       dd_parameter_set_date,
       dim_dateidsetdate,
       dd_rebifseclotid,
       ct_yieldpap1sec,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM fact_rebifyieldsec;