/*****************************************************************************************************************/
/*   Script         : exa_fact_ehscr360                                                                          */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 29 Aug 2018                                                                                */
/*   Description    : Populating script of fact_ehscr360                                                         */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   29 Aug 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_ehscr360;
CREATE TABLE tmp_fact_ehscr360
AS
SELECT *
FROM fact_ehscr360
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_ehscr360;
CREATE TABLE number_fountain_fact_ehscr360 LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_ehscr360
SELECT 'fact_ehscr360', ifnull(max(fact_ehscr360id), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_ehscr360;

/* Blend the raw data from credit360 with the master data files to get additional fields */
DROP TABLE IF EXISTS tmp_rawdata;
CREATE TABLE tmp_rawdata
AS
SELECT cr.start_date as dd_startdate,
       cr.end_date as dd_enddate,
       cr.region as dd_region,
       site.functionn as dd_function,
       site.display_name as dd_displayname,
       cr.indicatorr as dd_indicator,
       ind.indicator_code as dd_indicatorcode,
       ind.period as dd_period,
       ind.timeline as dd_timeline,
       cr.measure as dd_measure,
       cr.source_value as amt_sourcevalue,
       cr.source_file as dd_sourcefile
FROM credit360 cr
     INNER JOIN ehs_cr360_master_site_info site ON site.region_from_cr360_file = cr.region
     LEFT JOIN ehs_cr360_master_indicator ind ON ind.indicator_from_credit_360 = cr.indicatorr;

/* We have indicators that are reported annually/quarterly but we need them to be available monthly.
Bellow we are selecting all those rows and we multiply them using dim_date to get 12 months period */
DROP TABLE IF EXISTS tmp_rawdata_annual;
CREATE TABLE tmp_rawdata_annual
AS
SELECT tmp.dd_startdate,
       min(dt.datevalue) as new_dd_startdate,
       tmp.dd_enddate,
       max(case when datevalue = '9999-12-31' then '9999-12-30' else datevalue end) as new_dd_enddate,
       tmp.dd_region,
       tmp.dd_function,
       tmp.dd_displayname,
       tmp.dd_indicator,
       tmp.dd_indicatorcode,
       tmp.dd_period,
       tmp.dd_timeline,
       tmp.dd_measure,
       tmp.amt_sourcevalue,
       tmp.dd_sourcefile,
       dt.calendarmonthid
FROM tmp_rawdata tmp
     INNER JOIN dim_date dt on dt.companycode = 'Not Set' and dt.datevalue between dd_startdate and to_date(dd_enddate) - INTERVAL '1' DAY
WHERE tmp.dd_period in ('Annual', 'Quarterly')
GROUP BY tmp.dd_startdate,
         tmp.dd_enddate,
         tmp.dd_region,
         tmp.dd_function,
         tmp.dd_displayname,
         tmp.dd_indicator,
         tmp.dd_indicatorcode,
         tmp.dd_period,
         tmp.dd_timeline,
         tmp.dd_measure,
         tmp.amt_sourcevalue,
         tmp.dd_sourcefile,
         dt.CALENDARMONTHID;

/* Delete the existing rows and add the multiplied rows */
MERGE INTO tmp_rawdata tmp
USING (SELECT distinct dd_startdate,dd_enddate,dd_region,dd_function,dd_displayname,dd_indicator,dd_indicatorcode,dd_period,dd_timeline,dd_measure,amt_sourcevalue,dd_sourcefile
       FROM tmp_rawdata_annual) del 
        ON tmp.dd_startdate = del.dd_startdate
           AND tmp.dd_enddate = del.dd_enddate
           AND tmp.dd_region = del.dd_region
           AND tmp.dd_function = del.dd_function
           AND tmp.dd_displayname = del.dd_displayname
           AND tmp.dd_indicator = del.dd_indicator
           AND tmp.dd_indicatorcode = del.dd_indicatorcode
           AND tmp.dd_period = del.dd_period
           AND tmp.dd_timeline= del.dd_timeline
           AND tmp.amt_sourcevalue = del.amt_sourcevalue
           AND tmp.dd_sourcefile = del.dd_sourcefile
WHEN MATCHED THEN DELETE;

INSERT INTO tmp_rawdata(
dd_startdate,
dd_enddate,
dd_region,
dd_function,
dd_displayname,
dd_indicator,
dd_indicatorcode,
dd_period,
dd_timeline,
dd_measure,
amt_sourcevalue,
dd_sourcefile
)
SELECT new_dd_startdate as dd_startdate,
       new_dd_enddate as dd_enddate,
       dd_region,
       dd_function,
       dd_displayname,
       dd_indicator,
       dd_indicatorcode,
       dd_period,
       dd_timeline,
       dd_measure,
       amt_sourcevalue,
       dd_sourcefile
FROM tmp_rawdata_annual;

DROP TABLE IF EXISTS tmp_rawdata_annual;

/* Adding target values for CO2, Water and Waste */
DROP TABLE IF EXISTS tmp_rawdata_targets;
CREATE TABLE tmp_rawdata_targets
AS
SELECT min(dt.datevalue) as new_dd_startdate,
       to_date(max(case when datevalue = '9999-12-31' then '9999-12-30' else dt.datevalue end)) as new_dd_enddate,
       trg.cr360_region as dd_region,
       site.functionn as dd_function,
       site.display_name as dd_displayname,
       trg.cr360_indicator as dd_indicator,
       ind.indicator_code as dd_indicatorcode,
       trg.cr360_period as dd_period,
       ind.timeline as dd_timeline,
       trg.cr360_rate as dd_measure,
       trg.cr360_value as amt_sourcevalue,
       'Not Set' as dd_sourcefile,
       dt.calendarmonthid
FROM ehs_cr360_master_target trg
     INNER JOIN dim_date dt on dt.companycode = 'Not Set' and dt.datevalue between to_date(cr360_start_date, 'DD.MM.YYYY') and to_date(cr360_end_date, 'DD.MM.YYYY') - INTERVAL '1' DAY
     INNER JOIN ehs_cr360_master_site_info site ON site.region_from_cr360_file = trg.cr360_region
     INNER JOIN ehs_cr360_master_indicator ind ON ind.indicator_from_credit_360 = trg.cr360_indicator
GROUP BY trg.cr360_region,
         site.functionn,
         site.display_name,
         trg.cr360_indicator,
         ind.indicator_code,
         trg.cr360_period,
         ind.timeline,
         trg.cr360_rate,
         trg.cr360_value,
         dt.calendarmonthid;

INSERT INTO tmp_rawdata(
dd_startdate,
dd_enddate,
dd_region,
dd_function,
dd_displayname,
dd_indicator,
dd_indicatorcode,
dd_period,
dd_timeline,
dd_measure,
amt_sourcevalue,
dd_sourcefile
)
SELECT new_dd_startdate as dd_startdate,
       new_dd_enddate as dd_enddate,
       dd_region,
       dd_function,
       dd_displayname,
       dd_indicator,
       dd_indicatorcode,
       dd_period,
       dd_timeline,
       dd_measure,
       amt_sourcevalue,
       dd_sourcefile
FROM tmp_rawdata_targets;

DROP TABLE IF EXISTS tmp_rawdata_targets;

INSERT INTO tmp_fact_ehscr360(
fact_ehscr360id,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_startdate,
dim_dateidstart,
dd_enddate,
dim_dateidend,
dd_region,
dd_function,
dd_displayname,
dd_indicator,
dd_indicatorcode,
dd_period,
dd_timeline,
dd_measure,
amt_sourcevalue,
dd_sourcefile,
amt_co2year2006,
amt_wasteyear2016,
amt_wateryear2014,
dd_lastrefreshdate,
amt_target,
dd_dummyrow,
dd_indicator_target
)
SELECT (SELECT max_id from number_fountain_fact_ehscr360 WHERE table_name = 'fact_ehscr360') + ROW_NUMBER() over(order by '') AS fact_ehscr360id,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(dd_startdate, '0001-01-01') as dd_startdate,
       1 as dim_dateidstart,
       ifnull(dd_enddate, '0001-01-01') as dd_enddate,
       1 as dim_dateidend,
       ifnull(dd_region, 'Not Set') as dd_region,
       ifnull(dd_function, 'Not Set') as dd_function,
       ifnull(dd_displayname, 'Not Set') as dd_displayname,
       ifnull(dd_indicator, 'Not Set') as dd_indicator,
       ifnull(dd_indicatorcode, 'Not Set') as dd_indicatorcode,
       ifnull(dd_period, 'Not Set') as dd_period,
       ifnull(dd_timeline, 'Not Set') as dd_timeline,
       ifnull(dd_measure, 'Not Set') as dd_measure,
       ifnull(amt_sourcevalue, 0) as amt_sourcevalue,
       ifnull(dd_sourcefile, 'Not Set') as dd_sourcefile,
       0 as amt_co2year2006,
       0 as amt_wasteyear2016,
       0 as amt_wateryear2014,
       current_date as dd_lastrefreshdate,
       0 as amt_target,
       'Not Set' as dd_dummyrow,
       'Not Set' as dd_indicator_target
FROM tmp_rawdata tmp;

DROP TABLE IF EXISTS tmp_rawdata;

UPDATE tmp_fact_ehscr360 f
SET dim_dateidstart = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_ehscr360 f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_startdate)
      AND f.dim_dateidstart <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ehscr360 f
SET dim_dateidend = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_ehscr360 f,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = to_date(f.dd_enddate)
      AND f.dim_dateidend <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_ehscr360 tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_ehscr360 tmp;

/* The indicator value and the target value are populated in the same column but on different rows. In order to make the comparison between both, we need to have the values on the same line */
UPDATE tmp_fact_ehscr360
SET dd_indicator_target = CASE dd_indicator
                            WHEN '12 month EHS Incident Rate (EHS-IR)' THEN 'Target EHS-IR'
                            WHEN '12 month EHS Leading Indicator Rate (EHS-LR)' THEN 'Target EHS-LR'
                            WHEN 'Merck Waste Score (T)' THEN 'Target Merck Waste Score (T)'
                            WHEN 'Final result of the EHS Process Index' THEN 'Target Process Index'
                            WHEN '12 month Lost Time Injury Rate (LTIR) Contractors' THEN 'Target LTIR'
                            WHEN '12 month Lost Time Injury Rate (LTIR) Merck employees' THEN 'Target LTIR'
                            WHEN 'GHG Reporting Scope 1 + 2' THEN 'Target GHG Reporting Scope 1 + 2'
                            WHEN 'Total water intake' THEN 'Target Total water intake'
                            ELSE 'Not Set'
                          END;

DROP TABLE IF EXISTS tmp_ehscr360_targets;
CREATE TABLE tmp_ehscr360_targets
AS
SELECT dt.calendarmonthid,
       tmp.dd_region,
       tmp.dd_indicator,
       tmp.amt_sourcevalue as target
FROM tmp_fact_ehscr360 tmp,
     dim_date dt
WHERE tmp.dd_indicator in ('Target GHG Reporting Scope 1 + 2','Target EHS-IR','Target EHS-LR','Target LTIR','Target Process Index','Target Merck Waste Score (T)','Target Total water intake')
      AND tmp.dim_dateidstart = dt.dim_dateid;

UPDATE tmp_fact_ehscr360 tmp
SET amt_target = ifnull(trg.target, 0)
FROM tmp_fact_ehscr360 tmp,
     tmp_ehscr360_targets trg,
     dim_date dt
WHERE tmp.dd_region = trg.dd_region
      AND tmp.dd_indicator_target = trg.dd_indicator
      AND dt.dim_dateid = tmp.dim_dateidstart
      AND dt.calendarmonthid = trg.calendarmonthid;

DROP TABLE IF EXISTS tmp_ehscr360_targets;

/* For Quarterly rows we should mark first month of each Quarter as Not Set, rest of the months are marked with Yes */
DROP TABLE IF EXISTS tmp_ehscr360_first_month;
CREATE TABLE tmp_ehscr360_first_month
AS
SELECT dt.CALENDARQUARTERID,
       min(dt.calendarmonthid) as first_month
FROM tmp_fact_ehscr360 tmp,
     dim_date dt
WHERE tmp.dim_dateidstart = dt.dim_dateid
      AND tmp.dd_period = 'Quarterly'
GROUP BY dt.CALENDARQUARTERID;

UPDATE tmp_fact_ehscr360
SET dd_dummyrow = 'Yes'
WHERE dd_period = 'Quarterly';

UPDATE tmp_fact_ehscr360 tmp
SET dd_dummyrow = 'Not Set'
FROM tmp_fact_ehscr360 tmp,
     tmp_ehscr360_first_month fm,
     dim_date dt
WHERE dt.dim_dateid = tmp.dim_dateidstart
      AND dt.calendarmonthid = fm.first_month
      AND tmp.dd_period = 'Quarterly';

DROP TABLE IF EXISTS tmp_ehscr360_first_month;

DELETE FROM fact_ehscr360;

INSERT INTO fact_ehscr360(
fact_ehscr360id,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_startdate,
dim_dateidstart,
dd_enddate,
dim_dateidend,
dd_region,
dd_function,
dd_displayname,
dd_indicator,
dd_indicatorcode,
dd_period,
dd_timeline,
dd_measure,
amt_sourcevalue,
dd_sourcefile,
amt_co2year2006,
amt_wasteyear2016,
amt_wateryear2014,
dd_lastrefreshdate,
amt_target,
dd_dummyrow,
dd_indicator_target
)
SELECT fact_ehscr360id,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_startdate,
       dim_dateidstart,
       dd_enddate,
       dim_dateidend,
       dd_region,
       dd_function,
       dd_displayname,
       dd_indicator,
       dd_indicatorcode,
       dd_period,
       dd_timeline,
       dd_measure,
       amt_sourcevalue,
       dd_sourcefile,
       amt_co2year2006,
       amt_wasteyear2016,
       amt_wateryear2014,
       dd_lastrefreshdate,
       amt_target,
       dd_dummyrow,
       dd_indicator_target
FROM tmp_fact_ehscr360;

DROP TABLE IF EXISTS tmp_fact_ehscr360;
DROP TABLE IF EXISTS number_fountain_fact_ehscr360;

/* For CO2 calculation we take the value from 2006 to be used in the formula */
DROP TABLE IF EXISTS tmp_amt_co2year2006;
CREATE TABLE tmp_amt_co2year2006
AS
SELECT dd_region,
       dd_function,
       dd_displayname,
       dd_indicator,
       dd_indicatorcode,
       amt_sourcevalue as amt_co2year2006
FROM fact_ehscr360
WHERE DD_INDICATORCODE = 'CO2'
      AND dd_startdate = '2006-01-01';

UPDATE fact_ehscr360 fct
SET amt_co2year2006 = ifnull(tmp.amt_co2year2006, 0)
FROM fact_ehscr360 fct,
     tmp_amt_co2year2006 tmp
WHERE fct.dd_region = tmp.dd_region
      AND fct.dd_function = tmp.dd_function
      AND fct.dd_displayname = tmp.dd_displayname
      AND fct.dd_indicator = tmp.dd_indicator
      AND fct.dd_indicatorcode = tmp.dd_indicatorcode
      AND substr(fct.dd_startdate, 0, 4) <> '2006';

DROP TABLE IF EXISTS tmp_amt_co2year2006;

/* For Waste calculation we take the value from 2016 to be used in the formula */
DROP TABLE IF EXISTS tmp_amt_wasteyear2016;
CREATE TABLE tmp_amt_wasteyear2016
AS
SELECT dd_region,
       dd_function,
       dd_displayname,
       dd_indicator,
       dd_indicatorcode,
       amt_sourcevalue as amt_wasteyear2016
FROM fact_ehscr360
WHERE DD_INDICATORCODE = 'Waste'
      AND dd_startdate = '2016-01-01';

UPDATE fact_ehscr360 fct
SET amt_wasteyear2016 = ifnull(tmp.amt_wasteyear2016, 0)
FROM fact_ehscr360 fct,
     tmp_amt_wasteyear2016 tmp
WHERE fct.dd_region = tmp.dd_region
      AND fct.dd_function = tmp.dd_function
      AND fct.dd_displayname = tmp.dd_displayname
      AND fct.dd_indicator = tmp.dd_indicator
      AND fct.dd_indicatorcode = tmp.dd_indicatorcode
      AND substr(fct.dd_startdate, 0, 4) <> '2016';

DROP TABLE IF EXISTS tmp_amt_wasteyear2016;

/* For Water calculation we take the value from 2014 to be used in the formula */
DROP TABLE IF EXISTS tmp_amt_wateryear2014;
CREATE TABLE tmp_amt_wateryear2014
AS
SELECT dd_region,
       dd_function,
       dd_displayname,
       dd_indicator,
       dd_indicatorcode,
       amt_sourcevalue as amt_wateryear2014
FROM fact_ehscr360
WHERE DD_INDICATORCODE = 'Water'
      AND dd_startdate = '2014-01-01';

UPDATE fact_ehscr360 fct
SET amt_wateryear2014 = ifnull(tmp.amt_wateryear2014, 0)
FROM fact_ehscr360 fct,
     tmp_amt_wateryear2014 tmp
WHERE fct.dd_region = tmp.dd_region
      AND fct.dd_function = tmp.dd_function
      AND fct.dd_displayname = tmp.dd_displayname
      AND fct.dd_indicator = tmp.dd_indicator
      AND fct.dd_indicatorcode = tmp.dd_indicatorcode
      AND substr(fct.dd_startdate, 0, 4) <> '2014';

DROP TABLE IF EXISTS tmp_amt_wateryear2014;

/* For Indicator Code = PI we should keep latest quarter data
DROP TABLE IF EXISTS tmp_pi_quarter
CREATE TABLE tmp_pi_quarter
AS
SELECT cr.fact_ehscr360id,
       dt.calendaryear,
       cr.dd_startdate,
       cr.dd_enddate,
       cr.dd_region,
       cr.dd_function,
       cr.dd_indicator,
       cr.dd_indicatorcode,
       cr.amt_sourcevalue,
       ROW_NUMBER() over(partition by dt.calendaryear, cr.dd_region, cr.dd_function, cr.dd_indicator, cr.dd_indicatorcode order by cr.dd_enddate desc) as ro_no
FROM fact_ehscr360 cr,
     dim_date dt
WHERE 1 = 1
      AND cr.DD_INDICATORCODE = 'PI'
      AND cr.dim_dateidstart = dt.dim_dateid

DELETE FROM tmp_pi_quarter
WHERE ro_no = 1

UPDATE fact_ehscr360 cr
SET amt_sourcevalue = 0
FROM fact_ehscr360 cr,
     tmp_pi_quarter tmp
WHERE cr.fact_ehscr360id = tmp.fact_ehscr360id

DROP TABLE IF EXISTS tmp_pi_quarter
*/

TRUNCATE TABLE emd586.fact_ehscr360;
INSERT INTO emd586.fact_ehscr360
SELECT *
FROM fact_ehscr360;
