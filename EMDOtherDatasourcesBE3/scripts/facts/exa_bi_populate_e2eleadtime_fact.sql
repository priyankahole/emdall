/******************************************************************************************************************/
/*   Script         : exa_bi_populate_e2eleadtime_fact                                                            */
/*   Author         : Cristian Cleciu                                                                             */
/*   Created On     : 20 Sep 2017                                                                                 */
/*   Description    : Populating script of fact_e2eleadtime                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By             		Version      Desc                                                 */
/*   20 Sep 2017         Cristian Cleciu        1.0          Creating the script.                                 */
/******************************************************************************************************************/


DROP TABLE IF EXISTS number_fountain_fact_e2eleadtime;
CREATE TABLE number_fountain_fact_e2eleadtime LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_e2eleadtime WHERE table_name = 'fact_e2eleadtime';

INSERT INTO number_fountain_fact_e2eleadtime
SELECT 'fact_e2eleadtime', ifnull(max(fact_e2eleadtimeid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_e2eleadtime;

DROP TABLE IF EXISTS tmp_fact_e2eleadtime;
CREATE TABLE tmp_fact_e2eleadtime AS 	
SELECT 
	 ( SELECT max_id FROM number_fountain_fact_e2eleadtime WHERE table_name = 'fact_e2eleadtime') 
	+ row_number() over(order by '') AS fact_e2eLeadTimeid,
	CONVERT(BIGINT,1) AS Dim_Plantid,
    CONVERT(BIGINT,1) AS dim_partid,
    ifnull(CONVERT(VARCHAR(50), EBAN_BANFN),'Not Set')  AS dd_PurchaseReqNo,
    ifnull(CONVERT(VARCHAR(10), EBAN_BNFPO),'Not Set')  AS dd_PurchaseReqItemNo,
    CONVERT (BIGINT, 1) AS dim_dateidPRCreation,
    ifnull(CONVERT(VARCHAR(10), EBAN_EBELN),'Not Set')  AS dd_PurchaseOrderNo,
    ifnull(CONVERT(VARCHAR(18), EBAN_EBELP),'Not Set')  AS dd_PurchaseOrderItemNo,
    CONVERT (BIGINT, 1) AS dim_dateidPOCreation,
    CONVERT (BIGINT, 1) AS dim_dateidProcessing,
    TO_TIMESTAMP(ifnull(NAST_UHRVR,'000000'),'HH24MISS') AS dd_timeofProcessing,
    CONVERT (BIGINT, 1) AS dim_dateidConfReceived,
    TO_TIMESTAMP(ifnull(EKES_EZEIT,'000000'),'HH24MISS') AS dd_timeofConfReceived,
    ifnull(CONVERT(VARCHAR(10), MSEG_MBLNR),'Not Set')  AS dd_MaterialDocNo,
    ifnull(CONVERT(VARCHAR(18), MSEG_ZEILE),'Not Set')  AS dd_MaterialDocItemNo,
    ifnull(CONVERT(VARCHAR(10), MSEG_CHARG),'Not Set')  AS dd_BatchNumber,
    CONVERT (BIGINT, 1) AS dim_dateidDocEntry,
    TO_TIMESTAMP(ifnull(MKPF_CPUTM,'000000'),'HH24MISS') AS dd_timeofDocEntry,
    ifnull(CONVERT(VARCHAR(10), LTAK_TBNUM),'Not Set')  AS dd_TransferReqNo,
    ifnull(CONVERT(VARCHAR(10), LTAK_VBELN),'Not Set')  AS dd_SalesDistribDocNo,
    ifnull(CONVERT(VARCHAR(10), LTAP_TANUM),'Not Set')  AS dd_TransferOrderNo,
    ifnull(CONVERT(VARCHAR(18), LTAP_TAPOS),'Not Set')  AS dd_TransferOrderItem,
    CONVERT (BIGINT, 1) AS dim_dateidTOCreated,
    TO_TIMESTAMP(ifnull(LTAK_BZEIT,'000000'),'HH24MISS') AS dd_timeofTOCreated,
    CONVERT (BIGINT, 1) AS dim_dateidconfirmation,
    TO_TIMESTAMP(ifnull(LTAP_QZEIT,'000000'),'HH24MISS') AS dd_timeofconfirmation,
    ifnull(CONVERT(VARCHAR(12), QALS_PRUEFLOS),'Not Set')  AS dd_inspectionlotno,
    CONVERT (BIGINT, 1) AS dim_dateidlotcreated,		
    TO_TIMESTAMP(ifnull(QALS_ENTSTEZEIT,'000000'),'HH24MISS') AS dd_timeoflotcreation,
   ifnull(CONVERT(VARCHAR(18), QPRN_PN_NR),'Not Set')  AS dd_SampleDrawinNo,
    CONVERT (BIGINT, 1) AS dim_dateidsdopsrelease,	
    TO_TIMESTAMP(ifnull(QPRN_FRGZT,'000000'),'HH24MISS') AS dd_timeofRelease,
    CONVERT (BIGINT, 1) AS Dim_DateIdUsageDecision,	
    TO_TIMESTAMP(ifnull(QAVE_VEZEITERF,'000000'),'HH24MISS') AS dd_timeofUsageDecision,
    CONVERT (BIGINT, 1) AS amt_exhangerate,
    CONVERT (BIGINT, 1) AS amt_exchangerate_gbl,
    CONVERT (BIGINT, 1) AS dim_currencyid,
    CONVERT (BIGINT, 1) AS dim_currencyid_tra,
    CONVERT (BIGINT, 1) AS dim_currencyid_gbl,
    current_timestamp as DW_UPDATE_DATE,
    current_timestamp as DW_INSERT_DATE,
    CONVERT (BIGINT, 1) AS DIM_PROJECTSOURCEID,
	EBAN_WERKS, -- dim_plant
	EBAN_MATNR,	-- dim_part
	EBAN_BADAT, -- dim_dateidPRCreation
	EKKO_AEDAT, -- dim_dateidPOCreation
	ifnull(EKKO_AEDAT-EBAN_BADAT,0) AS ct_prCreationlt,	
	TO_DATE(NAST_DATVR,'YYYYMMDD') AS NAST_DATVR, -- dim_dateidProcessing
	TO_DATE(NAST_DATVR,'YYYYMMDD')-EKKO_AEDAT as ct_poCreationlt,
	EKES_ERDAT, -- dim_dateidConfReceived
	ifnull(EKES_ERDAT - TO_DATE(NAST_DATVR,'YYYYMMDD'),0) AS ct_ptconfirmationlt,
	MKPF_CPUDT, -- dim_dateidDocEntry
	ifnull(MKPF_CPUDT-EKES_ERDAT,0)  as ct_supplierlt,
	LTAK_BDATU, -- dim_dateidTOCreated
	ifnull(LTAK_BDATU-MKPF_CPUDT,0) AS ct_tocreationlt,
	LTAP_QDATU, -- dim_dateidconfirmation
	ifnull(LTAP_QDATU-LTAK_BDATU,0) AS ct_stockPlacementlt,
	QALS_ENSTEHDAT, -- dim_dateidlotcreated
	0 as ct_bachfirst,
	0 as ct_batchlast,
	QPRN_FRGDT, -- dim_dateidsdopsrelease
	ifnull(QPRN_FRGDT-QALS_ENSTEHDAT,0)  as ct_sampleDrawinglt,
	QAVE_VDATUM, -- Dim_DateIdUsageDecision 
	ifnull(QAVE_VDATUM-QPRN_FRGDT,0) as ct_qcqalt
FROM EKKO_EBAN_E2E f 

LEFT JOIN EKES_E2E e
ON e.EKES_EBELN = f.EBAN_EBELN AND e.EKES_EBELP = f.EBAN_EBELP
LEFT JOIN EKKO_NAST_E2E n
ON f.EBAN_EBELN =n.NAST_OBJKY
LEFT JOIN MKPF_MSEG_E2E M
ON M.MSEG_EBELN = f.EBAN_EBELN AND M.MSEG_EBELP = f.EBAN_EBELP
LEFT JOIN LTAK_LTAP_E2E l
ON l.LTAK_VBELN = f.EBAN_EBELN
LEFT JOIN QALS q
ON Q.QALS_PRUEFLOS = L.LTAP_QPLOS
LEFT JOIN QPRN_E2E
ON Q.QALS_PRUEFLOS = QPRN_PLOS
LEFT JOIN QAVE_E2E
ON Q.QALS_PRUEFLOS = QAVE_PRUEFLOS
 WHERE 1=1
--AND QALS_PRUEFLOS IS NOT NULL
--AND EBAN_EBELN='4501864213'
;

/*dim_partid*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_partid = IFNULL(pt.dim_partid, 1)
FROM tmp_fact_e2eleadtime f
LEFT JOIN dim_Part pt ON pt.PartNumber = f.EBAN_MATNR
		 AND pt.Plant      = f.EBAN_WERKS 
WHERE f.dim_partid <> IFNULL(pt.dim_partid, 1);

/*Dim_Plantid*/	
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_Plantid = IFNULL(pl.dim_plantid, 1)
FROM tmp_fact_e2eleadtime f
LEFT JOIN dim_plant pl ON pl.PlantCode = f.EBAN_WERKS 
WHERE f.Dim_Plantid <> IFNULL(pl.dim_plantid, 1);

/*dim_dateidPRCreation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidPRCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EBAN_BADAT
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidPRCreation <> ifnull(dd.dim_dateid,1);

/*dim_dateidPOCreation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidPOCreation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EKKO_AEDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidPOCreation <> ifnull(dd.dim_dateid,1);

/*dim_dateidProcessing*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidProcessing = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.NAST_DATVR
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidProcessing <> ifnull(dd.dim_dateid,1);

/*dim_dateidConfReceived*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidConfReceived = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.EKES_ERDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidConfReceived <> ifnull(dd.dim_dateid,1);

/*dim_dateidDocEntry*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidDocEntry = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.MKPF_CPUDT
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidDocEntry <> ifnull(dd.dim_dateid,1);

/*dim_dateidTOCreated*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidTOCreated = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.LTAK_BDATU
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidTOCreated <> ifnull(dd.dim_dateid,1);

/*dim_dateidconfirmation*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidconfirmation = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.LTAP_QDATU
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidconfirmation <> ifnull(dd.dim_dateid,1);

/*dim_dateidlotcreated*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidlotcreated = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QALS_ENSTEHDAT
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidlotcreated <> ifnull(dd.dim_dateid,1);

/*dim_dateidsdopsrelease*/
UPDATE tmp_fact_e2eleadtime f
SET f.dim_dateidsdopsrelease = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QPRN_FRGDT
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.dim_dateidsdopsrelease <> ifnull(dd.dim_dateid,1);

/*Dim_DateIdUsageDecision*/
UPDATE tmp_fact_e2eleadtime f
SET f.Dim_DateIdUsageDecision = ifnull(dd.dim_dateid,1)
FROM dim_plant pl,
	 dim_date dd,
	 tmp_fact_e2eleadtime f
WHERE dd.DateValue = f.QAVE_VDATUM
AND dd.CompanyCode = pl.CompanyCode
AND f.EBAN_WERKS = pl.PlantCode
AND f.Dim_DateIdUsageDecision <> ifnull(dd.dim_dateid,1);

DELETE FROM fact_e2eleadtime;

INSERT INTO fact_e2eleadtime(
Dim_Plantid 			
,dim_partid				
,dd_PurchaseReqNo		
,dd_PurchaseReqItemNo	
,dim_dateidPRCreation	
,dd_PurchaseOrderNo		
,dd_PurchaseOrderItemNo	
,dim_dateidPOCreation	
,dim_dateidProcessing	
,dd_timeofProcessing		
,dim_dateidConfReceived	
,dd_timeofConfReceived	
,dd_MaterialDocNo		
,dd_MaterialDocItemNo	
,dd_BatchNumber			
,dim_dateidDocEntry		
,dd_timeofDocEntry		
,dd_TransferReqNo		
,dd_SalesDistribDocNo	
,dd_TransferOrderNo		
,dd_TransferOrderItem	
,dim_dateidTOCreated		
,dd_timeofTOCreated		
,dim_dateidconfirmation	
,dd_timeofconfirmation	
,dd_inspectionlotno		
,dim_dateidlotcreated	
,dd_timeoflotcreation	
,dd_SampleDrawinNo		
,dim_dateidsdopsrelease	
,dd_timeofRelease		
,Dim_DateIdUsageDecision	
,dd_timeofUsageDecision	
,ct_prCreationlt			
,ct_poCreationlt			
,ct_ptconfirmationlt		
,ct_supplierlt			
,ct_tocreationlt			
,ct_stockPlacementlt		
,ct_sampleDrawinglt		
,ct_qcqalt				
,ct_bachfirst			
,ct_batchlast			
,fact_e2eLeadTimeid		
,amt_exhangerate			
,amt_exchangerate_gbl	
,dim_currencyid			
,dim_currencyid_tra		
,dim_currencyid_gbl		
,DW_UPDATE_DATE			
,DW_INSERT_DATE			
,DIM_PROJECTSOURCEID		
)
SELECT 
Dim_Plantid 			
,dim_partid				
,dd_PurchaseReqNo		
,dd_PurchaseReqItemNo	
,dim_dateidPRCreation	
,dd_PurchaseOrderNo		
,dd_PurchaseOrderItemNo	
,dim_dateidPOCreation	
,dim_dateidProcessing	
,dd_timeofProcessing		
,dim_dateidConfReceived	
,dd_timeofConfReceived	
,dd_MaterialDocNo		
,dd_MaterialDocItemNo	
,dd_BatchNumber			
,dim_dateidDocEntry		
,dd_timeofDocEntry		
,dd_TransferReqNo		
,dd_SalesDistribDocNo	
,dd_TransferOrderNo		
,dd_TransferOrderItem	
,dim_dateidTOCreated		
,dd_timeofTOCreated		
,dim_dateidconfirmation	
,dd_timeofconfirmation	
,dd_inspectionlotno		
,dim_dateidlotcreated	
,dd_timeoflotcreation	
,dd_SampleDrawinNo		
,dim_dateidsdopsrelease	
,dd_timeofRelease		
,Dim_DateIdUsageDecision	
,dd_timeofUsageDecision	
,ct_prCreationlt			
,ct_poCreationlt			
,ct_ptconfirmationlt		
,ct_supplierlt			
,ct_tocreationlt			
,ct_stockPlacementlt		
,ct_sampleDrawinglt		
,ct_qcqalt				
,ct_bachfirst			
,ct_batchlast			
,fact_e2eLeadTimeid		
,amt_exhangerate			
,amt_exchangerate_gbl	
,dim_currencyid			
,dim_currencyid_tra		
,dim_currencyid_gbl		
,DW_UPDATE_DATE			
,DW_INSERT_DATE			
,DIM_PROJECTSOURCEID		
FROM tmp_fact_e2eleadtime;

DROP TABLE IF EXISTS tmp_fact_e2eleadtime;

DELETE FROM emd586.fact_e2eleadtime;

INSERT INTO emd586.fact_e2eleadtime
SELECT *
FROM fact_e2eleadtime;