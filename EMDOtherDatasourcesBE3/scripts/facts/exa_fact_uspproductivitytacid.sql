/*****************************************************************************************************************/
/*   Script         : exa_fact_uspproductivitytacid                                                              */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 18 Dec 2018                                                                                */
/*   Description    : Populating script of fact_uspproductivitytacid                                             */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   18 Dec 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_uspproductivitytacid;
CREATE TABLE tmp_fact_uspproductivitytacid
AS
SELECT *
FROM fact_uspproductivitytacid
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_uspproductivitytacid;
CREATE TABLE number_fountain_fact_uspproductivitytacid LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_uspproductivitytacid
SELECT 'fact_uspproductivitytacid', ifnull(max(fact_uspproductivitytacidid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_uspproductivitytacid;

UPDATE new_productivity_taci_d_crude__harvest
SET harvest_filtration_volume_recovery_buffer = 0
WHERE harvest_filtration_volume_recovery_buffer = 'null';

UPDATE new_productivity_taci_d_crude__harvest
SET harvest_hat_volume = 0
WHERE harvest_hat_volume = 'null';

UPDATE new_productivity_taci_d_crude__harvest
SET productivity_clarified = 0
WHERE productivity_clarified = 'null';

INSERT INTO tmp_fact_uspproductivitytacid(
fact_uspproductivitytacidid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_crude_protein_titer,
ct_harvest_crude_volume,
ct_harvest_filtration_volume_recovery_buffer,
ct_harvest_hat_protein_titer,
ct_harvest_hat_volume,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT (SELECT max_id from number_fountain_fact_uspproductivitytacid WHERE table_name = 'fact_uspproductivitytacid') + ROW_NUMBER() over(order by '') AS fact_uspproductivitytacidid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(ps_name, 'Not Set') as dd_ps_name,
       ifnull(ps_date, '0001-01-01') as dd_ps_date,
       1 as dim_dateid,
       ifnull(harvest_batch_id, 'Not Set') as dd_harvest_batch_id,
       ifnull(harvest_crude_protein_titer, 0) as ct_harvest_crude_protein_titer,
       ifnull(harvest_crude_volume, 0) as ct_harvest_crude_volume,
       ifnull(harvest_filtration_volume_recovery_buffer, 0) as ct_harvest_filtration_volume_recovery_buffer,
       ifnull(harvest_hat_protein_titer, 0) as ct_harvest_hat_protein_titer,
       ifnull(harvest_hat_volume, 0) as ct_harvest_hat_volume,
       ifnull(productivity_clarified, 0) as ct_productivity_clarified,
       ifnull(productivity_pbr, 0) as ct_productivity_pbr
FROM new_productivity_taci_d_crude__harvest;

/* Calculating the AVG of previous years */
DELETE FROM number_fountain_fact_uspproductivitytacid WHERE table_name = 'fact_uspproductivitytacid';

INSERT INTO number_fountain_fact_uspproductivitytacid
SELECT 'fact_uspproductivitytacid', ifnull(max(fact_uspproductivitytacidid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_uspproductivitytacid;

INSERT INTO tmp_fact_uspproductivitytacid(
fact_uspproductivitytacidid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_crude_protein_titer,
ct_harvest_crude_volume,
ct_harvest_filtration_volume_recovery_buffer,
ct_harvest_hat_protein_titer,
ct_harvest_hat_volume,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT (SELECT max_id from number_fountain_fact_uspproductivitytacid WHERE table_name = 'fact_uspproductivitytacid') + ROW_NUMBER() over(order by '') AS fact_uspproductivitytacidid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Average ' || substr(ps_date, 0, 4) as dd_ps_name,
       current_date as dd_ps_date,
       1 as dim_dateid,
       'Average ' || substr(ps_date, 0, 4) as dd_harvest_batch_id,
       ifnull(AVG(harvest_crude_protein_titer), 0) as ct_harvest_crude_protein_titer,
       ifnull(AVG(harvest_crude_volume), 0) as ct_harvest_crude_volume,
       ifnull(AVG(harvest_filtration_volume_recovery_buffer), 0) as ct_harvest_filtration_volume_recovery_buffer,
       ifnull(AVG(harvest_hat_protein_titer), 0) as ct_harvest_hat_protein_titer,
       ifnull(AVG(harvest_hat_volume), 0) as ct_harvest_hat_volume,
       ifnull(AVG(productivity_clarified), 0) as ct_productivity_clarified,
       ifnull(AVG(productivity_pbr), 0) as ct_productivity_pbr
FROM new_productivity_taci_d_crude__harvest
GROUP BY substr(ps_date, 0, 4);

UPDATE tmp_fact_uspproductivitytacid tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_uspproductivitytacid tmp;

UPDATE tmp_fact_uspproductivitytacid tmp
SET tmp.dim_dateid = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivitytacid tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_ps_date;

TRUNCATE TABLE fact_uspproductivitytacid;
INSERT INTO fact_uspproductivitytacid(
fact_uspproductivitytacidid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_crude_protein_titer,
ct_harvest_crude_volume,
ct_harvest_filtration_volume_recovery_buffer,
ct_harvest_hat_protein_titer,
ct_harvest_hat_volume,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT fact_uspproductivitytacidid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_ps_name,
       dd_ps_date,
       dim_dateid,
       dd_harvest_batch_id,
       ct_harvest_crude_protein_titer,
       ct_harvest_crude_volume,
       ct_harvest_filtration_volume_recovery_buffer,
       ct_harvest_hat_protein_titer,
       ct_harvest_hat_volume,
       ct_productivity_clarified,
       ct_productivity_pbr
FROM tmp_fact_uspproductivitytacid;

DROP TABLE IF EXISTS tmp_fact_uspproductivitytacid;

DELETE FROM emd586.fact_uspproductivitytacid;
INSERT INTO emd586.fact_uspproductivitytacid SELECT * FROM fact_uspproductivitytacid;