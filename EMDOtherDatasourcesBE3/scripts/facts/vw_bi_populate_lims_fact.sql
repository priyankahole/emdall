/******************************************************************************************************************/
/*   Script         : bi_populate_lims_fact                                                                       */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 07 Mar 2017                                                                                 */
/*   Description    : Populating script of fact_lims                                                              */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   07 Mar 2017         CristianT       1.0               Creating the script.                                   */
/*   20 Mar 2017         CristianT       1.1               QC Comment and QC Analytical Status logic              */
/*   27 Apr 2017         CristianT       1.2               Adding methods to exclude for Total Count of Worklist Filtered and Success Rate Filtered measures */
/*   09 May 2017         CristianT       1.3               Adding logic for Analyst                               */
/*   13 Sep 2017         CristianT       1.4               FTE calculation based on t_qc_exclude_user table       */
/*   27 Oct 2017         CristianT       1.5               Added dim_qualityusersiduserstamp to be used for Samples Received by FTE measure */
/*   18 Dec 2017         CristianT       1.6               Adding QC Analytical Status logic for Non-Commercial molecules */
/*   23 May 2018         CristianT       1.8               Change of logic based on new method grup and sub-service names */
/*   04 Mar 2019         CristianT       2.0               Add 3 new columns from t_method_datgrp_molec_2 table APP-11620 */
/*   27 Jun 2019         CristianT       2.1               Add new fields OJ-27                                   */
/******************************************************************************************************************/

/* 24 Aug 2017 CristianT: Added specific number_fountain table for the autocommit property */

DROP TABLE IF EXISTS number_fountain_fact_lims;
CREATE TABLE number_fountain_fact_lims LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

/* 24 Aug 2017 CristianT End */

/* CristianT: Using limshistory_delete table in case we reprocess so we won't have duplicate data for same snapshot date in the historical table.
Historical data: fact_limshistory will preserve all rows, any alter made on fact_lims we should make it on fact_limshistory aswell.
Reporting data: fact_lims will keep only 1 day for each week of year. In case we have 1 full week in history table we should keep only Saturday. In case the week just started we should keep last processed day.
*/
DROP TABLE IF EXISTS limshistory_delete;
CREATE TABLE limshistory_delete
AS
SELECT fact_limsid
FROM fact_limshistory
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_limshistory
WHERE fact_limsid IN (SELECT fact_limsid FROM limshistory_delete);

DROP TABLE IF EXISTS limshistory_delete;

DELETE FROM number_fountain_fact_lims WHERE table_name = 'fact_limshistory';

INSERT INTO number_fountain_fact_lims
SELECT 'fact_limshistory', ifnull(max(fact_limsid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_limshistory;

DROP TABLE IF EXISTS tmp_fact_lims;
CREATE TABLE tmp_fact_lims
AS
SELECT *
FROM fact_limshistory
WHERE 1 = 0;

INSERT INTO tmp_fact_lims(
fact_limsid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
snapshotdate,
dim_dateidsnapshot,
dd_lot_number,
dd_lot_status,
dd_lot_condition,
dd_batch_number,
dd_sample_id,
dd_user_sampleid,
dd_material_name,
dd_material_type,
dd_material_datagroup,
dd_supervisor,
dd_sample_status,
dd_sample_condition,
dd_logged_by,
dd_date_logged,
dd_sample_text,
dd_date_received,
dd_sampled_by,
dd_sampling_date,
dd_start_sampling_date,
dd_end_sampling_date,
dd_storage_temperature,
dd_ccp_capa_number,
dd_deviation_number,
dd_mes_sample_id,
dd_worklist_id,
dd_worklist_text,
dd_worklist_status,
dd_worklist_condition,
dd_task_status,
dd_task_condition,
dd_parent_task_method,
dd_task_text,
dd_worklist_validity,
dd_component,
dd_sample_history_date,
dd_task_history_date,
dd_worklist_history_date,
dim_dateidlogged,
dim_dateidrecieved,
dim_dateidsample,
dim_dateidsamplestart,
dim_dateidsampleend,
dim_dateidsamplehistory,
dim_dateidtaskhistory,
dim_dateidworklisthistory,
dd_methodgroup,
dim_dateidwrkinitiation,
dd_qc_status,
dd_qc_comment,
dd_product_name,
dim_dateidentrydate,
dd_methodexcluded,
dd_analyst,
dim_qualityusersidanalyst,
dim_dateidmesprodday,
ct_totaltasks,
ct_donetasks,
dim_dateidmaxtaskhistory,
dd_batchcompletionstatus,
ct_fteothergroups,
ct_ftechemistry,
ct_ftebiochemistry,
ct_ftemicrobio,
dim_qualityusersiduserstamp,
ct_ftephysicochemistry,
dd_binocs_service_code,
dd_binocs_service_name,
dd_testable,
dd_testing_lead_time_target,
dd_column1,
dd_column2,
dd_column3,
dd_column4,
dd_column5,
dd_result_information_purpose_only
)
SELECT (SELECT max_id from number_fountain_fact_lims WHERE table_name = 'fact_limshistory') + ROW_NUMBER() over(order by '') AS fact_limsid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END as snapshotdate,
       dt.dim_dateid as dim_dateidsnapshot,
       CASE WHEN ms.lot_number = 'null' THEN 'Not Set' ELSE ifnull(ms.lot_number, 'Not Set') END as dd_lot_number,
       CASE WHEN ms.lot_status = 'null' THEN 'Not Set' ELSE ifnull(ms.lot_status, 'Not Set') END as dd_lot_status,
       CASE WHEN ms.lot_condition = 'null' THEN 'Not Set' ELSE ifnull(ms.lot_condition, 'Not Set') END as dd_lot_condition,
       CASE WHEN ms.batch_number = 'null' THEN 'Not Set' ELSE ifnull(ms.batch_number, 'Not Set') END as dd_batch_number,
       ifnull(ms.sample_id, 0) as dd_sample_id,
       CASE WHEN ms.user_sampleid = 'null' THEN 'Not Set' ELSE ifnull(ms.user_sampleid, 'Not Set') END as dd_user_sampleid,
       CASE WHEN ms.material_name = 'null' THEN 'Not Set' ELSE ifnull(ms.material_name, 'Not Set') END as dd_material_name,
       CASE WHEN ms.material_type = 'null' THEN 'Not Set' ELSE ifnull(ms.material_type, 'Not Set') END as dd_material_type,
       CASE WHEN ms.material_datagroup = 'null' THEN 'Not Set' ELSE ifnull(ms.material_datagroup, 'Not Set') END as dd_material_datagroup,
       CASE WHEN ms.supervisor = 'null' THEN 'Not Set' ELSE ifnull(ms.supervisor, 'Not Set') END as dd_supervisor,
       CASE WHEN ms.sample_status = 'null' THEN 'Not Set' ELSE ifnull(ms.sample_status, 'Not Set') END as dd_sample_status,
       CASE WHEN ms.sample_condition = 'null' THEN 'Not Set' ELSE ifnull(ms.sample_condition, 'Not Set') END as dd_sample_condition,
       CASE WHEN ms.logged_by = 'null' THEN 'Not Set' ELSE ifnull(ms.logged_by, 'Not Set') END as dd_logged_by,
       CASE WHEN ms.date_logged = 'null' THEN '0001-01-01' ELSE ifnull(ms.date_logged, '0001-01-01') END as dd_date_logged,
       CASE WHEN ms.sample_text = 'null' THEN 'Not Set' ELSE ifnull(ms.sample_text, 'Not Set') END as dd_sample_text,
       CASE WHEN ms.date_received = 'null' THEN '0001-01-01' ELSE ifnull(ms.date_received, '0001-01-01') END as dd_date_received,
       CASE WHEN ms.sampled_by = 'null' THEN 'Not Set' ELSE ifnull(ms.sampled_by, 'Not Set') END as dd_sampled_by,
       CASE WHEN ms.sampling_date = 'null' THEN '0001-01-01' ELSE ifnull(ms.sampling_date, '0001-01-01') END as dd_sampling_date,
       CASE WHEN ms.start_sampling_date = 'null' THEN '0001-01-01' ELSE ifnull(ms.start_sampling_date, '0001-01-01') END as dd_start_sampling_date,
       CASE WHEN ms.end_sampling_date = 'null' THEN '0001-01-01' ELSE ifnull(ms.end_sampling_date, '0001-01-01') END as dd_end_sampling_date,
       CASE WHEN ms.storage_temperature = 'null' THEN 'Not Set' ELSE ifnull(ms.storage_temperature, 'Not Set') END as dd_storage_temperature,
       CASE WHEN ms.ccp_capa_number = 'null' THEN 'Not Set' ELSE ifnull(ms.ccp_capa_number, 'Not Set') END as dd_ccp_capa_number,
       CASE WHEN ms.deviation_number = 'null' THEN 'Not Set' ELSE ifnull(ms.deviation_number, 'Not Set') END as dd_deviation_number,
       CASE WHEN ms.mes_sample_id = 'null' THEN 'Not Set' ELSE ifnull(ms.mes_sample_id, 'Not Set') END as dd_mes_sample_id,
       ifnull(ms.worklist_id, 0) as dd_worklist_id,
       CASE WHEN ms.worklist_text = 'null' THEN 'Not Set' ELSE ifnull(ms.worklist_text, 'Not Set') END as dd_worklist_text,
       CASE WHEN ms.worklist_status = 'null' THEN 'Not Set' ELSE ifnull(ms.worklist_status, 'Not Set') END as dd_worklist_status,
       CASE WHEN ms.worklist_condition = 'null' THEN 'Not Set' ELSE ifnull(ms.worklist_condition, 'Not Set') END as dd_worklist_condition,
       CASE WHEN ms.task_status = 'null' THEN 'Not Set' ELSE ifnull(ms.task_status, 'Not Set') END as dd_task_status,
       CASE WHEN ms.task_condition = 'null' THEN 'Not Set' ELSE ifnull(ms.task_condition, 'Not Set') END as dd_task_condition,
       CASE WHEN ms.parent_task_method = 'null' THEN 'Not Set' ELSE ifnull(ms.parent_task_method, 'Not Set') END as dd_parent_task_method,
       CASE WHEN ms.task_text = 'null' THEN 'Not Set' ELSE ifnull(ms.task_text, 'Not Set') END as dd_task_text,
       CASE WHEN ms.worklist_validity = 'null' THEN 'Not Set' ELSE ifnull(ms.worklist_validity, 'Not Set') END as dd_worklist_validity,
       CASE WHEN ms.component = 'null' THEN 'Not Set' ELSE ifnull(ms.component, 'Not Set') END as dd_component,
       CASE WHEN ms.sample_history_date = 'null' THEN '0001-01-01' ELSE ifnull(ms.sample_history_date, '0001-01-01') END as dd_sample_history_date,
       CASE WHEN ms.task_history_date = 'null' THEN '0001-01-01' ELSE ifnull(ms.task_history_date, '0001-01-01') END as dd_task_history_date,
       CASE WHEN ms.worklist_history_date = 'null' THEN '0001-01-01' ELSE ifnull(ms.worklist_history_date, '0001-01-01') END as dd_worklist_history_date,
       1 as dim_dateidlogged,
       1 as dim_dateidrecieved,
       1 as dim_dateidsample,
       1 as dim_dateidsamplestart,
       1 as dim_dateidsampleend,
       1 as dim_dateidsamplehistory,
       1 as dim_dateidtaskhistory,
       1 as dim_dateidworklisthistory,
       'Not Set' as dd_methodgroup,
       1 as dim_dateidwrkinitiation,
       'Not Set' as dd_qc_status,
       'Not Set' as dd_qc_comment,
       'Not Set' as dd_product_name,
       1 as dim_dateidentrydate,
       'Not Set' as dd_methodexcluded,
       'Not Set' as dd_analyst,
       1 as dim_qualityusersidanalyst,
       1 as dim_dateidmesprodday,
       0 as ct_totaltasks,
       0 as ct_donetasks,
       1 as dim_dateidmaxtaskhistory,
       'Not Set' as dd_batchcompletionstatus,
       0 as ct_fteothergroups,
       0 as ct_ftechemistry,
       0 as ct_ftebiochemistry,
       0 as ct_ftemicrobio,
       1 as dim_qualityusersiduserstamp,
       0 as ct_ftephysicochemistry,
       'Not Set' as dd_binocs_service_code,
       'Not Set' as dd_binocs_service_name,
       'Not Set' as dd_testable,
       'Not Set' as dd_testing_lead_time_target,
       'Not Set' as dd_column1,
       'Not Set' as dd_column2,
       'Not Set' as dd_column3,
       'Not Set' as dd_column4,
       'Not Set' as dd_column5,
       ifnull(ms.result_information_purpose_only, 'Not Set') as dd_result_information_purpose_only
FROM t_eve_ms_all_qc_res ms
     INNER JOIN dim_date dt ON dt.companycode = 'Not Set' AND dt.datevalue = CASE WHEN extract(hour from current_timestamp) between 0 AND 18 THEN current_date - 1 ELSE current_date END
     ) t;

UPDATE tmp_fact_lims tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_lims tmp;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidlogged = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND to_date(tmp.dd_date_logged) = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidlogged <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidrecieved = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND case when tmp.dd_date_received = '0001-01-01' then '0001-01-01' when length(tmp.dd_date_received) = 10 then to_date(tmp.dd_date_received, 'DD.MM.YYYY') else to_date(tmp.dd_date_received, 'DD-MON-YYYY') end = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidrecieved <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidsample = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND case when tmp.dd_sampling_date = '0001-01-01' then '0001-01-01' when length(tmp.dd_sampling_date) = 10 then to_date(tmp.dd_sampling_date, 'DD.MM.YYYY') else to_date(tmp.dd_sampling_date, 'DD-MON-YYYY') end = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidsample <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidsamplestart = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND case when tmp.dd_start_sampling_date = '0001-01-01' then '0001-01-01' when length(tmp.dd_start_sampling_date) = 10 then to_date(tmp.dd_start_sampling_date, 'DD.MM.YYYY') else to_date(tmp.dd_start_sampling_date, 'DD-MON-YYYY') end = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidsamplestart <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidsampleend = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND case when tmp.dd_end_sampling_date = '0001-01-01' then '0001-01-01' when length(tmp.dd_end_sampling_date) = 10 then to_date(tmp.dd_end_sampling_date, 'DD.MM.YYYY') else to_date(tmp.dd_end_sampling_date, 'DD-MON-YYYY') end = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidsampleend <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidsamplehistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND to_date(tmp.dd_sample_history_date) = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidsamplehistory <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND to_date(tmp.dd_task_history_date) = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidtaskhistory <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidworklisthistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND to_date(tmp.dd_worklist_history_date) = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidworklisthistory <> ifnull(dt.dim_dateid, 1);

/* 28 Apr 2017 CristianT Start: Adding Entry Date */
DROP TABLE IF EXISTS tmp_maxentrydate;
CREATE TABLE tmp_maxentrydate
AS
SELECT NAIT_SAMPLE_ID as NAIT_SAMPLE_ID,
       nait_component as nait_component,
       ifnull(max(to_date(NAIT_ENTRY_DATE, 'YYYY-MM-DD')), '0001-01-01') as entry_date
FROM nait_ms_results
GROUP BY NAIT_SAMPLE_ID,
         nait_component;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidentrydate = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_lims tmp,
     tmp_maxentrydate entr
WHERE dt.companycode = 'Not Set'
      AND entr.entry_date = dt.datevalue
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dd_sample_id = entr.NAIT_SAMPLE_ID
      AND tmp.dd_component = entr.nait_component
      AND tmp.dim_dateidentrydate <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_maxentrydate;

/* 28 Apr 2017 CristianT End */


DROP TABLE IF EXISTS tmp_wrkinitiationdate;
CREATE TABLE tmp_wrkinitiationdate
AS
SELECT distinct worklist_id as worklist_id,
       substr(i_timestamp, 0, 10) as initiationdate
FROM nait_ms_worklists;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidwrkinitiation = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_wrkinitiationdate wrk,
     tmp_fact_lims tmp
WHERE dt.companycode = 'Not Set'
      AND wrk.initiationdate = dt.datevalue
      AND wrk.worklist_id = tmp.dd_worklist_id
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidwrkinitiation <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_wrkinitiationdate;

/* 23 May 2018 CristianT Start: Change of logic based on new method grup and sub-service names */
/* Replaced t_method_datagrp_molec table with t_method_datgrp_molec_2 */
UPDATE tmp_fact_lims tmp
SET tmp.dd_methodgroup = ifnull(mg.datagroup, 'Not Set')
FROM t_method_datgrp_molec_2 mg,
     tmp_fact_lims tmp
WHERE tmp.dd_parent_task_method = mg.name
      AND mg.datagroup <> 'N/A'
      AND tmp.dd_methodgroup <> ifnull(mg.datagroup, 'Not Set');

/* 23 May 2018 CristianT End */

DROP TABLE IF EXISTS tmp_productname;
CREATE TABLE tmp_productname
AS
SELECT attrs_material_name,
       max(attrs_text_value) as attrs_text_value
FROM t_eve_material_templ_attrs
WHERE 1 = 1
      AND attrs_name = 'Product Name'
      AND upper(attrs_text_value) not like 'REBIF%'
GROUP BY attrs_material_name;

UPDATE tmp_fact_lims tmp
SET tmp.dd_product_name = ifnull(pg.attrs_text_value, 'Not Set')
FROM tmp_productname pg,
     tmp_fact_lims tmp
WHERE tmp.dd_material_name = pg.attrs_material_name
      AND tmp.dd_product_name <> ifnull(pg.attrs_text_value, 'Not Set');

DROP TABLE IF EXISTS tmp_productname;

/* For Rebif G1 and Rebif G2 look only for records with material_name like B14A5 */
DROP TABLE IF EXISTS tmp_productrebif;
CREATE TABLE tmp_productrebif
AS
SELECT DISTINCT attrs_material_name,
       attrs_text_value
FROM t_eve_material_templ_attrs
WHERE 1 = 1
      AND attrs_name = 'Product Name'
      AND upper(attrs_text_value) like 'REBIF%';

UPDATE tmp_fact_lims tmp
SET tmp.dd_product_name = ifnull(pg.attrs_text_value, 'Not Set')
FROM tmp_productrebif pg,
     tmp_fact_lims tmp
WHERE tmp.dd_material_name = pg.attrs_material_name
      AND tmp.dd_material_name like 'B14A5%'
      AND tmp.dd_product_name <> ifnull(pg.attrs_text_value, 'Not Set');

DROP TABLE IF EXISTS tmp_productrebif;


/* 20 Mar 2017 CristianT Start: QC Comment and QC Analytical Status. QC Analytical Status its acting like a Traffic Light with Green, Yellow and Red values */
/*        Start ErbituX logic        */
/* IPC logic: Material_ID from BN1A5H40 to BN1A5H51 and join is made using LOT_NUMBER from lims data */

/* 08 Nov 2017 CristianT: Commented the initial logic for production date from MES and added new logic from MCH1
DROP TABLE IF EXISTS tmp_erbituxiproddate
CREATE TABLE tmp_erbituxiproddate
AS
SELECT distinct mo.batch as childbatch,
       substr(mo.batch, 0, 3) as parentbatch,
       mo.material_id as materialid,
       to_date(mo.end_date) as proddate
FROM fops_mo_list mo
     INNER JOIN fops_sfo_list sfo on mo.mo = sfo.mo
WHERE mo.material_id in ('BN1A5H52','BN1A5H53','BN1A5H54')
      and mo.end_date is not null
      and substr(mo.batch, 0, 1) BETWEEN '0' AND '9'
*/

DROP TABLE IF EXISTS tmp_erbituxiproddate;
CREATE TABLE tmp_erbituxiproddate
AS
SELECT distinct mo.batch as childbatch,
       substr(mo.batch, 0, 3) as parentbatch,
       mo.material_id as materialid,
       to_date(mc.hsdat, 'YYYYMMDD') as proddate
FROM fops_mo_list mo
     INNER JOIN pack_mch1 mc on mo.batch = mc.charg and mc.MATNR = mo.material_id
WHERE mo.material_id in ('BN1A5H52','BN1A5H53','BN1A5H54')
      and mc.hsdat is not null
      and substr(mo.batch, 0, 1) BETWEEN '0' AND '9';

DROP TABLE IF EXISTS tmp_erbituxipc;
CREATE TABLE tmp_erbituxipc
AS
SELECT f_lms.dd_lot_number as dd_batch_number,
       substr(f_lms.dd_lot_number, 0, 3) as limsparentbatch,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name BETWEEN 'BN1A5H40' AND 'BN1A5H51'
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

/* DS logic: Material_ID = BN1A5H52 and join is made using BATCH_NUMBER from lims data */
DROP TABLE IF EXISTS tmp_erbituxds;
CREATE TABLE tmp_erbituxds
AS
SELECT f_lms.dd_batch_number,
       substr(f_lms.dd_batch_number, 0, 3) as limsparentbatch,
       f_lms.dd_material_name,
       f_lms.dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name in ('BN1A5H52','BN1A5H53','BN1A5H54')
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_batch_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_batch_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

DROP TABLE IF EXISTS tmp_erbitux;
CREATE TABLE tmp_erbitux
AS
SELECT dd_batch_number, limsparentbatch, dd_material_name, dd_product_name, maxtskhistdate FROM tmp_erbituxipc
UNION ALL
SELECT dd_batch_number, limsparentbatch, dd_material_name, dd_product_name, maxtskhistdate FROM tmp_erbituxds;

DROP TABLE IF EXISTS tmp_materialcolor;
CREATE TABLE tmp_materialcolor
AS
SELECT ds.dd_batch_number as dd_batch_number,
       ds.limsparentbatch as limsparentbatch,
       ds.dd_material_name as dd_material_name,
       ds.dd_product_name as dd_product_name,
       ds.maxtskhistdate as maxtskhistdate,
       prdt.proddate as proddate,
       days_between(ds.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ds.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ds.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_erbitux ds,
     tmp_erbituxiproddate prdt
WHERE ds.limsparentbatch = prdt.parentbatch;

DROP TABLE IF EXISTS tmp_batchcolor;
CREATE TABLE tmp_batchcolor
AS
SELECT limsparentbatch,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by limsparentbatch order by lot_material_color desc) as color_order
FROM tmp_materialcolor tmp
GROUP BY limsparentbatch,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_batchcolor
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_batchcolor col
WHERE tmp.dd_batch_number = col.limsparentbatch
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_batchcolor col
WHERE tmp.dd_lot_number = col.limsparentbatch
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_erbituxiproddate prdd,
     tmp_fact_lims tmp,
     (select distinct limsparentbatch, dd_material_name, dd_product_name from tmp_erbituxipc) ipc
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.parentbatch = ipc.limsparentbatch /* ipc.dd_batch_number */
      AND tmp.dd_lot_number = ipc.limsparentbatch /* ipc.dd_batch_number */
      AND tmp.dd_material_name = ipc.dd_material_name
      AND tmp.dd_product_name = ipc.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_erbituxiproddate prdd,
     tmp_fact_lims tmp,
     (select distinct limsparentbatch, dd_material_name, dd_product_name from tmp_erbituxds) ds
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.parentbatch = ds.limsparentbatch /* ds.dd_batch_number */
      AND tmp.dd_batch_number = ds.limsparentbatch /* ds.dd_batch_number */
      AND tmp.dd_material_name = ds.dd_material_name
      AND tmp.dd_product_name = ds.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_erbituxmaxtaskhistdate;
CREATE TABLE tmp_erbituxmaxtaskhistdate
AS
SELECT limsparentbatch,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_erbitux
GROUP BY limsparentbatch,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_erbituxmaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     (select distinct limsparentbatch, dd_material_name, dd_product_name from tmp_erbituxipc) ipc
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.limsparentbatch = ipc.limsparentbatch /* ipc.dd_batch_number */
      AND prdd.dd_product_name = ipc.dd_product_name
      AND tmp.dd_lot_number = ipc.limsparentbatch /* ipc.dd_batch_number */
      AND tmp.dd_material_name = ipc.dd_material_name
      AND tmp.dd_product_name = ipc.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_erbituxmaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     (select distinct limsparentbatch, dd_material_name, dd_product_name from tmp_erbituxds) ds
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.limsparentbatch = ds.limsparentbatch /* ds.dd_batch_number */
      AND prdd.dd_product_name = ds.dd_product_name
      AND tmp.dd_batch_number = ds.limsparentbatch /* ds.dd_batch_number */
      AND tmp.dd_material_name = ds.dd_material_name
      AND tmp.dd_product_name = ds.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_erbituxmaxtaskhistdate;
DROP TABLE IF EXISTS tmp_erbitux;
DROP TABLE IF EXISTS tmp_erbituxipc;
DROP TABLE IF EXISTS tmp_erbituxds;
DROP TABLE IF EXISTS tmp_materialcolor;
DROP TABLE IF EXISTS tmp_batchcolor;
DROP TABLE IF EXISTS tmp_erbituxiproddate;

/* Start: Calculating Total Tasks and Done Tasks */
DROP TABLE IF EXISTS tmp_erbitotaltasks;
CREATE TABLE tmp_erbitotaltasks
AS
SELECT dd_batch_number,
       dd_product_name,
       count(distinct dd_sample_id) as total_tasks
FROM (
SELECT distinct ipc.dd_lot_number as dd_batch_number,
       ipc.dd_sample_id as dd_sample_id,
       ipc.dd_product_name as dd_product_name
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'ERBITUX H'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
UNION ALL
SELECT distinct ds.dd_batch_number as dd_batch_number,
       ds.dd_sample_id as dd_sample_id,
       ds.dd_product_name as dd_product_name
FROM tmp_fact_lims ds
WHERE ds.dd_product_name = 'ERBITUX H'
      AND lower(ds.dd_parent_task_method) not like '%back%'
) t
GROUP BY dd_batch_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_erbitotaltasks ttl
WHERE tmp.dd_batch_number = ttl.dd_batch_number
      AND tmp.dd_product_name = ttl.dd_product_name;


DROP TABLE IF EXISTS tmp_erbidonetasks;
CREATE TABLE tmp_erbidonetasks
AS
SELECT dd_batch_number,
       dd_product_name,
       count(distinct dd_sample_id) as done_tasks
FROM (
SELECT distinct ipc.dd_lot_number as dd_batch_number,
       ipc.dd_sample_id as dd_sample_id,
       ipc.dd_product_name
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'ERBITUX H'
UNION ALL
SELECT distinct ds.dd_batch_number as dd_batch_number,
       ds.dd_sample_id as dd_sample_id,
       ds.dd_product_name
FROM tmp_fact_lims ds
WHERE ds.dd_task_condition = 'APPROVED'
      AND lower(ds.dd_parent_task_method) not like '%back%'
      AND ds.dd_product_name = 'ERBITUX H'
) t
GROUP BY dd_batch_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_erbidonetasks dnt
WHERE tmp.dd_batch_number = dnt.dd_batch_number
      AND tmp.dd_product_name = dnt.dd_product_name;


DROP TABLE IF EXISTS tmp_erbitotaltasks;
DROP TABLE IF EXISTS tmp_erbidonetasks;
/* End: Calculating Total Tasks and Done Tasks */

/*          End ErbituX logic        */

/*          Start Rebif logic        */
/* IPC G1 logic: Material_ID from B14A5A07 to B14A5A12 and join is made using LOT_NUMBER from lims data */

DROP TABLE IF EXISTS tmp_rebifproddate;
CREATE TABLE tmp_rebifproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as material_id,
       case when material_id = 'B14A5A12' then 'G1' else 'G2' end as rebif_type,
       to_date(mo.end_date) as proddate
FROM fops_mo_list mo
WHERE mo.material_id in ('B14A5A12', 'B14A5A42')
      and mo.end_date is not null;


DROP TABLE IF EXISTS tmp_rebifg1ipc;
CREATE TABLE tmp_rebifg1ipc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       f_lms.dd_parent_task_method as dd_parent_task_method,
       f_lms.dd_user_sampleid as dd_user_sampleid,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name = 'B14A5A12'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
      AND f_lms.dd_user_sampleid like '%B14A5A12%'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_parent_task_method,
         f_lms.dd_product_name,
         f_lms.dd_user_sampleid;

DROP TABLE IF EXISTS tmp_g1materialcolor;
CREATE TABLE tmp_g1materialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.dd_parent_task_method as dd_parent_task_method,
       ipc.maxtskhistdate as maxtskhistdate,
       ipc.dd_user_sampleid as dd_user_sampleid,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_rebifg1ipc ipc,
     tmp_rebifproddate prdt
WHERE ipc.dd_lot_number = prdt.batch
      AND prdt.rebif_type = 'G1';

DROP TABLE IF EXISTS tmp_g1color;
CREATE TABLE tmp_g1color
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_g1materialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_g1color
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_g1color col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_rebifproddate prdd,
     tmp_fact_lims tmp,
     tmp_g1color g1
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = g1.dd_lot_number
      AND tmp.dd_lot_number = g1.dd_lot_number
      AND tmp.dd_product_name = g1.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_rebifg1maxtaskhistdate;
CREATE TABLE tmp_rebifg1maxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_rebifg1ipc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_rebifg1maxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_g1color g1
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = g1.dd_lot_number
      AND prdd.dd_product_name = g1.dd_product_name
      AND tmp.dd_lot_number = g1.dd_lot_number
      AND tmp.dd_product_name = g1.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_rebifg1maxtaskhistdate;
DROP TABLE IF EXISTS tmp_rebifg1ipc;
DROP TABLE IF EXISTS tmp_g1materialcolor;
DROP TABLE IF EXISTS tmp_g1color;

/* IPC G2 logic: Material_ID from B14A5A37 to B14A5A42 and join is made using LOT_NUMBER from lims data */
DROP TABLE IF EXISTS tmp_rebifg2ipc;
CREATE TABLE tmp_rebifg2ipc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       f_lms.dd_parent_task_method as dd_parent_task_method,
       f_lms.dd_user_sampleid as dd_user_sampleid,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name = 'B14A5A42'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
      AND f_lms.dd_user_sampleid like '%B14A5A42%'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_parent_task_method,
         f_lms.dd_product_name,
         f_lms.dd_user_sampleid;

DROP TABLE IF EXISTS tmp_g2materialcolor;
CREATE TABLE tmp_g2materialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.dd_parent_task_method as dd_parent_task_method,
       ipc.maxtskhistdate as maxtskhistdate,
       ipc.dd_user_sampleid as dd_user_sampleid,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_rebifg2ipc ipc,
     tmp_rebifproddate prdt
WHERE ipc.dd_lot_number = prdt.batch
      AND prdt.rebif_type = 'G2';

DROP TABLE IF EXISTS tmp_g2color;
CREATE TABLE tmp_g2color
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_g2materialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_g2color
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_g2color col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_rebifproddate prdd,
     tmp_fact_lims tmp,
     tmp_g2color g2
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = g2.dd_lot_number
      AND tmp.dd_lot_number = g2.dd_lot_number
      AND tmp.dd_product_name = g2.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_rebifg2maxtaskhistdate;
CREATE TABLE tmp_rebifg2maxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_rebifg2ipc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_rebifg2maxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_g2color g2
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = g2.dd_lot_number
      AND prdd.dd_product_name = g2.dd_product_name
      AND tmp.dd_lot_number = g2.dd_lot_number
      AND tmp.dd_product_name = g2.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_rebifg2maxtaskhistdate;
DROP TABLE IF EXISTS tmp_rebifproddate;
DROP TABLE IF EXISTS tmp_rebifg2ipc;
DROP TABLE IF EXISTS tmp_g2materialcolor;
DROP TABLE IF EXISTS tmp_g2color;

/* Start: Calculating Total Tasks and Done Tasks for G1 */
DROP TABLE IF EXISTS tmp_g1totaltasks;
CREATE TABLE tmp_g1totaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'REBIF G1'
      AND ipc.dd_material_name = 'B14A5A12'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_g1totaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_g1donetasks;
CREATE TABLE tmp_g1donetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'REBIF G1'
      AND ipc.dd_material_name = 'B14A5A12'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_g1donetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_g1totaltasks;
DROP TABLE IF EXISTS tmp_g1donetasks;
/* End: Calculating Total Tasks and Done Tasks for G1 */

/* Start: Calculating Total Tasks and Done Tasks for G2 */
DROP TABLE IF EXISTS tmp_g1totaltasks;
CREATE TABLE tmp_g1totaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'REBIF G2'
      AND ipc.dd_material_name = 'B14A5A42'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_g1totaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_g1donetasks;
CREATE TABLE tmp_g1donetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'REBIF G2'
      AND ipc.dd_material_name = 'B14A5A42'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_g1donetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_g1totaltasks;
DROP TABLE IF EXISTS tmp_g1donetasks;
/* End: Calculating Total Tasks and Done Tasks for G2 */

/*          End Rebif logic          */

/*          Start AntiPDL logic      */
/* IPC logic: USER_SAMPLEID like '%PDA5A%' and join is made using LOT_NUMBER from lims data */

/* 08 Nov 2017 CristianT: Commented the initial logic for production date from MES and added new logic from MCH1
DROP TABLE IF EXISTS tmp_otherproddate
CREATE TABLE tmp_otherproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as material_id,
       to_date(mo.end_date) as proddate
FROM fops_mo_list mo
WHERE mo.material_id = 'BPDA5A11'
      and mo.end_date is not null
*/

DROP TABLE IF EXISTS tmp_otherproddate;
CREATE TABLE tmp_otherproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as material_id,
       to_date(mc.hsdat, 'YYYYMMDD') as proddate
FROM fops_mo_list mo
     INNER JOIN pack_mch1 mc on mo.batch = mc.charg and mc.MATNR = mo.material_id
WHERE mo.material_id = 'BPDA5A11'
      and mc.hsdat is not null;

DROP TABLE IF EXISTS tmp_otheripc;
CREATE TABLE tmp_otheripc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_user_sampleid like '%PDA5A%'
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

DROP TABLE IF EXISTS tmp_othermaterialcolor;
CREATE TABLE tmp_othermaterialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.maxtskhistdate as maxtskhistdate,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_otheripc ipc,
     tmp_otherproddate prdt
WHERE ipc.dd_lot_number = prdt.batch;

DROP TABLE IF EXISTS tmp_othercolor;
CREATE TABLE tmp_othercolor
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_othermaterialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_othercolor
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_othercolor col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_otherproddate prdd,
     tmp_fact_lims tmp,
     tmp_othercolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = oth.dd_lot_number
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_othermaxtaskhistdate;
CREATE TABLE tmp_othermaxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_otheripc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_othermaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_othercolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = oth.dd_lot_number
      AND prdd.dd_product_name = oth.dd_product_name
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_othermaxtaskhistdate;
DROP TABLE IF EXISTS tmp_othermaterialcolor;
DROP TABLE IF EXISTS tmp_othercolor;
DROP TABLE IF EXISTS tmp_otherproddate;
DROP TABLE IF EXISTS tmp_otheripc;

/* Start: Calculating Total Tasks and Done Tasks */
DROP TABLE IF EXISTS tmp_antipdltotaltasks;
CREATE TABLE tmp_antipdltotaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'ANTI-PD-L1'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_antipdltotaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdldonetasks;
CREATE TABLE tmp_antipdldonetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'ANTI-PD-L1'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_antipdldonetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdltotaltasks;
DROP TABLE IF EXISTS tmp_antipdldonetasks;
/* End: Calculating Total Tasks and Done Tasks */

/*          End AntiPDL logic        */

/* 20 Mar 2017 CristianT End */

/* 18 Dec 2017 CristianT: Adding QC Analytical Status logic for Non-Commercial molecules */
/*          Start TOCILIZUMAB A logic      */
DROP TABLE IF EXISTS tmp_tocilizumabaproddate;
CREATE TABLE tmp_tocilizumabaproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as materialid,
       to_date(mc.hsdat, 'YYYYMMDD') as proddate
FROM fops_mo_list mo
     INNER JOIN pack_mch1 mc on mo.batch = mc.charg and mc.MATNR = mo.material_id
WHERE mo.material_id in ('ATZA5A01')
      and mc.hsdat is not null
      and substr(mo.batch, 0, 1) BETWEEN '0' AND '9';

DROP TABLE IF EXISTS tmp_tocilizumabaipc;
CREATE TABLE tmp_tocilizumabaipc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name BETWEEN 'BTZA5A0' AND 'BTZA5A11'
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

DROP TABLE IF EXISTS tmp_tocilizumabamaterialcolor;
CREATE TABLE tmp_tocilizumabamaterialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.maxtskhistdate as maxtskhistdate,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_tocilizumabaipc ipc,
     tmp_tocilizumabaproddate prdt
WHERE ipc.dd_lot_number = prdt.batch;

DROP TABLE IF EXISTS tmp_tocilizumabacolor;
CREATE TABLE tmp_tocilizumabacolor
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_tocilizumabamaterialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_tocilizumabacolor
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_tocilizumabacolor col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_tocilizumabaproddate prdd,
     tmp_fact_lims tmp,
     tmp_tocilizumabacolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = oth.dd_lot_number
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_tocilizumabamaxtaskhistdate;
CREATE TABLE tmp_tocilizumabamaxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_tocilizumabaipc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_tocilizumabamaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_tocilizumabacolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = oth.dd_lot_number
      AND prdd.dd_product_name = oth.dd_product_name
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_tocilizumabamaxtaskhistdate;
DROP TABLE IF EXISTS tmp_tocilizumabamaterialcolor;
DROP TABLE IF EXISTS tmp_tocilizumabacolor;
DROP TABLE IF EXISTS tmp_tocilizumabaproddate;
DROP TABLE IF EXISTS tmp_tocilizumabaipc;

/* Start: Calculating Total Tasks and Done Tasks */
DROP TABLE IF EXISTS tmp_tocilizumabatotaltasks;
CREATE TABLE tmp_tocilizumabatotaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'TOCILIZUMAB A'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_tocilizumabatotaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_tocilizumabadonetasks;
CREATE TABLE tmp_tocilizumabadonetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'TOCILIZUMAB A'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_tocilizumabadonetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_tocilizumabatotaltasks;
DROP TABLE IF EXISTS tmp_tocilizumabadonetasks;
/* End: Calculating Total Tasks and Done Tasks */

/*          End TOCILIZUMAB A logic        */

/*          Start ATACICEPT G2D logic      */
DROP TABLE IF EXISTS tmp_ataciceptproddate;
CREATE TABLE tmp_ataciceptproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as materialid,
       to_date(mc.hsdat, 'YYYYMMDD') as proddate
FROM fops_mo_list mo
     INNER JOIN pack_mch1 mc on mo.batch = mc.charg and mc.MATNR = mo.material_id
WHERE mo.material_id in ('A87A5004')
      and mc.hsdat is not null
      and substr(mo.batch, 0, 1) BETWEEN '0' AND '9';

DROP TABLE IF EXISTS tmp_ataciceptipc;
CREATE TABLE tmp_ataciceptipc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name BETWEEN 'B87A5012' AND 'B87A5022'
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

DROP TABLE IF EXISTS tmp_ataciceptmaterialcolor;
CREATE TABLE tmp_ataciceptmaterialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.maxtskhistdate as maxtskhistdate,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_ataciceptipc ipc,
     tmp_ataciceptproddate prdt
WHERE ipc.dd_lot_number = prdt.batch;

DROP TABLE IF EXISTS tmp_ataciceptcolor;
CREATE TABLE tmp_ataciceptcolor
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_ataciceptmaterialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_ataciceptcolor
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_ataciceptcolor col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_ataciceptproddate prdd,
     tmp_fact_lims tmp,
     tmp_ataciceptcolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = oth.dd_lot_number
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_ataciceptmaxtaskhistdate;
CREATE TABLE tmp_ataciceptmaxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_ataciceptipc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_ataciceptmaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_ataciceptcolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = oth.dd_lot_number
      AND prdd.dd_product_name = oth.dd_product_name
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_ataciceptmaxtaskhistdate;
DROP TABLE IF EXISTS tmp_ataciceptmaterialcolor;
DROP TABLE IF EXISTS tmp_ataciceptcolor;
DROP TABLE IF EXISTS tmp_ataciceptproddate;
DROP TABLE IF EXISTS tmp_ataciceptipc;

/* Start: Calculating Total Tasks and Done Tasks */
DROP TABLE IF EXISTS tmp_atacicepttotaltasks;
CREATE TABLE tmp_atacicepttotaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'ATACICEPT G2D'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_atacicepttotaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_ataciceptdonetasks;
CREATE TABLE tmp_ataciceptdonetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'ATACICEPT G2D'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_ataciceptdonetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_atacicepttotaltasks;
DROP TABLE IF EXISTS tmp_ataciceptdonetasks;
/* End: Calculating Total Tasks and Done Tasks */

/*          End ATACICEPT G2D logic        */

/*          Start ADALIMUMAB logic      */
/*          End ADALIMUMAB logic        */

/*          Start ADALIMUMAB B logic      */
/*          End ADALIMUMAB B logic        */

/*          Start ADALIMUMAB C logic      */
/*          End ADALIMUMAB C logic        */

/*          Start ANTI-PDL1 TRAP logic      */
DROP TABLE IF EXISTS tmp_antipdl1trapproddate;
CREATE TABLE tmp_antipdl1trapproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as materialid,
       to_date(mc.hsdat, 'YYYYMMDD') as proddate
FROM fops_mo_list mo
     INNER JOIN pack_mch1 mc on mo.batch = mc.charg and mc.MATNR = mo.material_id
WHERE mo.material_id in ('APTA5A01')
      and mc.hsdat is not null
      and substr(mo.batch, 0, 1) BETWEEN '0' AND '9';

DROP TABLE IF EXISTS tmp_antipdl1trapipc;
CREATE TABLE tmp_antipdl1trapipc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name BETWEEN 'BPTA5A01' AND 'BPTA5A11'
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdl1trapmaterialcolor;
CREATE TABLE tmp_antipdl1trapmaterialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.maxtskhistdate as maxtskhistdate,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_antipdl1trapipc ipc,
     tmp_antipdl1trapproddate prdt
WHERE ipc.dd_lot_number = prdt.batch;

DROP TABLE IF EXISTS tmp_antipdl1trapcolor;
CREATE TABLE tmp_antipdl1trapcolor
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_antipdl1trapmaterialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_antipdl1trapcolor
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_antipdl1trapcolor col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_antipdl1trapproddate prdd,
     tmp_fact_lims tmp,
     tmp_antipdl1trapcolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = oth.dd_lot_number
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_antipdl1trapmaxtaskhistdate;
CREATE TABLE tmp_antipdl1trapmaxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_antipdl1trapipc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_antipdl1trapmaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_antipdl1trapcolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = oth.dd_lot_number
      AND prdd.dd_product_name = oth.dd_product_name
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_antipdl1trapmaxtaskhistdate;
DROP TABLE IF EXISTS tmp_antipdl1trapmaterialcolor;
DROP TABLE IF EXISTS tmp_antipdl1trapcolor;
DROP TABLE IF EXISTS tmp_antipdl1trapproddate;
DROP TABLE IF EXISTS tmp_antipdl1trapipc;

/* Start: Calculating Total Tasks and Done Tasks */
DROP TABLE IF EXISTS tmp_antipdl1traptotaltasks;
CREATE TABLE tmp_antipdl1traptotaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'ANTI-PDL1 TRAP'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_antipdl1traptotaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdl1trapdonetasks;
CREATE TABLE tmp_antipdl1trapdonetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'ANTI-PDL1 TRAP'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_antipdl1trapdonetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdl1traptotaltasks;
DROP TABLE IF EXISTS tmp_antipdl1trapdonetasks;
/* End: Calculating Total Tasks and Done Tasks */

/*          End ANTI-PDL1 TRAP logic        */

/*          Start ANTI-PDL1 TRAP B logic      */
DROP TABLE IF EXISTS tmp_antipdl1trapbproddate;
CREATE TABLE tmp_antipdl1trapbproddate
AS
SELECT distinct mo.batch as batch,
       mo.material_id as materialid,
       to_date(mc.hsdat, 'YYYYMMDD') as proddate
FROM fops_mo_list mo
     INNER JOIN pack_mch1 mc on mo.batch = mc.charg and mc.MATNR = mo.material_id
WHERE mo.material_id in ('APTA5B01')
      and mc.hsdat is not null
      and substr(mo.batch, 0, 1) BETWEEN '0' AND '9';

DROP TABLE IF EXISTS tmp_antipdl1trapbipc;
CREATE TABLE tmp_antipdl1trapbipc
AS
SELECT f_lms.dd_lot_number as dd_lot_number,
       f_lms.dd_material_name as dd_material_name,
       f_lms.dd_product_name as dd_product_name,
       max(tskhis.datevalue) as maxtskhistdate
FROM tmp_fact_lims f_lms,
     dim_date tskhis
WHERE 1 = 1
      AND f_lms.dim_dateidtaskhistory = tskhis.dim_dateid
      AND f_lms.dd_material_name BETWEEN 'BPTA5B01' AND 'BPTA5B11'
      AND upper(f_lms.dd_component) not like '%DOCUMENTATION%'
      AND upper(f_lms.dd_component) not like 'INTERMEDIATE TOTAL%'
      AND upper(f_lms.dd_component) <> 'TOTAL COUNT'
      AND f_lms.dd_lot_number <> 'Not Set'
      AND f_lms.dd_sample_condition = 'APPROVED'
GROUP BY f_lms.dd_lot_number,
         f_lms.dd_material_name,
         f_lms.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdl1trapbmaterialcolor;
CREATE TABLE tmp_antipdl1trapbmaterialcolor
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_material_name as dd_material_name,
       ipc.dd_product_name as dd_product_name,
       ipc.maxtskhistdate as maxtskhistdate,
       prdt.proddate as proddate,
       days_between(ipc.maxtskhistdate,prdt.proddate) as no_days,
       case
         when days_between(ipc.maxtskhistdate,prdt.proddate) <= 45 then 0
         when days_between(ipc.maxtskhistdate,prdt.proddate) between 46 and 50 then 1
         else 2
       end lot_material_color
FROM tmp_antipdl1trapbipc ipc,
     tmp_antipdl1trapbproddate prdt
WHERE ipc.dd_lot_number = prdt.batch;

DROP TABLE IF EXISTS tmp_antipdl1trapbcolor;
CREATE TABLE tmp_antipdl1trapbcolor
AS
SELECT dd_lot_number,
       dd_product_name,
       case lot_material_color when 0 then 'Green' when 1 then 'Yellow' else 'Red' end as lot_color,
       row_number() over (partition by dd_lot_number order by lot_material_color desc) as color_order
FROM tmp_antipdl1trapbmaterialcolor tmp
GROUP BY dd_lot_number,
         dd_product_name,
         lot_material_color;

DELETE FROM tmp_antipdl1trapbcolor
WHERE color_order > 1;

UPDATE tmp_fact_lims tmp
SET tmp.dd_qc_status = col.lot_color
FROM tmp_fact_lims tmp,
     tmp_antipdl1trapbcolor col
WHERE tmp.dd_lot_number = col.dd_lot_number
      AND tmp.dd_product_name = col.dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmesprodday = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_antipdl1trapbproddate prdd,
     tmp_fact_lims tmp,
     tmp_antipdl1trapbcolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.proddate = dt.datevalue
      AND prdd.batch = oth.dd_lot_number
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmesprodday <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_antipdl1trapbmaxtaskhistdate;
CREATE TABLE tmp_antipdl1trapbmaxtaskhistdate
AS
SELECT dd_lot_number as dd_lot_number,
       dd_product_name as dd_product_name,
       max(maxtskhistdate) as maxtskhistdate
FROM tmp_antipdl1trapbipc
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET tmp.dim_dateidmaxtaskhistory = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_antipdl1trapbmaxtaskhistdate prdd,
     tmp_fact_lims tmp,
     tmp_antipdl1trapbcolor oth
WHERE dt.companycode = 'Not Set'
      AND prdd.maxtskhistdate = dt.datevalue
      AND prdd.dd_lot_number = oth.dd_lot_number
      AND prdd.dd_product_name = oth.dd_product_name
      AND tmp.dd_lot_number = oth.dd_lot_number
      AND tmp.dd_product_name = oth.dd_product_name
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidmaxtaskhistory <> ifnull(dt.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_antipdl1trapbmaxtaskhistdate;
DROP TABLE IF EXISTS tmp_antipdl1trapbmaterialcolor;
DROP TABLE IF EXISTS tmp_antipdl1trapbcolor;
DROP TABLE IF EXISTS tmp_antipdl1trapbproddate;
DROP TABLE IF EXISTS tmp_antipdl1trapbipc;

/* Start: Calculating Total Tasks and Done Tasks */
DROP TABLE IF EXISTS tmp_antipdl1trapbtotaltasks;
CREATE TABLE tmp_antipdl1trapbtotaltasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as total_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_product_name = 'ANTI-PDL1 TRAP B'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_totaltasks = ifnull(ttl.total_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_antipdl1trapbtotaltasks ttl
WHERE tmp.dd_lot_number = ttl.dd_lot_number
      AND tmp.dd_product_name = ttl.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdl1trapbdonetasks;
CREATE TABLE tmp_antipdl1trapbdonetasks
AS
SELECT ipc.dd_lot_number as dd_lot_number,
       ipc.dd_product_name as dd_product_name,
       count(distinct ipc.dd_sample_id) as done_tasks
FROM tmp_fact_lims ipc
WHERE ipc.dd_task_condition = 'APPROVED'
      AND lower(ipc.dd_parent_task_method) not like '%back%'
      AND ipc.dd_product_name = 'ANTI-PDL1 TRAP B'
GROUP BY dd_lot_number,
         dd_product_name;

UPDATE tmp_fact_lims tmp
SET ct_donetasks = ifnull(dnt.done_tasks, 0)
FROM tmp_fact_lims tmp,
     tmp_antipdl1trapbdonetasks dnt
WHERE tmp.dd_lot_number = dnt.dd_lot_number
      AND tmp.dd_product_name = dnt.dd_product_name;

DROP TABLE IF EXISTS tmp_antipdl1trapbtotaltasks;
DROP TABLE IF EXISTS tmp_antipdl1trapbdonetasks;
/* End: Calculating Total Tasks and Done Tasks */

/*          End ANTI-PDL1 TRAP B logic        */

/* 18 Dec 2017 CristianT End */

/* 23 May 2018 CristianT Start: Change of logic based on new method grup and sub-service names */
/* Replaced t_method_datagrp_molec table with t_method_datgrp_molec_2 */

/* 27 Apr 2017 CristianT Start: Adding methods to exclude for Total Count of Worklist Filtered and Success Rate Filtered measures */
UPDATE tmp_fact_lims tmp
SET tmp.dd_methodexcluded = ifnull(me.exclude, 'Not Set')
FROM t_method_datgrp_molec_2 me,
     tmp_fact_lims tmp
WHERE tmp.dd_parent_task_method = me.name;

/* 27 Apr 2017 CristianT End */
/* 23 May 2018 CristianT End */

/* 09 May 2017 CristianT Start: Adding logic for Analyst */
DROP TABLE IF EXISTS tmp_analyst;
CREATE TABLE tmp_analyst
AS
SELECT distinct WORKLIST_ID as WORKLIST_ID,
       ANALYST
FROM nait_ms_worklists;

UPDATE tmp_fact_lims tmp
SET tmp.dd_analyst = ifnull(an.ANALYST, 'Not Set')
FROM tmp_fact_lims tmp,
     tmp_analyst an
WHERE tmp.dd_worklist_id = an.WORKLIST_ID
      AND tmp.dd_analyst <> ifnull(an.ANALYST, 'Not Set');

UPDATE tmp_fact_lims tmp
SET tmp.dim_qualityusersidanalyst = ifnull(usr.dim_qualityusersid, 1)
FROM tmp_fact_lims tmp,
     dim_qualityusers usr
WHERE tmp.dd_analyst = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dim_qualityusersidanalyst <> ifnull(usr.dim_qualityusersid, 1);

DROP TABLE IF EXISTS tmp_analyst;
/* 09 May 2017 CristianT End */

/* 11 Jul 2017 CristianT Start: Batch Completion Status */
UPDATE tmp_fact_lims tmp
SET tmp.dd_batchcompletionstatus = 'Yes'
WHERE tmp.ct_donetasks = tmp.ct_totaltasks;

UPDATE tmp_fact_lims tmp
SET tmp.dd_batchcompletionstatus = 'No'
WHERE tmp.ct_donetasks < tmp.ct_totaltasks;

/* 11 Jul 2017 CristianT End */

/* 23 May 2018 CristianT Start: Change of logic based on new method grup and sub-service names */
/* The bellow piece of code was commented
13 Sep 2017 CristianT Start: FTE calculation based on t_qc_exclude_user table 
DROP TABLE IF EXISTS fte_calculation
CREATE TABLE fte_calculation
AS
SELECT CASE
         WHEN sub_service = 'QC Microbiology' THEN 'MICROBIO'
         WHEN sub_service = 'QC Product Testing' THEN 'PRODUCT TESTING'
         WHEN sub_service = 'QC Raw Material' THEN 'RAW MATERIAL'
         ELSE 'Not Set'
       END as methodgroup,
       count(*) as ct_fte
FROM t_qc_exclude_user
WHERE 1 = 1
      AND exclude = 'N'
GROUP BY CASE
           WHEN sub_service = 'QC Microbiology' THEN 'MICROBIO'
           WHEN sub_service = 'QC Product Testing' THEN 'PRODUCT TESTING'
           WHEN sub_service = 'QC Raw Material' THEN 'RAW MATERIAL'
           ELSE 'Not Set'
         END

UPDATE tmp_fact_lims tmp
SET tmp.ct_ftemicrobio = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'MICROBIO'

UPDATE tmp_fact_lims tmp
SET tmp.ct_fteprodtesting = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'PRODUCT TESTING'

UPDATE tmp_fact_lims tmp
SET tmp.ct_fterawmaterial = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'RAW MATERIAL'

UPDATE tmp_fact_lims tmp
SET tmp.ct_fteothergroups = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup NOT IN('RAW MATERIAL', 'PRODUCT TESTING', 'MICROBIO')
      AND fte.methodgroup = 'Not Set'

DROP TABLE IF EXISTS fte_calculation

13 Sep 2017 CristianT End */
DROP TABLE IF EXISTS fte_calculation;
CREATE TABLE fte_calculation
AS
SELECT CASE
         WHEN sub_service = 'QC Microbiology' THEN 'MICROBIO'
         WHEN sub_service = 'QC Biochemistry' THEN 'BIOCHEMISTRY'
         WHEN sub_service = 'QC Chemistry' THEN 'CHEMISTRY'
         WHEN sub_service = 'QC Physico-Chemistry' THEN 'PHYSICO-CHEMISTRY'
         ELSE 'Not Set'
       END as methodgroup,
       count(*) as ct_fte
FROM T_QC_EXCLUDE_USER_2
WHERE 1 = 1
      AND exclude = 'N'
GROUP BY CASE
           WHEN sub_service = 'QC Microbiology' THEN 'MICROBIO'
           WHEN sub_service = 'QC Biochemistry' THEN 'BIOCHEMISTRY'
           WHEN sub_service = 'QC Chemistry' THEN 'CHEMISTRY'
           WHEN sub_service = 'QC Physico-Chemistry' THEN 'PHYSICO-CHEMISTRY'
           ELSE 'Not Set'
         END;

UPDATE tmp_fact_lims tmp
SET tmp.ct_ftemicrobio = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'MICROBIO';

UPDATE tmp_fact_lims tmp
SET tmp.ct_ftebiochemistry = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'BIOCHEMISTRY';

UPDATE tmp_fact_lims tmp
SET tmp.ct_ftechemistry = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'CHEMISTRY';

UPDATE tmp_fact_lims tmp
SET tmp.ct_ftephysicochemistry = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup = fte.methodgroup
      AND tmp.dd_methodgroup = 'PHYSICO-CHEMISTRY';

UPDATE tmp_fact_lims tmp
SET tmp.ct_fteothergroups = fte.ct_fte
FROM tmp_fact_lims tmp,
     fte_calculation fte
WHERE tmp.dd_methodgroup NOT IN('MICROBIO', 'BIOCHEMISTRY', 'CHEMISTRY', 'PHYSICO-CHEMISTRY')
      AND fte.methodgroup = 'Not Set';

DROP TABLE IF EXISTS fte_calculation;

/* 23 MAy 2018 CristianT End */

/* 27 Oct 2017 CristianT Start: Added dim_qualityusersiduserstamp to be used for Samples Received by FTE measure */
DROP TABLE IF EXISTS tmp_userstamp_1step;
CREATE TABLE tmp_userstamp_1step
AS
SELECT distinct tmp.dd_sample_id as dd_sample_id,
       his.t_userstamp,
       his.t_i_timestamp,
       ROW_NUMBER() OVER (PARTITION BY tmp.dd_sample_id ORDER BY his.t_i_timestamp DESC) AS rowno
FROM t_eve_instance_history his
     INNER JOIN tmp_fact_lims tmp ON his.t_object_id = tmp.dd_sample_id
WHERE his.t_initial_state = 'INCOMPLETE'
      AND his.t_final_state = 'ONLINE';
      
     
UPDATE tmp_fact_lims tmp
SET tmp.dim_qualityusersiduserstamp = ifnull(dim.dim_qualityusersid, 1)
FROM tmp_fact_lims tmp,
     tmp_userstamp_1step usr,
     dim_qualityusers dim
WHERE tmp.dd_sample_id = usr.dd_sample_id
      AND usr.t_userstamp = dim.merck_uid
      AND dim.rowiscurrent = 1
      AND usr.rowno = 1
      AND tmp.dim_qualityusersiduserstamp <> ifnull(dim.dim_qualityusersid, 1);


UPDATE tmp_fact_lims tmp
SET tmp.dim_qualityusersiduserstamp = ifnull(dim.dim_qualityusersid, 1)
FROM tmp_fact_lims tmp,
     dim_qualityusers dim
WHERE tmp.dim_qualityusersiduserstamp = 1
      AND tmp.dd_logged_by = dim.merck_uid
      AND dim.rowiscurrent = 1
      AND tmp.dim_qualityusersiduserstamp <> ifnull(dim.dim_qualityusersid, 1);

DROP TABLE IF EXISTS tmp_userstamp_1step;
/* 27 Oct 2017 CristianT End */

/* 27 Jun 2019 CristianT Start: Add new fields OJ-27 */
/* 04 Mar 2019 CristianT Start: Add 3 new columns from t_method_datgrp_molec_2 table APP-11620 */
UPDATE tmp_fact_lims tmp
SET dd_binocs_service_code = ifnull(me.binocs_service_code, 'Not Set'),
    dd_binocs_service_name = ifnull(me.binocs_service_name, 'Not Set'),
    dd_testable = ifnull(me.testable, 'Not Set'),
    dd_testing_lead_time_target = ifnull(me.testing_lead_time_target, 'Not Set'),
    dd_column1 = ifnull(me.column1, 'Not Set'),
    dd_column2 = ifnull(me.column2, 'Not Set'),
    dd_column3 = ifnull(me.column3, 'Not Set'),
    dd_column4 = ifnull(me.column4, 'Not Set'),
    dd_column5 = ifnull(me.column5, 'Not Set')
FROM t_method_datgrp_molec_2 me,
     tmp_fact_lims tmp
WHERE tmp.dd_parent_task_method = me.name;

/* 04 Mar 2019 CristianT End */
/* 27 Jun 2019 CristianT End */

INSERT INTO fact_limshistory (
fact_limsid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
snapshotdate,
dim_dateidsnapshot,
dd_lot_number,
dd_lot_status,
dd_lot_condition,
dd_batch_number,
dd_sample_id,
dd_user_sampleid,
dd_material_name,
dd_material_type,
dd_material_datagroup,
dd_supervisor,
dd_sample_status,
dd_sample_condition,
dd_logged_by,
dd_date_logged,
dd_sample_text,
dd_date_received,
dd_sampled_by,
dd_sampling_date,
dd_start_sampling_date,
dd_end_sampling_date,
dd_storage_temperature,
dd_ccp_capa_number,
dd_deviation_number,
dd_mes_sample_id,
dd_worklist_id,
dd_worklist_text,
dd_worklist_status,
dd_worklist_condition,
dd_task_status,
dd_task_condition,
dd_parent_task_method,
dd_task_text,
dd_worklist_validity,
dd_component,
dd_sample_history_date,
dd_task_history_date,
dd_worklist_history_date,
dim_dateidlogged,
dim_dateidrecieved,
dim_dateidsample,
dim_dateidsamplestart,
dim_dateidsampleend,
dim_dateidsamplehistory,
dim_dateidtaskhistory,
dim_dateidworklisthistory,
dd_methodgroup,
dim_dateidwrkinitiation,
dd_qc_status,
dd_qc_comment,
dd_product_name,
dim_dateidentrydate,
dd_methodexcluded,
dd_analyst,
dim_qualityusersidanalyst,
dim_dateidmesprodday,
ct_totaltasks,
ct_donetasks,
dim_dateidmaxtaskhistory,
dd_batchcompletionstatus,
ct_fteothergroups,
ct_ftechemistry,
ct_ftebiochemistry,
ct_ftemicrobio,
dim_qualityusersiduserstamp,
ct_ftephysicochemistry,
dd_binocs_service_code,
dd_binocs_service_name,
dd_testable,
dd_testing_lead_time_target,
dd_column1,
dd_column2,
dd_column3,
dd_column4,
dd_column5,
dd_result_information_purpose_only
)
SELECT fact_limsid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       snapshotdate,
       dim_dateidsnapshot,
       dd_lot_number,
       dd_lot_status,
       dd_lot_condition,
       dd_batch_number,
       dd_sample_id,
       dd_user_sampleid,
       dd_material_name,
       dd_material_type,
       dd_material_datagroup,
       dd_supervisor,
       dd_sample_status,
       dd_sample_condition,
       dd_logged_by,
       dd_date_logged,
       dd_sample_text,
       dd_date_received,
       dd_sampled_by,
       dd_sampling_date,
       dd_start_sampling_date,
       dd_end_sampling_date,
       dd_storage_temperature,
       dd_ccp_capa_number,
       dd_deviation_number,
       dd_mes_sample_id,
       dd_worklist_id,
       dd_worklist_text,
       dd_worklist_status,
       dd_worklist_condition,
       dd_task_status,
       dd_task_condition,
       dd_parent_task_method,
       dd_task_text,
       dd_worklist_validity,
       dd_component,
       dd_sample_history_date,
       dd_task_history_date,
       dd_worklist_history_date,
       dim_dateidlogged,
       dim_dateidrecieved,
       dim_dateidsample,
       dim_dateidsamplestart,
       dim_dateidsampleend,
       dim_dateidsamplehistory,
       dim_dateidtaskhistory,
       dim_dateidworklisthistory,
       dd_methodgroup,
       dim_dateidwrkinitiation,
       dd_qc_status,
       dd_qc_comment,
       dd_product_name,
       dim_dateidentrydate,
       dd_methodexcluded,
       dd_analyst,
       dim_qualityusersidanalyst,
       dim_dateidmesprodday,
       ct_totaltasks,
       ct_donetasks,
       dim_dateidmaxtaskhistory,
       dd_batchcompletionstatus,
       ct_fteothergroups,
       ct_ftechemistry,
       ct_ftebiochemistry,
       ct_ftemicrobio,
       dim_qualityusersiduserstamp,
       ct_ftephysicochemistry,
       dd_binocs_service_code,
       dd_binocs_service_name,
       dd_testable,
       dd_testing_lead_time_target,
       dd_column1,
       dd_column2,
       dd_column3,
       dd_column4,
       dd_column5,
       dd_result_information_purpose_only
FROM tmp_fact_lims;

DROP TABLE IF EXISTS tmp_fact_lims;

DROP TABLE IF EXISTS lims_delete;
CREATE TABLE lims_delete
AS
SELECT fact_limsid
FROM fact_lims
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_lims
WHERE fact_limsid IN (SELECT fact_limsid FROM lims_delete);

DROP TABLE IF EXISTS lims_delete;


DROP TABLE IF EXISTS lims_insert;
CREATE TABLE lims_insert
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_limshistory f,
     dim_date dt
WHERE dim_dateidsnapshot = dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

DELETE FROM number_fountain_fact_lims WHERE table_name = 'fact_lims';

INSERT INTO number_fountain_fact_lims
SELECT 'fact_lims', ifnull(max(fact_limsid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_lims;

INSERT INTO fact_lims(
fact_limsid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
snapshotdate,
dim_dateidsnapshot,
dd_lot_number,
dd_lot_status,
dd_lot_condition,
dd_batch_number,
dd_sample_id,
dd_user_sampleid,
dd_material_name,
dd_material_type,
dd_material_datagroup,
dd_supervisor,
dd_sample_status,
dd_sample_condition,
dd_logged_by,
dd_date_logged,
dd_sample_text,
dd_date_received,
dd_sampled_by,
dd_sampling_date,
dd_start_sampling_date,
dd_end_sampling_date,
dd_storage_temperature,
dd_ccp_capa_number,
dd_deviation_number,
dd_mes_sample_id,
dd_worklist_id,
dd_worklist_text,
dd_worklist_status,
dd_worklist_condition,
dd_task_status,
dd_task_condition,
dd_parent_task_method,
dd_task_text,
dd_worklist_validity,
dd_component,
dd_sample_history_date,
dd_task_history_date,
dd_worklist_history_date,
dim_dateidlogged,
dim_dateidrecieved,
dim_dateidsample,
dim_dateidsamplestart,
dim_dateidsampleend,
dim_dateidsamplehistory,
dim_dateidtaskhistory,
dim_dateidworklisthistory,
dd_methodgroup,
dim_dateidwrkinitiation,
dd_qc_status,
dd_qc_comment,
dd_product_name,
dim_dateidentrydate,
dd_methodexcluded,
dd_analyst,
dim_qualityusersidanalyst,
dim_dateidmesprodday,
ct_totaltasks,
ct_donetasks,
dim_dateidmaxtaskhistory,
dd_batchcompletionstatus,
ct_fteothergroups,
ct_ftechemistry,
ct_ftebiochemistry,
ct_ftemicrobio,
dim_qualityusersiduserstamp,
ct_ftephysicochemistry,
dd_binocs_service_code,
dd_binocs_service_name,
dd_testable,
dd_testing_lead_time_target,
dd_column1,
dd_column2,
dd_column3,
dd_column4,
dd_column5,
dd_result_information_purpose_only
)
SELECT (SELECT max_id from number_fountain_fact_lims WHERE table_name = 'fact_lims') + ROW_NUMBER() over(order by '') AS fact_limsid,
       h.dim_projectsourceid,
       h.amt_exhangerate,
       h.amt_exchangerate_gbl,
       h.dim_currencyid,
       h.dim_currencyid_tra,
       h.dim_currencyid_gbl,
       h.dw_insert_date,
       h.dw_update_date,
       h.snapshotdate,
       h.dim_dateidsnapshot,
       h.dd_lot_number,
       h.dd_lot_status,
       h.dd_lot_condition,
       h.dd_batch_number,
       h.dd_sample_id,
       h.dd_user_sampleid,
       h.dd_material_name,
       h.dd_material_type,
       h.dd_material_datagroup,
       h.dd_supervisor,
       h.dd_sample_status,
       h.dd_sample_condition,
       h.dd_logged_by,
       h.dd_date_logged,
       h.dd_sample_text,
       h.dd_date_received,
       h.dd_sampled_by,
       h.dd_sampling_date,
       h.dd_start_sampling_date,
       h.dd_end_sampling_date,
       h.dd_storage_temperature,
       h.dd_ccp_capa_number,
       h.dd_deviation_number,
       h.dd_mes_sample_id,
       h.dd_worklist_id,
       h.dd_worklist_text,
       h.dd_worklist_status,
       h.dd_worklist_condition,
       h.dd_task_status,
       h.dd_task_condition,
       h.dd_parent_task_method,
       h.dd_task_text,
       h.dd_worklist_validity,
       h.dd_component,
       h.dd_sample_history_date,
       h.dd_task_history_date,
       h.dd_worklist_history_date,
       h.dim_dateidlogged,
       h.dim_dateidrecieved,
       h.dim_dateidsample,
       h.dim_dateidsamplestart,
       h.dim_dateidsampleend,
       h.dim_dateidsamplehistory,
       h.dim_dateidtaskhistory,
       h.dim_dateidworklisthistory,
       h.dd_methodgroup,
       h.dim_dateidwrkinitiation,
       h.dd_qc_status,
       h.dd_qc_comment,
       h.dd_product_name,
       h.dim_dateidentrydate,
       h.dd_methodexcluded,
       h.dd_analyst,
       h.dim_qualityusersidanalyst,
       h.dim_dateidmesprodday,
       h.ct_totaltasks,
       h.ct_donetasks,
       h.dim_dateidmaxtaskhistory,
       h.dd_batchcompletionstatus,
       h.ct_fteothergroups,
       h.ct_ftechemistry,
       h.ct_ftebiochemistry,
       h.ct_ftemicrobio,
       h.dim_qualityusersiduserstamp,
       h.ct_ftephysicochemistry,
       h.dd_binocs_service_code,
       h.dd_binocs_service_name,
       h.dd_testable,
       h.dd_testing_lead_time_target,
       h.dd_column1,
       h.dd_column2,
       h.dd_column3,
       h.dd_column4,
       h.dd_column5,
       h.dd_result_information_purpose_only
FROM fact_limshistory h,
     lims_insert ins
WHERE h.dim_dateidsnapshot = ins.dim_dateidsnapshot
      AND ins.rowno = 1
      AND NOT EXISTS (SELECT 1 FROM fact_lims t WHERE t.dim_dateidsnapshot = ins.dim_dateidsnapshot);

DROP TABLE IF EXISTS lims_insert;

DROP TABLE IF EXISTS tmp_delete_lims;
CREATE TABLE tmp_delete_lims
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM fact_lims f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO fact_lims f
USING tmp_delete_lims del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_delete_lims;

/* Keeping insert/delete into emd586 inside the script to prevent harmonization delays to affect the users */
DROP TABLE IF EXISTS emd586lims_delete;
CREATE TABLE emd586lims_delete
AS
SELECT fact_limsid
FROM emd586.fact_lims
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM emd586.fact_lims
WHERE fact_limsid IN (SELECT fact_limsid FROM emd586lims_delete);

DROP TABLE IF EXISTS emd586lims_delete;

INSERT INTO emd586.fact_lims
SELECT h.*
FROM fact_lims h
WHERE h.snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;


DROP TABLE IF EXISTS tmp_emd586deletelims;
CREATE TABLE tmp_emd586deletelims
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarweekyr ORDER BY dt.datevalue DESC) as rowno
FROM emd586.fact_lims f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO emd586.fact_lims f
USING tmp_emd586deletelims del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_emd586deletelims;
