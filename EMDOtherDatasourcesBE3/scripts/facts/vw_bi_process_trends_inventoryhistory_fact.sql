/**********************************************************************************/
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty			  */
/*  15 Oct 2013   1.81        Issam 	 Added material movement measures		  */
/*  21  Oct 2013  1.82        Shanthi New Fields for prod orders  */
/*  20 Dec 2013   1.82        Issam 	 Update optimization					  */
/*  9 Nov 2016                Cornelia   Add dd_batch_status_qkz                  */
/*  13 Mar 2017   1.83        Cristi B   Add Weekly, Monthly and Yearly Trend     */           
/**********************************************************************************/


drop table if exists fact_inventoryhistory_delete;
create table fact_inventoryhistory_delete as
select fact_inventoryhistoryid from fact_inventoryhistory
where SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

MERGE INTO fact_inventoryhistory t
USING
( select fact_inventoryhistoryid
  from fact_inventoryhistory_delete ) del
ON (t.fact_inventoryhistoryid = del.fact_inventoryhistoryid)
WHEN MATCHED THEN DELETE;

drop table if exists fact_inventoryhistory_delete;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventoryhistory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventoryhistory', ifnull(max(fact_inventoryhistoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM fact_inventoryhistory;

INSERT INTO fact_inventoryhistory(ct_POOpenQty, /* Issam change 24th Jun */
                                  ct_SOOpenQty, /* Issam change 24th Jun */
                                  amt_BlockedStockAmt,
                                  amt_BlockedStockAmt_GBL,
                                  amt_CommericalPrice1,
                                  amt_CurPlannedPrice,
                                  amt_MovingAvgPrice,
                                  amt_MtlDlvrCost,
                                  amt_OtherCost,
                                  amt_OverheadCost,
                                  amt_PlannedPrice1,
                                  amt_PreviousPrice,
                                  amt_PrevPlannedPrice,
                                  amt_StdUnitPrice,
                                  amt_StdUnitPrice_GBL,
                                  amt_StockInQInspAmt,
                                  amt_StockInQInspAmt_GBL,
                                  amt_StockInTransferAmt,
                                  amt_StockInTransferAmt_GBL,
                                  amt_StockInTransitAmt,
                                  amt_StockInTransitAmt_GBL,
                                  amt_StockValueAmt,
                                  amt_StockValueAmt_GBL,
                                  amt_UnrestrictedConsgnStockAmt,
                                  amt_UnrestrictedConsgnStockAmt_GBL,
                                  amt_WIPBalance,
				  				  ct_WIPQty,
                                  ct_BlockedConsgnStock,
                                  ct_BlockedStock,
                                  ct_BlockedStockReturns,
                                  ct_ConsgnStockInQInsp,
                                  ct_LastReceivedQty,
                                  ct_RestrictedConsgnStock,
                                  ct_StockInQInsp,
                                  ct_StockInTransfer,
                                  ct_StockInTransit,
                                  ct_StockQty,
                                  ct_TotalRestrictedStock,
                                  ct_UnrestrictedConsgnStock,
                                  ct_WIPAging,
                                  dd_BatchNo,
                                  dd_DocumentItemNo,
                                  dd_DocumentNo,
                                  dd_MovementType,
                                  dd_ValuationType,
                                  dim_Companyid,
                                  Dim_ConsumptionTypeid,
                                  dim_costcenterid,
                                  dim_Currencyid,
                                  Dim_DateIdLastChangedPrice,
                                  Dim_DateIdPlannedPrice2,
                                  Dim_DocumentStatusid,
                                  Dim_DocumentTypeid,
                                  Dim_IncoTermid,
                                  Dim_ItemCategoryid,
                                  Dim_ItemStatusid,
                                  dim_LastReceivedDateid,
                                  Dim_MovementIndicatorid,
                                  dim_Partid,
                                  dim_Plantid,
				  				  Dim_ProfitCenterId,
                                  dim_producthierarchyid,
                                  Dim_PurchaseGroupid,
                                  Dim_PurchaseMiscid,
                                  Dim_PurchaseOrgid,
                                  dim_specialstockid,
                                  dim_stockcategoryid,
                                  dim_StockTypeid,
                                  dim_StorageLocationid,
                                  dim_StorageLocEntryDateid,
                                  Dim_SupplyingPlantId,
                                  Dim_Termid,
                                  Dim_UnitOfMeasureid,
                                  dim_Vendorid,
                                  SnapshotDate,
                                  Dim_DateidSnapshot,
                                  fact_inventoryhistoryid,
				  				  amt_ExchangeRate_GBL,
				  				  amt_ExchangeRate,
				 				  dim_Currencyid_TRA,
								  dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */
				  				  ct_GRQty_Late30,
								  ct_GRQty_31_60,
								  ct_GRQty_61_90,
								  ct_GRQty_91_180,
								  ct_GIQty_Late30,
								  ct_GIQty_31_60,
								  ct_GIQty_61_90,
								  ct_GIQty_91_180,
/* End 15 Oct 2013 changes */
								  dd_prodordernumber,
								  dd_prodorderitemno,
								  dim_productionorderstatusid,
								  dim_productionordertypeid,
								  dim_partsalesid,
								  dim_bwproducthierarchyid,
								  dim_mdg_partid,
								  dd_invcovflag_emd,
								  ct_avgfcst_emd,
								  amt_cogsactualrate_emd,
								  amt_cogsfixedrate_emd,
								  amt_cogsfixedplanrate_emd,
								  amt_cogsplanrate_emd,
								  amt_cogsprevyearfixedrate_emd,
								  amt_cogsprevyearrate_emd,
								  amt_cogsprevyearto1_emd,
								  amt_cogsprevyearto2_emd,
								  amt_cogsprevyearto3_emd,
								  amt_cogsturnoverrate1_emd,
								  amt_cogsturnoverrate2_emd,
								  amt_cogsturnoverrate3_emd,
								  dim_bwhierarchycountryid,
								  dim_countryhierpsid,
								  dim_countryhierarid,
								  dim_clusterid,
								  ct_countmaterialsafetystock,
								  dd_batch_status_qkz,
								  dim_productionschedulerid
								  ,CT_ORDERITEMQTY
,CT_GRQTY,
								  ct_baseuomratioKG,
								  ct_intransitkg,
								  ct_dependentdemandkg,
								  amt_OnHand,
								  dd_batchstatus,
								  dim_dateidexpirydate,
								  dim_batchstatusid
				  )
   SELECT ifnull(ct_POOpenQty, 0),/* Issam change 24th Jun */
          ifnull(ct_SOOpenQty, 0), /* Issam change 24th Jun */
          ifnull(amt_BlockedStockAmt,0) amt_BlockedStockAmt,
          ifnull(amt_BlockedStockAmt_GBL,0) amt_BlockedStockAmt_GBL,
          ifnull(amt_CommericalPrice1,0) amt_CommericalPrice1,
          ifnull(amt_CurPlannedPrice,0) amt_CurPlannedPrice,
          ifnull(amt_MovingAvgPrice,0) amt_MovingAvgPrice,
          ifnull(amt_MtlDlvrCost,0) amt_MtlDlvrCost,
          ifnull(amt_OtherCost,0) amt_OtherCost,
          ifnull(amt_OverheadCost,0) amt_OverheadCost,
          ifnull(amt_PlannedPrice1,0) amt_PlannedPrice1,
          ifnull(amt_PreviousPrice,0) amt_PreviousPrice,
          ifnull(amt_PrevPlannedPrice,0) amt_PrevPlannedPrice,
          ifnull(amt_StdUnitPrice,0) amt_StdUnitPrice,
          ifnull(amt_StdUnitPrice_GBL,0) amt_StdUnitPrice_GBL,
          ifnull(amt_StockInQInspAmt,0) amt_StockInQInspAmt,
          ifnull(amt_StockInQInspAmt_GBL,0) amt_StockInQInspAmt_GBL,
          ifnull(amt_StockInTransferAmt,0) amt_StockInTransferAmt,
          ifnull(amt_StockInTransferAmt_GBL,0) amt_StockInTransferAmt_GBL,
          ifnull(amt_StockInTransitAmt,0) amt_StockInTransitAmt,
          ifnull(amt_StockInTransitAmt_GBL,0) amt_StockInTransitAmt_GBL,
          ifnull(amt_StockValueAmt,0) amt_StockValueAmt,
          ifnull(amt_StockValueAmt_GBL,0) amt_StockValueAmt_GBL,
          ifnull(amt_UnrestrictedConsgnStockAmt,0) amt_UnrestrictedConsgnStockAmt,
          ifnull(amt_UnrestrictedConsgnStockAmt_GBL,0) amt_UnrestrictedConsgnStockAmt_GBL,
          ifnull(amt_WIPBalance,0) amt_WIPBalance,
	      ifnull(ct_WIPQty,0) ct_WIPQty,
          ifnull(ct_BlockedConsgnStock,0) ct_BlockedConsgnStock,
          ifnull(ct_BlockedStock,0) ct_BlockedStock,
          ifnull(ct_BlockedStockReturns,0) ct_BlockedStockReturns,
          ifnull(ct_ConsgnStockInQInsp,0) ct_ConsgnStockInQInsp,
          ifnull(ct_LastReceivedQty,0) ct_LastReceivedQty,
          ifnull(ct_RestrictedConsgnStock,0) ct_RestrictedConsgnStock,
          ifnull(ct_StockInQInsp,0) ct_StockInQInsp,
          ifnull(ct_StockInTransfer,0) ct_StockInTransfer,
          ifnull(ct_StockInTransit,0) ct_StockInTransit,
          ifnull(ct_StockQty,0) ct_StockQty,
          ifnull(ct_TotalRestrictedStock,0) ct_TotalRestrictedStock,
          ifnull(ct_UnrestrictedConsgnStock,0) ct_UnrestrictedConsgnStock,
          ifnull(ct_WIPAging,0) ct_WIPAging,
          ifnull(dd_BatchNo,'Not Set') dd_BatchNo,
          ifnull(dd_DocumentItemNo,0) dd_DocumentItemNo,
          ifnull(dd_DocumentNo,'Not Set') dd_DocumentNo,
          ifnull(dd_MovementType,'Not Set') dd_MovementType,
          ifnull(dd_ValuationType,'Not Set') dd_ValuationType,
          ifnull(iag.dim_Companyid,convert(bigint,1)) dim_Companyid,
          ifnull(Dim_ConsumptionTypeid,convert(bigint,1)) Dim_ConsumptionTypeid,
          ifnull(dim_costcenterid,convert(bigint,1)) dim_costcenterid,
          ifnull(dim_Currencyid,convert(bigint,1)) dim_Currencyid,
          ifnull(Dim_DateIdLastChangedPrice,convert(bigint,1)) Dim_DateIdLastChangedPrice,
          ifnull(Dim_DateIdPlannedPrice2,convert(bigint,1)) Dim_DateIdPlannedPrice2,
          ifnull(Dim_DocumentStatusid,convert(bigint,1)) Dim_DocumentStatusid,
          ifnull(Dim_DocumentTypeid,convert(bigint,1)) Dim_DocumentTypeid,
          ifnull(Dim_IncoTermid,convert(bigint,1)) Dim_IncoTermid,
          ifnull(Dim_ItemCategoryid,convert(bigint,1)) Dim_ItemCategoryid,
          ifnull(Dim_ItemStatusid,convert(bigint,1)) Dim_ItemStatusid,
          ifnull(dim_LastReceivedDateid,convert(bigint,1)) dim_LastReceivedDateid,
          ifnull(Dim_MovementIndicatorid,convert(bigint,1)) Dim_MovementIndicatorid,
          ifnull(dim_Partid,convert(bigint,1)) dim_Partid,
          ifnull(dim_Plantid,convert(bigint,1)) dim_Plantid,
	      ifnull(Dim_ProfitCenterId,convert(bigint,1)) Dim_ProfitCenterId,
          ifnull(dim_producthierarchyid,convert(bigint,1)) dim_producthierarchyid,
          ifnull(Dim_PurchaseGroupid,convert(bigint,1)) Dim_PurchaseGroupid,
          ifnull(Dim_PurchaseMiscid,convert(bigint,1)) Dim_PurchaseMiscid,
          ifnull(Dim_PurchaseOrgid,convert(bigint,1)) Dim_PurchaseOrgid,
          ifnull(dim_specialstockid,convert(bigint,1)) dim_specialstockid,
          ifnull(dim_stockcategoryid,convert(bigint,1)) dim_stockcategoryid,
          ifnull(dim_StockTypeid,convert(bigint,1)) dim_StockTypeid,
          ifnull(dim_StorageLocationid,convert(bigint,1)) dim_StorageLocationid,
          ifnull(dim_StorageLocEntryDateid,convert(bigint,1)) dim_StorageLocEntryDateid,
          ifnull(Dim_SupplyingPlantId,convert(bigint,1)) Dim_SupplyingPlantId,
          ifnull(Dim_Termid,convert(bigint,1)) Dim_Termid,
          ifnull(Dim_UnitOfMeasureid,convert(bigint,1)) Dim_UnitOfMeasureid,
          ifnull(dim_Vendorid,convert(bigint,1)) dim_Vendorid,
          case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end SnapshotDate,
          dt.dim_dateid Dim_DateidSnapshot,
          (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryhistory') + row_number() over (order by ''),
	      amt_ExchangeRate_GBL,
	      amt_ExchangeRate,
	      ifnull(dim_Currencyid_TRA,convert(bigint,1)) dim_Currencyid_TRA,
	      ifnull(dim_Currencyid_GBL,convert(bigint,1)) dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */
	 ct_GRQty_Late30,
	 ct_GRQty_31_60,
	 ct_GRQty_61_90,
	 ct_GRQty_91_180,
	 ct_GIQty_Late30,
	 ct_GIQty_31_60,
	 ct_GIQty_61_90,
	 ct_GIQty_91_180,
/* End 15 Oct 2013 changes */
	  ifnull(dd_prodordernumber,'Not Set'),
	  ifnull(dd_prodorderitemno,0),
	  ifnull(dim_productionorderstatusid,1),
	  ifnull(dim_productionordertypeid,1),
	  ifnull(dim_partsalesid,convert(bigint,1)) dim_partsalesid,
	  ifnull(dim_bwproducthierarchyid,convert(bigint,1)) dim_bwproducthierarchyid,
	  ifnull(dim_mdg_partid,convert(bigint,1)) dim_mdg_partid,
	  ifnull(dd_invcovflag_emd,'Not Set') dd_invcovflag_emd,
	  ifnull(ct_avgfcst_emd,0) ct_avgfcst_emd,
	  ifnull(amt_cogsactualrate_emd,0) amt_cogsactualrate_emd,
	  ifnull(amt_cogsfixedrate_emd,0) amt_cogsfixedrate_emd,
	  ifnull(amt_cogsfixedplanrate_emd,0) amt_cogsfixedplanrate_emd,
	  ifnull(amt_cogsplanrate_emd,0) amt_cogsplanrate_emd,
	  ifnull(amt_cogsprevyearfixedrate_emd,0) amt_cogsprevyearfixedrate_emd,
	  ifnull(amt_cogsprevyearrate_emd,0) amt_cogsprevyearrate_emd,
	  ifnull(amt_cogsprevyearto1_emd,0) amt_cogsprevyearto1_emd,
	  ifnull(amt_cogsprevyearto2_emd,0) amt_cogsprevyearto2_emd,
	  ifnull(amt_cogsprevyearto3_emd,0) amt_cogsprevyearto3_emd,
	  ifnull(amt_cogsturnoverrate1_emd,0) amt_cogsturnoverrate1_emd,
	  ifnull(amt_cogsturnoverrate2_emd,0) amt_cogsturnoverrate2_emd,
	  ifnull(amt_cogsturnoverrate3_emd,0) amt_cogsturnoverrate3_emd,
	  ifnull(dim_bwhierarchycountryid,1) dim_bwhierarchycountryid,
	  ifnull(dim_countryhierpsid,1) dim_countryhierpsid,
	  ifnull(dim_countryhierarid,1) dim_countryhierarid,
	  ifnull(dim_clusterid,1) dim_clusterid,
	  ifnull(ct_countmaterialsafetystock,0) ct_countmaterialsafetystock,
	  ifnull(dd_batch_status_qkz,'Not Set') dd_batch_status_qkz,
	  ifnull(dim_productionschedulerid,1) dim_productionschedulerid
	  ,ifnull(CT_ORDERITEMQTY,0)
,ifnull(CT_GRQTY,0),
	  ifnull(ct_baseuomratioKG,0) ct_baseuomratioKG,
	  ifnull(ct_intransitkg,0) ct_intransitkg,
	  ifnull(ct_dependentdemandkg,0) ct_dependentdemandkg,
	  ifnull(amt_OnHand,0) amt_OnHand,
	  ifnull(dd_batchstatus,'Not Set') dd_batchstatus,
	  dim_dateidexpirydate,
	  ifnull(dim_batchstatusid,convert(bigint,1)) dim_batchstatusid
     FROM    facT_inventoryaging iag
          INNER JOIN
             dim_Company dc
          ON iag.dim_companyid = dc.dim_Companyid
		  INNER JOIN dim_date dt ON dt.companycode = dc.companycode and dt.DateValue = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;
update fact_inventoryhistory f
set f.dim_clusterid = dc.dim_clusterid
from fact_inventoryhistory f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
   and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
   and businesssector = 'BS-02'
   and mdg.primary_production_location = dc.primary_manufacturing_site
   and f.dim_clusterid <> dc.dim_clusterid;	

update emd586.fact_inventoryhistory f
set f.dim_clusterid = ff.dim_clusterid
from emd586.fact_inventoryhistory f,EMDTempoCC4.fact_inventoryhistory ff
where f.fact_inventoryhistoryid = ff.fact_inventoryhistoryid
	and f.dim_projectsourceid=1
	and f.dim_clusterid <> ff.dim_clusterid;
   
/*
UPDATE fact_inventoryhistory t0
SET t0.Dim_DateidSnapshot = dt.dim_dateid
FROM
fact_inventoryhistory t0,
dim_Company dc,
dim_date dt
WHERE       t0.dim_companyid = dc.dim_Companyid
	AND dt.DateValue = case when extract(hour from current_timestamp) between 0 and 16 then current_date - 1 else current_date end
	AND dt.companycode = dc.companycode
	AND t0.Dim_DateidSnapshot = 1*/

UPDATE NUMBER_FOUNTAIN
   SET max_id =
          (SELECT ifnull(max(fact_inventoryhistoryid), 0) FROM fact_inventoryhistory)
 WHERE table_name = 'fact_inventoryhistory';

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'processinglog', ifnull(max(processinglogid),  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) FROM processinglog;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'Start of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_StorageLocationid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_StorageLocationid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid IN (2, 3)
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end,
                  (case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - 1),
                  (case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - 7),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_StorageLocationid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
and ih1.SnapshotDate - (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate - ( INTERVAL '7' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Storagelocationid = ih2.dim_storagelocationid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid in (2,3)
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_Vendorid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_Vendorid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 4
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end,
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '1' DAY),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '7' DAY),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_Vendorid,
            dim_stockcategoryid,
            SnapshotDate;

UPDATE
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


truncate table fact_inventoryhistory_tmp;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
where  ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
and ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_Vendorid = ih2.dim_Vendorid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

truncate table fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock)
   SELECT dim_Partid,
          dim_Plantid,
          dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock)
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 5
          AND SnapshotDate IN
                 (case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end,
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '1' DAY),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '7' DAY),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '1' MONTH),
                  case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


 UPDATE
fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory_tmp ih1
SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt
FROM
fact_inventoryhistory_tmp ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end
AND     ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange
FROM
fact_inventoryhistory ih1,
fact_inventoryhistory_tmp ih2
WHERE ih1.dim_stockcategoryid = 5
AND ih1.dim_partid = ih2.dim_partid
AND ih1.dim_plantid = ih2.dim_plantid
AND ih1.dim_stockcategoryid = ih2.dim_stockcategoryid
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.SnapshotDate = case when extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'End of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

truncate table fact_inventoryhistory_tmp;

/* Begin 15 Oct 2013 changes */
/* Begin 20 Dec 2013 changes */
UPDATE fact_inventoryhistory
SET ct_GRQty_Late30 = 0
WHERE ct_GRQty_Late30 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_31_60 = 0
WHERE ct_GRQty_31_60 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_61_90 = 0
WHERE ct_GRQty_61_90 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_91_180 = 0
WHERE ct_GRQty_91_180 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_Late30 = 0
WHERE ct_GIQty_Late30 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_31_60 = 0
WHERE ct_GIQty_31_60 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_61_90 = 0
WHERE ct_GIQty_61_90 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_91_180 = 0
WHERE ct_GIQty_91_180 IS NULL;

UPDATE fact_inventoryhistory f
SET f.dim_dateidlastprocessed = (select dim_dateid from dim_date d where d.datevalue = current_date and d.companycode = 'Not Set');

/* Marius 1 sep 2016 add Daily variance */

DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-1 max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT MAX(snp.datevalue)-7 max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_1DAY_CHANGE;
CREATE TABLE TMP_1DAY_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock))
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = snp.datevalue -1 AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1daychange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1daychange = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = snp.datevalue - 7 AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1weekchange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1weekchange = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_1DAY_CHANGE a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_1DAY_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogs_1monthchange <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogs_1monthchange = x.v_cogs;

/* @Catalin BI-4971 add qty for current, 1m, 3m, 12m*/
DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt -- month
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-3) max_dt -- 3 months
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-12) max_dt -- 1 year
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_CHANGE;
CREATE TABLE TMP_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(f_ih.ct_StockQty  
    + f_ih.ct_StockInQInsp
    + f_ih.ct_BlockedStock 
    + f_ih.ct_StockInTransit 
    + f_ih.ct_StockInTransfer 
    + f_ih.ct_TotalRestrictedStock) COGS_ON_HAND_QTY
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;


DROP TABLE IF EXISTS TMP_CURRENT;
CREATE TABLE TMP_CURRENT
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(f_ih.ct_StockQty  
    + f_ih.ct_StockInQInsp
    + f_ih.ct_BlockedStock 
    + f_ih.ct_StockInTransit 
    + f_ih.ct_StockInTransfer 
    + f_ih.ct_TotalRestrictedStock) COGS_ON_HAND_QTY
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID 
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;



MERGE INTO fact_inventoryhistory f_ih0
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND_QTY,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.ct_cogsonhand <> ifnull(a.COGS_ON_HAND_QTY,0)) x
ON f_ih0.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih0.ct_cogsonhand = x.v_cogs;



MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND_QTY,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CHANGE a ON a.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.ct_cogsonhand_1m <> ifnull(a.COGS_ON_HAND_QTY,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.ct_cogsonhand_1m = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih3
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND_QTY,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CHANGE a ON a.snapshotdate = add_months(snp.datevalue,-3) AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.ct_cogsonhand_3m <> ifnull(a.COGS_ON_HAND_QTY,0)) x
ON f_ih3.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih3.ct_cogsonhand_3m = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih12
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND_QTY,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CHANGE a ON a.snapshotdate = add_months(snp.datevalue,-12) AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.ct_cogsonhand_12m <> ifnull(a.COGS_ON_HAND_QTY,0)) x
ON f_ih12.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih12.ct_cogsonhand_12m = x.v_cogs;


/* @ Catalin add delta qty for 1m, 3m, 12m*/
DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt -- month
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-3) max_dt -- 3 months
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-12) max_dt -- 1 year
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_CHANGE;
CREATE TABLE TMP_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt 
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty 
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock)) 
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;


DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID 
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;

DROP TABLE IF EXISTS TMP_CURRENT;
CREATE TABLE TMP_CURRENT
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt 
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty 
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock)) 
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_1mdelta <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogsonhand_1mdelta = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih3
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-3) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_3mdelta <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih3.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih3.amt_cogsonhand_3mdelta = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih12
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-12) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_12mdelta <> ifnull(a.COGS_ON_HAND,0) - ifnull(b.COGS_ON_HAND,0)) x
ON f_ih12.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih12.amt_cogsonhand_3mdelta = x.v_cogs;

/* @ Catalin add amt for 1m, 3m, 12m*/
DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt -- month
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-3) max_dt -- 3 months
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-12) max_dt -- 1 year
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_CHANGE;
CREATE TABLE TMP_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt 
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty 
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock)) 
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;


DROP TABLE IF EXISTS TMP_CURRENT;
CREATE TABLE TMP_CURRENT
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(CAST(CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN ((f_ih.amt_StockValueAmt + f_ih.amt_StockInQInspAmt + f_ih.amt_BlockedStockAmt 
    + f_ih.amt_StockInTransitAmt + f_ih.amt_StockInTransferAmt + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice))) ELSE ((f_ih.ct_StockQty 
    + f_ih.ct_StockInQInsp + f_ih.ct_BlockedStock + f_ih.ct_StockInTransit + f_ih.ct_StockInTransfer + f_ih.ct_TotalRestrictedStock)) 
    * (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) END AS DECIMAL (18,4))) COGS_ON_HAND
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;


DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID 
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;


MERGE INTO fact_inventoryhistory f_ih0
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0)  v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
   WHERE f_ih.amt_cogsonhand <> ifnull(a.COGS_ON_HAND,0)) x
ON f_ih0.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih0.amt_cogsonhand = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0)  v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CHANGE a ON a.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.amt_cogsonhand_1m <> ifnull(a.COGS_ON_HAND,0)) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogsonhand_1m = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih3
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CHANGE a ON a.snapshotdate = add_months(snp.datevalue,-3) AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.amt_cogsonhand_3m <> ifnull(a.COGS_ON_HAND,0)) x
ON f_ih3.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih3.amt_cogsonhand_3m = x.v_cogs;

MERGE INTO fact_inventoryhistory f_ih12
USING (
  SELECT f_ih.fact_inventoryhistoryid, ifnull(a.COGS_ON_HAND,0) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CHANGE a ON a.snapshotdate = add_months(snp.datevalue,-12) AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
  WHERE f_ih.amt_cogsonhand_12m <> ifnull(a.COGS_ON_HAND,0)) x
ON f_ih12.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih12.amt_cogsonhand_12m = x.v_cogs;

/* @ Catalin add volumeeffect for 1m, 3m, 12m*/
DROP TABLE IF EXISTS TMP_LATEST_DATE;
CREATE TABLE TMP_LATEST_DATE
AS
SELECT MAX(snp.datevalue) max_dt
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-1) max_dt -- month
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-3) max_dt -- 3 months
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
UNION
SELECT add_months(MAX(snp.datevalue),-12) max_dt -- 1 year
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid;

DROP TABLE IF EXISTS TMP_MAX_ID;
CREATE TABLE TMP_MAX_ID
AS
SELECT Dim_DateidSnapshot,dim_plantid,dim_partid,max(fact_inventoryhistoryid) ID 
FROM fact_inventoryhistory f_ih
	INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY Dim_DateidSnapshot,dim_plantid,dim_partid;


DROP TABLE IF EXISTS TMP_CHANGE;
CREATE TABLE TMP_CHANGE
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(f_ih.ct_StockQty  
    + f_ih.ct_StockInQInsp
    + f_ih.ct_BlockedStock 
    + f_ih.ct_StockInTransit 
    + f_ih.ct_StockInTransfer 
    + f_ih.ct_TotalRestrictedStock) COGS_ON_HAND_QTY
	,CASE WHEN (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) = 0 THEN  amt_StdUnitPrice else (amt_cogsfixedplanrate_emd/amt_exchangerate_gbl) end cogs_plan_rate
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max_dt FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid,5;

DROP TABLE IF EXISTS TMP_CURRENT;
CREATE TABLE TMP_CURRENT
AS
SELECT
  snp.datevalue snapshotdate,
  dim_plantid,
  dim_partid,
  SUM(f_ih.ct_StockQty  
    + f_ih.ct_StockInQInsp
    + f_ih.ct_BlockedStock 
    + f_ih.ct_StockInTransit 
    + f_ih.ct_StockInTransfer 
    + f_ih.ct_TotalRestrictedStock) COGS_ON_HAND_QTY
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue IN (SELECT max(max_dt) FROM TMP_LATEST_DATE)
GROUP BY snp.datevalue,dim_plantid,dim_partid;

MERGE INTO fact_inventoryhistory f_ih1
USING (
  SELECT f_ih.fact_inventoryhistoryid, sum((ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-1) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_1mcef <> (ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate group by 1) x
ON f_ih1.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih1.amt_cogsonhand_1mcef = x.v_cogs;

/* Change 28 Feb 2017 added sum
MERGE INTO fact_inventoryhistory f_ih3
USING (
  SELECT f_ih.fact_inventoryhistoryid, (ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-3) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_3mcef <> (ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate) x
ON f_ih3.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih3.amt_cogsonhand_3mcef = x.v_cog*/

MERGE INTO fact_inventoryhistory f_ih3
USING (
  SELECT f_ih.fact_inventoryhistoryid, sum((ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate) v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-3) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_3mcef <> (ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate  group by 1) x
ON f_ih3.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih3.amt_cogsonhand_3mcef = x.v_cogs;


MERGE INTO fact_inventoryhistory f_ih12
USING (
  SELECT f_ih.fact_inventoryhistoryid, (ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate v_cogs
  FROM fact_inventoryhistory f_ih
    INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
    INNER JOIN TMP_MAX_ID i on f_ih.fact_inventoryhistoryid = i.ID
    LEFT JOIN TMP_CURRENT a ON a.snapshotdate = snp.datevalue AND f_ih.dim_plantid = a.dim_plantid AND f_ih.dim_partid = a.dim_partid
    LEFT JOIN TMP_CHANGE b ON b.snapshotdate = add_months(snp.datevalue,-12) AND f_ih.dim_plantid = b.dim_plantid AND f_ih.dim_partid = b.dim_partid
  WHERE f_ih.amt_cogsonhand_12mcef <> (ifnull(a.COGS_ON_HAND_QTY,0) - ifnull(b.COGS_ON_HAND_QTY,0))*b.cogs_plan_rate) x
ON f_ih12.fact_inventoryhistoryid = x.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE SET f_ih12.amt_cogsonhand_12mcef = x.v_cogs;


/* Begin 13 March Cristi B Add weekly, monthly and yearly trend */

/* weekly */

DROP TABLE IF EXISTS TMP_CHANGE_WEEKLY;
CREATE TABLE TMP_CHANGE_WEEKLY
AS
SELECT
  snp.CALENDARWEEKYR,
  dim_plantid,
  dim_partid,
  SUM(f_ih.amt_StockValueAmt
     + f_ih.amt_StockInQInspAmt
     + f_ih.amt_BlockedStockAmt
     + f_ih.amt_StockInTransitAmt
     + f_ih.amt_StockInTransferAmt
     + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice)
     ) ON_HAND_AMT
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.WEEKDAYABBREVIATION = 'Sat'
AND
(
snp.CALENDARWEEKYR
= ( CASE
        WHEN WEEK(CURRENT_DATE) = 0
        THEN CONCAT(EXTRACT(YEAR FROM ADD_YEARS(CURRENT_DATE,-1)),53)
        ELSE TO_CHAR(ADD_WEEKS(CURRENT_DATE,-1),'YYYYuW')
     END )
 OR
 snp.CALENDARWEEKYR = TO_CHAR(CURRENT_DATE,'YYYYuW')
 )
GROUP BY snp.CALENDARWEEKYR,dim_plantid,dim_partid
ORDER BY TO_NUMBER(snp.CALENDARWEEKYR) DESC;

DROP TABLE IF EXISTS TMP_CHANGE_WEEKLY_R;
CREATE TABLE TMP_CHANGE_WEEKLY_R AS
SELECT
      DENSE_RANK() OVER( ORDER BY TO_NUMBER(CALENDARWEEKYR)) rn
     ,t.*
  FROM TMP_CHANGE_WEEKLY t;


UPDATE fact_inventoryhistory f
SET f.amt_onhand_weekly_diff = t2.on_hand_amt - t1.on_hand_amt, f.dw_update_date = current_date
    FROM TMP_CHANGE_WEEKLY_R t1
        ,TMP_CHANGE_WEEKLY_R t2
        ,fact_inventoryhistory f
        ,dim_date snpd
   WHERE t1.rn = t2.rn - 1
     AND t1.dim_partid = t2.dim_partid
     AND t1.dim_plantid = t2.dim_plantid
     AND t2.dim_partid = f.dim_partid
     AND t2.dim_plantid = f.dim_plantid
     AND f.dim_dateidsnapshot = snpd.dim_dateid
     AND t2.CALENDARWEEKYR = snpd.CALENDARWEEKYR
	 AND f.amt_onhand_weekly_diff <> t2.on_hand_amt - t1.on_hand_amt;

	 
	 
/* monthly */ 

DROP TABLE IF EXISTS TMP_CHANGE_MONTHLY;
CREATE TABLE TMP_CHANGE_MONTHLY
AS
SELECT
  snp.CALENDARMONTHID,
  dim_plantid,
  dim_partid,
  SUM(f_ih.amt_StockValueAmt
     + f_ih.amt_StockInQInspAmt
     + f_ih.amt_BlockedStockAmt
     + f_ih.amt_StockInTransitAmt
     + f_ih.amt_StockInTransferAmt
     + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice)
     ) ON_HAND_AMT
    ,snp.datevalue
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE snp.datevalue = snp.MONTHENDDATE AND CALENDARMONTHID IN (TO_CHAR(CURRENT_DATE,'YYYYMM'), TO_CHAR(ADD_MONTHS(CURRENT_DATE,-1),'YYYYMM'))
GROUP BY snp.CALENDARMONTHID,dim_plantid,dim_partid,snp.datevalue
ORDER BY TO_NUMBER(snp.CALENDARMONTHID);

DROP TABLE IF EXISTS TMP_CHANGE_MONTHLY_R;
CREATE TABLE TMP_CHANGE_MONTHLY_R AS
SELECT
      DENSE_RANK() OVER( ORDER BY TO_NUMBER(CALENDARMONTHID)) rn
     ,t.*
  FROM TMP_CHANGE_MONTHLY t;


UPDATE fact_inventoryhistory f
SET f.amt_onhand_monthly_diff = t2.on_hand_amt - t1.on_hand_amt, f.dw_update_date = current_date
    FROM TMP_CHANGE_MONTHLY_R t1
        ,TMP_CHANGE_MONTHLY_R t2
        ,fact_inventoryhistory f
        ,dim_date snpd
   WHERE t1.rn = t2.rn - 1
     AND t1.dim_partid = t2.dim_partid
     AND t1.dim_plantid = t2.dim_plantid
     AND t2.dim_partid = f.dim_partid
     AND t2.dim_plantid = f.dim_plantid
     AND f.dim_dateidsnapshot = snpd.dim_dateid
     AND t2.CALENDARMONTHID = snpd.CALENDARMONTHID
	 AND f.amt_onhand_monthly_diff <> t2.on_hand_amt - t1.on_hand_amt;


/* yearly */


DROP TABLE IF EXISTS TMP_CHANGE_YEARLY;
CREATE TABLE TMP_CHANGE_YEARLY
AS
SELECT
  snp.CALENDARYEAR,
  dim_plantid,
  dim_partid,
  SUM(f_ih.amt_StockValueAmt
     + f_ih.amt_StockInQInspAmt
     + f_ih.amt_BlockedStockAmt
     + f_ih.amt_StockInTransitAmt
     + f_ih.amt_StockInTransferAmt
     + (f_ih.ct_TotalRestrictedStock * f_ih.amt_StdUnitPrice)
     ) ON_HAND_AMT
FROM fact_inventoryhistory f_ih
  INNER JOIN dim_date snp ON f_ih.Dim_DateidSnapshot = snp.dim_dateid
WHERE TO_CHAR(snp.datevalue,'MMDD')  = '1231' AND TO_CHAR(snp.datevalue,'YYYY') IN ( TO_CHAR(ADD_YEARS(CURRENT_DATE,-1),'YYYY'),TO_CHAR(CURRENT_DATE-1,'YYYY'))
GROUP BY snp.CALENDARYEAR,dim_plantid,dim_partid
ORDER BY TO_NUMBER(snp.CALENDARYEAR);

DROP TABLE IF EXISTS TMP_CHANGE_YEARLY_R;
CREATE TABLE TMP_CHANGE_YEARLY_R AS
SELECT
      DENSE_RANK() OVER( ORDER BY TO_NUMBER(CALENDARYEAR)) rn
     ,t.*
  FROM TMP_CHANGE_YEARLY t;


UPDATE fact_inventoryhistory f
SET f.amt_onhand_yearly_diff = t2.on_hand_amt - t1.on_hand_amt, f.dw_update_date = current_date
    FROM TMP_CHANGE_YEARLY_R t1
        ,TMP_CHANGE_YEARLY_R t2
        ,fact_inventoryhistory f
        ,dim_date snpd
   WHERE t1.rn = t2.rn - 1
     AND t1.dim_partid = t2.dim_partid
     AND t1.dim_plantid = t2.dim_plantid
     AND t2.dim_partid = f.dim_partid
     AND t2.dim_plantid = f.dim_plantid
     AND f.dim_dateidsnapshot = snpd.dim_dateid
     AND t2.CALENDARYEAR = snpd.CALENDARYEAR
	 AND f.amt_onhand_yearly_diff <> t2.on_hand_amt - t1.on_hand_amt;

/* End 13 March Cristi B Add weekly, monthly and yearly trend */
	 