/******************************************************************************************************************/
/*   Script         : bi_populate_harvestantipdl1_fact                                                            */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_harvestantipdl1                                                   */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/******************************************************************************************************************/

UPDATE antipdl1_bio_ppq
SET volume_bio_extrait_ebrs = null
WHERE volume_bio_extrait_ebrs = 'n/a';

DROP TABLE IF EXISTS tmp_fact_harvestantipdl1;
CREATE TABLE tmp_fact_harvestantipdl1
AS
SELECT *
FROM fact_harvestantipdl1
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_harvestantipdl1';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_harvestantipdl1', ifnull(max(fact_harvestantipdl1id), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_harvestantipdl1;

INSERT INTO tmp_fact_harvestantipdl1(
fact_harvestantipdl1id,
dd_runid,
dd_durationgrowing,
dd_harvest_date,
dim_dateidharvest,
ct_volumebio,
ct_titrebio,
ct_volumeharvest,
ct_titreharvest,
ct_yieldcla,
ct_productinharvest,
ct_campagneaverage,
ct_drugsubstance,
ct_yieldbiods,
ct_volbioextraitebrs,
ct_volhatextraitebrs,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_harvestantipdl1') + ROW_NUMBER() over(order by '') as fact_harvestantipdl1id,
       ifnull(an.run, 'Not Set') as dd_runid,
       ifnull(an.duration_growing, 'Not Set') as dd_durationgrowing,
       ifnull(an.harvest_date, 'Not Set') as dd_harvest_date,
       1 as dim_dateidharvest,
       ifnull(an.volume_bio, 0) as ct_volumebio,
       ifnull(an.titre_bio, 0) as ct_titrebio,
       ifnull(an.volume_harvest, 0) as ct_volumeharvest,
       ifnull(an.titre_harvest, 0) as ct_titreharvest,
       ifnull(an.yield_cla, 0) as ct_yieldcla,
       ifnull(an.product_in_harvest, 0) as ct_productinharvest,
       ifnull(an.campagne_average, 0) as ct_campagneaverage,
       ifnull(an.drug_substance, 0) as ct_drugsubstance,
       ifnull(an.yield_bio_ds, 0) as ct_yieldbiods,
       ifnull(an.volume_bio_extrait_ebrs, 0)  as ct_volbioextraitebrs,
       ifnull(an.volume_hat_extrait_ebrs_kg, 0)  as ct_volhatextraitebrs,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM AntiPDL1_Bio_PPQ an
WHERE to_date(harvest_date, 'DD.MM.YYYY') >=date_trunc('year', current_date) - interval '5' YEAR;

UPDATE tmp_fact_harvestantipdl1 tmp
SET tmp.dim_dateidharvest =  dt.dim_dateid
FROM dim_date dt,
     tmp_fact_harvestantipdl1 tmp
WHERE dt.datevalue = to_date(tmp.dd_harvest_date, 'DD.MM.YYYY')
      AND dt.companycode = 'Not Set';

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_harvestantipdl1';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_harvestantipdl1', ifnull(max(fact_harvestantipdl1id), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_harvestantipdl1;

DROP TABLE IF EXISTS tmp_avgpreviousyears;
CREATE TABLE tmp_avgpreviousyears
AS
SELECT dt.calendaryear as calendaryear,
       max(dd_runid) as maxrun,
       max(ct_campagneaverage) as ct_productinharvest,
       ' Average ' || dt.calendaryear as dd_runid,
       convert(bigint, 1) as DIM_DATEIDHARVEST
FROM tmp_fact_harvestantipdl1 fct,
     dim_date dt
WHERE 1 = 1
      AND fct.DIM_DATEIDHARVEST = dt.dim_dateid
GROUP BY dt.calendaryear;

UPDATE tmp_avgpreviousyears av
SET av.DIM_DATEIDHARVEST = dt.dim_dateid
FROM tmp_avgpreviousyears av,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = current_date;

INSERT INTO tmp_fact_harvestantipdl1(
fact_harvestantipdl1id,
dd_runid,
dd_durationgrowing,
dd_harvest_date,
dim_dateidharvest,
ct_volumebio,
ct_titrebio,
ct_volumeharvest,
ct_titreharvest,
ct_yieldcla,
ct_productinharvest,
ct_campagneaverage,
ct_drugsubstance,
ct_yieldbiods,
ct_volbioextraitebrs,
ct_volhatextraitebrs,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_harvestantipdl1') + ROW_NUMBER() over(order by '') as fact_harvestantipdl1id,
       dd_runid,
       'Not Set' as dd_durationgrowing,
	   'Not Set' as dd_harvest_date,
       dim_dateidharvest,
       0 as ct_volumebio,
       0 as ct_titrebio,
       0 as ct_volumeharvest,
       0 as ct_titreharvest,
       0 as ct_yieldcla,
       ct_productinharvest as ct_productinharvest,
       0 as ct_campagneaverage,
       0 as ct_drugsubstance,
       0 as ct_yieldbiods,
       0 as ct_volbioextraitebrs,
       0 as ct_volhatextraitebrs,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM tmp_avgpreviousyears;

DROP TABLE IF EXISTS tmp_avgpreviousyears;

UPDATE tmp_fact_harvestantipdl1 tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_harvestantipdl1 tmp;

TRUNCATE TABLE fact_harvestantipdl1;

INSERT INTO fact_harvestantipdl1(
fact_harvestantipdl1id,
dd_runid,
dd_durationgrowing,
dd_harvest_date,
dim_dateidharvest,
ct_volumebio,
ct_titrebio,
ct_volumeharvest,
ct_titreharvest,
ct_yieldcla,
ct_productinharvest,
ct_campagneaverage,
ct_drugsubstance,
ct_yieldbiods,
ct_volbioextraitebrs,
ct_volhatextraitebrs,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_harvestantipdl1id,
       dd_runid,
       dd_durationgrowing,
       dd_harvest_date,
       dim_dateidharvest,
       ct_volumebio,
       ct_titrebio,
       ct_volumeharvest,
       ct_titreharvest,
       ct_yieldcla,
       ct_productinharvest,
       ct_campagneaverage,
       ct_drugsubstance,
       ct_yieldbiods,
       ct_volbioextraitebrs,
       ct_volhatextraitebrs,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_harvestantipdl1;

DROP TABLE IF EXISTS tmp_fact_harvestantipdl1;

TRUNCATE TABLE emd586.fact_harvestantipdl1;

INSERT INTO emd586.fact_harvestantipdl1(
fact_harvestantipdl1id,
dd_runid,
dd_durationgrowing,
dd_harvest_date,
dim_dateidharvest,
ct_volumebio,
ct_titrebio,
ct_volumeharvest,
ct_titreharvest,
ct_yieldcla,
ct_productinharvest,
ct_campagneaverage,
ct_drugsubstance,
ct_yieldbiods,
ct_volbioextraitebrs,
ct_volhatextraitebrs,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_harvestantipdl1id,
       dd_runid,
       dd_durationgrowing,
       dd_harvest_date,
       dim_dateidharvest,
       ct_volumebio,
       ct_titrebio,
       ct_volumeharvest,
       ct_titreharvest,
       ct_yieldcla,
       ct_productinharvest,
       ct_campagneaverage,
       ct_drugsubstance,
       ct_yieldbiods,
       ct_volbioextraitebrs,
       ct_volhatextraitebrs,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM fact_harvestantipdl1;