/******************************************************************************************************************/
/*   Script         : bi_populate_rebifproductivity_fact                                                          */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_rebifproductivity                                                 */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   31 Jan 2017         CristianT       1.0               Creating the script.                                   */
/*   10 Feb 2017         CristianT       1.1               Calculating the AVG of previous years                  */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_rebifproductivity;
CREATE TABLE tmp_fact_rebifproductivity
AS
SELECT *
FROM fact_rebifproductivity
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifproductivity', ifnull(max(fact_rebifproductivityid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifproductivity;

INSERT INTO tmp_fact_rebifproductivity(
fact_rebifproductivityid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_runid,
ct_prod_perfusion_g_run,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity') + ROW_NUMBER() over(order by '') as fact_rebifproductivityid,
       tmp.*
FROM (
SELECT distinct 'RBIF G1' as dd_type,
       ifnull(disco.parameter_set_name ,'Not Set') as dd_paramater_set_name,
       ifnull(disco.parameter_set_date ,'Not Set') as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ifnull(disco.bioreactor_inoc_run_id ,'Not Set') as dd_runid,
       ifnull(disco.prod_perfusion_g_run, 0) as ct_prod_perfusion_g_run,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM g_rbif_per_run_g1 disco,
     fops_ebr_execution ebr
WHERE right(disco.bioreactor_inoc_run_id, 3) = ebr.batch
      AND ebr.material_id in ('B14A5000','B14A5001')
      AND ebr.bo = 'BO09'
) tmp;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifproductivity', ifnull(max(fact_rebifproductivityid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifproductivity;

INSERT INTO tmp_fact_rebifproductivity(
fact_rebifproductivityid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_runid,
ct_prod_perfusion_g_run,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity') + ROW_NUMBER() over(order by '') as fact_rebifproductivityid,
       tmp.*
FROM (
SELECT distinct 'RBIF G2' as dd_type,
       ifnull(disco.parameter_set_name ,'Not Set') as dd_paramater_set_name,
       ifnull(disco.parameter_set_date ,'Not Set') as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ifnull(disco.bioreactor_inoc_run_id ,'Not Set') as dd_runid,
       ifnull(disco.prod_perfusion_g_run, 0) as ct_prod_perfusion_g_run,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM g_rbif_per_run_g2 disco,
     fops_ebr_execution ebr
WHERE right(disco.bioreactor_inoc_run_id, 3) = ebr.batch
      AND ebr.material_id in ('B14A5A30','B14A5A31')
      AND ebr.bo = 'BO08'
) tmp;

UPDATE tmp_fact_rebifproductivity tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_rebifproductivity tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = substr(tmp.dd_parameter_set_date, 0, 10);


/* 10 Feb 2016 CristianT Start: Calculating the AVG of previous years */
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifproductivity', ifnull(max(fact_rebifproductivityid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifproductivity;


INSERT INTO tmp_fact_rebifproductivity(
fact_rebifproductivityid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_runid,
ct_prod_perfusion_g_run,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity') + ROW_NUMBER() over(order by '') as fact_rebifproductivityid,
       tmp.*
FROM (
SELECT distinct 'RBIF G1' as dd_type,
       ' Average ' || YEAR(substr(disco.parameter_set_date, 0, 10)) as dd_PARAMATER_SET_NAME,
       current_date as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ' Average ' || YEAR(substr(disco.parameter_set_date, 0, 10)) as dd_RUNID,
       ifnull(AVG(disco.prod_perfusion_g_run), 0) as ct_PROD_PERFUSION_G_RUN,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM g_rbif_per_run_g1 disco,
     fops_ebr_execution ebr
WHERE right(disco.bioreactor_inoc_run_id, 3) = ebr.batch
      AND ebr.material_id in ('B14A5000','B14A5001')
      AND ebr.bo = 'BO09'
GROUP BY YEAR(substr(disco.parameter_set_date, 0, 10))
) tmp;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_rebifproductivity', ifnull(max(fact_rebifproductivityid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_rebifproductivity;

INSERT INTO tmp_fact_rebifproductivity(
fact_rebifproductivityid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_runid,
ct_prod_perfusion_g_run,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_rebifproductivity') + ROW_NUMBER() over(order by '') as fact_rebifproductivityid,
       tmp.*
FROM (
SELECT distinct 'RBIF G2' as dd_type,
       ' Average ' || YEAR(substr(disco.parameter_set_date, 0, 10)) as dd_PARAMATER_SET_NAME,
       current_date as dd_parameter_set_date,
       1 as dim_dateidsetdate,
       ' Average ' || YEAR(substr(disco.parameter_set_date, 0, 10)) as dd_RUNID,
       ifnull(AVG(disco.prod_perfusion_g_run), 0) as ct_PROD_PERFUSION_G_RUN,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date
FROM g_rbif_per_run_g2 disco,
     fops_ebr_execution ebr
WHERE right(disco.bioreactor_inoc_run_id, 3) = ebr.batch
      AND ebr.material_id in ('B14A5A30','B14A5A31')
      AND ebr.bo = 'BO08'
GROUP BY YEAR(substr(disco.parameter_set_date, 0, 10))
) tmp;

UPDATE tmp_fact_rebifproductivity tmp
SET tmp.dim_dateidsetdate = dt.dim_dateid
FROM tmp_fact_rebifproductivity tmp,
     dim_date dt
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_parameter_set_date
      AND tmp.dim_dateidsetdate = 1;
/* 10 Feb 2016 CristianT End */

UPDATE tmp_fact_rebifproductivity tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_rebifproductivity tmp;

TRUNCATE TABLE fact_rebifproductivity;

INSERT INTO fact_rebifproductivity(
fact_rebifproductivityid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_runid,
ct_prod_perfusion_g_run,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_rebifproductivityid,
       dd_type,
       dd_paramater_set_name,
       dd_parameter_set_date,
       dim_dateidsetdate,
       dd_runid,
       ct_prod_perfusion_g_run,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM tmp_fact_rebifproductivity;

DROP TABLE IF EXISTS tmp_fact_rebifproductivity;

TRUNCATE TABLE emd586.fact_rebifproductivity;

INSERT INTO emd586.fact_rebifproductivity(
fact_rebifproductivityid,
dd_type,
dd_paramater_set_name,
dd_parameter_set_date,
dim_dateidsetdate,
dd_runid,
ct_prod_perfusion_g_run,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date
)
SELECT fact_rebifproductivityid,
       dd_type,
       dd_paramater_set_name,
       dd_parameter_set_date,
       dim_dateidsetdate,
       dd_runid,
       ct_prod_perfusion_g_run,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date
FROM fact_rebifproductivity;