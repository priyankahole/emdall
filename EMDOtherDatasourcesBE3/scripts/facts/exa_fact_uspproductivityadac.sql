/*****************************************************************************************************************/
/*   Script         : exa_fact_uspproductivityadac                                                               */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 18 Dec 2018                                                                                */
/*   Description    : Populating script of fact_uspproductivityadac                                              */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   18 Dec 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_uspproductivityadac;
CREATE TABLE tmp_fact_uspproductivityadac
AS
SELECT *
FROM fact_uspproductivityadac
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_uspproductivityadac;
CREATE TABLE number_fountain_fact_uspproductivityadac LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_uspproductivityadac
SELECT 'fact_uspproductivityadac', ifnull(max(fact_uspproductivityadacid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_uspproductivityadac;


INSERT INTO tmp_fact_uspproductivityadac(
fact_uspproductivityadacid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_crude_protein_titer,
ct_harvest_crude_volume,
ct_harvest_filtration_volume_recovery_buffer,
ct_harvest_hat_protein_titer,
ct_harvest_hat_volume,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT (SELECT max_id from number_fountain_fact_uspproductivityadac WHERE table_name = 'fact_uspproductivityadac') + ROW_NUMBER() over(order by '') AS fact_uspproductivityadacid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(ps_name, 'Not Set') as dd_ps_name,
       ifnull(ps_date, '0001-01-01') as dd_ps_date,
       1 as dim_dateid,
       ifnull(harvest_batch_id, 'Not Set') as dd_harvest_batch_id,
       ifnull(harvest_crude_protein_titer, 0) as ct_harvest_crude_protein_titer,
       ifnull(harvest_crude_volume, 0) as ct_harvest_crude_volume,
       ifnull(harvest_filtration_volume_recovery_buffer, 0) as ct_harvest_filtration_volume_recovery_buffer,
       ifnull(harvest_hat_protein_titer, 0) as ct_harvest_hat_protein_titer,
       ifnull(harvest_hat_volume, 0) as ct_harvest_hat_volume,
       ifnull(productivity_clarified, 0) as ct_productivity_clarified,
       ifnull(productivity_pbr, 0) as ct_productivity_pbr
FROM new_productivity_ada_c_crude__harvest;

/* Calculating the AVG of previous years */
DELETE FROM number_fountain_fact_uspproductivityadac WHERE table_name = 'fact_uspproductivityadac';

INSERT INTO number_fountain_fact_uspproductivityadac
SELECT 'fact_uspproductivityadac', ifnull(max(fact_uspproductivityadacid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_uspproductivityadac;

INSERT INTO tmp_fact_uspproductivityadac(
fact_uspproductivityadacid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_crude_protein_titer,
ct_harvest_crude_volume,
ct_harvest_filtration_volume_recovery_buffer,
ct_harvest_hat_protein_titer,
ct_harvest_hat_volume,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT (SELECT max_id from number_fountain_fact_uspproductivityadac WHERE table_name = 'fact_uspproductivityadac') + ROW_NUMBER() over(order by '') AS fact_uspproductivityadacid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Average ' || substr(ps_date, 0, 4) as dd_ps_name,
       current_date as dd_ps_date,
       1 as dim_dateid,
       'Average ' || substr(ps_date, 0, 4) as dd_harvest_batch_id,
       ifnull(AVG(harvest_crude_protein_titer), 0) as ct_harvest_crude_protein_titer,
       ifnull(AVG(harvest_crude_volume), 0) as ct_harvest_crude_volume,
       ifnull(AVG(harvest_filtration_volume_recovery_buffer), 0) as ct_harvest_filtration_volume_recovery_buffer,
       ifnull(AVG(harvest_hat_protein_titer), 0) as ct_harvest_hat_protein_titer,
       ifnull(AVG(harvest_hat_volume), 0) as ct_harvest_hat_volume,
       ifnull(AVG(productivity_clarified), 0) as ct_productivity_clarified,
       ifnull(AVG(productivity_pbr), 0) as ct_productivity_pbr
FROM new_productivity_ada_c_crude__harvest
GROUP BY substr(ps_date, 0, 4);

UPDATE tmp_fact_uspproductivityadac tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_uspproductivityadac tmp;

UPDATE tmp_fact_uspproductivityadac tmp
SET tmp.dim_dateid = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityadac tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_ps_date;

TRUNCATE TABLE fact_uspproductivityadac;
INSERT INTO fact_uspproductivityadac(
fact_uspproductivityadacid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_crude_protein_titer,
ct_harvest_crude_volume,
ct_harvest_filtration_volume_recovery_buffer,
ct_harvest_hat_protein_titer,
ct_harvest_hat_volume,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT fact_uspproductivityadacid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_ps_name,
       dd_ps_date,
       dim_dateid,
       dd_harvest_batch_id,
       ct_harvest_crude_protein_titer,
       ct_harvest_crude_volume,
       ct_harvest_filtration_volume_recovery_buffer,
       ct_harvest_hat_protein_titer,
       ct_harvest_hat_volume,
       ct_productivity_clarified,
       ct_productivity_pbr
FROM tmp_fact_uspproductivityadac;

DROP TABLE IF EXISTS tmp_fact_uspproductivityadac;

DELETE FROM emd586.fact_uspproductivityadac;
INSERT INTO emd586.fact_uspproductivityadac SELECT * FROM fact_uspproductivityadac;