/*****************************************************************************************************************/
/*   Script         : bi_populate_trackwiseproduct_fact                                                          */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 10 Oct 2017                                                                                */
/*   Description    : Populating script of fact_trackwiseproduct                                                 */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   10 Oct 2017         CristianT      1.0          Creating the script.                                        */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS number_fountain_trackwiseproduct;
CREATE TABLE number_fountain_trackwiseproduct LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_trackwiseproduct;
CREATE TABLE tmp_fact_trackwiseproduct LIKE fact_trackwiseproduct INCLUDING DEFAULTS INCLUDING IDENTITY;


INSERT INTO number_fountain_trackwiseproduct
SELECT 'fact_trackwiseproduct', ifnull(max(fact_trackwiseproductid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_trackwiseproduct;

/* Inserting Change Control family */
INSERT INTO tmp_fact_trackwiseproduct(
fact_trackwiseproductid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pr_id,
dd_seq_no,
dd_product_name,
dd_item_code,
dd_batch_number,
dd_process_step,
dd_supplier,
dd_region
)
SELECT (SELECT max_id from number_fountain_trackwiseproduct WHERE table_name = 'fact_trackwiseproduct') + ROW_NUMBER() over(order by '') AS fact_trackwiseproductid,
       t.*
FROM (
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp1.prdin1_pr_id, 0) as dd_pr_id,
       ifnull(grp1.prdin1_seq_no, 0) as dd_seq_no,
       ifnull(grp1.prdin1_product_name,'Not Set') as dd_product_name,
       ifnull(grp1.prdin1_item_code,'Not Set') as dd_item_code,
       ifnull(grp1.prdin1_batch_number,'Not Set') as dd_batch_number,
       ifnull(grp1.prdin1_process_step,'Not Set') as dd_process_step,
       ifnull(grp1.prdin1_supplier,'Not Set') as dd_supplier,
       'Europe' as dd_region
FROM mv_xtr_grp1_rpt_prdin grp1
UNION ALL
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp2.prdin2_pr_id, 0) as dd_pr_id,
       ifnull(grp2.prdin2_seq_no, 0) as dd_seq_no,
       ifnull(grp2.prdin2_product_name,'Not Set') as dd_product_name,
       ifnull(grp2.prdin2_item_code,'Not Set') as dd_item_code,
       ifnull(grp2.prdin2_batch_number,'Not Set') as dd_batch_number,
       ifnull(grp2.prdin2_process_step,'Not Set') as dd_process_step,
       ifnull(grp2.prdin2_supplier,'Not Set') as dd_supplier,
       'China' as dd_region
FROM mv_xtr_grp2_rpt_prdin grp2
UNION ALL
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp3.prdin3_pr_id, 0) as dd_pr_id,
       ifnull(grp3.prdin3_seq_no, 0) as dd_seq_no,
       ifnull(grp3.prdin3_product_name,'Not Set') as dd_product_name,
       ifnull(grp3.prdin3_item_code,'Not Set') as dd_item_code,
       ifnull(grp3.prdin3_batch_number,'Not Set') as dd_batch_number,
       ifnull(grp3.prdin3_process_step,'Not Set') as dd_process_step,
       ifnull(grp3.prdin3_supplier,'Not Set') as dd_supplier,
       'Brazil & Uruguay' as dd_region
FROM mv_xtr_grp3_rpt_prdin grp3
UNION ALL
SELECT 1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(grp4.prdin4_pr_id, 0) as dd_pr_id,
       ifnull(grp4.prdin4_seq_no, 0) as dd_seq_no,
       ifnull(grp4.prdin4_product_name,'Not Set') as dd_product_name,
       ifnull(grp4.prdin4_item_code,'Not Set') as dd_item_code,
       ifnull(grp4.prdin4_batch_number,'Not Set') as dd_batch_number,
       ifnull(grp4.prdin4_process_step,'Not Set') as dd_process_step,
       ifnull(grp4.prdin4_supplier,'Not Set') as dd_supplier,
       'Mexico' as dd_region
FROM mv_xtr_grp4_rpt_prdin grp4
     ) t;

UPDATE tmp_fact_trackwiseproduct tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_trackwiseproduct tmp;


TRUNCATE TABLE fact_trackwiseproduct;

INSERT INTO fact_trackwiseproduct(
fact_trackwiseproductid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pr_id,
dd_seq_no,
dd_product_name,
dd_item_code,
dd_batch_number,
dd_process_step,
dd_supplier,
dd_region
)
SELECT fact_trackwiseproductid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_pr_id,
       dd_seq_no,
       dd_product_name,
       dd_item_code,
       dd_batch_number,
       dd_process_step,
       dd_supplier,
       dd_region
FROM tmp_fact_trackwiseproduct;

DROP TABLE IF EXISTS tmp_fact_trackwiseproduct;

TRUNCATE TABLE emd586.fact_trackwiseproduct;

INSERT INTO emd586.fact_trackwiseproduct
SELECT *
FROM fact_trackwiseproduct;