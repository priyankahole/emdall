

DROP TABLE IF EXISTS number_fountain_fact_ppm;
CREATE TABLE number_fountain_fact_ppm
  LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_ppm;
CREATE TABLE tmp_fact_ppm
  LIKE fact_ppm INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_ppm 
WHERE table_name = 'fact_ppm';	

INSERT INTO number_fountain_fact_ppm
SELECT 'fact_ppm', IFNULL(MAX(f.fact_ppmid),
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM tmp_fact_ppm AS f;

INSERT INTO tmp_fact_ppm(
  fact_ppmid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_subname,
  dd_min_pstng_date,
  dim_min_pstng_dateid,
  ct_sum_ppm_deb_cre_l2,
  dd_vendor,
  dd_vendor_invoice_name,
  dd_material_txtmd,
  dd_prod_description,
  dd_countryname,
  dd_regionname,
  dd_categoryname_l2,
  dd_calmonth,
  dd_calyear,
  dd_categoryname,
  dd_costcenter,
  dd_costcentername,
  dd_postxt,
  dd_material,
  dd_business_filter_description,
  dd_postxt_derived,
  dd_material_code_value,
  dd_sfdc_account_name,
  dd_sfdc_account_id
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_ppm
    WHERE table_name = 'fact_ppm') 
      + ROW_NUMBER() OVER(ORDER BY '') AS fact_ppmid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS dw_insert_date, 
  current_timestamp AS dw_update_date,
  IFNULL(ppm_subname,'Not Set') AS dd_subname,
  IFNULL(ppm_min_pstng_date,'0001-01-01') AS dd_min_pstng_date,
  1 AS dim_min_pstng_dateid,
  CAST(IFNULL(sum_ppm_deb_cre_l2,0) AS DECIMAL(18,4)) ct_sum_ppm_deb_cre_l2,
  IFNULL(ppm_vendor,'Not Set') AS dd_vendor,
  IFNULL(ppm_vendor_invoice_name,'Not Set') AS dd_vendor_invoice_name,
  IFNULL(ppm_material_txtmd,'Not Set') AS dd_material_txtmd,
  IFNULL(ppm_prod_description,'Not Set') AS dd_prod_description,
  IFNULL(ppm_countryname,'Not Set') AS dd_countryname,
  IFNULL(ppm_regionname,'Not Set') AS dd_regionname,
  IFNULL(ppm_categoryname_l2,'Not Set') AS dd_categoryname_l2,
  IFNULL(ppm_calmonth,'Not Set') AS dd_calmonth,
  IFNULL(ppm_calyear,'Not Set') AS dd_calyear,
  IFNULL(ppm_categoryname,'Not Set') AS dd_categoryname,
  IFNULL(ppm_costcenter,'Not Set') AS dd_costcenter,
  IFNULL(ppm_costcentername,'Not Set') AS dd_costcentername,
  IFNULL(ppm_postxt,'Not Set') AS dd_postxt,
  IFNULL(material,'Not Set') AS dd_material,
  IFNULL(ppm_business_filter_description,'Not Set') AS dd_business_filter_description,
  IFNULL(ppm_postxt_derived,'Not Set') AS dd_postxt_derived,
  IFNULL(material_code_value,'Not Set') AS dd_material_code_value,
  'Not Set' AS dd_sfdc_account_name,
  'Not Set' AS dd_sfdc_account_id
FROM
  ppm_spend_data;


/* BEGIN - update dim dates */
UPDATE tmp_fact_ppm AS f
SET f.dim_min_pstng_dateid = dd.dim_dateid
FROM  tmp_fact_ppm AS f, dim_date AS dd 
WHERE f.dd_min_pstng_date = dd.datevalue 
  AND dd.companycode = 'Not Set'
  AND f.dim_min_pstng_dateid <> dd.dim_dateid;
/* END - update dim dates */

/* BEGIN - update sfdc attributes */
UPDATE tmp_fact_ppm AS f
SET 
  f.dd_sfdc_account_name = t.account_name,
  f.dd_sfdc_account_id = t.account_id 
FROM tmp_fact_ppm AS f, csv_cmo_spend_name_mapping AS t
WHERE f.dd_vendor_invoice_name = t.spend_name
  AND t.spend_name IS NOT NULL;
/* END - update sfdc attributes */

DELETE FROM fact_ppm;

INSERT INTO fact_ppm(
  fact_ppmid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_subname,
  dd_min_pstng_date,
  dim_min_pstng_dateid,
  ct_sum_ppm_deb_cre_l2,
  dd_vendor,
  dd_vendor_invoice_name,
  dd_material_txtmd,
  dd_prod_description,
  dd_countryname,
  dd_regionname,
  dd_categoryname_l2,
  dd_calmonth,
  dd_calyear,
  dd_categoryname,
  dd_costcenter,
  dd_costcentername,
  dd_postxt,
  dd_material,
  dd_business_filter_description,
  dd_postxt_derived,
  dd_material_code_value,
  dd_sfdc_account_name,
  dd_sfdc_account_id
)
SELECT
  fact_ppmid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_subname,
  dd_min_pstng_date,
  dim_min_pstng_dateid,
  ct_sum_ppm_deb_cre_l2,
  dd_vendor,
  dd_vendor_invoice_name,
  dd_material_txtmd,
  dd_prod_description,
  dd_countryname,
  dd_regionname,
  dd_categoryname_l2,
  dd_calmonth,
  dd_calyear,
  dd_categoryname,
  dd_costcenter,
  dd_costcentername,
  dd_postxt,
  dd_material,
  dd_business_filter_description,
  dd_postxt_derived,
  dd_material_code_value,
  dd_sfdc_account_name,
  dd_sfdc_account_id
FROM tmp_fact_ppm;

DROP TABLE IF EXISTS tmp_fact_ppm;

DELETE FROM emd586.fact_ppm;

INSERT INTO emd586.fact_ppm
SELECT *
FROM fact_ppm;