/******************************************************************************************************************/
/*   Script         : bi_populate_joule_fact                                                                      */
/*   Author         : Cornelia                                                                                    */
/*   Created On     : 10 May 2017                                                                                 */
/*   Description    : Populating script of fact_joule                                                             */
/*********************************************Change History*******************************************************/
/*   Date                By             Version      Desc                                                         */
/*   10 May 2017         Cornelia       1.0          Creating the script.                                         */
/******************************************************************************************************************/


DROP TABLE IF EXISTS tmp_fact_joule;
CREATE TABLE tmp_fact_joule
AS
SELECT *
FROM fact_joule
WHERE 1 = 0;

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_joule';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_joule',IFNULL(MAX(fact_jouleID),0)
FROM tmp_fact_joule;

INSERT INTO tmp_fact_joule( 
FACT_JOULEID,
    DW_UPDATE_DATE,
    DW_INSERT_DATE,
    AMT_EXCHANGERATE_GBL,
    AMT_EXCHANGERATE,
    dd_time,
    ct_Electricity_Supply,
    ct_Gas_Supply,
    ct_Water_City_MSBC,
    ct_Water_City_LSC,
    ct_Water_City_WWTP,
    ct_Water_Recycled,
    ct_Water_Technique,
    ct_Water_Douches,
    ct_Water_WWTP_Rejet_SIGE,
    ct_Water_WWTP_Rejet_Veveyse,
    ct_HPW_WFI_S3_S4_5785,
    ct_HPW_WFI_S3_S4_5786,
    ct_HPW_WFI_S3_S4_5787,
    ct_RO_WFI_S1_S2_BPS_5120_FT_51_211,
    ct_RO_WFI_S1_S2_BPS_5120_FT_51_212,
    dim_projectsourceid,
    dim_datetimeid,
    ct_Sanitary_Water_0308_FT_01_22,
    ct_deltaWeekGasSupply,
    ct_deltaWeekElectricitySupply,
    ct_deltaWeekWaterSupply,
    ct_deltaMonthGasSupply,
    ct_deltaMonthElectricitySupply,
    ct_deltaMonthWaterSupply,
    dd_timelong
)
SELECT 
(select ifnull(max_id,1) 
 from number_fountain 
 where table_name = 'tmp_fact_joule') + row_number() over(order by '') AS fact_jouleID,
current_timestamp as DW_INSERT_DATE, 
current_timestamp as DW_UPDATE_DATE, 
1 as amt_exchangerate_gbl,
1 as amt_exchangerate,
ifnull(st.t_time,'Not Set') as dd_time,
ifnull(st.Electricity_Supply ,'Not Set') as ct_Electricity_Supply,
ifnull(st.Gas_Supply ,0) as ct_Gas_Supply,
ifnull(st.Water_City_MSBC ,'Not Set') as ct_Water_City_MSBC,
ifnull(st.Water_City_LSC ,'Not Set') as ct_Water_City_LSC,
ifnull(st.Water_City_WWTP ,0) as ct_Water_City_WWTP,
ifnull(st.Water_Recycled ,0) as ct_Water_Recycled,
ifnull(st.Water_Technique ,'Not Set') as ct_Water_Technique,
ifnull(st.Water_Douches ,0) as ct_Water_Douches,
ifnull(st.Water_WWTP_Rejet_SIGE ,0) as ct_Water_WWTP_Rejet_SIGE,
ifnull(st.Water_WWTP_Rejet_Veveyse ,0) as ct_Water_WWTP_Rejet_Veveyse,
ifnull(st.HPW_WFI_S3_S4_5785 ,0) as ct_HPW_WFI_S3_S4_5785,
ifnull(st.HPW_WFI_S3_S4_5786 ,0) as ct_HPW_WFI_S3_S4_5786,
ifnull(st.HPW_WFI_S3_S4_5787 ,0) as ct_HPW_WFI_S3_S4_5787,
ifnull(st.RO_WFI_S1_S2_BPS_5120_FT_51_211 ,0) as ct_RO_WFI_S1_S2_BPS_5120_FT_51_211,
ifnull(st.RO_WFI_S1_S2_BPS_5120_FT_51_212 ,0) as ct_RO_WFI_S1_S2_BPS_5120_FT_51_212,
1 as dim_projectsourceid,
1 as dim_datetimeid,
ifnull(st.Sanitary_Water_0308_FT_01_22 ,0) as ct_Sanitary_Water_0308_FT_01_22,
0 as ct_deltaWeekGasSupply,
0 as ct_deltaWeekElectricitySupply,
0 as ct_deltaWeekWaterSupply,
0 as ct_deltaMonthGasSupply,
0 as ct_deltaMonthElectricitySupply,
0 as ct_deltaMonthWaterSupply,
ifnull(st.t_time,'Not Set') as dd_timelong
FROM  t_em_joule_data st;

update tmp_fact_joule
set dd_time = MID(dd_time,7,4) ||'-'|| MID(dd_time,4,2)  ||'-'|| left(dd_time,2);

update tmp_fact_joule
set dd_timelong = MID(dd_timelong,7,4) ||'-'|| MID(dd_timelong,4,2)  ||'-'|| left(dd_timelong,2) || right(dd_timelong,9);

UPDATE tmp_fact_joule F
SET F.dim_datetimeid = D.DIM_DATEID
FROM  tmp_fact_joule F, DIM_DATE D 
WHERE f.dd_time = D.datevalue 
      AND d.CompanyCode  = 'Not Set'
      AND F.dim_datetimeid  <> D.DIM_DATEID;

/* 8 June 2017 CristianB Start: update ct_gas_supply with the Gas Conversion Factor */

update tmp_fact_joule as f
set f.ct_gas_supply = f.ct_gas_supply * c.conversionfactor
from tmp_fact_joule as f, csv_gasconversionfactor as c 
where concat(substr(f.dd_time,6,2),'-',substr(f.dd_time,1,4)) = c.monthyear
	and f.ct_gas_supply <> f.ct_gas_supply * c.conversionfactor;

/* 8 June 2017 CristianB End: update ct_gas_supply with the Gas Conversion Factor */



/* 5 July 2017 CristianB Start: update the delta week and month for gas, water and electricity */

/* week calculation */

DROP TABLE IF EXISTS tmp_deltaWeek;
CREATE TABLE tmp_deltaWeek
AS
select 
   calendarweekyr, 
   datevalue,
   cast('Not Set' as varchar(30)) as datevaluelong,
   cast(0 as decimal(18,4)) as thisWeekGasSupply,
   cast(0 as decimal(18,4)) as deltaWeekGasSupply,
   cast(0 as decimal(18,4)) as thisWeekElectricitySupply,
   cast(0 as decimal(18,4)) as deltaWeekElectricitySupply,
   cast(0 as decimal(18,4)) as thisWeekWaterSupply,
   cast(0 as decimal(18,4)) as deltaWeekWaterSupply,
   ROW_NUMBER() OVER(ORDER BY d.calendarweekyr asc) as rowNumber
from dim_date as d
where datevalue = (select min(datevalue) from dim_date as dd where d.calendarweekyr = dd.calendarweekyr and dd.companycode = 'Not Set')
   and d.companycode = 'Not Set';

update tmp_deltaWeek as tmp
set tmp.datevaluelong = j.minim
from tmp_deltaWeek as tmp
inner join (select dd_time, min(dd_timelong) as minim 
              from tmp_fact_joule
              group by dd_time) as j
on tmp.datevalue = j.dd_time;

update tmp_deltaWeek as tmp
set tmp.thisweekgassupply = j.ct_gas_supply,
    tmp.thisWeekElectricitySupply = j.ct_electricity_supply,
    tmp.thisWeekWaterSupply = j.ct_Water_City_MSBC + j.ct_Water_City_LSC + j.ct_water_city_wwtp
from tmp_deltaWeek as tmp, tmp_fact_joule as j
where tmp.datevaluelong = j.dd_timelong;

update tmp_deltaWeek as t
set t.deltaWeekGasSupply = tt.thisWeekGasSupply - t.thisWeekGasSupply,
    t.deltaWeekElectricitySupply = tt.thisWeekElectricitySupply - t.thisWeekElectricitySupply,
    t.deltaWeekWaterSupply = tt.thisWeekWaterSupply - t.thisWeekWaterSupply 
from tmp_deltaWeek t, tmp_deltaWeek tt
where t.rowNumber = tt.rowNumber - 1;

/* eliminate the last value, because it will be very small as it substracts 0 from a big number,
and 0 is corect because it substracts from future */

update tmp_deltaWeek as t
set t.deltaWeekGasSupply = 0,
    t.deltaWeekElectricitySupply = 0,
    t.deltaWeekWaterSupply = 0 
from tmp_deltaWeek t
where t.datevaluelong = 
  (select max(datevaluelong)
   from tmp_deltaWeek
   where datevaluelong <> 'Not Set')
;

update tmp_fact_joule as fj
set fj.ct_deltaWeekGasSupply = tmp.deltaWeekGasSupply,
    fj.ct_deltaWeekElectricitySupply = tmp.deltaWeekElectricitySupply,
    fj.ct_deltaWeekWaterSupply = tmp.deltaWeekWaterSupply
from tmp_fact_joule as fj,
    dim_date as dd,
    tmp_deltaWeek as tmp
where fj.dim_datetimeid = dd.dim_dateid
    and dd.calendarweekyr = tmp.calendarweekyr
    and dd.companycode = 'Not Set';

/* month calculation */
	
DROP TABLE IF EXISTS tmp_deltaMonth;
CREATE TABLE tmp_deltaMonth
AS
select 
   calendarmonthid, 
   datevalue,
   cast('Not Set' as varchar(30)) as datevaluelong,
   cast(0 as decimal(18,4)) as thisMonthGasSupply,
   cast(0 as decimal(18,4)) as deltaMonthGasSupply,
   cast(0 as decimal(18,4)) as thisMonthElectricitySupply,
   cast(0 as decimal(18,4)) as deltaMonthElectricitySupply,
   cast(0 as decimal(18,4)) as thisMonthWaterSupply,
   cast(0 as decimal(18,4)) as deltaMonthWaterSupply,
   ROW_NUMBER() OVER(ORDER BY d.calendarmonthid asc) as rowNumber
from dim_date as d
where datevalue = (select min(datevalue) from dim_date as dd where d.calendarmonthid = dd.calendarmonthid and dd.companycode = 'Not Set')
   and d.companycode = 'Not Set';	

update tmp_deltaMonth as tmp
set tmp.datevaluelong = j.minim
from tmp_deltaMonth as tmp
inner join (select dd_time, min(dd_timelong) as minim 
              from tmp_fact_joule
              group by dd_time) as j
on tmp.datevalue = j.dd_time;

update tmp_deltaMonth as tmp
set tmp.thismonthgassupply = j.ct_gas_supply,
    tmp.thismonthElectricitySupply = j.ct_electricity_supply,
    tmp.thismonthWaterSupply = j.ct_Water_City_MSBC + j.ct_Water_City_LSC + j.ct_water_city_wwtp
from tmp_deltaMonth as tmp, tmp_fact_joule as j
where tmp.datevaluelong = j.dd_timelong;

update tmp_deltaMonth as t
set t.deltamonthgassupply = tt.thismonthgassupply - t.thismonthgassupply,
    t.deltamonthElectricitySupply = tt.thismonthElectricitySupply - t.thismonthElectricitySupply,
    t.deltamonthWaterSupply = tt.thismonthWaterSupply - t.thismonthWaterSupply 
from tmp_deltaMonth t, tmp_deltaMonth tt
where t.rowNumber = tt.rowNumber - 1;

/* eliminate the last value, because it will be very small as it substracts 0 from a big number,
and 0 is corect because it substracts from future */

update tmp_deltaMonth as t
set t.deltamonthgassupply = 0,
    t.deltamonthElectricitySupply = 0,
    t.deltamonthWaterSupply = 0 
from tmp_deltaMonth t
where t.datevaluelong = (select max(datevaluelong)
   from tmp_deltaMonth
   where datevaluelong <> 'Not Set')
;

update tmp_fact_joule as fj
set fj.ct_deltaMonthGasSupply = tmp.deltaMonthGasSupply,
    fj.ct_deltaMonthElectricitySupply = tmp.deltaMonthElectricitySupply,
    fj.ct_deltaMonthWaterSupply = tmp.deltaMonthWaterSupply
from tmp_fact_joule as fj,
    dim_date as dd,
    tmp_deltaMonth as tmp
where fj.dim_datetimeid = dd.dim_dateid
    and dd.calendarmonthid = tmp.calendarmonthid
    and dd.companycode = 'Not Set';

/* 5 July 2017 CristianB End: update the delta week and month for gas, water and electricity */

UPDATE tmp_fact_joule tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_joule tmp;

DELETE FROM fact_joule;

INSERT INTO fact_joule(
FACT_JOULEID,
    DW_UPDATE_DATE,
    DW_INSERT_DATE,
    AMT_EXCHANGERATE_GBL,
    AMT_EXCHANGERATE,
    dd_time,
    ct_Electricity_Supply,
    ct_Gas_Supply,
    ct_Water_City_MSBC,
    ct_Water_City_LSC,
    ct_Water_City_WWTP,
    ct_Water_Recycled,
    ct_Water_Technique,
    ct_Water_Douches,
    ct_Water_WWTP_Rejet_SIGE,
    ct_Water_WWTP_Rejet_Veveyse,
    ct_HPW_WFI_S3_S4_5785,
    ct_HPW_WFI_S3_S4_5786,
    ct_HPW_WFI_S3_S4_5787,
    ct_RO_WFI_S1_S2_BPS_5120_FT_51_211,
    ct_RO_WFI_S1_S2_BPS_5120_FT_51_212,
    dim_projectsourceid,
    dim_datetimeid,
    ct_Sanitary_Water_0308_FT_01_22,
    ct_deltaWeekGasSupply,
    ct_deltaWeekElectricitySupply,
    ct_deltaWeekWaterSupply,
    ct_deltaMonthGasSupply,
    ct_deltaMonthElectricitySupply,
    ct_deltaMonthWaterSupply,
    dd_timelong
)
SELECT FACT_JOULEID,
    DW_UPDATE_DATE,
    DW_INSERT_DATE,
    AMT_EXCHANGERATE_GBL,
    AMT_EXCHANGERATE,
    dd_time,
    ct_Electricity_Supply,
    ct_Gas_Supply,
    ct_Water_City_MSBC,
    ct_Water_City_LSC,
    ct_Water_City_WWTP,
    ct_Water_Recycled,
    ct_Water_Technique,
    ct_Water_Douches,
    ct_Water_WWTP_Rejet_SIGE,
    ct_Water_WWTP_Rejet_Veveyse,
    ct_HPW_WFI_S3_S4_5785,
    ct_HPW_WFI_S3_S4_5786,
    ct_HPW_WFI_S3_S4_5787,
    ct_RO_WFI_S1_S2_BPS_5120_FT_51_211,
    ct_RO_WFI_S1_S2_BPS_5120_FT_51_212,
    dim_projectsourceid,
    dim_datetimeid,
    ct_Sanitary_Water_0308_FT_01_22,
    ct_deltaWeekGasSupply,
    ct_deltaWeekElectricitySupply,
    ct_deltaWeekWaterSupply,
    ct_deltaMonthGasSupply,
    ct_deltaMonthElectricitySupply,
    ct_deltaMonthWaterSupply,
    dd_timelong
FROM TMP_FACT_joule;

DROP TABLE IF EXISTS tmp_fact_joule;

DELETE FROM emd586.fact_joule;

INSERT INTO emd586.fact_joule
SELECT *
FROM fact_joule;