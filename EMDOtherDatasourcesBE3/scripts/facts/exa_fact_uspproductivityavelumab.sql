/*****************************************************************************************************************/
/*   Script         : exa_fact_uspproductivityavelumab                                                           */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 18 Dec 2018                                                                                */
/*   Description    : Populating script of fact_uspproductivityavelumab                                          */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   18 Dec 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_uspproductivityavelumab;
CREATE TABLE tmp_fact_uspproductivityavelumab
AS
SELECT *
FROM fact_uspproductivityavelumab
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_uspproductivityavelumab;
CREATE TABLE number_fountain_fact_uspproductivityavelumab LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_uspproductivityavelumab
SELECT 'fact_uspproductivityavelumab', ifnull(max(fact_uspproductivityavelumabid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_uspproductivityavelumab;


INSERT INTO tmp_fact_uspproductivityavelumab(
fact_uspproductivityavelumabid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_centrifugation_discharge_timer,
ct_harvest_centrifugation_duration,
dd_harvest_centrifugation_end_dt,
dim_dateid_centrifugation_end,
dd_harvest_centrifugation_equipment_id,
dd_harvest_centrifugation_start_dt,
dim_dateid_centrifugation_start,
ct_harvest_crude_pa_hplc,
ct_harvest_crude_solid_fraction,
ct_harvest_crude_turbidity,
ct_harvest_crude_volume,
dd_harvest_event_ccp_id,
dd_harvest_event_ccp_id_event,
dd_harvest_event_comment,
dd_harvest_event_comment_event,
dd_harvest_event_dev_id,
dd_harvest_event_dev_id_event,
ct_harvest_filtration_duration,
dd_harvest_filtration_end_dt,
dim_dateid_filtration_end,
dd_harvest_filtration_start_dt,
dim_dateid_filtration_start,
dd_harvest_hat_cfu,
ct_harvest_hat_ct,
ct_harvest_hat_dna,
dd_harvest_hat_dna_text_value,
ct_harvest_hat_filtrated_volume,
ct_harvest_hat_hcp,
dd_harvest_hat_hcp_text_value,
dd_harvest_hat_insulin,
dd_harvest_hat_insulin_text_value,
ct_harvest_hat_lal,
dd_harvest_hat_lal_text_value,
ct_harvest_hat_pa_hplc,
ct_harvest_hat_ph,
ct_harvest_hat_turbidity,
ct_harvest_hat_weight,
ct_harvest_hat_yield,
dd_harvest_run_id,
dd_harvest_start_dt,
dim_dateid_harvest_start,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT (SELECT max_id from number_fountain_fact_uspproductivityavelumab WHERE table_name = 'fact_uspproductivityavelumab') + ROW_NUMBER() over(order by '') AS fact_uspproductivityavelumabid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(ps_name, 'Not Set') as dd_ps_name,
       ifnull(ps_date, '0001-01-01') as dd_ps_date,
       1 as dim_dateid,
       ifnull(harvest_batch_id, 'Not Set') as dd_harvest_batch_id,
       ifnull(harvest_centrifugation_discharge_timer, 0) as ct_harvest_centrifugation_discharge_timer,
       ifnull(harvest_centrifugation_duration, 0) as ct_harvest_centrifugation_duration,
       ifnull(harvest_centrifugation_end_dt, '0001-01-01') as dd_harvest_centrifugation_end_dt,
       1 as dim_dateid_centrifugation_end,
       ifnull(harvest_centrifugation_equipment_id, 'Not Set') as dd_harvest_centrifugation_equipment_id,
       ifnull(harvest_centrifugation_start_dt, '0001-01-01') as dd_harvest_centrifugation_start_dt,
       1 as dim_dateid_centrifugation_start,
       ifnull(harvest_crude_pa_hplc, 0) as ct_harvest_crude_pa_hplc,
       ifnull(harvest_crude_solid_fraction, 0) as ct_harvest_crude_solid_fraction,
       ifnull(harvest_crude_turbidity, 0) as ct_harvest_crude_turbidity,
       ifnull(harvest_crude_volume, 0) as ct_harvest_crude_volume,
       ifnull(harvest_event_ccp_id, 'Not Set') as dd_harvest_event_ccp_id,
       ifnull(harvest_event_ccp_id_event, 'Not Set') as dd_harvest_event_ccp_id_event,
       ifnull(harvest_event_comment, 'Not Set') as dd_harvest_event_comment,
       ifnull(harvest_event_comment_event, 'Not Set') as dd_harvest_event_comment_event,
       ifnull(harvest_event_dev_id, 'Not Set') as dd_harvest_event_dev_id,
       ifnull(harvest_event_dev_id_event, 'Not Set') as dd_harvest_event_dev_id_event,
       ifnull(harvest_filtration_duration, 0) as ct_harvest_filtration_duration,
       ifnull(harvest_filtration_end_dt, '0001-01-01') as dd_harvest_filtration_end_dt,
       1 as dim_dateid_filtration_end,
       ifnull(harvest_filtration_start_dt, '0001-01-01') as dd_harvest_filtration_start_dt,
       1 as dim_dateid_filtration_start,
       ifnull(harvest_hat_cfu, 'Not Set') as dd_harvest_hat_cfu,
       ifnull(harvest_hat_ct, 0) as ct_harvest_hat_ct,
       ifnull(harvest_hat_dna, 0) as ct_harvest_hat_dna,
       ifnull(harvest_hat_dna_text_value, 'Not Set') as dd_harvest_hat_dna_text_value,
       ifnull(harvest_hat_filtrated_volume, 0) as ct_harvest_hat_filtrated_volume,
       ifnull(harvest_hat_hcp, 0) as ct_harvest_hat_hcp,
       ifnull(harvest_hat_hcp_text_value, 'Not Set') as dd_harvest_hat_hcp_text_value,
       ifnull(harvest_hat_insulin, 'Not Set') as dd_harvest_hat_insulin,
       ifnull(harvest_hat_insulin_text_value, 'Not Set') as dd_harvest_hat_insulin_text_value,
       ifnull(harvest_hat_lal, 0) as ct_harvest_hat_lal,
       ifnull(harvest_hat_lal_text_value, 'Not Set') as dd_harvest_hat_lal_text_value,
       ifnull(harvest_hat_pa_hplc, 0) as ct_harvest_hat_pa_hplc,
       ifnull(harvest_hat_ph, 0) as ct_harvest_hat_ph,
       ifnull(harvest_hat_turbidity, 0) as ct_harvest_hat_turbidity,
       ifnull(harvest_hat_weight, 0)  ct_harvest_hat_weight,
       ifnull(harvest_hat_yield, 0) as ct_harvest_hat_yield,
       ifnull(harvest_run_id, 'Not Set') as dd_harvest_run_id,
       ifnull(harvest_start_dt, '0001-01-01') as dd_harvest_start_dt,
       1 as dim_dateid_harvest_start,
       ifnull(productivity_clarified, 0) as ct_productivity_clarified,
       ifnull(productivity_pbr, 0) as ct_productivity_pbr
FROM new_productivity_avelumab_harvest;

/* Calculating the AVG of previous years */
DELETE FROM number_fountain_fact_uspproductivityavelumab WHERE table_name = 'fact_uspproductivityavelumab';

INSERT INTO number_fountain_fact_uspproductivityavelumab
SELECT 'fact_uspproductivityavelumab', ifnull(max(fact_uspproductivityavelumabid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_uspproductivityavelumab;

INSERT INTO tmp_fact_uspproductivityavelumab(
fact_uspproductivityavelumabid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_centrifugation_discharge_timer,
ct_harvest_centrifugation_duration,
dd_harvest_centrifugation_end_dt,
dim_dateid_centrifugation_end,
dd_harvest_centrifugation_equipment_id,
dd_harvest_centrifugation_start_dt,
dim_dateid_centrifugation_start,
ct_harvest_crude_pa_hplc,
ct_harvest_crude_solid_fraction,
ct_harvest_crude_turbidity,
ct_harvest_crude_volume,
dd_harvest_event_ccp_id,
dd_harvest_event_ccp_id_event,
dd_harvest_event_comment,
dd_harvest_event_comment_event,
dd_harvest_event_dev_id,
dd_harvest_event_dev_id_event,
ct_harvest_filtration_duration,
dd_harvest_filtration_end_dt,
dim_dateid_filtration_end,
dd_harvest_filtration_start_dt,
dim_dateid_filtration_start,
dd_harvest_hat_cfu,
ct_harvest_hat_ct,
ct_harvest_hat_dna,
dd_harvest_hat_dna_text_value,
ct_harvest_hat_filtrated_volume,
ct_harvest_hat_hcp,
dd_harvest_hat_hcp_text_value,
dd_harvest_hat_insulin,
dd_harvest_hat_insulin_text_value,
ct_harvest_hat_lal,
dd_harvest_hat_lal_text_value,
ct_harvest_hat_pa_hplc,
ct_harvest_hat_ph,
ct_harvest_hat_turbidity,
ct_harvest_hat_weight,
ct_harvest_hat_yield,
dd_harvest_run_id,
dd_harvest_start_dt,
dim_dateid_harvest_start,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT (SELECT max_id from number_fountain_fact_uspproductivityavelumab WHERE table_name = 'fact_uspproductivityavelumab') + ROW_NUMBER() over(order by '') AS fact_uspproductivityavelumabid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       'Average ' || substr(ps_date, 0, 4) as dd_ps_name,
       current_date as dd_ps_date,
       1 as dim_dateid,
       'Average ' || substr(ps_date, 0, 4) as dd_harvest_batch_id,
       ifnull(AVG(harvest_centrifugation_discharge_timer), 0) as ct_harvest_centrifugation_discharge_timer,
       ifnull(AVG(harvest_centrifugation_duration), 0) as ct_harvest_centrifugation_duration,
       current_date as dd_harvest_centrifugation_end_dt,
       1 as dim_dateid_centrifugation_end,
       'Not Set' as dd_harvest_centrifugation_equipment_id,
       current_date as dd_harvest_centrifugation_start_dt,
       1 as dim_dateid_centrifugation_start,
       ifnull(AVG(harvest_crude_pa_hplc), 0) as ct_harvest_crude_pa_hplc,
       ifnull(AVG(harvest_crude_solid_fraction), 0) as ct_harvest_crude_solid_fraction,
       ifnull(AVG(harvest_crude_turbidity), 0) as ct_harvest_crude_turbidity,
       ifnull(AVG(harvest_crude_volume), 0) as ct_harvest_crude_volume,
       'Not Set' as dd_harvest_event_ccp_id,
       'Not Set' as dd_harvest_event_ccp_id_event,
       'Not Set' as dd_harvest_event_comment,
       'Not Set' as dd_harvest_event_comment_event,
       'Not Set' as dd_harvest_event_dev_id,
       'Not Set' as dd_harvest_event_dev_id_event,
       ifnull(AVG(harvest_filtration_duration), 0) as ct_harvest_filtration_duration,
       current_date as dd_harvest_filtration_end_dt,
       1 as dim_dateid_filtration_end,
       current_date as dd_harvest_filtration_start_dt,
       1 as dim_dateid_filtration_start,
       'Not Set' as dd_harvest_hat_cfu,
       ifnull(AVG(harvest_hat_ct), 0) as ct_harvest_hat_ct,
       ifnull(AVG(harvest_hat_dna), 0) as ct_harvest_hat_dna,
       'Not Set' as dd_harvest_hat_dna_text_value,
       ifnull(AVG(harvest_hat_filtrated_volume), 0) as ct_harvest_hat_filtrated_volume,
       ifnull(AVG(harvest_hat_hcp), 0) as ct_harvest_hat_hcp,
       'Not Set' as dd_harvest_hat_hcp_text_value,
       'Not Set' as dd_harvest_hat_insulin,
       'Not Set' as dd_harvest_hat_insulin_text_value,
       ifnull(AVG(harvest_hat_lal), 0) as ct_harvest_hat_lal,
       'Not Set' as dd_harvest_hat_lal_text_value,
       ifnull(AVG(harvest_hat_pa_hplc), 0) as ct_harvest_hat_pa_hplc,
       ifnull(AVG(harvest_hat_ph), 0) as ct_harvest_hat_ph,
       ifnull(AVG(harvest_hat_turbidity), 0) as ct_harvest_hat_turbidity,
       ifnull(AVG(harvest_hat_weight), 0)  ct_harvest_hat_weight,
       ifnull(AVG(harvest_hat_yield), 0) as ct_harvest_hat_yield,
       'Not Set' as dd_harvest_run_id,
       current_date as dd_harvest_start_dt,
       1 as dim_dateid_harvest_start,
       ifnull(AVG(productivity_clarified), 0) as ct_productivity_clarified,
       ifnull(AVG(productivity_pbr), 0) as ct_productivity_pbr
FROM new_productivity_avelumab_harvest
GROUP BY substr(ps_date, 0, 4);

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_uspproductivityavelumab tmp;

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_dateid = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_ps_date;

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_dateid_centrifugation_end = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_harvest_centrifugation_end_dt;

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_dateid_centrifugation_start = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_harvest_centrifugation_start_dt;

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_dateid_filtration_end = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_harvest_filtration_end_dt;

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_dateid_filtration_start = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_harvest_filtration_start_dt;

UPDATE tmp_fact_uspproductivityavelumab tmp
SET tmp.dim_dateid_harvest_start = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_uspproductivityavelumab tmp
WHERE dt.companycode = 'Not Set'
      AND dt.datevalue = tmp.dd_harvest_start_dt;

TRUNCATE TABLE fact_uspproductivityavelumab;
INSERT INTO fact_uspproductivityavelumab(
fact_uspproductivityavelumabid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_ps_name,
dd_ps_date,
dim_dateid,
dd_harvest_batch_id,
ct_harvest_centrifugation_discharge_timer,
ct_harvest_centrifugation_duration,
dd_harvest_centrifugation_end_dt,
dim_dateid_centrifugation_end,
dd_harvest_centrifugation_equipment_id,
dd_harvest_centrifugation_start_dt,
dim_dateid_centrifugation_start,
ct_harvest_crude_pa_hplc,
ct_harvest_crude_solid_fraction,
ct_harvest_crude_turbidity,
ct_harvest_crude_volume,
dd_harvest_event_ccp_id,
dd_harvest_event_ccp_id_event,
dd_harvest_event_comment,
dd_harvest_event_comment_event,
dd_harvest_event_dev_id,
dd_harvest_event_dev_id_event,
ct_harvest_filtration_duration,
dd_harvest_filtration_end_dt,
dim_dateid_filtration_end,
dd_harvest_filtration_start_dt,
dim_dateid_filtration_start,
dd_harvest_hat_cfu,
ct_harvest_hat_ct,
ct_harvest_hat_dna,
dd_harvest_hat_dna_text_value,
ct_harvest_hat_filtrated_volume,
ct_harvest_hat_hcp,
dd_harvest_hat_hcp_text_value,
dd_harvest_hat_insulin,
dd_harvest_hat_insulin_text_value,
ct_harvest_hat_lal,
dd_harvest_hat_lal_text_value,
ct_harvest_hat_pa_hplc,
ct_harvest_hat_ph,
ct_harvest_hat_turbidity,
ct_harvest_hat_weight,
ct_harvest_hat_yield,
dd_harvest_run_id,
dd_harvest_start_dt,
dim_dateid_harvest_start,
ct_productivity_clarified,
ct_productivity_pbr
)
SELECT fact_uspproductivityavelumabid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_ps_name,
       dd_ps_date,
       dim_dateid,
       dd_harvest_batch_id,
       ct_harvest_centrifugation_discharge_timer,
       ct_harvest_centrifugation_duration,
       dd_harvest_centrifugation_end_dt,
       dim_dateid_centrifugation_end,
       dd_harvest_centrifugation_equipment_id,
       dd_harvest_centrifugation_start_dt,
       dim_dateid_centrifugation_start,
       ct_harvest_crude_pa_hplc,
       ct_harvest_crude_solid_fraction,
       ct_harvest_crude_turbidity,
       ct_harvest_crude_volume,
       dd_harvest_event_ccp_id,
       dd_harvest_event_ccp_id_event,
       dd_harvest_event_comment,
       dd_harvest_event_comment_event,
       dd_harvest_event_dev_id,
       dd_harvest_event_dev_id_event,
       ct_harvest_filtration_duration,
       dd_harvest_filtration_end_dt,
       dim_dateid_filtration_end,
       dd_harvest_filtration_start_dt,
       dim_dateid_filtration_start,
       dd_harvest_hat_cfu,
       ct_harvest_hat_ct,
       ct_harvest_hat_dna,
       dd_harvest_hat_dna_text_value,
       ct_harvest_hat_filtrated_volume,
       ct_harvest_hat_hcp,
       dd_harvest_hat_hcp_text_value,
       dd_harvest_hat_insulin,
       dd_harvest_hat_insulin_text_value,
       ct_harvest_hat_lal,
       dd_harvest_hat_lal_text_value,
       ct_harvest_hat_pa_hplc,
       ct_harvest_hat_ph,
       ct_harvest_hat_turbidity,
       ct_harvest_hat_weight,
       ct_harvest_hat_yield,
       dd_harvest_run_id,
       dd_harvest_start_dt,
       dim_dateid_harvest_start,
       ct_productivity_clarified,
       ct_productivity_pbr
FROM tmp_fact_uspproductivityavelumab;

DROP TABLE IF EXISTS tmp_fact_uspproductivityavelumab;

DELETE FROM emd586.fact_uspproductivityavelumab;
INSERT INTO emd586.fact_uspproductivityavelumab SELECT * FROM fact_uspproductivityavelumab;