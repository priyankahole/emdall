/*****************************************************************************************************************/
/*   Script         : exa_fact_cmoperformancemanagement                                                          */
/*   Author         : Cristian T                                                                                 */
/*   Created On     : 17 Oct 2018                                                                                */
/*   Description    : Populating script of fact_cmoperformancemanagement                                         */
/*********************************************Change History******************************************************/
/*   Date                By             Version      Desc                                                        */
/*   17 Oct 2018         CristianT      1.0          Creating the script                                         */
/*****************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_cmoperformancemanagement;
CREATE TABLE tmp_fact_cmoperformancemanagement
AS
SELECT *
FROM fact_cmoperformancemanagement
WHERE 1 = 0;

DROP TABLE IF EXISTS number_fountain_fact_cmoperformancemanagement;
CREATE TABLE number_fountain_fact_cmoperformancemanagement LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO number_fountain_fact_cmoperformancemanagement
SELECT 'fact_cmoperformancemanagement', ifnull(max(fact_cmoperformancemanagementid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_cmoperformancemanagement;


INSERT INTO tmp_fact_cmoperformancemanagement(
fact_cmoperformancemanagementid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pm_id,
dd_pm_name,
dd_pm_merck_entity,
dd_pm_domain,
dd_pm_period,
dd_pm_year,
dd_pm_month,
dim_dateid,
dd_pm_report_frequency,
dd_pm_kpi_guideline_id,
dd_pm_kpi_guideline_name,
ct_kpi_value,
dd_pm_kpi_n_a_flag,
dd_pm_kpi_cmo_target_id,
dd_pm_kpi_cmo_target_name,
ct_kpi_cmo_target_green_value,
ct_kpi_cmo_target_yellow_value,
dd_pm_cmo_id,
dd_pm_cmo_name,
dd_pm_cmo_type,
dd_pm_cmo_merck_biopharma_flag,
dd_pm_cmo_status_mb,
dd_pm_cmo_mb_regions,
dd_pm_cmo_mb_level,
dd_pm_comments,
dd_bottom4cmo_otif,
dd_bottom4cmo_rft,
ct_kpi_value_prv_month,
dim_cmoaccountsid,
ct_sales_impact,
ct_weighted_sales_impact,
dd_pc_comment_cmo,
dd_pc_comment_region,
dd_bottom4cmo_inv
)
SELECT (SELECT max_id from number_fountain_fact_cmoperformancemanagement WHERE table_name = 'fact_cmoperformancemanagement') + ROW_NUMBER() over(order by '') AS fact_cmoperformancemanagementid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       ifnull(pm_id, 'Not Set') as dd_pm_id,
       ifnull(pm_name, 'Not Set') as dd_pm_name,
       ifnull(pm_merck_entity, 'Not Set') as dd_pm_merck_entity,
       ifnull(pm_domain, 'Not Set') as dd_pm_domain,
       ifnull(pm_period, 'Not Set') as dd_pm_period,
       ifnull(pm_year, '0') as dd_pm_year,
       ifnull(pm_month, 0) as dd_pm_month,
       1 as dim_dateid,
       ifnull(pm_report_frequency, 'Not Set') as dd_pm_report_frequency,
       ifnull(pm_kpi_guideline_id, 'Not Set') as dd_pm_kpi_guideline_id,
       ifnull(pm_kpi_guideline_name, 'Not Set') as dd_pm_kpi_guideline_name,
       ifnull(pm_kpi_value, 0) as ct_kpi_value,
       ifnull(pm_kpi_n_a_flag, 'Not Set') as dd_pm_kpi_n_a_flag,
       ifnull(pm_kpi_cmo_target_id, 'Not Set') as dd_pm_kpi_cmo_target_id,
       ifnull(pm_kpi_cmo_target_name, 'Not Set') as dd_pm_kpi_cmo_target_name,
       ifnull(pm_kpi_cmo_target_green_value, 0) as ct_kpi_cmo_target_green_value,
       ifnull(pm_kpi_cmo_target_yellow_value, 0) as ct_kpi_cmo_target_yellow_value,
       ifnull(pm_cmo_id, 'Not Set') as dd_pm_cmo_id,
       ifnull(pm_cmo_name, 'Not Set') as dd_pm_cmo_name,
       ifnull(pm_cmo_type, 'Not Set') as dd_pm_cmo_type,
       ifnull(pm_cmo_merck_biopharma_flag, 'Not Set') as dd_pm_cmo_merck_biopharma_flag,
       ifnull(pm_cmo_status_mb, 'Not Set') as dd_pm_cmo_status_mb,
       ifnull(pm_cmo_mb_regions, 'Not Set') as dd_pm_cmo_mb_regions,
       ifnull(pm_cmo_mb_level, 'Not Set') as dd_pm_cmo_mb_level,
       ifnull(pm_comments, 'Not Set') as dd_pm_comments,
       'Not Set' as dd_bottom4cmo_otif,
       'Not Set' as dd_bottom4cmo_rft,
       0 as ct_kpi_value_prv_month,
       1 as dim_cmoaccountsid,
       ifnull(sales_impact, 0) as ct_sales_impact,
       ifnull(weighted_sales_impact, 0) as ct_weighted_sales_impact,
       'Not Set' as dd_pc_comment_cmo,
       'Not Set' as dd_pc_comment_region,
       'Not Set' as dd_bottom4cmo_inv
FROM performance_monitoring cmo;

UPDATE tmp_fact_cmoperformancemanagement tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_cmoperformancemanagement tmp;

UPDATE tmp_fact_cmoperformancemanagement tmp
SET tmp.dim_cmoaccountsid = ifnull(dim.dim_cmoaccountsid, 1)
FROM dim_cmoaccounts dim,
     tmp_fact_cmoperformancemanagement tmp
WHERE dim.cmo_id = tmp.dd_pm_cmo_id;

DROP TABLE IF EXISTS tmp_cmo_mim_dateid;
CREATE TABLE tmp_cmo_mim_dateid
AS
SELECT min(dt.dim_dateid) as dim_dateid,
       dt.calendaryear,
       dt.calendarmonthnumber
FROM dim_date dt,
     tmp_fact_cmoperformancemanagement tmp
WHERE dt.companycode = 'Not Set'
      AND dt.calendaryear = tmp.dd_pm_year
      AND dt.calendarmonthnumber = tmp.dd_pm_month
GROUP BY dt.calendaryear,
         dt.calendarmonthnumber;

UPDATE tmp_fact_cmoperformancemanagement tmp
SET dim_dateid = ifnull(dt.dim_dateid, 1)
FROM tmp_fact_cmoperformancemanagement tmp,
     tmp_cmo_mim_dateid dt
WHERE tmp.dd_pm_year = dt.calendaryear
      AND tmp.dd_pm_month = dt.calendarmonthnumber;

DROP TABLE IF EXISTS tmp_cmo_mim_dateid;

/* Identify bottom 4 CMO */
DROP TABLE IF EXISTS tmp_bottom4_cmo;
CREATE TABLE tmp_bottom4_cmo
AS
SELECT dim.cmo_mb_regions,
       tmp.dd_pm_kpi_guideline_name,
       tmp.dd_pm_cmo_name,
       avg(tmp.ct_kpi_value) as avg_kpi,
       case when sum(tmp.ct_weighted_sales_impact) = 0 then 0 when sum(tmp.ct_sales_impact) = 0 then 0 else sum(tmp.ct_weighted_sales_impact) / sum(tmp.ct_sales_impact) end as Weighted_KPI_Value,
       row_number() over(partition by dim.cmo_mb_regions, tmp.dd_pm_kpi_guideline_name
                         order by case when sum(tmp.ct_weighted_sales_impact) = 0 then 0 when sum(tmp.ct_sales_impact) = 0 then 0 else sum(tmp.ct_weighted_sales_impact) / sum(tmp.ct_sales_impact) end asc,
                                  tmp.dd_pm_cmo_name) as ro_no
FROM tmp_fact_cmoperformancemanagement tmp,
     dim_date dt,
     dim_cmoaccounts dim
WHERE 1 = 1
      AND tmp.dim_cmoaccountsid = dim.dim_cmoaccountsid
      AND tmp.dim_dateid = dt.dim_dateid
      AND dt.datevalue between (trunc(current_date, 'month') - INTERVAL '3' MONTH) AND (trunc(current_date, 'month') - INTERVAL '1' DAY)
      AND tmp.dd_pm_kpi_guideline_name in ('CMO OTIF', 'QA19-Right First Time batch disposition', 'QA19-Investigation Closure on Time (30 days)')
      AND tmp.dd_pm_cmo_mb_level in (1, 2)
GROUP BY dim.cmo_mb_regions,
         tmp.dd_pm_kpi_guideline_name,
         tmp.dd_pm_cmo_name;

DELETE FROM tmp_bottom4_cmo WHERE ro_no > 4;

UPDATE tmp_fact_cmoperformancemanagement tmp
SET dd_bottom4cmo_otif =  'X'
FROM tmp_fact_cmoperformancemanagement tmp,
     tmp_bottom4_cmo cmo
WHERE tmp.DD_PM_CMO_NAME = cmo.DD_PM_CMO_NAME
      AND tmp.DD_PM_KPI_GUIDELINE_NAME = cmo.DD_PM_KPI_GUIDELINE_NAME
      AND cmo.DD_PM_KPI_GUIDELINE_NAME = 'CMO OTIF';
      
UPDATE tmp_fact_cmoperformancemanagement tmp
SET dd_bottom4cmo_rft =  'X'
FROM tmp_fact_cmoperformancemanagement tmp,
     tmp_bottom4_cmo cmo
WHERE tmp.DD_PM_CMO_NAME = cmo.DD_PM_CMO_NAME
      AND tmp.DD_PM_KPI_GUIDELINE_NAME = cmo.DD_PM_KPI_GUIDELINE_NAME
      AND cmo.DD_PM_KPI_GUIDELINE_NAME = 'QA19-Right First Time batch disposition';

UPDATE tmp_fact_cmoperformancemanagement tmp
SET dd_bottom4cmo_inv =  'X'
FROM tmp_fact_cmoperformancemanagement tmp,
     tmp_bottom4_cmo cmo
WHERE tmp.DD_PM_CMO_NAME = cmo.DD_PM_CMO_NAME
      AND tmp.DD_PM_KPI_GUIDELINE_NAME = cmo.DD_PM_KPI_GUIDELINE_NAME
      AND cmo.DD_PM_KPI_GUIDELINE_NAME = 'QA19-Investigation Closure on Time (30 days)';

DROP TABLE IF EXISTS tmp_bottom4_cmo;

/* Previous month KPI value to be used in the measure logic of KPI Value vs Last month */
DROP TABLE IF exists tmp_cmo_kpi_value_prv_month;
CREATE TABLE tmp_cmo_kpi_value_prv_month
AS
SELECT dd_pm_kpi_guideline_name,
       dd_pm_cmo_name,
       dd_pm_cmo_mb_regions,
       dt.datevalue,
       avg(ct_kpi_value) as ct_kpi_value_prv_month
FROM tmp_fact_cmoperformancemanagement tmp,
     dim_date dt
WHERE tmp.dim_dateid = dt.dim_dateid
      AND dt.datevalue BETWEEN (trunc(current_date, 'month') - INTERVAL '2' MONTH) AND (trunc(current_date, 'month') - INTERVAL '1' MONTH)-1
GROUP BY dd_pm_kpi_guideline_name,
         dd_pm_cmo_name,
         dd_pm_cmo_mb_regions,
         dt.datevalue;

UPDATE tmp_fact_cmoperformancemanagement tmp
SET ct_kpi_value_prv_month = ifnull(prv.ct_kpi_value_prv_month, 0)
FROM tmp_fact_cmoperformancemanagement tmp,
     dim_date dt,
     tmp_cmo_kpi_value_prv_month prv
WHERE tmp.dim_dateid = dt.dim_dateid
      AND dt.datevalue = (trunc(current_date, 'month') - INTERVAL '1' MONTH)
      AND tmp.dd_pm_kpi_guideline_name = prv.dd_pm_kpi_guideline_name
      AND tmp.dd_pm_cmo_name = prv.dd_pm_cmo_name
      AND tmp.dd_pm_cmo_mb_regions = prv.dd_pm_cmo_mb_regions;

DROP TABLE IF exists tmp_cmo_kpi_value_prv_month;

/* Performance Comment CMO level */
UPDATE tmp_fact_cmoperformancemanagement tmp
SET dd_pc_comment_cmo = ifnull(pc_comment, 'Not Set')
FROM tmp_fact_cmoperformancemanagement tmp,
     performance_comments com
WHERE tmp.dd_pm_cmo_id = com.pc_cmo_id
      AND com.pc_cmo_id is not null
      AND tmp.dd_pm_year = com.pc_year
      AND tmp.dd_pm_period = com.pc_month;

/* Performance Comment Region level */
UPDATE tmp_fact_cmoperformancemanagement tmp
SET dd_pc_comment_region = ifnull(pc_comment, 'Not Set')
FROM tmp_fact_cmoperformancemanagement tmp,
     performance_comments com
WHERE tmp.dd_pm_cmo_mb_regions = com.pc_mb_regions
      AND com.pc_mb_regions is not null
      AND com.pc_cmo_id is null
      AND tmp.dd_pm_year = com.pc_year
      AND tmp.dd_pm_period = com.pc_month;


TRUNCATE TABLE fact_cmoperformancemanagement;
INSERT INTO fact_cmoperformancemanagement(
fact_cmoperformancemanagementid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
dd_pm_id,
dd_pm_name,
dd_pm_merck_entity,
dd_pm_domain,
dd_pm_period,
dd_pm_year,
dd_pm_month,
dim_dateid,
dd_pm_report_frequency,
dd_pm_kpi_guideline_id,
dd_pm_kpi_guideline_name,
ct_kpi_value,
dd_pm_kpi_n_a_flag,
dd_pm_kpi_cmo_target_id,
dd_pm_kpi_cmo_target_name,
ct_kpi_cmo_target_green_value,
ct_kpi_cmo_target_yellow_value,
dd_pm_cmo_id,
dd_pm_cmo_name,
dd_pm_cmo_type,
dd_pm_cmo_merck_biopharma_flag,
dd_pm_cmo_status_mb,
dd_pm_cmo_mb_regions,
dd_pm_cmo_mb_level,
dd_pm_comments,
dd_bottom4cmo_otif,
dd_bottom4cmo_rft,
ct_kpi_value_prv_month,
dim_cmoaccountsid,
ct_sales_impact,
ct_weighted_sales_impact,
dd_pc_comment_cmo,
dd_pc_comment_region,
dd_bottom4cmo_inv
)
SELECT fact_cmoperformancemanagementid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       dd_pm_id,
       dd_pm_name,
       dd_pm_merck_entity,
       dd_pm_domain,
       dd_pm_period,
       dd_pm_year,
       dd_pm_month,
       dim_dateid,
       dd_pm_report_frequency,
       dd_pm_kpi_guideline_id,
       dd_pm_kpi_guideline_name,
       ct_kpi_value,
       dd_pm_kpi_n_a_flag,
       dd_pm_kpi_cmo_target_id,
       dd_pm_kpi_cmo_target_name,
       ct_kpi_cmo_target_green_value,
       ct_kpi_cmo_target_yellow_value,
       dd_pm_cmo_id,
       dd_pm_cmo_name,
       dd_pm_cmo_type,
       dd_pm_cmo_merck_biopharma_flag,
       dd_pm_cmo_status_mb,
       dd_pm_cmo_mb_regions,
       dd_pm_cmo_mb_level,
       dd_pm_comments,
       dd_bottom4cmo_otif,
       dd_bottom4cmo_rft,
       ct_kpi_value_prv_month,
       dim_cmoaccountsid,
       ct_sales_impact,
       ct_weighted_sales_impact,
       dd_pc_comment_cmo,
       dd_pc_comment_region,
       dd_bottom4cmo_inv
FROM tmp_fact_cmoperformancemanagement;

DROP TABLE IF EXISTS tmp_fact_cmoperformancemanagement;

DELETE FROM emd586.fact_cmoperformancemanagement;
INSERT INTO emd586.fact_cmoperformancemanagement SELECT * FROM fact_cmoperformancemanagement;