
DROP TABLE IF EXISTS tmp_fact_cmo;
CREATE TABLE tmp_fact_cmo
AS
SELECT *
FROM fact_cmo
WHERE 1 = 0;

/* begin - create a table with all the data we need from JDA for this project */

DROP TABLE IF EXISTS tmp_dataFromJDAstepOne;
CREATE TABLE tmp_dataFromJDAstepOne
AS
SELECT 
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('Consensus Forecast EURO') THEN ct_packs else 0 END AS ct_type_consensus_forecast_euro_packs,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('Consensus Forecast EURO') THEN ct_units else 0 END AS ct_type_consensus_forecast_euro_units,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('Consensus Forecast QTY') THEN ct_packs else 0 END AS ct_type_consensus_forecast_qty_packs,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('Consensus Forecast QTY') THEN ct_units else 0 END AS ct_type_consensus_forecast_qty_units,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('OPERATING PLAN') THEN ct_packs else 0 END AS ct_type_op_plan_qty_packs,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('OPERATING PLAN') THEN ct_units else 0 END AS ct_type_op_plan_qty_units,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('OPERATING PLAN EURO') THEN ct_packs else 0 END AS ct_type_op_plan_euro_packs,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('OPERATING PLAN EURO') THEN ct_units else 0 END AS ct_type_op_plan_euro_units,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('OP CONSENSUS FORECAST FROZEN (IN EURO)') THEN ct_packs else 0 END AS ct_type_consensus_forecast_frozen_euro_packs,
  CASE WHEN UPPER(TRIM(dd_type)) = UPPER('OP CONSENSUS FORECAST FROZEN (IN QTY)') THEN ct_packs else 0 END AS ct_type_consensus_forecast_frozen_qty_packs
FROM fact_jda_demandforecast
WHERE UPPER(TRIM(dd_type)) IN 
  ('CONSENSUS FORECAST EURO',
  'CONSENSUS FORECAST QTY',
  'OPERATING PLAN',
  'OPERATING PLAN EURO',
  'OP CONSENSUS FORECAST FROZEN (IN EURO)',
  'OP CONSENSUS FORECAST FROZEN (IN QTY)');


DROP TABLE IF EXISTS tmp_dataFromJDA;
CREATE TABLE tmp_dataFromJDA
AS
SELECT 
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  SUM(ct_type_consensus_forecast_euro_packs) AS ct_type_consensus_forecast_euro_packs,
  SUM(ct_type_consensus_forecast_euro_units) AS ct_type_consensus_forecast_euro_units,
  SUM(ct_type_consensus_forecast_qty_packs) AS ct_type_consensus_forecast_qty_packs,
  SUM(ct_type_consensus_forecast_qty_units) AS ct_type_consensus_forecast_qty_units,
  SUM(ct_type_op_plan_qty_packs) AS ct_type_op_plan_qty_packs,
  SUM(ct_type_op_plan_qty_units) AS ct_type_op_plan_qty_units,
  SUM(ct_type_op_plan_euro_packs) AS ct_type_op_plan_euro_packs,
  SUM(ct_type_op_plan_euro_units) AS ct_type_op_plan_euro_units,
  SUM(ct_type_consensus_forecast_frozen_euro_packs) AS ct_type_consensus_forecast_frozen_euro_packs,
  SUM(ct_type_consensus_forecast_frozen_qty_packs) AS ct_type_consensus_forecast_frozen_qty_packs
FROM tmp_dataFromJDAstepOne
GROUP BY 
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate
;

/* end - create a table with all the data we need from JDA for this project */

/* begin - format the csv_SFDC table */

DROP TABLE IF EXISTS tmp_csv_SFDC;
CREATE TABLE tmp_csv_SFDC
AS
SELECT *
FROM csv_SFDC;

UPDATE tmp_csv_SFDC
SET productbrand = 'Not Set'
WHERE productbrand is NULL;

/* end - format the csv_SFDC table */


/* begin - fixing the issue with multiple lines in SFDC Production file */

DROP TABLE IF EXISTS tmp_eliminateCommaFromSFDC;
CREATE TABLE tmp_eliminateCommaFromSFDC
AS
SELECT 
  IFNULL(accountname,'Not Set') AS accountname,
  IFNULL(accountid,'Not Set') AS accountid,
  IFNULL([year],'Not Set') AS [year],
  IFNULL([month],'Not Set') AS [month],
  IFNULL(productiontype,'Not Set') AS productiontype,
  IFNULL(scenario,'Not Set') AS scenario, 
  CASE WHEN [value] = '-' THEN '0' WHEN [value] IS NULL THEN '0' ELSE REPLACE([value],',','') END AS [value],
  IFNULL(productbrand,'Not Set') AS productbrand
FROM csv_SFDC_production
WHERE UPPER(productiontype) = 'PACKS'
  AND UPPER(globalyearlyvalue) = 'FALSE' ;

DROP TABLE IF EXISTS tmp_csv_SFDC_production;
CREATE TABLE tmp_csv_SFDC_production
AS
SELECT 
  accountname,
  accountid,
  [year],
  [month],
  productiontype,
  scenario,
  SUM(cast([value] AS DECIMAL(18,4))) AS [value],
  productbrand
FROM tmp_eliminateCommaFromSFDC
GROUP BY 
  accountname,
  accountid,
  [year],
  [month],
  productiontype,
  scenario,
  productbrand
;

DROP TABLE IF EXISTS stepOneSFDC;
CREATE TABLE stepOneSFDC
AS
SELECT 
  accountname,
  accountid,
  [year],
  [month],
  productiontype,
  CASE WHEN UPPER(scenario) ='PRODUCTION' THEN [value] ELSE 0 END AS ct_scenario_production,
  CASE WHEN UPPER(scenario) ='OPERATING PLAN' THEN [value] ELSE 0 END AS ct_scenario_operating_plan,
  CASE WHEN UPPER(scenario) ='FORECAST' THEN [value] ELSE 0 END AS ct_scenario_forecast,
  CASE WHEN UPPER(scenario) ='CAPACITY' THEN [value] ELSE 0 END AS ct_scenario_capacity,
  productbrand
FROM tmp_csv_SFDC_production
;

DROP TABLE IF EXISTS tmp_csv_SFDC_production_final;
CREATE TABLE tmp_csv_SFDC_production_final
AS
SELECT 
  accountname,
  accountid,
  [year],
  [month],
  productiontype,
  SUM(ct_scenario_production) AS ct_scenario_production,
  SUM(ct_scenario_operating_plan) AS ct_scenario_operating_plan,
  SUM(ct_scenario_forecast) AS ct_scenario_forecast,
  SUM(ct_scenario_capacity) AS ct_scenario_capacity,
  productbrand
FROM stepOneSFDC
GROUP BY
  accountname,
  accountid,
  [year],
  [month],
  productiontype,
  productbrand
;

/* end - fixing the issue with multiple lines in SFDC Production file */


/* BEGIN - create SFDC dummy data for combination of all Accounts, last year, current year
and next year, all months and brands that come from the SFDC mapping */

/* first create dummy data */

DROP TABLE IF EXISTS tmp_dummyDataCsvSFDC;
CREATE TABLE tmp_dummyDataCsvSFDC
AS
SELECT *
FROM tmp_csv_SFDC_production_final
WHERE 1 = 0;

INSERT INTO tmp_dummyDataCsvSFDC(
  accountname,                
  accountid,                  
  [year],                     
  [month],                    
  productiontype,             
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,       
  ct_scenario_capacity,       
  productbrand
)
WITH generateMonths AS
(
  SELECT DISTINCT monthabbreviation FROM dim_date
    WHERE companycode = 'Not Set'
      AND monthabbreviation <> 'Not Set'
), 
generateYears AS
(
  SELECT * 
  FROM (
    VALUES YEAR(CURRENT_DATE),
    YEAR(CURRENT_DATE) - 1,
    YEAR(CURRENT_DATE) + 1
  ) AS X(genYear)
)
SELECT 
  IFNULL(accountname,'Not Set') AS accountname,
  IFNULL(accountid,'Not Set') AS accountid,
  gY.genYear AS [Year], --year
  gM.monthabbreviation AS [Month], --month
  'Packs' AS Productiontype, -- dummy rows only for Packs
  0 AS CT_SCENARIO_PRODUCTION,
  0 AS CT_SCENARIO_OPERATING_PLAN,
  0 AS CT_SCENARIO_FORECAST,       
  0 AS CT_SCENARIO_CAPACITY,
  IFNULL(productbrand,'Not Set') AS productbrand
FROM 
  tmp_csv_SFDC AS c, generateMonths AS gM, generateYears AS gY
;

/* second add dummy data to the rest of the data */

DROP TABLE IF EXISTS tmp_csv_SFDC_production_with_dummydata;
CREATE TABLE tmp_csv_SFDC_production_with_dummydata
AS
SELECT *
FROM tmp_csv_SFDC_production_final
WHERE 1 = 0;

INSERT INTO tmp_dummyDataCsvSFDC
(
  accountname,                
  accountid,                  
  [year],                     
  [month],                    
  productiontype,             
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,       
  ct_scenario_capacity,       
  productbrand
)
SELECT 
  accountname,                
  accountid,                  
  [year],                     
  [month],                    
  productiontype,             
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,       
  ct_scenario_capacity,       
  productbrand
FROM 
  tmp_csv_SFDC_production_final
;

/* finally insert data into the final table */

INSERT INTO tmp_csv_SFDC_production_with_dummydata
(
  accountname,                
  accountid,                  
  [year],                     
  [month],                    
  productiontype,             
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,       
  ct_scenario_capacity,       
  productbrand
)
SELECT 
  accountname,                
  accountid,                  
  [year],                     
  [month],                    
  productiontype,             
  SUM(ct_scenario_production),
  SUM(ct_scenario_operating_plan),
  SUM(ct_scenario_forecast),       
  SUM(ct_scenario_capacity),       
  productbrand
FROM 
  tmp_dummyDataCsvSFDC
GROUP BY
  accountname,                
  accountid,                  
  [year],                     
  [month],                    
  productiontype,                
  productbrand
;

/* END - create SFDC dummy data for combination of all Accounts, last year, current year
and next year, all months and brands that come from the SFDC mapping */


/* BEGIN - update the account id to the first 15 characters */

UPDATE tmp_csv_SFDC
SET accountid = LEFT(accountid, 15);

UPDATE tmp_csv_SFDC_production_with_dummydata
SET accountid = LEFT(accountid, 15);

/* END - update the account id to the first 15 characters */


/* BEGIN - only have the values FROM SFDC for a particular list */

UPDATE tmp_csv_SFDC_production_with_dummydata
SET ct_scenario_capacity = 0,
  ct_scenario_forecast = 0,
  ct_scenario_operating_plan = 0,
  ct_scenario_production = 0
WHERE productbrand NOT IN ('Glucophage IR','Glucophage XR','Glucovance',
                           'Concor','Euthyrox','Cetrotide','Crinone');

/* END - only have the values FROM SFDC for a particular list */


/* CASE 1 - INSERT SFDC DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Packer and Supplier' AS dd_packersupplier,
  1 AS dim_jda_locationid, -- jda
  1 AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  0 AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  0 AS ct_type_consensus_forecast_frozen_qty_packs,
  0 AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  0 AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  IFNULL(sp.[year],'Not Set') AS dd_year,
  IFNULL(sp.[month],'Not Set') AS dd_month,
  IFNULL(sp.productionType,'Not Set') AS dd_productionType,
  IFNULL(sp.ct_scenario_production,0) AS ct_scenario_production,
  IFNULL(sp.ct_scenario_operating_plan,0) AS ct_scenario_operating_plan,
  IFNULL(sp.ct_scenario_forecast,0) AS ct_scenario_forecast,
  IFNULL(sp.ct_scenario_capacity,0) AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  csv_jda_gbu_sfdc_businessfranchise AS mfr, 
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_date AS dd,
  tmp_csv_SFDC AS s
LEFT JOIN tmp_csv_SFDC_production_with_dummydata AS sp ON sp.accountid = s.accountid
  AND UPPER(sp.productbrand) = UPPER(s.productbrand)
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)  -- map business franchise
  AND UPPER(mfr.sfdc_business_franchise) = UPPER(s.businessfranchises)  -- map business franchise
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND jp.SupplierCode = s.accountid              -- map packer/supplier -- condition 1
  AND jp.PackerCode = s.accountid                -- map packer/supplier -- condition 1
  AND LENGTH(s.accountid) <> 3                   -- map packer/supplier  -- condition 1
  AND t.dim_dateidstartdate = dd.dim_dateid      -- map year and month
  AND dd.calendaryear = sp.[year]                -- map year and month
  AND UPPER(dd.monthabbreviation) = UPPER(sp.[month])          -- map year and month
  AND dd.companycode = 'Not Set'                 -- map year and month               
  AND t.dim_jda_productid = jp.dim_jda_productid)
;

/* CASE 1 - INSERT JDA DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Packer and Supplier' AS dd_packersupplier,
  t.dim_jda_locationid AS dim_jda_locationid, -- jda
  t.dim_jda_productid AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  t.ct_type_consensus_forecast_euro_packs AS ct_type_consensus_forecast_euro_packs,
  t.ct_type_consensus_forecast_euro_units AS ct_type_consensus_forecast_euro_units,
  t.ct_type_consensus_forecast_qty_packs AS ct_type_consensus_forecast_qty_packs,
  t.ct_type_consensus_forecast_qty_units AS ct_type_consensus_forecast_qty_units,
  t.ct_type_consensus_forecast_frozen_qty_packs AS ct_type_consensus_forecast_frozen_qty_packs,
  t.ct_type_consensus_forecast_frozen_euro_packs AS ct_type_consensus_forecast_frozen_euro_packs,
  t.ct_type_op_plan_qty_packs AS ct_type_op_plan_qty_packs,
  t.ct_type_op_plan_qty_units AS ct_type_op_plan_qty_units,
  t.ct_type_op_plan_euro_packs AS ct_type_op_plan_euro_packs,
  t.ct_type_op_plan_euro_units AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  IFNULL(sp.[year],'Not Set') AS dd_year,
  IFNULL(sp.[month],'Not Set') AS dd_month,
  IFNULL(sp.productionType,'Not Set') AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  csv_jda_gbu_sfdc_businessfranchise AS mfr,
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_date AS dd,
  tmp_csv_SFDC AS s
LEFT JOIN tmp_csv_SFDC_production_with_dummydata AS sp ON sp.accountid = s.accountid
  AND UPPER(sp.productbrand) = UPPER(s.productbrand)
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)  -- map business franchise
  AND UPPER(mfr.sfdc_business_franchise) = UPPER(s.businessfranchises)  -- map business franchise
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND jp.SupplierCode = s.accountid -- map packer/supplier -- condition 1
  AND jp.PackerCode = s.accountid -- map packer/supplier  -- condition 1
  AND LENGTH(s.accountid) <> 3                    -- map packer/supplier  -- condition 1
  AND t.dim_dateidstartdate = dd.dim_dateid      -- map year and month
  AND dd.calendaryear = sp.[year]                -- map year and month
  AND UPPER(dd.monthabbreviation) = UPPER(sp.[month])          -- map year and month
  AND dd.companycode = 'Not Set'                 -- map year and month               
  AND t.dim_jda_productid = jp.dim_jda_productid)
;

/* CASE 1 - INSERT LICENSING OUT DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Packer and Supplier' AS dd_packersupplier,
  t.dim_jda_locationid AS dim_jda_locationid, -- jda
  t.dim_jda_productid AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  t.ct_type_consensus_forecast_euro_packs AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  t.ct_type_consensus_forecast_frozen_qty_packs AS ct_type_consensus_forecast_frozen_qty_packs,
  t.ct_type_consensus_forecast_frozen_euro_packs AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  t.ct_type_op_plan_euro_packs AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(cl.country,'Not Set') AS dd_countrysfdc,
  'Not Set' AS dd_mbregions,
  'Not Set' AS dd_merckentity,
  'Not Set' AS dd_typeofscope,
  IFNULL(jp.branddescr,'Not Set') AS dd_productbrand,
  IFNULL(mfr.sfdc_business_franchise,'Not Set') AS dd_businessfranchises,
  'Not Set' AS dd_productGalenicForm,
  'Not Set' AS dd_servicePerformed,
  IFNULL(dd.calendaryear,'Not Set') AS dd_year,
  IFNULL(dd.monthabbreviation,'Not Set') AS dd_month,
  'Packs' AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM
  csv_jda_gbu_sfdc_businessfranchise AS mfr,
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_jda_location AS jl,
  dim_date AS dd,
  csv_licensing_out AS cl,
  tmp_csv_SFDC AS s
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)
  AND left(cl.packercode,15) = s.accountid
  AND left(cl.SupplierCode,15) = s.accountid  
  AND cl.SKU = jp.itemglobalcode                    
  AND t.dim_jda_productid = jp.dim_jda_productid   
  AND t.dim_dateidstartdate = dd.dim_dateid       
  AND t.dim_jda_locationid = jl.dim_jda_locationid 
  AND UPPER(jl.country) = UPPER(cl.country)
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
);


/* CASE 2 - INSERT SFDC DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;


INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Supplier' AS dd_packersupplier,
  1 AS dim_jda_locationid, -- jda
  1 AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  0 AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  0 AS ct_type_consensus_forecast_frozen_qty_packs,
  0 AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  0 AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  IFNULL(sp.[year],'Not Set') AS dd_year,
  IFNULL(sp.[month],'Not Set') AS dd_month,
  IFNULL(sp.productionType,'Not Set') AS dd_productionType,
  IFNULL(sp.ct_scenario_production,0) AS ct_scenario_production,
  IFNULL(sp.ct_scenario_operating_plan,0) AS ct_scenario_operating_plan,
  IFNULL(sp.ct_scenario_forecast,0) AS ct_scenario_forecast,
  IFNULL(sp.ct_scenario_capacity,0) AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  csv_jda_gbu_sfdc_businessfranchise AS mfr, 
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_date AS dd,
  tmp_csv_SFDC AS s
LEFT JOIN tmp_csv_SFDC_production_with_dummydata AS sp ON sp.accountid = s.accountid
  AND UPPER(sp.productbrand) = UPPER(s.productbrand)
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)  -- map business franchise
  AND UPPER(mfr.sfdc_business_franchise) = UPPER(s.businessfranchises)  -- map business franchise
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND jp.SupplierCode = s.accountid            -- map packer/supplier -- condition 2
  AND (jp.PackerCode = 'Not Set' OR length(jp.PackerCode) = 3)  -- map packer/supplier -- condition 2
  AND t.dim_dateidstartdate = dd.dim_dateid      -- map year and month
  AND dd.calendaryear = sp.[year]                -- map year and month
  AND UPPER(dd.monthabbreviation) = UPPER(sp.[month])          -- map year and month
  AND dd.companycode = 'Not Set'                 -- map year and month               
  AND t.dim_jda_productid = jp.dim_jda_productid
  AND (dim_jda_locationid,jp.dim_jda_productid, dim_dateidstartdate,UPPER(s.accountname),s.accountid,UPPER(s.primarycountry),UPPER(s.mbregions),UPPER(s.merckentity),UPPER(s.typeofscope),
          UPPER(s.productbrand),UPPER(s.businessfranchises),UPPER(s.productGalenicForm),UPPER(s.servicePerformed),
          sp.[year],UPPER(sp.[month]),UPPER(sp.productionType)) NOT IN 
      (SELECT DISTINCT dim_jda_locationid,dim_jda_productid, dim_dateidstartdate,UPPER(dd_accountname),dd_accountid,UPPER(dd_countrysfdc),UPPER(dd_mbregions),UPPER(dd_merckentity),UPPER(dd_typeofscope),
          UPPER(dd_productbrand),UPPER(dd_businessfranchises),UPPER(dd_productGalenicForm),UPPER(dd_servicePerformed),
          dd_year,UPPER(dd_month),UPPER(dd_productionType) FROM tmp_fact_cmo WHERE UPPER(dd_packersupplier) = 'PACKER AND SUPPLIER')
  AND NOT (jp.SupplierCode = s.accountid                          -- map packer/supplier -- condition to not count
  AND s.accountid != jp.PackerCode                                -- map packer/supplier -- condition to not count
  AND (jp.PackerCode <> 'Not Set' OR length(jp.PackerCode) != 3))  -- map packer/supplier -- condition to not count
)
;

/* CASE 2 - INSERT JDA DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Supplier' AS dd_packersupplier,
  t.dim_jda_locationid AS dim_jda_locationid, -- jda
  t.dim_jda_productid AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  t.ct_type_consensus_forecast_euro_packs AS ct_type_consensus_forecast_euro_packs,
  t.ct_type_consensus_forecast_euro_units AS ct_type_consensus_forecast_euro_units,
  t.ct_type_consensus_forecast_qty_packs AS ct_type_consensus_forecast_qty_packs,
  t.ct_type_consensus_forecast_qty_units AS ct_type_consensus_forecast_qty_units,
  t.ct_type_consensus_forecast_frozen_qty_packs AS ct_type_consensus_forecast_frozen_qty_packs,
  t.ct_type_consensus_forecast_frozen_euro_packs AS ct_type_consensus_forecast_frozen_euro_packs,
  t.ct_type_op_plan_qty_packs AS ct_type_op_plan_qty_packs,
  t.ct_type_op_plan_qty_units AS ct_type_op_plan_qty_units,
  t.ct_type_op_plan_euro_packs AS ct_type_op_plan_euro_packs,
  t.ct_type_op_plan_euro_units AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  IFNULL(sp.[year],'Not Set') AS dd_year,
  IFNULL(sp.[month],'Not Set') AS dd_month,
  IFNULL(sp.productionType,'Not Set') AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  csv_jda_gbu_sfdc_businessfranchise AS mfr,
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_date AS dd,
  tmp_csv_SFDC AS s
LEFT JOIN tmp_csv_SFDC_production_with_dummydata AS sp ON sp.accountid = s.accountid
  AND UPPER(sp.productbrand) = UPPER(s.productbrand)
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)  -- map business franchise
  AND UPPER(mfr.sfdc_business_franchise) = UPPER(s.businessfranchises)  -- map business franchise
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND jp.SupplierCode = s.accountid            -- map packer/supplier -- condition 2
  AND (jp.PackerCode = 'Not Set' OR length(jp.PackerCode) = 3)  -- map packer/supplier -- condition 2
  AND t.dim_dateidstartdate = dd.dim_dateid      -- map year and month
  AND dd.calendaryear = sp.[year]                -- map year and month
  AND UPPER(dd.monthabbreviation) = UPPER(sp.[month])          -- map year and month
  AND dd.companycode = 'Not Set'                 -- map year and month               
  AND t.dim_jda_productid = jp.dim_jda_productid
  AND (dim_jda_locationid,jp.dim_jda_productid, dim_dateidstartdate,UPPER(s.accountname),s.accountid,UPPER(s.primarycountry),UPPER(s.mbregions),UPPER(s.merckentity),UPPER(s.typeofscope),
          UPPER(s.productbrand),UPPER(s.businessfranchises),UPPER(s.productGalenicForm),UPPER(s.servicePerformed),
          sp.[year],UPPER(sp.[month]),UPPER(sp.productionType)) NOT IN 
      (SELECT DISTINCT dim_jda_locationid,dim_jda_productid, dim_dateidstartdate,UPPER(dd_accountname),dd_accountid,UPPER(dd_countrysfdc),UPPER(dd_mbregions),UPPER(dd_merckentity),UPPER(dd_typeofscope),
          UPPER(dd_productbrand),UPPER(dd_businessfranchises),UPPER(dd_productGalenicForm),UPPER(dd_servicePerformed),
          dd_year,UPPER(dd_month),UPPER(dd_productionType) FROM tmp_fact_cmo WHERE UPPER(dd_packersupplier) = 'PACKER AND SUPPLIER')
  AND NOT (jp.SupplierCode = s.accountid                          -- map packer/supplier -- condition to not count
  AND s.accountid != jp.PackerCode                                -- map packer/supplier -- condition to not count
  AND (jp.PackerCode IS NOT NULL OR LENGTH(jp.PackerCode) != 3))  -- map packer/supplier -- condition to not count
)
;

/* CASE 2 - INSERT LICENSING OUT DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Supplier' AS dd_packersupplier,
  t.dim_jda_locationid AS dim_jda_locationid, -- jda
  t.dim_jda_productid AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  t.ct_type_consensus_forecast_euro_packs AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  t.ct_type_consensus_forecast_frozen_qty_packs AS ct_type_consensus_forecast_frozen_qty_packs,
  t.ct_type_consensus_forecast_frozen_euro_packs AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  t.ct_type_op_plan_euro_packs AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(cl.country,'Not Set') AS dd_countrysfdc,
  'Not Set' AS dd_mbregions,
  'Not Set' AS dd_merckentity,
  'Not Set' AS dd_typeofscope,
  IFNULL(jp.branddescr,'Not Set') AS dd_productbrand,
  IFNULL(mfr.sfdc_business_franchise,'Not Set') AS dd_businessfranchises,
  'Not Set' AS dd_productGalenicForm,
  'Not Set' AS dd_servicePerformed,
  IFNULL(dd.calendaryear,'Not Set') AS dd_year,
  IFNULL(dd.monthabbreviation,'Not Set') AS dd_month,
  'Packs' AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM
  csv_jda_gbu_sfdc_businessfranchise AS mfr,
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_jda_location AS jl,
  dim_date AS dd,
  csv_licensing_out AS cl,
  tmp_csv_SFDC AS s
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)
  AND left(cl.SupplierCode,15) = s.accountid    -- map packer/supplier -- condition 2
  AND (left(cl.PackerCode,15) = 'Not Set' OR length(left(cl.PackerCode,15)) = 3)  -- map packer/supplier -- condition 2                      
  AND t.dim_jda_productid = jp.dim_jda_productid   
  AND t.dim_dateidstartdate = dd.dim_dateid       
  AND t.dim_jda_locationid = jl.dim_jda_locationid 
  AND UPPER(jl.country) = UPPER(cl.country)
  AND cl.SKU = jp.itemglobalcode
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND (t.dim_jda_locationid,jp.dim_jda_productid, t.dim_dateidstartdate,UPPER(s.accountname),s.accountid,UPPER(s.primarycountry),UPPER(s.mbregions),UPPER(s.merckentity),UPPER(s.typeofscope),
          UPPER(s.productbrand),UPPER(s.businessfranchises),UPPER(s.productGalenicForm),UPPER(s.servicePerformed),
          dd.calendaryear,UPPER(dd.monthabbreviation),UPPER('Packs')) NOT IN 
      (SELECT DISTINCT dim_jda_locationid,dim_jda_productid, dim_dateidstartdate,UPPER(dd_accountname),dd_accountid,UPPER(dd_countrysfdc),UPPER(dd_mbregions),UPPER(dd_merckentity),UPPER(dd_typeofscope),
          UPPER(dd_productbrand),UPPER(dd_businessfranchises),UPPER(dd_productGalenicForm),UPPER(dd_servicePerformed),
          dd_year,UPPER(dd_month),UPPER(dd_productionType) FROM tmp_fact_cmo WHERE UPPER(dd_packersupplier) = 'PACKER AND SUPPLIER')
  AND NOT (left(cl.SupplierCode,15) = s.accountid                          -- map packer/supplier -- condition to not count
  AND s.accountid != left(cl.PackerCode,15)                                -- map packer/supplier -- condition to not count
  AND (left(cl.PackerCode,15) IS NOT NULL OR length(left(cl.PackerCode,15)) != 3))  -- map packer/supplier -- condition to not count
)
;


/* CASE 3 - INSERT SFDC DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Packer' AS dd_packersupplier,
  1 AS dim_jda_locationid, -- jda
  1 AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  0 AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  0 AS ct_type_consensus_forecast_frozen_qty_packs,
  0 AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  0 AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  IFNULL(sp.[year],'Not Set') AS dd_year,
  IFNULL(sp.[month],'Not Set') AS dd_month,
  IFNULL(sp.productionType,'Not Set') AS dd_productionType,
  IFNULL(sp.ct_scenario_production,0) AS ct_scenario_production,
  IFNULL(sp.ct_scenario_operating_plan,0) AS ct_scenario_operating_plan,
  IFNULL(sp.ct_scenario_forecast,0) AS ct_scenario_forecast,
  IFNULL(sp.ct_scenario_capacity,0) AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  csv_jda_gbu_sfdc_businessfranchise AS mfr, 
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_date AS dd,
  tmp_csv_SFDC AS s
LEFT JOIN tmp_csv_SFDC_production_with_dummydata AS sp ON sp.accountid = s.accountid
  AND UPPER(sp.productbrand) = UPPER(s.productbrand)
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)  -- map business franchise
  AND UPPER(mfr.sfdc_business_franchise) = UPPER(s.businessfranchises)  -- map business franchise
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND jp.PackerCode = s.accountid           -- map packer/supplier  -- condition 3
  AND (jp.SupplierCode <> s.accountid       -- map packer/supplier  -- condition 3
            OR jp.SupplierCode = 'Not Set'                -- map packer/supplier  -- condition 3
			OR length(jp.SupplierCode) = 3)               -- map packer/supplier  -- condition 3
  AND t.dim_dateidstartdate = dd.dim_dateid      -- map year and month
  AND dd.calendaryear = sp.[year]                -- map year and month
  AND UPPER(dd.monthabbreviation) = UPPER(sp.[month])          -- map year and month
  AND dd.companycode = 'Not Set'                 -- map year and month               
  AND t.dim_jda_productid = jp.dim_jda_productid
  AND (dim_jda_locationid,jp.dim_jda_productid, dim_dateidstartdate,UPPER(s.accountname),s.accountid,UPPER(s.primarycountry),UPPER(s.mbregions),UPPER(s.merckentity),UPPER(s.typeofscope),
          UPPER(s.productbrand),UPPER(s.businessfranchises),UPPER(s.productGalenicForm),UPPER(s.servicePerformed),
          sp.[year],UPPER(sp.[month]),UPPER(sp.productionType)) NOT IN 
      (SELECT DISTINCT dim_jda_locationid,dim_jda_productid, dim_dateidstartdate,UPPER(dd_accountname),dd_accountid,UPPER(dd_countrysfdc),UPPER(dd_mbregions),UPPER(dd_merckentity),UPPER(dd_typeofscope),
          UPPER(dd_productbrand),UPPER(dd_businessfranchises),UPPER(dd_productGalenicForm),UPPER(dd_servicePerformed),
          dd_year,UPPER(dd_month),UPPER(dd_productionType) FROM tmp_fact_cmo WHERE UPPER(dd_packersupplier) IN ('PACKER AND SUPPLIER','SUPPLIER'))
  AND NOT (jp.SupplierCode = s.accountid                          -- map packer/supplier -- condition to not count
  AND s.accountid != jp.PackerCode                                -- map packer/supplier -- condition to not count
  AND (jp.PackerCode IS NOT NULL OR length(jp.PackerCode) != 3))  -- map packer/supplier -- condition to not count
)
;

/* CASE 3 - INSERT JDA DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Packer' AS dd_packersupplier,
  t.dim_jda_locationid AS dim_jda_locationid, -- jda
  t.dim_jda_productid AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  t.ct_type_consensus_forecast_euro_packs AS ct_type_consensus_forecast_euro_packs,
  t.ct_type_consensus_forecast_euro_units AS ct_type_consensus_forecast_euro_units,
  t.ct_type_consensus_forecast_qty_packs AS ct_type_consensus_forecast_qty_packs,
  t.ct_type_consensus_forecast_qty_units AS ct_type_consensus_forecast_qty_units,
  t.ct_type_consensus_forecast_frozen_qty_packs AS ct_type_consensus_forecast_frozen_qty_packs,
  t.ct_type_consensus_forecast_frozen_euro_packs AS ct_type_consensus_forecast_frozen_euro_packs,
  t.ct_type_op_plan_qty_packs AS ct_type_op_plan_qty_packs,
  t.ct_type_op_plan_qty_units AS ct_type_op_plan_qty_units,
  t.ct_type_op_plan_euro_packs AS ct_type_op_plan_euro_packs,
  t.ct_type_op_plan_euro_units AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  IFNULL(sp.[year],'Not Set') AS dd_year,
  IFNULL(sp.[month],'Not Set') AS dd_month,
  IFNULL(sp.productionType,'Not Set') AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  csv_jda_gbu_sfdc_businessfranchise AS mfr,
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_date AS dd,
  tmp_csv_SFDC AS s
LEFT JOIN tmp_csv_SFDC_production_with_dummydata AS sp ON sp.accountid = s.accountid
  AND UPPER(sp.productbrand) = UPPER(s.productbrand)
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)  -- map business franchise
  AND UPPER(mfr.sfdc_business_franchise) = UPPER(s.businessfranchises)  -- map business franchise
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND jp.PackerCode = s.accountid           -- map packer/supplier  -- condition 3
  AND (jp.SupplierCode <> s.accountid       -- map packer/supplier  -- condition 3
            OR jp.SupplierCode = 'Not Set'                -- map packer/supplier  -- condition 3
			OR length(jp.SupplierCode) = 3)               -- map packer/supplier  -- condition 3
  AND t.dim_dateidstartdate = dd.dim_dateid      -- map year and month
  AND dd.calendaryear = sp.[year]                -- map year and month
  AND UPPER(dd.monthabbreviation) = UPPER(sp.[month])          -- map year and month
  AND dd.companycode = 'Not Set'                 -- map year and month               
  AND t.dim_jda_productid = jp.dim_jda_productid
  AND (dim_jda_locationid,jp.dim_jda_productid, dim_dateidstartdate,UPPER(s.accountname),s.accountid,UPPER(s.primarycountry),UPPER(s.mbregions),UPPER(s.merckentity),UPPER(s.typeofscope),
          UPPER(s.productbrand),UPPER(s.businessfranchises),UPPER(s.productGalenicForm),UPPER(s.servicePerformed),
          sp.[year],UPPER(sp.[month]),UPPER(sp.productionType)) NOT IN 
      (SELECT DISTINCT dim_jda_locationid,dim_jda_productid, dim_dateidstartdate,UPPER(dd_accountname),dd_accountid,UPPER(dd_countrysfdc),UPPER(dd_mbregions),UPPER(dd_merckentity),UPPER(dd_typeofscope),
          UPPER(dd_productbrand),UPPER(dd_businessfranchises),UPPER(dd_productGalenicForm),UPPER(dd_servicePerformed),
          dd_year,UPPER(dd_month),UPPER(dd_productionType) FROM tmp_fact_cmo WHERE UPPER(dd_packersupplier) IN ('PACKER AND SUPPLIER','SUPPLIER'))
  AND NOT (jp.SupplierCode = s.accountid                          -- map packer/supplier -- condition to not count
  AND s.accountid != jp.PackerCode                                -- map packer/supplier -- condition to not count
  AND (jp.PackerCode IS NOT NULL OR length(jp.PackerCode) != 3))  -- map packer/supplier -- condition to not count
)
;


/* CASE 3 - INSERT LICENSING OUT DATA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Packer' AS dd_packersupplier,
  t.dim_jda_locationid AS dim_jda_locationid, -- jda
  t.dim_jda_productid AS dim_jda_productid,
  t.dim_dateidstartdate AS dim_dateidstartdate,
  t.ct_type_consensus_forecast_euro_packs AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  0 AS ct_type_consensus_forecast_frozen_qty_packs,
  0 AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  t.ct_type_op_plan_euro_packs AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(cl.country,'Not Set') AS dd_countrysfdc,
  'Not Set' AS dd_mbregions,
  'Not Set' AS dd_merckentity,
  'Not Set' AS dd_typeofscope,
  IFNULL(jp.branddescr,'Not Set') AS dd_productbrand,
  IFNULL(mfr.sfdc_business_franchise,'Not Set') AS dd_businessfranchises,
  'Not Set' AS dd_productGalenicForm,
  'Not Set' AS dd_servicePerformed,
  IFNULL(dd.calendaryear,'Not Set') AS dd_year,
  IFNULL(dd.monthabbreviation,'Not Set') AS dd_month,
  'Packs' AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM
  csv_jda_gbu_sfdc_businessfranchise AS mfr,
  tmp_dataFromJDA AS t,
  dim_jda_product AS jp,
  dim_jda_location AS jl,
  dim_date AS dd,
  csv_licensing_out AS cl,
  tmp_csv_SFDC AS s
WHERE
  UPPER(jp.gbu) = UPPER(mfr.jda_gbu)
  AND left(cl.PackerCode,15) = s.accountid           -- map packer/supplier  -- condition 3
  AND (left(cl.SupplierCode,15) <> s.accountid       -- map packer/supplier  -- condition 3
            OR left(cl.SupplierCode,15) = 'Not Set'                -- map packer/supplier  -- condition 3
			OR length(left(cl.SupplierCode,15)) = 3)               -- map packer/supplier  -- condition 3                      
  AND t.dim_jda_productid = jp.dim_jda_productid   
  AND t.dim_dateidstartdate = dd.dim_dateid       
  AND t.dim_jda_locationid = jl.dim_jda_locationid 
  AND UPPER(jl.country) = UPPER(cl.country)
  AND cl.SKU = jp.itemglobalcode
  AND ((UPPER(jp.itemdescr) LIKE CONCAT('%', UPPER(s.productbrand), '%')       -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%GLUCOPHAGE%'                            -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%ILOSTAL%'                               -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%AZOL%'                                  -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DIP%'                                   -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DANCOR%'                                -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%DECORTIN%'                              -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%EMCOR%'                                 -- map brand
  AND UPPER(s.productbrand) NOT LIKE '%CONCOR%'                                -- map brand
  AND UPPER(jp.itemdescr) NOT LIKE '%GLISULIN%')                               -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE'                                      -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE IR')                                 -- map brand
  OR (UPPER(jp.branddescr) = 'GLUCOPHAGE XR'                                   -- map brand
  AND UPPER(s.productbrand) = 'GLUCOPHAGE XR')                                 -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ILOSTAL','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ILOSTAL')                                       -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAL','%')                          -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAL')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('AZOL','%')                              -- map brand
  AND UPPER(s.productbrand) = 'AZOL')                                          -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('PANTOPRAZOL','%')                       -- map brand
  AND UPPER(s.productbrand) = 'PANTOPRAZOL')                                   -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CILOSTAZOL','%')                        -- map brand
  AND UPPER(s.productbrand) = 'CILOSTAZOL')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('CONCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'CONCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'EMCONCOR')                                      -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DIP','%')                               -- map brand
  AND UPPER(s.productbrand) = 'DIP')                                           -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('VASODIPIN','%')                         -- map brand
  AND UPPER(s.productbrand) = 'VASODIPIN')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('DANCOR','%')                            -- map brand
  AND UPPER(s.productbrand) = 'DANCOR')                                        -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('ADANCOR','%')                           -- map brand
  AND UPPER(s.productbrand) = 'ADANCOR')                                       -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN'                                        -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN')                                      -- map brand
  OR (UPPER(jp.branddescr) = 'DECORTIN H'                                      -- map brand
  AND UPPER(s.productbrand) = 'DECORTIN H')                                    -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('SOLU-DECORTIN H','%')                   -- map brand
  AND UPPER(s.productbrand) = 'SOLU-DECORTIN H')                               -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCOR ','%')                            -- map brand
  AND UPPER(s.productbrand) = 'EMCOR')                                         -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('EMCORETIC','%')                         -- map brand
  AND UPPER(s.productbrand) = 'EMCORETIC')                                     -- map brand
  OR (UPPER(jp.itemdescr) LIKE CONCAT('BICONCOR','%')                          -- map brand
  AND UPPER(s.productbrand) = 'BICONCOR'))                                     -- map brand
  AND (t.dim_jda_locationid,jp.dim_jda_productid, t.dim_dateidstartdate,UPPER(s.accountname),s.accountid,UPPER(s.primarycountry),UPPER(s.mbregions),UPPER(s.merckentity),UPPER(s.typeofscope),
          UPPER(s.productbrand),UPPER(s.businessfranchises),UPPER(s.productGalenicForm),UPPER(s.servicePerformed),
          dd.calendaryear,UPPER(dd.monthabbreviation),UPPER('Packs')) NOT IN 
      (SELECT DISTINCT dim_jda_locationid,dim_jda_productid, dim_dateidstartdate,UPPER(dd_accountname),dd_accountid,UPPER(dd_countrysfdc),UPPER(dd_mbregions),UPPER(dd_merckentity),UPPER(dd_typeofscope),
          UPPER(dd_productbrand),UPPER(dd_businessfranchises),UPPER(dd_productGalenicForm),UPPER(dd_servicePerformed),
          dd_year,UPPER(dd_month),UPPER(dd_productionType) FROM tmp_fact_cmo WHERE UPPER(dd_packersupplier) IN ('PACKER AND SUPPLIER','SUPPLIER'))
  AND NOT (left(cl.SupplierCode,15) = s.accountid                          -- map packer/supplier -- condition to not count
  AND s.accountid != left(cl.PackerCode,15)                                -- map packer/supplier -- condition to not count
  AND (left(cl.PackerCode,15) IS NOT NULL OR length(left(cl.PackerCode,15)) != 3))  -- map packer/supplier -- condition to not count
)
;

/* CASE 4 - Insert all account from SFDC that are not mapped in JDA */

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_cmo';	

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_cmo',IFNULL(MAX(fact_cmoID),0)
FROM tmp_fact_cmo;

INSERT INTO tmp_fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  (SELECT IFNULL(max_id,1) 
    FROM number_fountain 
    WHERE table_name = 'fact_cmo') + ROW_NUMBER() OVER(ORDER BY '') AS fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM
(SELECT DISTINCT
  14 AS dim_projectsourceid, -- Project Source ID
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS DW_INSERT_DATE, 
  current_timestamp AS DW_UPDATE_DATE,
  'Not Mapped' AS dd_packersupplier,
  1 AS dim_jda_locationid, -- jda
  1 AS dim_jda_productid,
  1 AS dim_dateidstartdate,
  0 AS ct_type_consensus_forecast_euro_packs,
  0 AS ct_type_consensus_forecast_euro_units,
  0 AS ct_type_consensus_forecast_qty_packs,
  0 AS ct_type_consensus_forecast_qty_units,
  0 AS ct_type_consensus_forecast_frozen_qty_packs,
  0 AS ct_type_consensus_forecast_frozen_euro_packs,
  0 AS ct_type_op_plan_qty_packs,
  0 AS ct_type_op_plan_qty_units,
  0 AS ct_type_op_plan_euro_packs,
  0 AS ct_type_op_plan_euro_units,
  IFNULL(s.accountname,'Not Set') AS dd_accountname, --sfdc
  IFNULL(s.accountid,'Not Set') AS dd_accountid,
  IFNULL(s.primarycountry,'Not Set') AS dd_countrysfdc,
  IFNULL(s.mbregions,'Not Set') AS dd_mbregions,
  IFNULL(s.merckentity,'Not Set') AS dd_merckentity,
  IFNULL(s.typeofscope,'Not Set') AS dd_typeofscope,
  IFNULL(s.productbrand,'Not Set') AS dd_productbrand,
  IFNULL(s.businessfranchises,'Not Set') AS dd_businessfranchises,
  IFNULL(s.productGalenicForm,'Not Set') AS dd_productGalenicForm,
  IFNULL(s.servicePerformed,'Not Set') AS dd_servicePerformed,
  'Not Set' AS dd_year,
  'Not Set' AS dd_month,
  'Not Set' AS dd_productionType,
  0 AS ct_scenario_production,
  0 AS ct_scenario_operating_plan,
  0 AS ct_scenario_forecast,
  0 AS ct_scenario_capacity,
  'Not Set' AS dd_topcmoaccount,
  0 AS ct_spend_quantity_value,
  'Not Set' AS dd_spend_quantity_type,
  0 AS ct_spend_value
FROM 
  tmp_csv_SFDC AS s
WHERE
  (UPPER(s.accountname),s.accountid) NOT IN (SELECT DISTINCT UPPER(dd_accountname),dd_accountid FROM tmp_fact_cmo)
)
;


/* begin - update the country FROM SFDC */

DROP TABLE IF EXISTS tmp_countries;
CREATE TABLE tmp_countries
AS
SELECT DISTINCT PRIMARYCOUNTRY, MBREGIONS
FROM csv_SFDC;

UPDATE tmp_fact_cmo AS t
SET t.dd_mbregions = c.mbregions
FROM tmp_fact_cmo AS t, tmp_countries AS c
WHERE t.dd_countrysfdc = c.PRIMARYCOUNTRY;

/* end - update the country from SFDC */


/* begin - Update the Top CMO Accounts */

UPDATE tmp_fact_cmo
SET dd_topcmoaccount =
  CASE WHEN DD_ACcountid IN 
    (
      '001w000001F6j2i', -- Famar L'Aigle
      '001w000001F6j2w', -- Pierre Fabre
      '001w000001F6j2l', -- Baxter
      '001w000001Ip7yi', -- Nanolek
      '001w000001F6j2z', -- Neopharma
      '001w000001F6j30', -- Riyadh Pharma
      '001w000001F6j4c', -- Amoun
      '001w000001F6j4j', -- Minapharm
      '001w000001F6j4k', -- Novapharm
      '001w000001F6j7C', -- Yuyu Pharm
      '001w000001F6j5a', -- Richam
      '001w000001UHqlj', -- Jamaro
      '001w000001F6j2h', -- Famar SGL Lyon SAS
      '001w000001F6j2s', -- Corden Pharma Plankstadt
      '001w000001KnoQi', -- Catalent Schorndorf
      '001w000001F6j6J', -- Abbott (NL)
      '001w000001F6izl', -- Juniper Pharmaceuticals Inc. (Columbia)
      '001w000001F6j2k', -- Patheon Italy S.p.a
      '001w000001F6j2j', -- Patheon France SAS
      '001w000001JFt9c', -- Merck Spittal
      '001w000001F6j2n', -- Nkunzi Pharmaceuticals
      '001w000001JFtDP', -- Merck Jakarta (PTMI)
      '001w000001F6j4f', -- Interthai Pharmaceuticals
      '001w000001F6j2f', -- ALTEA Farmaceutica S.A.
      '001w000001F6j2m', -- Blisfarma Ind Farmac LTDA
      '001w000001VvrfT', -- Lupin AURANGABAD
      '001w000001I3zdy', -- Lupin GOA
      '001w000001Vvrkd', -- Lupin NAGPUR
      '001w000001Vvrlb' -- Lupin PITHAMPUR (INDORE)
    ) 
      THEN 'Top 24 CMOs' ELSE 'Others' END
;


/* end - Update the Top CMO Accounts */


/* begin - update units to 0 for BULK logic */

UPDATE tmp_fact_cmo
SET ct_type_consensus_forecast_euro_units = 0,
  ct_type_consensus_forecast_qty_units = 0,
  ct_type_op_plan_euro_units = 0,
  ct_type_op_plan_qty_units = 0
WHERE LOWER(dd_packersupplier) = LOWER('Packer');

/* end - update units to 0 for BULK logic */


/* begin - Spend Data logic */

DROP TABLE IF EXISTS tmp_spenddata;
CREATE TABLE tmp_spenddata
AS
SELECT 
  IFNULL(sku,'Not Set') AS sku,
  CAST((RIGHT(InvoiceDate,4)||'-'||left(InvoiceDate,2)||'-'||'01') AS DATE) AS InvoiceDate,
  CAST(1 AS BIGINT) AS dim_invoicedate,
  SUM(CAST(LEFT(REPLACE(quantity,',',''), INSTR(REPLACE(quantity,',',''),' ')-1) AS DECIMAL(18,4))) AS quantity_value,
  RIGHT(quantity,LEN(quantity)-INSTR(quantity,' ')) AS quantity_type,
  SUM(CAST(1000 * CAST(REPLACE(spend,',','') AS DECIMAL(18,4)) AS DECIMAL(18,4))) AS spend
FROM csv_spend_data
WHERE LOWER(InvoiceDate) <> LOWER('result')
GROUP BY 
  IFNULL(sku,'Not Set'),
  CAST((RIGHT(InvoiceDate,4)||'-'||LEFT(InvoiceDate,2)||'-'||'01') AS DATE),
  CAST(1 AS BIGINT),
  RIGHT(quantity,LEN(quantity)-INSTR(quantity,' '))
;

UPDATE tmp_spenddata AS t
SET dim_invoicedate = dim_dateid
FROM tmp_spenddata AS t, dim_date AS dd
WHERE t.invoicedate = dd.datevalue;

-- because we don't have the location id, we will update the spend data on 
-- the first appearance in the fact based on the date, sku and account

DROP TABLE IF EXISTS tmp_getfirstcmoaccountline;
CREATE TABLE tmp_getfirstcmoaccountline
AS
SELECT 
  MIN(fact_cmoid) AS fact_cmoid,
  dim_jda_productid,
  dim_dateidstartdate,
  dd_accountid
FROM tmp_fact_cmo
GROUP BY 
  dim_jda_productid,
  dim_dateidstartdate,
  dd_accountid
;
  
UPDATE tmp_fact_cmo AS f
SET f.ct_spend_quantity_value = t.quantity_value,
    f.dd_spend_quantity_type = t.quantity_type,
    f.ct_spend_value = t.spend
FROM tmp_fact_cmo AS f, tmp_spenddata AS t, dim_jda_product AS dj, tmp_getfirstcmoaccountline AS gf
WHERE f.dim_dateidstartdate = t.dim_invoicedate
  AND f.dim_jda_productid = dj.dim_jda_productid
  AND t.sku = dj.itemglobalcode
  AND f.dim_jda_productid = gf.dim_jda_productid
  AND f.dim_dateidstartdate = gf.dim_dateidstartdate
  AND f.dd_accountid = gf.dd_accountid
  AND f.fact_cmoid = gf.fact_cmoid
;


/* end - Spend Data logic */



/* Insert FROM temporary into the fact */


DELETE FROM fact_cmo;

INSERT INTO fact_cmo(
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
)
SELECT 
  fact_cmoid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date,
  dw_update_date,
  dd_packersupplier,
  dim_jda_locationid,
  dim_jda_productid,
  dim_dateidstartdate,
  ct_type_consensus_forecast_euro_packs,
  ct_type_consensus_forecast_euro_units,
  ct_type_consensus_forecast_qty_packs,
  ct_type_consensus_forecast_qty_units,
  ct_type_consensus_forecast_frozen_qty_packs,
  ct_type_consensus_forecast_frozen_euro_packs,
  ct_type_op_plan_qty_packs,
  ct_type_op_plan_qty_units,
  ct_type_op_plan_euro_packs,
  ct_type_op_plan_euro_units,
  dd_accountname,
  dd_accountid,
  dd_countrysfdc,
  dd_mbregions,
  dd_merckentity,
  dd_typeofscope,
  dd_productbrand,
  dd_businessfranchises,
  dd_productGalenicForm,
  dd_servicePerformed,
  dd_year,
  dd_month,
  dd_productionType,
  ct_scenario_production,
  ct_scenario_operating_plan,
  ct_scenario_forecast,
  ct_scenario_capacity,
  dd_topcmoaccount,
  ct_spend_quantity_value,
  dd_spend_quantity_type,
  ct_spend_value
FROM TMP_FACT_CMO;

DROP TABLE IF EXISTS tmp_fact_cmo;

DELETE FROM emd586.fact_cmo;

INSERT INTO emd586.fact_cmo
SELECT *
FROM fact_cmo;
