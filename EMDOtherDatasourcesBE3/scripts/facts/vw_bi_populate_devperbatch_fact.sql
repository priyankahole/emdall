/******************************************************************************************************************/
/*   Script         : bi_populate_devperbatch_fact                                                                */
/*   Author         : Cristian T                                                                                  */
/*   Created On     : 31 Jan 2017                                                                                 */
/*   Description    : Populating script of fact_devperbatch                                                       */
/*********************************************Change History*******************************************************/
/*   Date                By              Version           Desc                                                   */
/*   16 Feb 2017         CristianT       1.0               Creating the script.                                   */
/*   30 May 2017         CristianT       1.1               Changed the logic for MES and TW batches.              */
/*   05 Jul 2017         CristianT       1.2               Snapshots logic changed from weekly to monthly basis   */
/*   25 Jan 2018         CristianT       1.3               Changed First SFO logic for Erbitux. Replaced dd_bo_id = 'BO01' with dd_bo_id = 'BO00' */
/*   14 May 2019         CristianT       1.4               Changed the source of data to HDFS                     */
/******************************************************************************************************************/

/* 24 Aug 2017 CristianT Start: Added specific number_fountain table for the autocommit property */

DROP TABLE IF EXISTS number_fountain_fact_devperbatch;
CREATE TABLE number_fountain_fact_devperbatch LIKE NUMBER_FOUNTAIN INCLUDING DEFAULTS INCLUDING IDENTITY;

/* 24 Aug 2017 CristianT End */

DROP TABLE IF EXISTS devperbatchhistory_delete;
CREATE TABLE devperbatchhistory_delete
AS
SELECT fact_devperbatchid
FROM fact_devperbatchhistory
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM fact_devperbatchhistory
WHERE fact_devperbatchid IN (SELECT fact_devperbatchid FROM devperbatchhistory_delete);

DROP TABLE IF EXISTS devperbatchhistory_delete;

DELETE FROM number_fountain_fact_devperbatch WHERE table_name = 'fact_devperbatchhistory';

INSERT INTO number_fountain_fact_devperbatch
SELECT 'fact_devperbatchhistory', ifnull(max(fact_devperbatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_devperbatchhistory;

DROP TABLE IF EXISTS tmp_fact_devperbatch;
CREATE TABLE tmp_fact_devperbatch LIKE fact_devperbatchhistory INCLUDING DEFAULTS INCLUDING IDENTITY;


/* Identify Batches from MES data */
DROP TABLE IF EXISTS tmp_mesbatchdata;
CREATE TABLE tmp_mesbatchdata
AS
SELECT distinct mo.mo as dd_mo,
       mo.batch as dd_batchno,
       mo.material_id as dd_material_id,
       mo.material_desc as dd_material_desc,
       sfo.sfo as dd_sfo,
       sfo.sfo_status as dd_sfo_status,
       sfo.bo_id as dd_bo_id,
       sfo.change_to_status as dd_change_to_status,
       sfo.chg_muid as dd_chg_muid,
       sfo.chg_date as dd_chg_date,
       sfo.pu3 as dd_pu3,
       cast('Not Set' as varchar(300)) as mesdepartmenttype,
       cast('Not Set' as varchar(300)) as messervice,
       cast('Not Set' as varchar(300)) as messubservice
FROM fops_mo_list mo,
     fops_sfo_fte_list sfo
WHERE 1 = 1
      AND mo.mo = sfo.mo
      AND substr(sfo.chg_date, 0, 10) >= (CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end - INTERVAL '90' DAY);

UPDATE tmp_mesbatchdata mes
SET mes.mesdepartmenttype = ifnull(usr.dd_departmenttype,'Not Set'),
    mes.messervice = ifnull(usr.dd_service,'Not Set'),
    mes.messubservice = ifnull(usr.dd_subservice, 'Not Set')
FROM tmp_mesbatchdata mes,
     dim_qualityusers usr
WHERE usr.rowiscurrent = 1
      AND mes.dd_chg_muid = usr.MERCK_UID
      AND mes.dd_chg_muid <> 'Not Set';


/* Identify all Deviations from Trackwise */
DROP TABLE IF EXISTS tmp_twdeviatondata;
CREATE TABLE tmp_twdeviatondata
AS
SELECT distinct tw_main.pr_id as dd_twprid,
       tw_main.assignedto as dd_twassginedto,
       tw_main.assignedto_userid as dd_twassignedtouserid,
       tw_main.date_opened as dd_twdateopened,
       tw_main.date_closed as dd_twdateclosed,
       tw_main.date_due as dd_twdatedue,
       tw_main.site as dd_twsite,
	   tw_main.assessment_date as dd_twassessmentdate,
       cast('Not Set' as varchar(300)) as twdepartmenttype,
       cast('Not Set' as varchar(300)) as twservice,
       cast('Not Set' as varchar(300)) as twsubservice
FROM tw_rpt_main tw_main
WHERE 1 = 1
      AND tw_main.project = 'Deviation'
      AND tw_main.site = 'Switzerland, Vevey'
      AND (tw_main.date_opened >= date_trunc('month', CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end) or tw_main.assessment_date >= date_trunc('month', CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end));

UPDATE tmp_twdeviatondata tw
SET tw.twdepartmenttype = ifnull(usr.dd_departmenttype,'Not Set'),
    tw.twservice = ifnull(usr.dd_service,'Not Set'),
    tw.twsubservice = ifnull(usr.dd_subservice, 'Not Set')
FROM tmp_twdeviatondata tw,
     dim_qualityusers usr
WHERE usr.rowiscurrent = 1
      AND tw.dd_twassignedtouserid = usr.MERCK_UID
      AND tw.dd_twassignedtouserid <> 'Not Set';

INSERT INTO tmp_fact_devperbatch(
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
snapshotdate,
dim_dateidsnapshot,
dd_mo,
dd_batchno,
dd_sfo,
dd_start_date,
dim_dateidstart,
dd_end_date,
dim_dateidend,
dd_start_muid,
dim_qualityusersidstart,
dd_finish_muid,
dim_qualityusersidfinish,
dd_twprid,
dd_twbatchno,
dd_twassginedto,
dd_twassignedtouserid,
dim_qualityusersidassignedto,
dd_twdateopened,
dim_dateidopened,
dd_twdateclosed,
dim_dateidclosed,
dd_twdatedue,
dim_dateiddue,
dd_twsite,
dd_pu3,
dd_twassessmentdate,
dim_dateidassesment,
dd_material_id,
dd_material_desc,
dd_product_name,
dd_firstsfo,
dd_sfo_status,
dd_bo_id,
dd_change_to_status,
dd_chg_muid,
dim_qualityusersidchg,
dd_chg_date,
dim_dateidchg,
ct_firstsfodsp
)
SELECT (SELECT max_id from number_fountain_fact_devperbatch WHERE table_name = 'fact_devperbatchhistory') + ROW_NUMBER() over(order by '') as fact_devperbatchid,
       1 as dim_projectsourceid,
       1 as amt_exhangerate,
       1 as amt_exchangerate_gbl,
       1 as dim_currencyid,
       1 as dim_currencyid_tra,
       1 as dim_currencyid_gbl,
       current_timestamp as dw_insert_date,
       current_timestamp as dw_update_date,
       CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end as snapshotdate,
       1 as dim_dateidsnapshot,
       ifnull(mes.dd_mo, 'Not Set') as dd_mo,
       ifnull(mes.dd_batchno, 'Not Set') as dd_batchno,
       ifnull(mes.dd_sfo, 0) as dd_sfo,
       '0001-01-01' as dd_start_date,
       1 as dim_dateidstart,
       '0001-01-01' as dd_end_date,
       1 as dim_dateidend,
       'Not Set' as dd_start_muid,
       1 as dim_qualityusersidstart,
       'Not Set' as dd_finish_muid,
       1 as dim_qualityusersidfinish,
       ifnull(tw.dd_twprid, 0) as dd_twprid,
       'Not Set' as dd_twbatchno,
       ifnull(tw.dd_twassginedto, 'Not Set') as dd_twassginedto,
       ifnull(tw.dd_twassignedtouserid, 'Not Set') as dd_twassignedtouserid,
       1 as dim_qualityusersidassignedto,
       ifnull(tw.dd_twdateopened, '0001-01-01') as dd_twdateopened,
       1 as dim_dateidopened,
       ifnull(tw.dd_twdateclosed, '0001-01-01') as dd_twdateclosed,
       1 as dim_dateidclosed,
       ifnull(tw.dd_twdatedue, '0001-01-01') as dd_twdatedue,
       1 as dim_dateiddue,
       ifnull(tw.dd_twsite, 'Not Set') as dd_twsite,
       ifnull(mes.dd_pu3, 'Not Set') as dd_pu3,
       ifnull(tw.dd_twassessmentdate, '0001-01-01') as dd_twassessmentdate,
       1 as dim_dateidassesment,
       mes.dd_material_id,
       mes.dd_material_desc,
       'Not Set' as dd_product_name,
       'Not Set' as dd_firstsfo,
       ifnull(mes.dd_sfo_status, 0) as dd_sfo_status,
       ifnull(mes.dd_bo_id, 'Not Set') as dd_bo_id,
       ifnull(mes.dd_change_to_status, 0) as dd_change_to_status,
       ifnull(mes.dd_chg_muid, 'Not Set') as dd_chg_muid,
       1 as dim_qualityusersidchg,
       ifnull(mes.dd_chg_date, '0001-01-01') as dd_chg_date,
       1 as dim_dateidchg,
       0 as ct_firstsfodsp
FROM tmp_mesbatchdata mes
     LEFT OUTER JOIN tmp_twdeviatondata tw on tw.twdepartmenttype = mes.mesdepartmenttype AND tw.twservice = mes.messervice AND tw.twsubservice = mes.messubservice
WHERE 1 = 1;      
      

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_projectsourceid = prj.dim_projectsourceid
FROM dim_projectsource prj,
     tmp_fact_devperbatch tmp;

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidsnapshot = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE tmp.snapshotdate = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidsnapshot <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidstart = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_start_date, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_start_date <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidstart <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidend = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_end_date, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_end_date <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidend <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidopened = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_twdateopened, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_twdateopened <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidopened <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidclosed = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_twdateclosed, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_twdateclosed <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidclosed <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateiddue = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_twdatedue, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_twdatedue <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateiddue <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidassesment = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_twassessmentdate, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_twassessmentdate <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidassesment <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_dateidchg = ifnull(dt.dim_dateid, 1)
FROM dim_date dt,
     tmp_fact_devperbatch tmp
WHERE substr(tmp.dd_chg_date, 0, 10) = dt.datevalue
      AND dt.companycode = 'Not Set'
      AND tmp.dd_chg_date <> '0001-01-01'
      AND tmp.dim_projectsourceid = dt.projectsourceid
      AND tmp.dim_dateidchg <> ifnull(dt.dim_dateid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_qualityusersidstart = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_start_muid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dd_start_muid <> 'Not Set'
      AND tmp.dim_qualityusersidstart <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_qualityusersidfinish = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_finish_muid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dd_finish_muid <> 'Not Set'
      AND tmp.dim_qualityusersidfinish <> ifnull(usr.dim_qualityusersid, 1);


UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_qualityusersidassignedto = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_twassignedtouserid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dd_twassignedtouserid <> 'Not Set'
      AND tmp.dim_qualityusersidassignedto <> ifnull(usr.dim_qualityusersid, 1);

UPDATE tmp_fact_devperbatch tmp
SET tmp.dim_qualityusersidchg = ifnull(usr.dim_qualityusersid, 1)
FROM dim_qualityusers usr,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_chg_muid = usr.MERCK_UID
      AND usr.rowiscurrent = 1
      AND tmp.dd_start_muid <> 'Not Set'
      AND tmp.dim_qualityusersidchg <> ifnull(usr.dim_qualityusersid, 1);

DROP TABLE IF EXISTS tmp_productname_dev;
CREATE TABLE tmp_productname_dev
AS
SELECT attrs_material_name,
       max(attrs_text_value) as attrs_text_value
FROM t_eve_material_templ_attrs
WHERE 1 = 1
      AND attrs_name = 'Product Name'
      AND upper(attrs_text_value) not like 'REBIF%'
GROUP BY attrs_material_name;

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_product_name = ifnull(pg.attrs_text_value, 'Not Set')
FROM tmp_productname_dev pg,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_material_id = pg.attrs_material_name
      AND tmp.dd_product_name <> ifnull(pg.attrs_text_value, 'Not Set');

DROP TABLE IF EXISTS tmp_productname_dev;

/* For Rebif G1 and Rebif G2 look only for records with material_name like B14A5 */
DROP TABLE IF EXISTS tmp_productrebif_dev;
CREATE TABLE tmp_productrebif_dev
AS
SELECT DISTINCT attrs_material_name,
       attrs_text_value
FROM t_eve_material_templ_attrs
WHERE 1 = 1
      AND attrs_name = 'Product Name'
      AND upper(attrs_text_value) like 'REBIF%';

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_product_name = ifnull(pg.attrs_text_value, 'Not Set')
FROM tmp_productrebif_dev pg,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_material_id = pg.attrs_material_name
      AND tmp.dd_material_id like 'B14A5%'
      AND tmp.dd_product_name <> ifnull(pg.attrs_text_value, 'Not Set');

DROP TABLE IF EXISTS tmp_productrebif_dev;

/* Start: Identify First SFO occurance for each batch */
/* Rebif G1 */
DROP TABLE IF EXISTS tmp_firstsforebifg1;
CREATE TABLE tmp_firstsforebifg1
AS
SELECT dd_batchno,
       dd_sfo,
       dd_material_id,
       dd_bo_id,
       dd_change_to_status,
       dd_chg_date,
       row_number() over (partition by left(dd_batchno, 3) order by dd_chg_date asc) as sfo_order
FROM tmp_fact_devperbatch
WHERE dd_product_name = 'REBIF G1'
      AND dd_material_id = 'B14A5000'
      AND dd_bo_id = 'BO01'
      AND dd_change_to_status = '11104'
      --AND dd_start_date >= trunc(CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end, 'MM')
GROUP BY dd_batchno,
         dd_sfo,
         dd_material_id,
         dd_bo_id,
         dd_change_to_status,
         dd_chg_date;

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_firstsfo = 'X'
FROM tmp_firstsforebifg1 fsfo,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_batchno = fsfo.dd_batchno
      AND tmp.dd_sfo = fsfo.dd_sfo
      AND tmp.dd_material_id = fsfo.dd_material_id
      AND tmp.dd_bo_id = fsfo.dd_bo_id
      AND tmp.dd_change_to_status = fsfo.dd_change_to_status
      AND fsfo.sfo_order = 1;

DROP TABLE IF EXISTS tmp_firstsforebifg1;

/* Rebif G2 */
DROP TABLE IF EXISTS tmp_firstsforebifg2;
CREATE TABLE tmp_firstsforebifg2
AS
SELECT dd_batchno,
       dd_sfo,
       dd_material_id,
       dd_bo_id,
       dd_change_to_status,
       dd_chg_date,
       row_number() over (partition by left(dd_batchno, 3) order by dd_chg_date asc) as sfo_order
FROM tmp_fact_devperbatch
WHERE dd_product_name = 'REBIF G2'
      AND dd_material_id = 'B14A5A30'
      AND dd_bo_id = 'BO01'
      AND dd_change_to_status = '11104'
      --AND dd_start_date >= trunc(CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end, 'MM')
GROUP BY dd_batchno,
         dd_sfo,
         dd_material_id,
         dd_bo_id,
         dd_change_to_status,
         dd_chg_date;

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_firstsfo = 'X'
FROM tmp_firstsforebifg2 fsfo,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_batchno = fsfo.dd_batchno
      AND tmp.dd_sfo = fsfo.dd_sfo
      AND tmp.dd_material_id = fsfo.dd_material_id
      AND tmp.dd_bo_id = fsfo.dd_bo_id
      AND tmp.dd_change_to_status = fsfo.dd_change_to_status
      AND fsfo.sfo_order = 1;

DROP TABLE IF EXISTS tmp_firstsforebifg2;

/* Anti PDL */
DROP TABLE IF EXISTS tmp_firstsfoantipdl;
CREATE TABLE tmp_firstsfoantipdl
AS
SELECT dd_batchno,
       dd_sfo,
       dd_material_id,
       dd_bo_id,
       dd_change_to_status,
       dd_chg_date,
       row_number() over (partition by left(dd_batchno, 3) order by dd_chg_date asc) as sfo_order
FROM tmp_fact_devperbatch
WHERE upper(dd_product_name) like 'ANTI-PDL1%'
      AND dd_material_id in ('BPDA5A05', 'BPTA5A05')
      AND dd_bo_id = 'BO00'
      AND dd_change_to_status = '11104'
      --AND dd_start_date >= trunc(CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end, 'MM')
GROUP BY dd_batchno,
         dd_sfo,
         dd_material_id,
         dd_bo_id,
         dd_change_to_status,
         dd_chg_date;

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_firstsfo = 'X'
FROM tmp_firstsfoantipdl fsfo,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_batchno = fsfo.dd_batchno
      AND tmp.dd_sfo = fsfo.dd_sfo
      AND tmp.dd_material_id = fsfo.dd_material_id
      AND tmp.dd_bo_id = fsfo.dd_bo_id
      AND tmp.dd_change_to_status = fsfo.dd_change_to_status
      AND fsfo.sfo_order = 1;

DROP TABLE IF EXISTS tmp_firstsfoantipdl;

/* Erbitux */
DROP TABLE IF EXISTS tmp_firstsfoerbitux1;
CREATE TABLE tmp_firstsfoerbitux1
AS
SELECT dd_batchno,
       dd_sfo,
       dd_material_id,
       dd_bo_id,
       dd_change_to_status,
       dd_chg_date,
       row_number() over (partition by left(dd_batchno, 3) order by dd_chg_date asc) as sfo_order
FROM tmp_fact_devperbatch
WHERE upper(dd_product_name) like 'ERBITUX%'
      AND dd_material_id = 'BN1A5H44'
      AND dd_bo_id = 'BO00'
      AND dd_change_to_status = '11104'
      --AND dd_batchno >= '109'
      --AND dd_start_date >= trunc(CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end, 'MM')
GROUP BY dd_batchno,
         dd_sfo,
         dd_material_id,
         dd_bo_id,
         dd_change_to_status,
         dd_chg_date;

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_firstsfo = 'X'
FROM tmp_firstsfoerbitux1 fsfo,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_batchno = fsfo.dd_batchno
      AND tmp.dd_sfo = fsfo.dd_sfo
      AND tmp.dd_material_id = fsfo.dd_material_id
      AND tmp.dd_bo_id = fsfo.dd_bo_id
      AND tmp.dd_change_to_status = fsfo.dd_change_to_status
      AND fsfo.sfo_order = 1;

DROP TABLE IF EXISTS tmp_firstsfoerbitux1;

/* 04 Nov 2017 CristianT: Commented this part and also dd_batchno > 109 logic from tmp_firstsfoerbitux1 table
DROP TABLE IF EXISTS tmp_firstsfoerbitux2
CREATE TABLE tmp_firstsfoerbitux2
AS
SELECT dd_batchno,
       dd_sfo,
       dd_chg_date,
       row_number() over (partition by left(dd_batchno, 3) order by dd_chg_date asc) as sfo_order
FROM tmp_fact_devperbatch
WHERE --upper(dd_product_name) like 'ERBITUX%'
      dd_material_id = 'BN1A5H40'
      AND dd_bo_id = 'BO01'
      AND dd_change_to_status = '11104'
      AND dd_batchno < '109'
      --AND dd_start_date >= trunc(CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end, 'MM')
GROUP BY dd_batchno,
         dd_sfo,
         dd_chg_date

UPDATE tmp_fact_devperbatch tmp
SET tmp.dd_firstsfo = 'X'
FROM tmp_firstsfoerbitux2 fsfo,
     tmp_fact_devperbatch tmp
WHERE tmp.dd_batchno = fsfo.dd_batchno
      AND tmp.dd_sfo = fsfo.dd_sfo
      AND fsfo.sfo_order = 1

DROP TABLE IF EXISTS tmp_firstsfoerbitux2 */

/* End: Identify First SFO occurance for each batch */

/* 05 Nov 2017 CristianT Start: Added logic for Count DSP Batch measure */
DROP TABLE IF EXISTS tmp_ct_firstsfodsp;
CREATE TABLE tmp_ct_firstsfodsp
AS
SELECT min(f_devb.fact_devperbatchid) as fact_devperbatchid,
       f_devb.dd_batchno,
       f_devb.dd_product_name,
       f_devb.dd_firstsfo,
       max(case when snps.calendarmonthid = strt.calendarmonthid and f_devb.dd_product_name = 'REBIF G1' then 6
            when snps.calendarmonthid = strt.calendarmonthid and f_devb.dd_product_name = 'REBIF G2' then 1.5
            when snps.calendarmonthid = strt.calendarmonthid and f_devb.dd_product_name not in ('REBIF G1', 'REBIF G2') then 1
            else 0
       end) as ct_firstsfodsp
FROM tmp_fact_devperbatch f_devb,
     dim_date snps,
     dim_date strt,
     dim_date opnd
WHERE f_devb.dd_firstsfo = 'X'
      AND f_devb.dim_dateidsnapshot = snps.dim_dateid
      AND f_devb.dim_dateidstart = strt.dim_dateid
      AND f_Devb.dim_dateidopened = opnd.dim_dateid
GROUP BY dd_batchno,
         dd_product_name,
         dd_firstsfo;

UPDATE tmp_fact_devperbatch dev
SET ct_firstsfodsp = ifnull(tmp.ct_firstsfodsp, 0)
FROM tmp_fact_devperbatch dev,
     tmp_ct_firstsfodsp tmp
WHERE dev.fact_devperbatchid = tmp.fact_devperbatchid
      AND dev.ct_firstsfodsp <> ifnull(tmp.ct_firstsfodsp, 0);

DROP TABLE IF EXISTS tmp_ct_firstsfodsp;

/* 05 Nov 2017 CristianT End */


INSERT INTO fact_devperbatchhistory (
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
snapshotdate,
dim_dateidsnapshot,
dd_mo,
dd_batchno,
dd_sfo,
dd_start_date,
dim_dateidstart,
dd_end_date,
dim_dateidend,
dd_start_muid,
dim_qualityusersidstart,
dd_finish_muid,
dim_qualityusersidfinish,
dd_twprid,
dd_twbatchno,
dd_twassginedto,
dd_twassignedtouserid,
dim_qualityusersidassignedto,
dd_twdateopened,
dim_dateidopened,
dd_twdateclosed,
dim_dateidclosed,
dd_twdatedue,
dim_dateiddue,
dd_twsite,
dd_pu3,
dd_twassessmentdate,
dim_dateidassesment,
dd_material_id,
dd_material_desc,
dd_product_name,
dd_firstsfo,
dd_sfo_status,
dd_bo_id,
dd_change_to_status,
dd_chg_muid,
dim_qualityusersidchg,
dd_chg_date,
dim_dateidchg,
ct_firstsfodsp
)
SELECT fact_devperbatchid,
       dim_projectsourceid,
       amt_exhangerate,
       amt_exchangerate_gbl,
       dim_currencyid,
       dim_currencyid_tra,
       dim_currencyid_gbl,
       dw_insert_date,
       dw_update_date,
       snapshotdate,
       dim_dateidsnapshot,
       dd_mo,
       dd_batchno,
       dd_sfo,
       dd_start_date,
       dim_dateidstart,
       dd_end_date,
       dim_dateidend,
       dd_start_muid,
       dim_qualityusersidstart,
       dd_finish_muid,
       dim_qualityusersidfinish,
       dd_twprid,
       dd_twbatchno,
       dd_twassginedto,
       dd_twassignedtouserid,
       dim_qualityusersidassignedto,
       dd_twdateopened,
       dim_dateidopened,
       dd_twdateclosed,
       dim_dateidclosed,
       dd_twdatedue,
       dim_dateiddue,
       dd_twsite,
       dd_pu3,
       dd_twassessmentdate,
       dim_dateidassesment,
       dd_material_id,
       dd_material_desc,
       dd_product_name,
       dd_firstsfo,
       dd_sfo_status,
       dd_bo_id,
       dd_change_to_status,
       dd_chg_muid,
       dim_qualityusersidchg,
       dd_chg_date,
       dim_dateidchg,
       ct_firstsfodsp
FROM tmp_fact_devperbatch;

DROP TABLE IF EXISTS tmp_fact_devperbatch;

DROP TABLE IF EXISTS devperbatch_delete;
CREATE TABLE devperbatch_delete
AS
SELECT fact_devperbatchid
FROM fact_devperbatch
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;


DELETE FROM fact_devperbatch
WHERE fact_devperbatchid IN (SELECT fact_devperbatchid FROM devperbatch_delete);

DROP TABLE IF EXISTS devperbatch_delete;


DROP TABLE IF EXISTS devperbatch_insert;
CREATE TABLE devperbatch_insert
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarmonthid as calendarmonthid,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarmonthid ORDER BY dt.datevalue DESC) as rowno
FROM fact_devperbatchhistory f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarmonthid,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

DELETE FROM number_fountain_fact_devperbatch WHERE table_name = 'fact_devperbatch';

INSERT INTO number_fountain_fact_devperbatch
SELECT 'fact_devperbatch', ifnull(max(fact_devperbatchid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_devperbatch;


INSERT INTO fact_devperbatch (
fact_devperbatchid,
dim_projectsourceid,
amt_exhangerate,
amt_exchangerate_gbl,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
dw_insert_date,
dw_update_date,
snapshotdate,
dim_dateidsnapshot,
dd_mo,
dd_batchno,
dd_sfo,
dd_start_date,
dim_dateidstart,
dd_end_date,
dim_dateidend,
dd_start_muid,
dim_qualityusersidstart,
dd_finish_muid,
dim_qualityusersidfinish,
dd_twprid,
dd_twbatchno,
dd_twassginedto,
dd_twassignedtouserid,
dim_qualityusersidassignedto,
dd_twdateopened,
dim_dateidopened,
dd_twdateclosed,
dim_dateidclosed,
dd_twdatedue,
dim_dateiddue,
dd_twsite,
dd_pu3,
dd_twassessmentdate,
dim_dateidassesment,
dd_material_id,
dd_material_desc,
dd_product_name,
dd_firstsfo,
dd_sfo_status,
dd_bo_id,
dd_change_to_status,
dd_chg_muid,
dim_qualityusersidchg,
dd_chg_date,
dim_dateidchg,
ct_firstsfodsp
)
SELECT (SELECT max_id from number_fountain_fact_devperbatch WHERE table_name = 'fact_devperbatch') + ROW_NUMBER() over(order by '') as fact_devperbatchid,
       h.dim_projectsourceid,
       h.amt_exhangerate,
       h.amt_exchangerate_gbl,
       h.dim_currencyid,
       h.dim_currencyid_tra,
       h.dim_currencyid_gbl,
       h.dw_insert_date,
       h.dw_update_date,
       h.snapshotdate,
       h.dim_dateidsnapshot,
       h.dd_mo,
       h.dd_batchno,
       h.dd_sfo,
       h.dd_start_date,
       h.dim_dateidstart,
       h.dd_end_date,
       h.dim_dateidend,
       h.dd_start_muid,
       h.dim_qualityusersidstart,
       h.dd_finish_muid,
       h.dim_qualityusersidfinish,
       h.dd_twprid,
       h.dd_twbatchno,
       h.dd_twassginedto,
       h.dd_twassignedtouserid,
       h.dim_qualityusersidassignedto,
       h.dd_twdateopened,
       h.dim_dateidopened,
       h.dd_twdateclosed,
       h.dim_dateidclosed,
       h.dd_twdatedue,
       h.dim_dateiddue,
       h.dd_twsite,
       h.dd_pu3,
       h.dd_twassessmentdate,
       h.dim_dateidassesment,
       h.dd_material_id,
       h.dd_material_desc,
       h.dd_product_name,
       h.dd_firstsfo,
       h.dd_sfo_status,
       h.dd_bo_id,
       h.dd_change_to_status,
       h.dd_chg_muid,
       h.dim_qualityusersidchg,
       h.dd_chg_date,
       h.dim_dateidchg,
       h.ct_firstsfodsp
FROM fact_devperbatchhistory h,
     devperbatch_insert ins
WHERE h.dim_dateidsnapshot = ins.dim_dateidsnapshot
      AND ins.rowno = 1
      AND NOT EXISTS (SELECT 1 FROM fact_devperbatch t WHERE t.dim_dateidsnapshot = ins.dim_dateidsnapshot);

DROP TABLE IF EXISTS devperbatch_insert;

DROP TABLE IF EXISTS tmp_delete_dev;
CREATE TABLE tmp_delete_dev
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarmonthid as calendarmonthid,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarmonthid ORDER BY dt.datevalue DESC) as rowno
FROM fact_devperbatch f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarmonthid,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO fact_devperbatch f
USING tmp_delete_dev del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_delete_dev;


DROP TABLE IF EXISTS emd586devperbatch_delete;
CREATE TABLE emd586devperbatch_delete
AS
SELECT fact_devperbatchid
FROM emd586.fact_devperbatch
WHERE snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

DELETE FROM emd586.fact_devperbatch
WHERE fact_devperbatchid IN (SELECT fact_devperbatchid FROM emd586devperbatch_delete);

DROP TABLE IF EXISTS emd586devperbatch_delete;


INSERT INTO emd586.fact_devperbatch
SELECT h.*
FROM fact_devperbatch h
WHERE h.snapshotdate = CASE WHEN extract(hour from current_timestamp) between 0 and 18 then current_date - 1 else current_date end;

/* 21 Feb 2017 CristianT End */

DROP TABLE IF EXISTS tmp_emd586deletedev;
CREATE TABLE tmp_emd586deletedev
AS
SELECT f.dim_dateidsnapshot as dim_dateidsnapshot,
       dt.calendarmonthid as calendarmonthid,
       dt.calendarweekyr as calendarweekyr,
       dt.datevalue as datevalue,
       dt.weekdayname as weekdayname,
       ROW_NUMBER() over(PARTITION BY dt.calendarmonthid ORDER BY dt.datevalue DESC) as rowno
FROM emd586.fact_devperbatch f,
     dim_date dt
WHERE 1 = 1
      AND f.dim_dateidsnapshot = dt.dim_dateid
GROUP BY f.dim_dateidsnapshot,
         dt.calendarmonthid,
         dt.calendarweekyr,
         dt.datevalue,
         dt.weekdayname;

MERGE INTO emd586.fact_devperbatch f
USING tmp_emd586deletedev del ON f.dim_dateidsnapshot = del.dim_dateidsnapshot AND del.rowno > 1
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_emd586deletedev;