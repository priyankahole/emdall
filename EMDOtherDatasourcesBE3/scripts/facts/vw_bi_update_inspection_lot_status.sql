DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus;
CREATE TABLE  tmp_dim_inspectionlotstatus AS 
SELECT distinct j1.JEST_OBJNR AS dd_ObjectNumber,
       d.ApprovalGranted,
       d.ApprovalNotGranted,
       d.Created,
       d.LotCancelled,
       d.LotDetailDataDeleted,
       d.LotSampleDataDeleted,
       d.Released,
       d.SkippedLot,
       d.UsageDecisionMade,
       d.InspectionComplete 
FROM JEST_QALS j1, dim_inspectionlotstatus d
WHERE d.dim_inspectionlotstatusid = 1;


UPDATE   tmp_dim_inspectionlotstatus
SET ApprovalGranted = 'X'
 FROM JEST_QALS j, tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0322' 
AND j.JEST_INACT is NULL; 

UPDATE   tmp_dim_inspectionlotstatus
SET ApprovalNotGranted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0325' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET Created = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0001' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotCancelled = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0224' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotDetailDataDeleted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0227' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotSampleDataDeleted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0228' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET Released = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0002' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET SkippedLot = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0209' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET UsageDecisionMade = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0218' 
AND j.JEST_INACT is NULL;
     
/* Suchithra - 17Mar2015 - Update for the new field InspectionComplete */
UPDATE   tmp_dim_inspectionlotstatus
SET InspectionComplete = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0217' 
AND j.JEST_INACT is NULL; 
/* End of Update - 17Mar2015 - Suchithra */
   

/* Amar - 10-28-2016 - Dimension Is Missing Combination So Look For New */

/* Populate dim_inspectionlotstatus for new entries not available in DIM */
delete from number_fountain m where m.table_name = 'dim_inspectionlotstatus';

insert into number_fountain
select  'dim_inspectionlotstatus', ifnull(max(d.dim_inspectionlotstatusid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionlotstatus d
where d.dim_inspectionlotstatusid <> 1;

DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus_newcheck;
CREATE TABLE  tmp_dim_inspectionlotstatus_newcheck AS 
select distinct 
ApprovalGranted,
ApprovalNotGranted,
Created,
LotCancelled,
LotDetailDataDeleted,
LotSampleDataDeleted,
Released,
SkippedLot,
UsageDecisionMade,
InspectionComplete
from tmp_dim_inspectionlotstatus;


insert into dim_inspectionlotstatus (
dim_inspectionlotstatusid,
ApprovalGranted,
ApprovalNotGranted,
Created,
LotCancelled,
LotDetailDataDeleted,
LotSampleDataDeleted,
Released,
SkippedLot,
UsageDecisionMade,
InspectionComplete,
dw_insert_date,
dw_update_date
)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionlotstatus') + row_number() over(order by '') dim_inspectionlotstatusID,
j1.ApprovalGranted,
j1.ApprovalNotGranted,
j1.Created,
j1.LotCancelled,
j1.LotDetailDataDeleted,
j1.LotSampleDataDeleted,
j1.Released,
j1.SkippedLot,
j1.UsageDecisionMade,
j1.InspectionComplete,
current_timestamp dw_insert_date,
current_timestamp dw_update_date
from tmp_dim_inspectionlotstatus_newcheck j1
where not exists (
select 1
from dim_inspectionlotstatus ils
 WHERE ils.ApprovalGranted = j1.ApprovalGranted
       AND ils.ApprovalNotGranted = j1.ApprovalNotGranted 
       AND ils.Created = j1.Created 
       AND ils.LotCancelled = j1.LotCancelled
       AND ils.LotDetailDataDeleted = j1.LotDetailDataDeleted
       AND ils.LotSampleDataDeleted = j1.LotSampleDataDeleted
       AND ils.Released = j1.Released 
       AND ils.SkippedLot = j1.SkippedLot
       AND ils.UsageDecisionMade = j1.UsageDecisionMade
       AND ils.InspectionComplete = j1.InspectionComplete ) ;
   
   
   
/* Suchithra - 17Mar2015 - Additional Clause added for the new field InspectionComplete */     
UPDATE fact_inspectionlot fil
   SET fil.dim_inspectionlotstatusid = ils.Dim_InspectionLotStatusId,
       fil.dw_update_date = current_timestamp
	FROM
       dim_inspectionlotstatus ils,
       tmp_dim_inspectionlotstatus j1,
       fact_inspectionlot fil
 WHERE ils.ApprovalGranted = j1.ApprovalGranted
       AND ils.ApprovalNotGranted = j1.ApprovalNotGranted 
       AND ils.Created = j1.Created 
       AND ils.LotCancelled = j1.LotCancelled
       AND ils.LotDetailDataDeleted = j1.LotDetailDataDeleted
       AND ils.LotSampleDataDeleted = j1.LotSampleDataDeleted
       AND ils.Released = j1.Released 
       AND ils.SkippedLot = j1.SkippedLot
       AND ils.UsageDecisionMade = j1.UsageDecisionMade
       AND ils.InspectionComplete = j1.InspectionComplete 
       AND j1.dd_ObjectNumber = fil.dd_ObjectNumber;
       
       
       
DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus;
DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus_newcheck;


