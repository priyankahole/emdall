 /**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 26 Jun 2013 */
/*   Description    : Stored Proc bi_inventoryturn_processing migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   18 Aug 2013      Lokesh    1.7               Merged changes done in vw_funct_fiscal_year.standard.parallel.sql */
/*						  Order by Issue with fiscal year identified during ivturn testing 	  */
/******************************************************************************************************************/


/* Part 1 - getstdprice standard part 1 */

/* Pick up the current rows for processing */

UPDATE  tmp_getStdPrice
SET processed_flag = 'Y'
WHERE processed_flag IS NULL;


/* Query 1. Rows touched by Q1 should not be touched by other updates ( as it had a RETURN stmt ) */

UPDATE  tmp_getStdPrice
SET StandardPrice = 0,flag_upd = 'Q1'
WHERE PONumber is not null AND pUnitPrice = 0
AND processed_flag = 'Y';

/* Q2 */



UPDATE tmp_getStdPrice
SET pUMREZ_UMREN = 1,flag_upd = 'Q2A'   --Indicate with Q2A so that the next query ( which was in CASE - ELSE in mysql, should not update the same rows )
WHERE vUMREZ = 0 AND vUMREN <> 0
AND flag_upd <> 'Q1' 
AND processed_flag = 'Y';

UPDATE tmp_getStdPrice
SET pUMREZ_UMREN = NULL, flag_upd = 'Q2A'
WHERE vUMREN = 0
AND flag_upd <> 'Q1'
AND processed_flag = 'Y';


UPDATE tmp_getStdPrice
SET pUMREZ_UMREN = vUMREZ / vUMREN,flag_upd = 'Q2B'
WHERE vUMREZ <> 0
AND flag_upd NOT IN ( 'Q1', 'Q2A')
AND processed_flag = 'Y' ;    --As Q2A was inside Case, touch only rows not already updated by Q2A

/* There is no if condition on above query. Which means these rows should be processed by next queries and now the Q2 flags can be removed */

UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE flag_upd in ( 'Q2A','Q2B' )
AND processed_flag = 'Y';

/* Q3 MBEW_NO_BWTAR */

DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_getstdprice;
CREATE TABLE tmp_MBEW_NO_BWTAR_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEW_NO_BWTAR a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEW_NO_BWTAR_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEW_NO_BWTAR a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

/* By default, Q3 updated all rows to 0 in mysql where no match was found */
UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE flag_upd = 'N'
AND processed_flag = 'Y';

UPDATE tmp_getStdPrice x
SET StandardPrice = a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q3'
FROM 
tmp_getStdPrice x,
tmp_MBEW_NO_BWTAR_getstdprice a
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND x.flag_upd = 'N'
AND processed_flag = 'Y';

/* Q4  MBEW */

DROP TABLE IF EXISTS tmp_MBEW_getstdprice;
CREATE TABLE tmp_MBEW_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEW a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEW_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEW a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

/* By default, Q4 updated all rows to 0 in mysql where no match was found */
UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0
AND flag_upd <> 'Q1'
AND processed_flag = 'Y';

UPDATE 
tmp_getStdPrice x
SET StandardPrice = a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q4'
FROM 
tmp_getStdPrice x,
tmp_MBEW_getstdprice a
WHERE     
a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';


/* Q5  MBEWH_NO_BWTAR */

DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_getstdprice;
CREATE TABLE tmp_MBEWH_NO_BWTAR_getstdprice
AS
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM MBEWH_NO_BWTAR a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

INSERT INTO tmp_MBEWH_NO_BWTAR_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEWH_NO_BWTAR a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;



/* By default, Q4 updated all rows to 0 in mysql where no match was found */
UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0
AND flag_upd <> 'Q1'
AND processed_flag = 'Y';


UPDATE 
tmp_getStdPrice x
SET StandardPrice = a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q5'
FROM 
tmp_getStdPrice x,
tmp_MBEWH_NO_BWTAR_getstdprice a
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';

/* Q6  MBEWH */

/* Get parts/plant/year/month combinations where moving price is not available ( no rows with VPRSV = 'V' ) */
/* If moving price is also available then give preference to that - Such rows are inserted in the next query */
DROP TABLE IF EXISTS tmp_MBEWH_getstdprice;
CREATE TABLE tmp_MBEWH_getstdprice
AS
   SELECT a.MATNR,
          a.LFGJA,
          a.LFMON,
          a.BWKEY,
          sum(a.STPRS / (CASE WHEN a.PEINH = 0 THEN 1 ELSE a.peinh END))
          / count(*)
             stdprice_1
     FROM MBEWH a
    WHERE a.VPRSV = 'S' AND a.STPRS > 0
    AND NOT EXISTS 
    ( SELECT 1 FROM MBEWH b WHERE a.MATNR = b.MATNR and a.BWKEY = b.BWKEY and a.LFMON = b.LFMON AND a.LFGJA = b.LFGJA AND b.VPRSV = 'V' AND b.VERPR > 0 )
   GROUP BY a.MATNR,
            a.LFGJA,
            a.LFMON,
            a.BWKEY;

INSERT INTO tmp_MBEWH_getstdprice
select a.MATNR,a.LFGJA,a.LFMON,a.BWKEY,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM MBEWH a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.LFGJA,a.LFMON,a.BWKEY;

UPDATE tmp_getStdPrice
SET StandardPrice = 0  * pUMREZ_UMREN
WHERE StandardPrice < 0
AND flag_upd <> 'Q1'
AND processed_flag = 'Y';


UPDATE 
tmp_getStdPrice x
SET StandardPrice =  a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q6'
FROM 
tmp_getStdPrice x,
tmp_MBEWH_getstdprice a
WHERE     a.MATNR = pMaterialNo
AND a.LFGJA = pFiYear
AND a.LFMON = pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';



/* Q7 */


DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEW_NO_BWTAR a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_1;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEW_NO_BWTAR_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_2;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEW_NO_BWTAR_alt a,tmp_MBEW_NO_BWTAR_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_3;
CREATE TABLE tmp_MBEW_NO_BWTAR_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEW_NO_BWTAR_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEW_NO_BWTAR_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEW_NO_BWTAR_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0  * pUMREZ_UMREN
WHERE StandardPrice <= 0
AND processed_flag = 'Y';


UPDATE 
tmp_getStdPrice x
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q7'
FROM 
tmp_getStdPrice x,
tmp_MBEW_NO_BWTAR_alt_3 a
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';




/* Q8MBEW */

DROP TABLE IF EXISTS tmp_MBEW_alt;
CREATE TABLE tmp_MBEW_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEW a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEW_alt_1;
CREATE TABLE tmp_MBEW_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEW_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEW_alt_2;
CREATE TABLE tmp_MBEW_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEW_alt a,tmp_MBEW_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

/* Get parts/plant/year/month combinations where moving price is not available ( no rows with VPRSV = 'V' ) */
/* If moving price is also available then give preference to that - Such rows are inserted in the next query */
DROP TABLE IF EXISTS tmp_MBEW_alt_3;
CREATE TABLE tmp_MBEW_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEW_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
AND NOT EXISTS ( SELECT 1 FROM tmp_MBEW_alt_2 b WHERE a.MATNR = b.MATNR and a.BWKEY = b.BWKEY and a.pFiYear = b.pFiYear AND a.pPeriod = b.pPeriod AND b.VPRSV = 'V' AND b.VERPR > 0 )
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEW_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEW_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0  * pUMREZ_UMREN
WHERE StandardPrice <= 0
AND processed_flag = 'Y';


UPDATE 
tmp_getStdPrice x
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q8'
FROM 
tmp_getStdPrice x,
tmp_MBEW_alt_3 a
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';

/* Q9 MBEWH_NO_BWTAR */

DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEWH_NO_BWTAR a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_1;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEWH_NO_BWTAR_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_2;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEWH_NO_BWTAR_alt a,tmp_MBEWH_NO_BWTAR_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_3;
CREATE TABLE tmp_MBEWH_NO_BWTAR_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEWH_NO_BWTAR_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
AND NOT EXISTS ( SELECT 1 FROM tmp_MBEWH_NO_BWTAR_alt_2 b WHERE a.MATNR = b.MATNR and a.BWKEY = b.BWKEY and a.pFiYear = b.pFiYear AND a.pPeriod = b.pPeriod AND b.VPRSV = 'V' AND b.VERPR > 0 )
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEWH_NO_BWTAR_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEWH_NO_BWTAR_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0
AND processed_flag = 'Y';


UPDATE 
tmp_getStdPrice x
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q9'
FROM 
tmp_getStdPrice x,
tmp_MBEWH_NO_BWTAR_alt_3 a
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';


/* Q10 MBEWH */


DROP TABLE IF EXISTS tmp_MBEWH_alt;
CREATE TABLE tmp_MBEWH_alt
as
SELECT a.MATNR,a.BWKEY,(a.LFGJA * 100) + a.LFMON as LFGJA_LFMON,a.VPRSV,a.STPRS,a.VERPR,a.PEINH
FROM MBEWH a
WHERE ( a.VPRSV = 'S' AND a.STPRS > 0) or  (a.VPRSV = 'V' AND a.VERPR > 0) ;

UPDATE tmp_getStdPrice
SET pFiYear_pPeriod = (pFiYear * 100) + pPeriod;



DROP TABLE IF EXISTS tmp_MBEWH_alt_1;
CREATE TABLE tmp_MBEWH_alt_1
AS
SELECT a.MATNR,a.BWKEY,pFiYear,pPeriod,max(LFGJA_LFMON) as max_LFGJA_LFMON
FROM tmp_getStdPrice t,tmp_MBEWH_alt a
WHERE a.MATNR = pMaterialNo
AND a.BWKEY = pPlant
AND LFGJA_LFMON < pFiYear_pPeriod
AND t.StandardPrice <= 0
GROUP BY a.MATNR,a.BWKEY,pFiYear,pPeriod;


DROP TABLE IF EXISTS tmp_MBEWH_alt_2;
CREATE TABLE tmp_MBEWH_alt_2
AS
SELECT a.* ,b.pFiYear,b.pPeriod
FROM tmp_MBEWH_alt a,tmp_MBEWH_alt_1 b
WHERE a.MATNR = b.MATNR
AND a.BWKEY = b.BWKEY
AND a.LFGJA_LFMON = b.max_LFGJA_LFMON;

DROP TABLE IF EXISTS tmp_MBEWH_alt_3;
CREATE TABLE tmp_MBEWH_alt_3
AS
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.STPRS / a.PEINH)/ count(*) stdprice_1
FROM tmp_MBEWH_alt_2 a
where a.VPRSV = 'S' AND a.STPRS > 0
AND NOT EXISTS 
( SELECT 1 FROM tmp_MBEWH_alt_2 b WHERE a.MATNR = b.MATNR and a.BWKEY = b.BWKEY and a.pFiYear = b.pFiYear AND a.pPeriod = b.pPeriod AND b.VPRSV = 'V' AND b.VERPR > 0 )
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

INSERT INTO tmp_MBEWH_alt_3
select a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod,
sum(a.VERPR / a.PEINH) / count(*) stdprice_1
FROM tmp_MBEWH_alt_2 a
where a.VPRSV = 'V' AND a.VERPR > 0
GROUP BY a.MATNR,a.BWKEY,a.pFiYear,a.pPeriod;

UPDATE tmp_getStdPrice
SET StandardPrice = 0 * pUMREZ_UMREN
WHERE StandardPrice < 0
AND processed_flag = 'Y';


UPDATE 
tmp_getStdPrice x
SET StandardPrice =       a.stdprice_1 * pUMREZ_UMREN,
    flag_upd = 'Q9'
FROM 
tmp_getStdPrice x,
tmp_MBEWH_alt_3 a
WHERE     a.MATNR = pMaterialNo
AND a.pFiYear = x.pFiYear
AND a.pPeriod = x.pPeriod
AND a.BWKEY = pPlant
AND StandardPrice <= 0
AND x.flag_upd <> 'Q1'
AND processed_flag = 'Y';


/* Q11 MBEWH */
/* After the last query commenting here as can't put that onlast line */
/* Q11B  This calls another function which should populate this table tmp_Funct_Fiscal_Year */

/* Break up getstdprice_std into 2 parts so that funct_fiscal_year can be called */
/* Call vw_funct_fiscal_year.custom.getStdPrice.sql then vw_funct_fiscal_year.standard.sql then part 2 of this proc */

UPDATE tmp_getStdPrice
SET pprevFiYear = pFiYear - 1,pprevPeriod = 12,flag_upd = 'Q11A'
WHERE PONumber is null AND StandardPrice <= 0
AND pPeriod = 1
AND processed_flag = 'Y';

UPDATE tmp_getStdPrice
SET pprevFiYear = pFiYear,
        pprevPeriod = pPeriod - 1,
        flag_upd = 'Q12B'
WHERE PONumber is null AND StandardPrice <= 0
AND IFNULL(pPeriod,-1) <> 1 
AND processed_flag = 'Y';


DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_getstdprice;
DROP TABLE IF EXISTS tmp_MBEW_getstdprice;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_getstdprice;
DROP TABLE IF EXISTS tmp_MBEWH_getstdprice;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_1;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_2;
DROP TABLE IF EXISTS tmp_MBEW_NO_BWTAR_alt_3;
DROP TABLE IF EXISTS tmp_MBEW_alt;
DROP TABLE IF EXISTS tmp_MBEW_alt_1;
DROP TABLE IF EXISTS tmp_MBEW_alt_2;
DROP TABLE IF EXISTS tmp_MBEW_alt_3;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_1;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_2;
DROP TABLE IF EXISTS tmp_MBEWH_NO_BWTAR_alt_3;
DROP TABLE IF EXISTS tmp_MBEWH_alt;
DROP TABLE IF EXISTS tmp_MBEWH_alt_1;
DROP TABLE IF EXISTS tmp_MBEWH_alt_2;
DROP TABLE IF EXISTS tmp_MBEWH_alt_3;


/* Part 2 - Start fiscal yr custom */


/* Custom proc starts here */

DELETE FROM tmp_funct_fiscal_year
where fact_script_name = 'getstdprice';


INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT DISTINCT t.pCompanyCode,  t.pprevFiYear, t.pprevPeriod , 'getstdprice'
FROM tmp_getStdPrice t
WHERE PONumber is null AND StandardPrice <= 0
AND processed_flag = 'Y';


			
/* Part 3 - Start fiscal year standard */

/* Standard proc funct_fiscal_year */


UPDATE tmp_funct_fiscal_year
SET  processed_flag = 'Y'
where processed_flag IS NULL;

UPDATE tmp_funct_fiscal_year
SET upd_flag = 'N'
WHERE  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year t
set t.pPeriv = s.PERIV
FROM tmp_funct_fiscal_year t
	LEFT JOIN T001 s on s.BUKRS = t.pCompanyCode
WHERE  t.processed_flag = 'Y';

 UPDATE tmp_funct_fiscal_year
 SET pPrevPeriod = 12,pPrevFIYEAR = FiscalYear - 1,upd_flag = 'Q1A'
 WHERE Period = 1
AND  processed_flag = 'Y';


 UPDATE tmp_funct_fiscal_year
 SET pPrevPeriod = Period - 1,pPrevFIYEAR = FiscalYear,upd_flag = 'Q1B'
 WHERE upd_flag = 'N'
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year t
set t.pVariant1 = s.XJABH
FROM tmp_funct_fiscal_year t
	left join T009 s ON s.PERIV = t.pPeriv
WHERE t.processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year t
set t.pVariant2 = s.XKALE
FROM tmp_funct_fiscal_year t
	left join T009 s ON s.PERIV = t.pPeriv
WHERE  t.processed_flag = 'Y';


/**************************************************************************************MAIN IF SECTION ***************************/

 DROP TABLE IF EXISTS TMP_FFY_T009B_1;
 CREATE TABLE TMP_FFY_T009B_1
 AS
 SELECT PERIV,POPER,BDATJ,RELJR,MIN(BUMON) min_BUMON
 FROM T009B T,tmp_funct_fiscal_year R
WHERE PERIV = pPeriv and POPER = Period
and ((BDATJ = FiscalYear and RELJR = '0')
or (BDATJ = FiscalYear-1 and RELJR = '+1')
or (BDATJ = FiscalYear+1 and RELJR = '-1'))
AND  processed_flag = 'Y'
group BY PERIV,POPER,BDATJ,RELJR;

 DROP TABLE IF EXISTS TMP_FFY_T009B_2;
 CREATE TABLE TMP_FFY_T009B_2
 AS
 SELECT T.*
 FROM T009B T,TMP_FFY_T009B_1 x
 WHERE T.PERIV = x.PERIV
 AND T.POPER = x.POPER
 AND T.BDATJ = x.BDATJ
 AND T.RELJR = x.RELJR
 AND T.BUMON = x.min_BUMON;


UPDATE 
tmp_funct_fiscal_year
SET pYearShift = RELJR,upd_flag = 'Q2A'
FROM 
tmp_funct_fiscal_year,
TMP_FFY_T009B_2
WHERE PERIV = pPeriv and POPER = Period
and ((BDATJ = FiscalYear and RELJR = '0')
or (BDATJ = FiscalYear-1 and RELJR = '+1')
or (BDATJ = FiscalYear+1 and RELJR = '-1'))
AND pVariant1 = 'X'
AND  processed_flag = 'Y';


UPDATE 
tmp_funct_fiscal_year
SET pCalMth = min_BUMON,upd_flag = 'Q2B'
FROM 
tmp_funct_fiscal_year,
TMP_FFY_T009B_1
WHERE PERIV = pPeriv and POPER = Period
and ((BDATJ = FiscalYear and RELJR = '0')
or (BDATJ = FiscalYear-1 and RELJR = '+1')
or (BDATJ = FiscalYear+1 and RELJR = '-1'))
AND pVariant1 = 'X'
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear - 1, upd_flag = 'Q2C'
WHERE pVariant1 = 'X'
AND pYearShift = '+1'
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear + 1, upd_flag = 'Q2D'
WHERE pVariant1 = 'X'
AND pYearShift = '-1'
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pCalYear = FiscalYear , upd_flag = 'Q2E'
WHERE pVariant1 = 'X'
AND upd_flag NOT IN ( 'Q2C','Q2D' )
AND  processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
SET pcalyear_minus_1 = pCalYear - 1
WHERE  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
SET pcalyear_plus_1 = pCalYear + 1
WHERE  processed_flag = 'Y';

DROP TABLE IF EXISTS tmp_ffyr_minbutag;
CREATE TABLE tmp_ffyr_minbutag
AS
SELECT PERIV,POPER,BDATJ,BUMON,min(BUTAG) as min_BUTAG
FROM T009B
GROUP BY PERIV,POPER,BDATJ,BUMON;

UPDATE 
tmp_funct_fiscal_year
SET   pCalMthDay = min_BUTAG
FROM 
tmp_funct_fiscal_year,
tmp_ffyr_minbutag
WHERE PERIV = pPeriv 
and POPER = Period 
and BDATJ = pCalYear 
and BUMON = pCalMth
AND pVariant1 = 'X'
AND processed_flag = 'Y';


UPDATE tmp_funct_fiscal_year
/* SET CalDate = pCalMthDay || '/' || pCalMth || '/' || pCalYear */
SET CalDate = to_date(pCalMth || '-' || pCalMthDay || '-' || pCalYear, 'MM-DD-YYYY')
WHERE pVariant1 = 'X'
AND  processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_1;
CREATE TABLE TMP_T009B_1
as
/* SELECT PERIV,BDATJ,POPER,BUTAG||BUMON||BDATJ as tDT */
/* cast expects string in mm/dd/yyyy format */
SELECT PERIV,BDATJ,POPER,to_date(BUMON || '-' || BUTAG || '-' || BDATJ,'MM-DD-YYYY') as tDT
FROM T009B, tmp_funct_fiscal_year
WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear - 1 or BDATJ = pCalYear + 1)
AND  processed_flag = 'Y';


 /* if (pVariant1 = 'X') */

/*UPDATE tmp_funct_fiscal_year f
SET pFromDate = ( SELECT MAX(TMP.tDT) + INTERVAL '1' DAY
                                        FROM TMP_T009B_1 TMP
                                        WHERE TMP.PERIV = f.pPeriv and (TMP.BDATJ = f.pCalYear or TMP.BDATJ = f.pCalYear - 1)
                        and TMP.POPER = 12
                        and TMP.tDT < f.CalDate )
WHERE f.pVariant1 = 'X'
AND f.Period = 1
AND f.processed_flag = 'Y'*/

DROP TABLE IF EXISTS tmp_update_pFromDate;
CREATE TABLE tmp_update_pFromDate
AS
SELECT f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag, MAX(t.tDT) + INTERVAL '1' DAY pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv 
		AND (t.BDATJ = f.pCalYear OR t.BDATJ = f.pCalYear - 1)
		AND t.POPER = 12
		AND t.tDT < f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period = 1
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pFromDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pFromDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.Period = t.Period
	AND f.processed_flag = t.processed_flag;

/* UPDATE tmp_funct_fiscal_year
SET pFromDate = ( SELECT MAX(tDT) + INTERVAL '1' DAY
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv
                                            and (BDATJ = pCalYear or BDATJ = pCalYear - 1)
                        and POPER < Period
                        and tDT < CalDate )
WHERE pVariant1 = 'X'
AND Period > 1
AND  cast(processed_flag as varchar(1)) = 'Y' */	
	
DROP TABLE IF EXISTS tmp_update_pFromDate;
CREATE TABLE tmp_update_pFromDate
AS
SELECT f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag, MAX(t.tDT) + INTERVAL '1' DAY pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv
		and (t.BDATJ = f.pCalYear or t.BDATJ = f.pCalYear - 1)
		and t.POPER < f.Period
		and t.tDT < f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period > 1
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pFromDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pFromDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.Period = t.Period
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.processed_flag = t.processed_flag;

/*Update pToDate */

/*
UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MIN(tDT)
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and POPER = 1
                        and tDT > CalDate )
WHERE pVariant1 = 'X'
AND Period = 12
AND  cast(processed_flag as varchar(1)) = 'Y'
*/

DROP TABLE IF EXISTS tmp_update_pToDate;
CREATE TABLE tmp_update_pToDate
AS
SELECT f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag, MIN(t.tDT) pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv 
		AND (t.BDATJ = f.pCalYear OR t.BDATJ = f.pCalYear + 1)
		AND t.POPER = 1
		AND t.tDT > f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period = 12
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.CalDate, f.pVariant1, f.Period, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pToDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pToDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.Period = t.Period
	AND f.processed_flag = t.processed_flag;


/* 
UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MIN(tDT)
                                        FROM TMP_T009B_1
                                          WHERE PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
                        and POPER > Period
                        and tDT > CalDate )
WHERE pVariant1 = 'X'
AND Period < 12
AND  cast(processed_flag as varchar(1)) = 'Y'
*/

DROP TABLE IF EXISTS tmp_update_pToDate;
CREATE TABLE tmp_update_pToDate
AS
SELECT f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag, MIN(t.tDT) pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv
		and (t.BDATJ = f.pCalYear or t.BDATJ = f.pCalYear + 1)
		and t.POPER > f.Period
		and t.tDT > f.CalDate
WHERE f.pVariant1 = 'X'
	AND f.Period < 12
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.pCalYear, f.Period, f.CalDate, f.pVariant1, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pToDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pToDate t
WHERE f.pPeriv = t.pPeriv
	AND f.pCalYear = t.pCalYear
	AND f.Period = t.Period
	AND f.CalDate = t.CalDate
	AND f.pVariant1 = t.pVariant1
	AND f.processed_flag = t.processed_flag;


/*
UPDATE tmp_funct_fiscal_year
SET pToDate = ( SELECT MAX(tDT)
                                from TMP_T009B_1
                                WHERE PERIV = pPeriv and POPER = Period
                                and tDT < pToDate )

WHERE pVariant1 = 'X'
AND  cast(processed_flag as varchar(1)) = 'Y'
*/

DROP TABLE IF EXISTS tmp_update_pToDate;
CREATE TABLE tmp_update_pToDate
AS
SELECT f.pPeriv, f.Period, f.pToDate, f.pVariant1, f.processed_flag, MAX(t.tDT) pFromDate_update
FROM tmp_funct_fiscal_year f 
	LEFT JOIN TMP_T009B_1 t ON t.PERIV = f.pPeriv
		and t.POPER = f.Period
        and t.tDT < f.pToDate
WHERE f.pVariant1 = 'X'
	AND f.processed_flag = 'Y'
GROUP BY f.pPeriv, f.Period, f.pToDate, f.pVariant1, f.processed_flag;

UPDATE tmp_funct_fiscal_year f
SET f.pToDate = t.pFromDate_update
FROM tmp_funct_fiscal_year f, tmp_update_pToDate t
WHERE f.pPeriv = t.pPeriv
	AND f.Period = t.Period
	AND f.pToDate = t.pToDate
	AND f.pVariant1 = t.pVariant1
	AND f.processed_flag = t.processed_flag;

/*********************************************       ELSEIF SECTION      *********************************************************/
/********************************************(pVariant1 is null and pVariant2 is null)  ******************************************/

DROP TABLE IF EXISTS TMP_T009B_2a;
CREATE TABLE TMP_T009B_2a
as
SELECT PERIV,POPER,BDATJ,min(BUMON) min_BUMON
FROM T009B
GROUP BY PERIV,POPER,BDATJ;

DROP TABLE IF EXISTS TMP_T009B_2;
CREATE TABLE TMP_T009B_2
as
SELECT B.*
FROM T009B B,TMP_T009B_2a T
where B.PERIV = T.PERIV
AND B.POPER = T.POPER
AND B.BDATJ = T.BDATJ
AND B.BUMON = T.min_BUMON;

UPDATE tmp_funct_fiscal_year t0
set t0.pYearShift = t1.RELJR
FROM tmp_funct_fiscal_year t0
	LEFT JOIN TMP_T009B_2 t1 ON t1.PERIV = t0.pPeriv 
				and t1.POPER = t0.Period 
				and t1.BDATJ = 0
WHERE t0.pVariant1 is null 
and  t0.pVariant2 is null
AND  t0.processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year t0
set t0.pCalMth = t1.min_BUMON
FROM
tmp_funct_fiscal_year t0 
	left join TMP_T009B_2a t1 on t1.PERIV = t0.pPeriv 
				and  t1.POPER = t0.Period 
				and  t1.BDATJ = 0
WHERE  t0.pVariant1 is null 
and  t0.pVariant2 is null
AND  t0.processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        SET pCalYear = FiscalYear - 1
        WHERE pYearShift = '+1'
        AND  pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        SET pCalYear = FiscalYear + 1
        WHERE pYearShift = '-1'
        AND  pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

        UPDATE tmp_funct_fiscal_year
        SET pCalYear = FiscalYear
        WHERE pYearShift NOT IN ( '+1','-1')
        AND  pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';


                UPDATE tmp_funct_fiscal_year
                SET  pcalyear_minus_1 = pCalYear - 1
                WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

                UPDATE tmp_funct_fiscal_year
                SET  pcalyear_plus_1 = pCalYear + 1
                WHERE pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_temp6;
CREATE TABLE TMP_T009B_temp6
as
SELECT MIN(BUTAG) min_val,
PERIV,
POPER,
BUMON
FROM T009B
WHERE BDATJ = 0
GROUP BY PERIV,POPER,BUMON;

UPDATE 
tmp_funct_fiscal_year t0
set t0.pCalMthDay = t1.min_val
FROM
tmp_funct_fiscal_year t0 left join 
TMP_T009B_temp6 t1 ON t1.PERIV = t0.pPeriv and t1.POPER = t0.Period and t1.BUMON = t0.pCalMth
WHERE t0.pVariant1 is null and t0.pVariant2 is null
AND  t0.processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_temp6;

UPDATE tmp_funct_fiscal_year
set CalDate =  to_date(case when MOD(pCalYear,4) <> 0 AND pCalMthDay || '-' || pCalMth = '29-2'
                                        then '2-28-' || pCalYear
                                        else pCalMth || '-' || pCalMthDay || '-' || pCalYear
                                        END, 'MM-DD-YYYY')
WHERE 
case when MOD(pCalYear,4) <> 0 AND pCalMthDay || '-' || pCalMth = '29-2'
                                        then '2-28/' || pCalYear
                                        else pCalMth || '-' || pCalMthDay || '-' || pCalYear
                                        END <> '--'
and pVariant1 is null and pVariant2 is null
AND  processed_flag = 'Y';

DROP TABLE IF EXISTS TMP_T009B_3A;
CREATE TABLE TMP_T009B_3A
AS
SELECT 
t.pPeriv,
t.Period,
t.CalDate,
t.pCalYear,
to_date(case when (MOD(pCalYear,4) <> 0) AND (BUTAG||'-'||BUMON = '29-2')
THEN to_date(('2-28-' || pCalYear),'MM-DD-YYYY')  + INTERVAL '1' DAY
ELSE to_date((BUMON||'-'||BUTAG||'-'||pCalYear),'MM-DD-YYYY') + INTERVAL '1' DAY
END, 'MM-DD-YYYY')  tDT
FROM T009B,tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
AND CalDate > to_date(case when MOD(pCalYear,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
                                        THEN '2-28-' || pCalYear
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear */
                                        ELSE BUMON||'-'||BUTAG||'-'||pCalYear
                                END, 'MM-DD-YYYY')
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_3B;
CREATE TABLE TMP_T009B_3B
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,max(     tDT ) max_tDT
FROM TMP_T009B_3A t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
SET x.pFromDate = y.max_tDT,upd_flag = upd_flag || 'E1'
FROM 
tmp_funct_fiscal_year x,
TMP_T009B_3B y
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null and x.pVariant2 is null
AND  processed_flag = 'Y';



DROP TABLE IF EXISTS TMP_T009B_4A;
CREATE TABLE TMP_T009B_4A
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
to_date(case when MOD(pCalYear_minus_1,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
THEN to_date(('2-28-' || pCalYear_minus_1 ),'MM-DD-YYYY') + INTERVAL '1' DAY
ELSE to_date((BUMON||'/'||BUTAG||'/'||pCalYear_minus_1),'MM-DD-YYYY') + INTERVAL '1' DAY
END,'MM-DD-YYYY')  tDT
FROM T009B,  tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
AND CalDate >  to_date(case when MOD(pCalYear_minus_1,4) <> 0 AND BUTAG||'/'||BUMON = '29-2'
                                        THEN '2-28-' || pCalYear_minus_1
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear-1 */
                                        ELSE BUMON||'-'||BUTAG||'-'||pCalYear_minus_1
                                END, 'MM-DD-YYYY')
AND t.pVariant1 is null and t.pVariant2 is null
AND pFromDate is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_4B;
CREATE TABLE TMP_T009B_4B
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,max(     tDT ) max_tDT
FROM TMP_T009B_4A t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
SET x.pFromDate = y.max_tDT,upd_flag = upd_flag || 'E2'
FROM 
tmp_funct_fiscal_year x,
TMP_T009B_4B y
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null 
and x.pVariant2 is null
AND x.pFromDate is null
AND  processed_flag = 'Y';


/* To date queries now. */

/* To Date Q1 */

DROP TABLE IF EXISTS TMP_T009B_3A_TO;
CREATE TABLE TMP_T009B_3A_TO
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
to_date(case when MOD(pCalYear,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
THEN ('2-28/' || pCalYear)
ELSE (BUMON||'-'||BUTAG||'-'||pCalYear)
END,'MM-DD-YYYY')  tDT
FROM T009B,tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
AND CalDate < to_date(case when MOD(pCalYear,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
                                        THEN '2-28/' || pCalYear
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear */
                                        ELSE BUMON||'-'||BUTAG||'-'||pCalYear
                                END, 'MM-DD-YYYY')
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_3B_TO;
CREATE TABLE TMP_T009B_3B_TO
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,min(     tDT ) min_tDT
FROM TMP_T009B_3A_TO t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
SET x.pToDate1 = y.min_tDT,upd_flag = upd_flag || 'F1'
FROM 
tmp_funct_fiscal_year x,
TMP_T009B_3B_TO y
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null 
and x.pVariant2 is null
AND  processed_flag = 'Y';


/* Q2 */

DROP TABLE IF EXISTS TMP_T009B_4A_TO;
CREATE TABLE TMP_T009B_4A_TO
AS
SELECT t.pPeriv,t.Period,t.CalDate,t.pCalYear,
to_date(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
THEN ('2-28-' || pCalYear_plus_1 )
ELSE (BUMON||'-'||BUTAG||'-'||pCalYear_plus_1)
END,'MM-DD-YYYY')  tDT
FROM T009B, tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0
and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
AND CalDate <  to_date(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
                                        THEN '2-28-' || pCalYear_plus_1
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear+1 */
                                        ELSE BUMON||'-'||BUTAG||'-'||pCalYear_plus_1
                                END, 'MM-DD-YYYY')
AND t.pVariant1 is null 
and t.pVariant2 is null
AND pToDate1 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_T009B_4B_TO;
CREATE TABLE TMP_T009B_4B_TO
AS
SELECT  t.pPeriv,t.Period,t.CalDate,t.pCalYear,min(     tDT ) min_tDT
FROM TMP_T009B_4A_TO t
GROUP by t.pPeriv,t.Period,t.CalDate,t.pCalYear;


UPDATE  
tmp_funct_fiscal_year x
SET x.pToDate1 = y.min_tDT,upd_flag = upd_flag || 'F2'
FROM 
tmp_funct_fiscal_year x,
TMP_T009B_4B_TO y
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.CalDate = y.CalDate
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null 
and x.pVariant2 is null
AND x.pToDate1 is null
AND  processed_flag = 'Y';


/* To Date Q3 */

DROP TABLE IF EXISTS TMP_TO_3;
CREATE TABLE TMP_TO_3
AS
SELECT t.pPeriv,t.Period,t.pToDate1,t.pCalYear,
to_date(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
THEN ('2-28-' || pCalYear_plus_1 )
ELSE (BUMON||'-'||BUTAG||'-'||pCalYear_plus_1)
END,'MM-DD-YYYY')  tDT
FROM T009B, tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0 and POPER = Period
and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
AND pToDate1 >  to_date(case when MOD(pCalYear_plus_1,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
                                        THEN '2-28-' || pCalYear_plus_1
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear+1 */
                                        ELSE BUMON||'-'||BUTAG||'-'||pCalYear_plus_1
                                END,'MM-DD-YYYY')
AND t.pVariant1 is null 
and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_TO_3B;
CREATE TABLE TMP_TO_3B
AS
SELECT  t.pPeriv,t.Period,t.pToDate1,t.pCalYear,max(tDT ) max_tDT
FROM TMP_TO_3 t
GROUP by t.pPeriv,t.Period,t.pToDate1,t.pCalYear;


UPDATE  tmp_funct_fiscal_year x
SET x.pToDate = y.max_tDT,upd_flag = upd_flag || 'G1'
FROM 
tmp_funct_fiscal_year x,
TMP_TO_3B y
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.pToDate1 = y.pToDate1
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null 
and x.pVariant2 is null
AND  processed_flag = 'Y';


/* To Date Q4 */

DROP TABLE IF EXISTS TMP_TO_4;
CREATE TABLE TMP_TO_4
AS
SELECT t.pPeriv,t.Period,t.pToDate1,t.pCalYear,
to_date(case when MOD(pCalYear,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
THEN ('2-28-' || pCalYear )
ELSE (BUMON||'-'||BUTAG||'-'||pCalYear)
END,'MM-DD-YYYY')  tDT
FROM T009B, tmp_funct_fiscal_year t
WHERE PERIV = pPeriv  and BDATJ = 0 and POPER = Period
AND pToDate1 >  to_date(case when MOD(pCalYear,4) <> 0 AND BUTAG||'-'||BUMON = '29-2'
                                        THEN '2-28-' || pCalYear
                                        /* ELSE BUTAG||'/'||BUMON||'/'||pCalYear */
                                        ELSE BUMON||'-'||BUTAG||'-'||pCalYear
                                END,'MM-DD-YYYY')
AND t.pVariant1 is null and t.pVariant2 is null
AND  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_TO_4B;
CREATE TABLE TMP_TO_4B
AS
SELECT  t.pPeriv,t.Period,t.pToDate1,t.pCalYear,max(tDT ) max_tDT
FROM TMP_TO_4 t
GROUP by t.pPeriv,t.Period,t.pToDate1,t.pCalYear;


UPDATE  
tmp_funct_fiscal_year x
SET x.pToDate = y.max_tDT,upd_flag = upd_flag || 'G2'
FROM 
tmp_funct_fiscal_year x,
TMP_TO_4B y
WHERE x.pPeriv = y.pPeriv
AND x.Period = y.Period
AND x.pToDate1 = y.pToDate1
AND x.pCalYear = y.pCalYear
AND x.pVariant1 is null 
and x.pVariant2 is null
AND x.pToDate is null
AND  processed_flag = 'Y';


/************ELSE SECTION ***************************/

UPDATE tmp_funct_fiscal_year
/*set pFromDate = '01/' || Period ||  '/' || FiscalYear*/
set pFromDate = FiscalYear || '-' || Period || '-01'
WHERE   ifnull(pVariant1,'Y') <> 'X' AND ( pVariant1 IS NOT NULL or pVariant2 IS NOT NULL )
AND Period >=1 AND Period <= 12
AND FiscalYear >= 1900
AND  processed_flag = 'Y';

UPDATE tmp_funct_fiscal_year
set pToDate = to_date(pFromDate,'MM-DD-YYYY')  + INTERVAL '1' MONTH - INTERVAL '1' DAY
WHERE ifnull(pVariant1,'Y') <> 'X' AND ( pVariant1 IS NOT NULL or pVariant2 IS NOT NULL )
AND  processed_flag = 'Y';

/************End of if-elseif-else ***************************/

UPDATE tmp_funct_fiscal_year
SET pReturn = cast(pFromDate as char(20)) || '|' || cast(pToDate as char(20))
WHERE  processed_flag = 'Y';


DROP TABLE IF EXISTS TMP_FFY_T009B_1;
DROP TABLE IF EXISTS TMP_FFY_T009B_2;
DROP TABLE IF EXISTS TMP_T009B_1;
DROP TABLE IF EXISTS TMP_T009B_2a;
DROP TABLE IF EXISTS TMP_T009B_2;
DROP TABLE IF EXISTS TMP_T009B_3A;
DROP TABLE IF EXISTS TMP_T009B_3B;
DROP TABLE IF EXISTS TMP_T009B_4A;
DROP TABLE IF EXISTS TMP_T009B_4B;
DROP TABLE IF EXISTS TMP_T009B_3A_TO;
DROP TABLE IF EXISTS TMP_T009B_3B_TO;
DROP TABLE IF EXISTS TMP_T009B_4A_TO;
DROP TABLE IF EXISTS TMP_T009B_4B_TO;
DROP TABLE IF EXISTS TMP_TO_3;
DROP TABLE IF EXISTS TMP_TO_3B;
DROP TABLE IF EXISTS TMP_TO_4;
DROP TABLE IF EXISTS TMP_TO_4B;
DROP TABLE IF EXISTS tmp_ffyr_minbutag;

UPDATE tmp_funct_fiscal_year
SET processed_flag = 'D'
WHERE processed_flag = 'Y'
AND  processed_flag = 'Y';



/* Part 4 - getstdprice part 2 */

/* Call this part ( part 2 ) after vw_funct_fiscal_year.custom.getStdPrice.sql and vw_funct_fiscal_year.standard.sql */

UPDATE tmp_getStdPrice t
SET pprevDates = z.pReturn 
FROM 
tmp_getStdPrice t,
tmp_funct_fiscal_year z
WHERE z.pCompanyCode = t.pCompanyCode
and z.FiscalYear = t.pprevFiYear
and z.Period = t.pprevPeriod 
and z.fact_script_name = 'getstdprice'
AND PONumber is null 
AND StandardPrice <= 0
AND t.processed_flag = 'Y';


UPDATE 
tmp_getStdPrice
SET pprevFromDate = to_date(substr(pprevDates, 1, 10))
WHERE 
PONumber is null 
AND StandardPrice <= 0
AND pprevDates IS NOT NULL 
AND PPREVDATES <> '|';


UPDATE tmp_getStdPrice
SET pprevToDate = to_date(substr(pprevDates, 22, 32))
WHERE PONumber is null AND StandardPrice <= 0
AND processed_flag = 'Y'
and pprevDates is not null 
and PPREVDATES <> '|';


DROP TABLE IF EXISTS tmp_getStdPrice_FP1;
CREATE TABLE tmp_getStdPrice_FP1
AS
SELECT pprevFromDate,pprevToDate,pMaterialNo,pPlant,dd_DocumentNo,DateValue,
cast(b.amt_UnitPrice as decimal (18,5)) amt_UnitPrice ,
row_number() over (ORDER BY dd_DocumentNo desc,DateValue desc) row_num
FROM 
tmp_getStdPrice,
fact_purchase b 
inner join dim_part p on b.Dim_Partid = p.Dim_Partid
inner join dim_date dt on dt.Dim_Dateid = b.Dim_DateidCreate
inner join Dim_PurchaseMisc pmisc on b.Dim_PurchaseMiscid = pmisc.Dim_PurchaseMiscid
WHERE dt.DateValue BETWEEN ( pprevFromDate - INTERVAL '2' MONTH )
AND pprevToDate
AND p.PartNumber = pMaterialNo
AND b.amt_UnitPrice > 0
AND p.Plant = pPlant
AND pmisc.ItemReturn = 'Not Set'
AND PONumber is null AND StandardPrice <= 0
AND processed_flag = 'Y'
ORDER BY dd_DocumentNo desc,DateValue desc;

DELETE FROM tmp_getStdPrice_FP1  x  
WHERE EXISTS ( SELECT 1 FROM tmp_getStdPrice_FP1 y
WHERE x.pprevFromDate = y.pprevFromDate
AND x.pprevToDate = y.pprevToDate
AND x.pMaterialNo = y.pMaterialNo
AND x.pPlant = y.pPlant
AND x.row_num > y.row_num );


UPDATE tmp_getStdPrice x
SET StandardPrice = y.amt_UnitPrice
FROM 
tmp_getStdPrice x,
tmp_getStdPrice_FP1 y
WHERE x.pprevFromDate = y.pprevFromDate
AND x.pprevToDate = y.pprevToDate
AND x.pMaterialNo = y.pMaterialNo
AND x.pPlant = y.pPlant
AND x.PONumber is null AND x.StandardPrice <= 0
AND processed_flag = 'Y';



